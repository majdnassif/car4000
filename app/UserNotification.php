<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserNotification extends Model
{
  protected $table = 'user_notifications';

  protected $fillable = ['user_id', 'ad_id', 'title', 'body'];

  public function user()
  {
    return $this->belongsTo(User::class, 'user_id', 'id');
  }

  public function ad()
  {
    return $this->belongsTo(Ads::class, 'ad_id', 'id');
  }
}
