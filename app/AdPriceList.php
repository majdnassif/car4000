<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdPriceList extends Model
{
    protected $table = 'ad_price_list';

    protected $fillable = ['app_id', 'name', 'value', 'currency'];

    public function application()
    {
        return $this->belongsTo(Applications::class, 'app_id', 'id')->first();
    }
}
