<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserRoles extends Model
{
    use HasFactory;

    protected $table = 'user_roles';

    protected $fillable = ['app_id', 'user_id', 'type_id', 'category'];


  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
    'created_at', 'updated_at'
  ];

  public function type()
  {
    return $this->belongsTo(UserTypes::class, 'type_id', 'id');
  }

  public function app()
  {
    return $this->belongsTo(Applications::class, 'app_id', 'id');
  }
}
