<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OptionPriceOptions extends Model
{
    use HasFactory;

    protected $table = 'property_option_price_option';

    protected $fillable = ['price_option_id', 'option_id'];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
      'price_option_id', 'option_id'
    ];
    public function options()
    {
      return $this->belongsTo(Option::class, 'option_id', 'id');
    }
    public function price_options()
    {
      return $this->belongsTo(PriceOption::class, 'price_option_id', 'id');
    }
}
