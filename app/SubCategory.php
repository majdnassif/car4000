<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
  use HasFactory;

  protected $table = 'sub_category';

  protected $fillable = ['category_id', 'name', 'mobile_image', 'web_image', 'status'];

  public function category()
  {
    return $this->belongsTo(Category::class, 'category_id', 'id');
  }
  
  public function subsubs()
  {
    return $this->hasMany(SubSubCategory::class, 'sub_category_id', 'id');
  }
}
