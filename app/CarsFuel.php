<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CarsFuel extends Model
{
    use HasFactory;

    protected $table = 'car_fuel';
  
    protected $fillable = ['title', 'mobile_image','web_image'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
      'created_at', 'updated_at'
    ];
}
