<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdPayment extends Model
{
    protected $table = 'ad_payment';

    protected $fillable = ['ad_id', 'paypal_payment_id', 'total_amount', 'currency', 'days', 'executed'];
}
