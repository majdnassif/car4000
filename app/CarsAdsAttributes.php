<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CarsAdsAttributes extends Model
{
    use HasFactory;

    protected $table = 'car_ad_attributes';
  
    protected $fillable = ['ad_id', 'price', 'option_id', 'location', 'condition_id', 'body_style_id',
                            'model_id', 'year', 'milage', 'fuel_id',
                            'transmission_id', 'n_cylinders', 'color_id', 'n_doors', 'seats',
                            'service_history', 'finance', 'warranty', 'insurance', 'previous_keeper', 'regional_spec', 'seller',
                            'f_safety', 'f_comfort', 'f_interior', 'f_exterior', 'created_at', 'updated_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at','updated_at'
     ];
     
  public function option() {
    return $this->hasOne(CarsOptions::class, 'id', 'option_id');
  }
  public function model() {
    return $this->hasOne(CarsModels::class, 'id', 'model_id');
  }
  public function transmission() {
    return $this->hasOne(CarsTransmission::class, 'id', 'transmission_id');
  }
  public function fuel() {
    return $this->hasOne(CarsFuel::class, 'id', 'fuel_id');
  }

  public function condition() {
    return $this->hasOne(CarsCondition::class, 'id', 'condition_id');
  }

  public function color() {
    return $this->hasOne(CarsColor::class, 'id', 'color_id');
  }
  
  public function bodyStyle() {
    return $this->hasOne(BalconyNumber::class, 'id', 'body_style_id');
  }
}
