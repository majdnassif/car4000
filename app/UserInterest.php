<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserInterest extends Model
{
  protected $table = 'user_interest';

  protected $fillable = ['user_id', 'town_id', 'option_id', 'category_id', 'rooms', 'size_from', 'size_to',
                         'price_from', 'price_to', 'price_option_id', 'balcony', 'bath', 'garage', 
                         'terrace', 'furniture_id', 'size_unit_id', 'period', 'monthly_price', 'gym', 
                         'security'
  ];  
  
  public function option() {
    return $this->hasOne(Option::class, 'id', 'option_id');
  }
  public function category() {
    return $this->hasOne(Category::class, 'id', 'category_id');
  }

  public function size_unit() {
    return $this->hasOne(SizeUnites::class, 'id', 'size_unit_id');
  }
  public function price_option() {
    return $this->hasOne(PriceOption::class, 'id', 'price_option_id');
  }

  public function furniture() {
    return $this->hasOne(FurnitureTypes::class, 'id', 'furniture_id');
  }

  public function rooms() {
    return $this->hasOne(RoomsNumber::class, 'id', 'rooms');
  }
  
  public function balcony() {
    return $this->hasOne(BalconyNumber::class, 'id', 'balcony');
  }
  
  public function bath() {
    return $this->hasOne(BathsNumber::class, 'id', 'bath');
  }

  public function garage() {
    return $this->hasOne(GaragesNumber::class, 'id', 'garage');
  }
}
