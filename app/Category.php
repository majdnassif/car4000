<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
  use HasFactory;

  protected $table = 'category';

  protected $fillable = ['parent_id', 'name', 'mobile_image', 'web_image', 'status', 'app_id', 'ordering'];

  public function application()
  {
    return $this->belongsTo(Applications::class, 'app_id', 'id');
  }

  public function app()
  {
    return ($this->belongsTo(Applications::class, 'app_id', 'id')->first()) ? $this->belongsTo(Applications::class, 'app_id', 'id')->first()->name : 'No App';
  }

  public function parent()
  {
    return $this->belongsTo(Category::class, 'parent_id', 'id')->first();
  }

  public function full_name()
  {
    return ($this->parent()) ? $this->parent()->full_name() . ' => ' . $this->name : $this->name;
  }

  public function app_name()
  {
    return ($this->parent()) ? $this->parent()->app_name() : $this->app();
  }

  public function subs()
  {
    return $this->hasMany(Category::class, 'parent_id', 'id');
  }
}
