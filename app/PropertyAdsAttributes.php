<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PropertyAdsAttributes extends Model
{
    use HasFactory;

    protected $table = 'property_ad_attributes';
  
    protected $fillable = ['ad_id', 'category_id', 'option_id', 'price', 'price_option_id', 'size',
                            'size_unit_id', 'room_number_id', 'garage_number_id', 'furniture_type_id',
                            'bath_number_id', 'balcony_number_id', 'period', 'monthly_price', 'terrace',
                            'gym', 'security'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at','updated_at'
     ];
     
  public function option() {
    return $this->hasOne(Option::class, 'id', 'option_id');
  }
  public function category() {
    return $this->hasOne(Category::class, 'id', 'category_id');
  }

  public function size_unit() {
    return $this->hasOne(SizeUnites::class, 'id', 'size_unit_id');
  }
  public function price_option() {
    return $this->hasOne(PriceOption::class, 'id', 'price_option_id');
  }

  public function furniture() {
    return $this->hasOne(FurnitureTypes::class, 'id', 'furniture_type_id');
  }

  public function rooms() {
    return $this->hasOne(RoomsNumber::class, 'id', 'room_number_id');
  }
  
  public function balcony() {
    return $this->hasOne(BalconyNumber::class, 'id', 'balcony_number_id');
  }
  
  public function bath() {
    return $this->hasOne(BathsNumber::class, 'id', 'bath_number_id');
  }

  public function garage() {
    return $this->hasOne(GaragesNumber::class, 'id', 'garage_number_id');
  }
}
