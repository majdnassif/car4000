<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
  use HasApiTokens, Notifiable;

  protected $table = 'users';
  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'app_id','type_id','name', 'email', 'password', 'username', 'phone_number',
    'public_phone', 'logo', 'bio', 'profile_pic', 'town_id',
    'api_token', 'firebase_token', 'is_verify', 'profile_video',
    'website', 'status'
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
    'password', 'remember_token', 'api_token'
  ];

  /**
   * The attributes that should be cast to native types.
   *
   * @var array
   */
  protected $casts = [
    'email_verified_at' => 'datetime',
  ];

  public function town()
  {
    return $this->belongsTo(Towns::class, 'town_id', 'id');
  }

  public function type()
  {
    return $this->belongsTo(UserTypes::class, 'type_id', 'id');
  }
  
  public function application()
  {
    return $this->belongsTo(Applications::class, 'app_id', 'id');
  }
}
