<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ads extends Model
{
    use HasFactory;

    protected $table = 'ads';
  
    protected $fillable = ['title', 'description','owner_id','town_id','app_id','thumbnail','longitude','latitiude', 'total_views','status','validty_date','is_free','detailed_location','paused_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
       'updated_at'
    ];
    public function town()
    {
      return $this->belongsTo(Towns::class, 'town_id', 'id');
    }
    public function owner() {
      return $this->hasOne(User::class, 'id', 'owner_id');
    }
    public function media() {
      return $this->hasMany(Media::class, 'ad_id', 'id');
    }
    public function property_attributes() {
      return $this->hasOne(PropertyAdsAttributes::class, 'ad_id', 'id');
    }
    public function application() {
      return $this->belongsTo(Applications::class, 'app_id', 'id');
    }
    public function savedAds() {
      return $this->hasOne(SavedAds::class, 'ad_id', 'id');
    }
    public function cars_attributes() {
      return $this->hasOne(CarsAdsAttributes::class, 'ad_id', 'id');
    }
}
