<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PriceOption extends Model
{
    use HasFactory;
    
    protected $table = 'property_price_option';
  
    protected $fillable = ['title', 'image'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
      'created_at', 'updated_at'
    ];
       
    public function option() 
    {
      return $this->hasOne(OptionPriceOptions::class, 'price_option_id', 'id');
    }
}
