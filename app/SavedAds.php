<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SavedAds extends Model
{
    use HasFactory;

    protected $table = 'user_saved_ads';
  
    protected $fillable = ['user_id', 'ad_id', 'created_at', 'updated_at'];
  
    public function ads()
    {
      return $this->belongsTo(Ads::class, 'ad_id', 'id');
    }
}
