<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Towns extends Model
{
    use HasFactory;

    protected $table = 'uk_towns';

    protected $fillable = ['name', 'county', 'grid_reference', 'easting', 'northing', 'latitude', 'longitude', 'elevation', 'postcode_sector', 'local_government_area', 'nuts_region', 'type', 'country_id'];

    public function country() {
      return $this->belongsTo(Country::class, 'country_id', 'id');
    }
}
