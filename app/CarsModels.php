<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CarsModels extends Model
{
    use HasFactory;

    protected $table = 'car_models';
  
    protected $fillable = ['parent_id', 'title', 'mobile_image','web_image','model_order'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
      'created_at', 'updated_at'
    ];
}
