<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Login_Check
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(isset($_COOKIE['islogin']))
        {
            if($_COOKIE['isLogin']==1)
                return $next($request);
            else
                return redirect('/');
        }
        else
            return redirect('/');
    }
}
