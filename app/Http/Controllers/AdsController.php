<?php

namespace App\Http\Controllers;

use App\Ads;
use App\Applications;
use App\Towns;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class AdsController extends Controller
{
  private $index_route = 'ads.index';
  private $create_route = 'ads.create';
  private $index_view = 'dash.ads.index';
  private $create_view = 'dash.ads.create';
  private $edit_view = 'dash.ads.edit';
  private $show_view = 'dash.ads.show';
  private $model = Ads::class;

  public function __construct()
  {
    $this->middleware('auth');
  }

  public function index()
  {
    $data = Applications::all(['id', 'name']);
    $owner = User::all(['id', 'name']);
    $town = Towns::all(['id', 'name']);
    return view($this->index_view, compact('data', 'owner', 'town'));
  }


  public function list(Request $request)
  {
    $categories = $this->model::with(['application', 'town', 'owner'])
      ->skip($request->start)
      ->take($request->length)
      ->orderBy('id', 'DESC')
      ->when(isset($request->search_filter) && $request->search_filter !== '', function ($query) use ($request) {
        $query->where('name', 'like', '%' . $request->search_filter . '%');
      })
      ->when(isset($request->app_select) && $request->app_select != 0, function ($query) use ($request) {
        $query->where('app_id', $request->app_select);
      })
      ->when(isset($request->owner_select) && $request->owner_select != 0, function ($query) use ($request) {
        $query->where('owner_id', $request->owner_select);
      })
      ->when(isset($request->town_select) && $request->town_select != 0, function ($query) use ($request) {
        $query->where('town_id', $request->town_select);
      })
      ->when(isset($request->status_select) && $request->status_select !== '', function ($query) use ($request) {
        $query->where('status', $request->status_select);
      })
      ->get();
    $all = $this->model::when(isset($request->search_filter) && $request->search_filter !== '', function ($query) use ($request) {
      $query->where('name', 'like', '%' . $request->search_filter . '%');
    })
      ->when(isset($request->app_select) && $request->app_select != 0, function ($query) use ($request) {
        $query->where('app_id', $request->app_select);
      })
      ->when(isset($request->owner_select) && $request->owner_select != 0, function ($query) use ($request) {
        $query->where('owner_id', $request->owner_select);
      })
      ->when(isset($request->town_select) && $request->town_select != 0, function ($query) use ($request) {
        $query->where('town_id', $request->town_select);
      })
      ->when(isset($request->status_select) && $request->status_select !== '', function ($query) use ($request) {
        $query->where('status', $request->status_select);
      })
      ->get('id');
    $data['draw'] = $request->draw;
    $data['recordsTotal'] = count($all);
    $data['recordsFiltered'] = count($all);
    $data['data'] = array();
    $i = 1;
    foreach ($categories as $category) {
      $a = $category->getOriginal();
      $a['order'] = $i;
      array_push($data['data'], array_values($a));
      array_push($data['data'][$i - 1], $category->application->name);
      array_push($data['data'][$i - 1], $category->owner->name);
      array_push($data['data'][$i - 1], $category->town->name);
      $i++;
    }
    return json_encode($data);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    //
  }

  /**
   * Display the specified resource.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function show(Ads $ad)
  {
    $data = $ad->with([
      'application', 'town', 'owner',
      'property_attributes.option', 'property_attributes.category', 'property_attributes.size_unit', 'property_attributes.price_option', 'property_attributes.furniture',
      'property_attributes.rooms', 'property_attributes.balcony', 'property_attributes.bath', 'property_attributes.garage',
      'media'

    ])->first();
    return view($this->show_view, compact('data'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    //
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    //
  }


  public function changeStatus($id)
  {
    try {
      $status = [
        '1' => '2',
        '2' => '1'
      ];
      $data = $this->model::find($id);
      $data->update([
        'status' => $status[$data->status]
      ]);
      return redirect()->back();
    } catch (\Exception $ex) {
      Log::error($ex->getMessage());
      return redirect()->back();
    }
  }
}
