<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class AdminsController extends Controller
{
  private $index_route = 'admins.index';
  private $create_route = 'admins.create';
  private $index_view = 'dash.Admins.index';
  private $create_view = 'dash.Admins.create';
  private $edit_view = 'dash.Admins.edit';
  private $model = User::class;


  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $data = $this->model::where('type_id', 1)->where('status', '<>', '4')->orderBy('id', 'DESC')->get();
    return view($this->index_view, compact('data'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view($this->create_view);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $validator = $request->validate([
      'name' => 'required|string|unique:' . $this->model,
      'password' => 'required',
    ]);
    try {
      $validator['password'] = Hash::make($request->password);
      $validator['status'] = 1;
      $validator['type_id'] = 1;
      $this->model::create($validator);
      return redirect()->route($this->index_route);
    } catch (\Exception $ex) {
      Log::error($ex->getMessage());
      return redirect()->route($this->create_route);
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    $admin = User::findOrFail($id);
    return view($this->edit_view, compact('admin'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $validator = $request->validate([
      'name' => 'required|string|unique:' . $this->model . ',name,' . $id . ',id',
    ]);
    try {
      if(isset($request->password) && $request->password !== '') {
        $request->validate([
          'password' => 'required',
        ]);
        $validator['password'] = Hash::make($request->password);
      }
      $data = $this->model::findOrFail($id);
      $data->update($validator);
      return redirect()->route($this->index_route);
    } catch (\Exception $ex) {
      Log::error($ex->getMessage());
      return redirect()->back();
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    try {
      $data = $this->model::find($id);
      $data->update([
        'status' => 4
      ]);
      return redirect()->route($this->index_route);
    } catch (\Exception $ex) {
      Log::error($ex->getMessage());
      return redirect()->route($this->index_route);
    }
  }

  public function changeStatus($id)
  {
    try {
      $status = [
        '1' => '3',
        '3' => '1'
      ];
      $data = $this->model::find($id);
      $data->update([
        'status' => $status[$data->status]
      ]);
      return redirect()->route($this->index_route);
    } catch (\Exception $ex) {
      Log::error($ex->getMessage());
      return redirect()->route($this->index_route);
    }
  }
}
