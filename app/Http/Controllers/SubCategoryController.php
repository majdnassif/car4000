<?php

namespace App\Http\Controllers;

use App\Category;
use App\SubCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;

class SubCategoryController extends Controller
{
  private $index_route = 'subCategories.index';
  private $create_route = 'subCategories.create';
  private $index_view = 'dash.subCategories.index';
  private $create_view = 'dash.subCategories.create';
  private $edit_view = 'dash.subCategories.edit';
  private $model = SubCategory::class;

  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $data = $this->model::with('category')->where('status', '<>', '3')->orderBy('id', 'DESC')->get();
    $CategoryData = Category::all(['id', 'name']);
    // dd($data);
    return view($this->index_view, compact(['data','CategoryData']));
  }


  public function list(Request $request)
  {
    $categories = $this->model::with('category')
      ->where('status', '<>', '3')
      ->skip($request->start)
      ->take($request->length)
      ->orderBy('id', 'DESC')
      ->when(isset($request->search_filter) && $request->search_filter !== '', function ($query) use ($request) {
        $query->where('name', 'like', '%' . $request->search_filter . '%');
      })
      ->when(isset($request->category_select) && $request->category_select != 0, function ($query) use ($request) {
        $query->where('category_id', $request->category_select);
      })
      ->when(isset($request->status_select) && $request->status_select !== '', function ($query) use ($request) {
        $query->where('status', $request->status_select);
      })
      ->get();
    $all = $this->model::where('status', '<>', '3')
      ->when(isset($request->search_filter) && $request->search_filter !== '', function ($query) use ($request) {
        $query->where('name', 'like', '%' . $request->search_filter . '%');
      })
      ->when(isset($request->category_select) && $request->category_select != 0, function ($query) use ($request) {
        $query->where('category_id', $request->category_select);
      })
      ->when(isset($request->status_select) && $request->status_select !== '', function ($query) use ($request) {
        $query->where('status', $request->status_select);
      })
      ->get('id');
    $data['draw'] = $request->draw;
    $data['recordsTotal'] = count($all);
    $data['recordsFiltered'] = count($all);
    $data['data'] = array();
    $i = 1;
    foreach ($categories as $category) {
      $a = $category->getOriginal();
      $a['order'] = $i;
      array_push($data['data'], array_values($a));
      array_push($data['data'][$i - 1], $category->category->name);
      $i++;
    }
    return json_encode($data);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $categories = Category::all();
    return view($this->create_view, compact('categories'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $validator = $request->validate([
      'name' => ['required', 'string', Rule::unique('sub_category', 'name')->where(function($query) use ($request) {
        return $query->where('category_id', '=', $request->category_id)->where('status', '<>', 3);
      })],
      'category_id' => 'required|string|not_in:0|exists:category,id',
      'mobile_image' => 'nullable|image|mimes:jpeg,bmp,png',
      'web_image' => 'nullable|image|mimes:jpeg,bmp,png',
    ]);
    try {
      $i = 1;
      if ($request->file('mobile_image') !== null) {
        $image = $request->file('mobile_image');
        $name = time() . '-' . $i . '.' . $image->getClientOriginalExtension();
        $destinationPath = public_path('/img');
        $image->move($destinationPath, $name);
        $validator['mobile_image'] = $name;
        $i++;
      }
      if ($request->file('web_image') !== null) {
        $image = $request->file('web_image');
        $name = time() . '-' . $i . '.' . $image->getClientOriginalExtension();
        $destinationPath = public_path('/img');
        $image->move($destinationPath, $name);
        $validator['web_image'] = $name;
      }
      $validator['status'] = 1;
      $this->model::create($validator);
      return redirect()->route($this->index_route);
    } catch (\Exception $ex) {
      Log::error($ex->getMessage());
      return redirect()->route($this->create_route);
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\SubCategory $subCategory
   * @return \Illuminate\Http\Response
   */
  public function show(SubCategory $subCategory)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  \App\SubCategory $subCategory
   * @return \Illuminate\Http\Response
   */
  public function edit(SubCategory $subCategory)
  {
    $categories = Category::all();
    return view($this->edit_view, compact('subCategory', 'categories'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @param  \App\SubCategory $subCategory
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $validator = $request->validate([
      'name' => ['required', 'string', Rule::unique('sub_category', 'name')->where(function($query) use ($request, $id) {
        return $query->where('category_id', '=', $request->category_id)->where('id', '<>', $id);
      })],
      'category_id' => 'required|string|not_in:0|exists:category,id',
    ]);
    try {
      $i = 1;
      if ($request->file('mobile_image') !== null) {
        $request->validate([
          'mobile_image' => 'required|image|mimes:jpeg,bmp,png',
        ]);
        $image = $request->file('mobile_image');
        $name = time() . '-' . $i . '.' . $image->getClientOriginalExtension();
        $destinationPath = public_path('/img');
        $image->move($destinationPath, $name);
        $validator['mobile_image'] = $name;
        $i++;
      }
      if ($request->file('web_image') !== null) {
        $request->validate([
          'web_image' => 'required|image|mimes:jpeg,bmp,png',
        ]);
        $image = $request->file('web_image');
        $name = time() . '-' . $i . '.' . $image->getClientOriginalExtension();
        $destinationPath = public_path('/img');
        $image->move($destinationPath, $name);
        $validator['web_image'] = $name;
      }
      $data = $this->model::find($id);
      $data->update($validator);
      return redirect()->route($this->index_route);
    } catch (\Exception $ex) {
      Log::error($ex->getMessage());
      return redirect()->back();
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\SubCategory $subCategory
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    try {
      $data = $this->model::find($id);
      $data->update([
        'status' => 3
      ]);
      return redirect()->route($this->index_route);
    } catch (\Exception $ex) {
      Log::error($ex->getMessage());
      return redirect()->route($this->index_route);
    }
  }

  public function changeStatus($id)
  {
    try {
      $status = [
        '1' => '2',
        '2' => '1'
      ];
      $data = $this->model::find($id);
      $data->update([
        'status' => $status[$data->status]
      ]);
      return redirect()->route($this->index_route);
    } catch (\Exception $ex) {
      Log::error($ex->getMessage());
      return redirect()->route($this->index_route);
    }
  }
}
