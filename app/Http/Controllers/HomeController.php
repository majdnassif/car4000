<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        return view('dking.home');
    }

    public function loginRegister()
    {
        return view('dking.auth.login-register',['LR'=>'L']);
    }
}