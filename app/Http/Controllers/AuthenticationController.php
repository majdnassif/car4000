<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;

class AuthenticationController extends Controller
{

  public function showRegistrationForm()
    {
        return view('dking.auth.login-register',['LR'=>'R']);

    }

    public function register(Request $request)
    {
        if (is_numeric($request->email_or_phone)) {
            $this->validate($request,[
                'email_or_phone' => 'required|unique:users,phone_number',
                'name' => 'required|string',
                'password' => 'required|string',
            ]);
        }else{

            $this->validate($request,[
                'email_or_phone' => 'required|unique:users,email',
                'name' => 'required|string',
                'password' => 'required|string',
            ]);
        }
        $response = app('App\Http\Controllers\API\UsersController')->signUp($request)->getData();
        if($response->code == 201)
            return view('dking.auth.verify', [
                'email_or_phone' => $request->email_or_phone,
            ]);
        else
            return view('dking.auth.login-register', [
                'LR' => 'R',
                'message' => $response->message,
            ]);
    }

    public function showVerifyForm()
    {
        return view('dking.auth.verify');
    }

    public function verify(Request $request)
    {
        $response = app('App\Http\Controllers\API\UsersController')->verifyUser($request)->getData();
        if($response->code == 200)
        {
            $user = User::findOrFail($response->data->id);
            Auth::login($user);
            return view('dking.home');
        }
        else
            return view('dking.auth.verify', [
                'message' => $response->message,
                'email_or_phone' => $request->email_or_phone,
            ]);
    }

    public function showLoginForm()
    {
        return view('dking.auth.login-register',['LR'=>'L']);

    }

    public function login(Request $request)
    {
        $this->validate($request,[
            'email_or_phone' => "required|min:3",
            'password' => "required|min:3",
            ]);
        $response = app('App\Http\Controllers\API\UsersController')->login($request)->getData();
        if($response->code == 200)
        {
            $user = User::findOrFail($response->data->id);

            Auth::login($user);
            if($user->hasRoles(1,NULL))
                {
                  $pageConfigs = [
                    'pageHeader' => false
                ];

                return view('/pages/dashboard-analytics', [
                    'pageConfigs' => $pageConfigs
                ]);
                }
            else
                return view('dking.home');
        }
        else
            return view('dking.auth.login-register', [
                'LR' => 'L',
                'message' => $response->message,
                'email_or_phone' => $request->email_or_phone,
            ]);
    }

    public function showForgetPasswordForm()
    {
      return view('dking.auth.forget_password');
    }

    public function forgetPassword(Request $request)
    {
    if (is_numeric($request->email_or_phone)) {
    $this->validate($request,[
        'email_or_phone' => "required|exists:users,phone_number",
        ]);
    }else{
      $this->validate($request,[
        'email_or_phone' => "required|exists:users,email",
        ]);
    }
    $response = app('App\Http\Controllers\API\UsersController')->forgetPassword($request)->getData();
    if($response->code == 200)
    {
        return view('dking.auth.reset_password', [
            'email_phone' => $request->email_or_phone,
        ]);
    }
    else
        return view('dking.auth.forget_password', [
            'message' => $response->message,
            'email_or_phone' => $request->email_or_phone,
        ]);
    }

    public function showResetPasswordForm()
    {
      return view('dking.auth.reset_password');
    }

    public function resetPassword(Request $request)
    {
        $response = app('App\Http\Controllers\API\UsersController')->resetPassword($request)->getData();
        if($response->code == 200)
        {
            return view('dking.auth.login-register', [
                'LR' => 'L',
                'message' => $response->message,
            ]);
        }
        else
            return view('dking.auth.reset_password', [
                'message' => $response->message,
            ]);
    }

    public function resendVerificationCode(Request $request)
    {
      if (is_numeric($request->email_phone))
      {
        $request->merge(['typeSend' => 2]);
      }
      else
      {
        $request->merge(['typeSend' => 1]);
      }
      $response = app('App\Http\Controllers\API\UsersController')->resendVerificationCode($request);

      if($request->type == 1)
          return view('dking.auth.verify', [
            'email_or_phone' => $request->email_phone,
        ]);

      if($request->type == 2)
          return view('dking.auth.reset_password', [
            'email_phone' => $request->email_phone,
        ]);
    }








  // Login
  // public function index()
  // {
  //   $pageConfigs = [
  //     'bodyClass' => "bg-full-screen-image",
  //     'blankPage' => true
  //   ];
  //   return view('/pages/auth-login', [
  //     'pageConfigs' => $pageConfigs
  //   ]);
  // }


  // public function login(Request $request)
  // {
  //   $validator = $request->validate([
  //     'username' => "required|min:3",
  //     'password' => "required|min:3",
  //   ]);
  //   $user = User::where('username', $validator['username'])->first();
  //   if ($user) {
  //     if (Hash::check($validator['password'], $user['password'])) {
  //       Auth::login($user);
  //       return redirect()->route('home');
  //     } else {
  //       return Redirect::back()->withInput($request->all())->withErrors(['Login Error']);
  //     }
  //   } else {
  //     return Redirect::back()->withInput($request->all())->withErrors(['Login Error']);
  //   }
  // }
}
