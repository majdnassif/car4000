<?php

namespace App\Http\Controllers;

use App\Country;
use App\Towns;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class TownsController extends Controller
{
  private $index_route = 'towns.index';
  private $create_route = 'towns.create';
  private $index_view = 'dash.UkTowns.index';
  private $create_view = 'dash.UkTowns.create';
  private $edit_view = 'dash.UkTowns.edit';
  private $model = Towns::class;

  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $data = $this->model::with('country')->orderBy('id', 'DESC')->get();
    return view($this->index_view, compact('data'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $data = Country::all(['id', 'name']);
    return view($this->create_view, compact('data'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $validator = $request->validate([
      'name' => 'required|string|unique:' . $this->model,
      'country_id' => 'required|string|exists:country,id',
      'county' => 'required|string',
      'grid_reference' => 'required|string',
      'easting' => 'required|numeric',
      'northing' => 'required|numeric',
      'latitude' => 'required|numeric',
      'longitude' => 'required|numeric',
      'elevation' => 'required|numeric',
      'postcode_sector' => 'required|string',
      'local_government_area' => 'required|string',
      'nuts_region' => 'required|string',
      'type' => 'required|string',
    ]);
    try {
      $this->model::create($validator);
      return redirect()->route($this->index_route);
    } catch (\Exception $ex) {
      Log::error($ex->getMessage());
      return redirect()->route($this->create_route);
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\Towns $towns
   * @return \Illuminate\Http\Response
   */
  public function show(Towns $towns)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  \App\Towns $towns
   * @return \Illuminate\Http\Response
   */
  public function edit(Towns $town)
  {
    $data = Country::all(['id', 'name']);
    return view($this->edit_view, compact('town', 'data'));

  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @param  \App\Towns $towns
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $validator = $request->validate([
      'name' => 'required|string|unique:' . $this->model . ',name,' . $id . ',id',
      'country_id' => 'required|string|exists:country,id',
      'county' => 'required|string',
      'grid_reference' => 'required|string',
      'easting' => 'required|numeric',
      'northing' => 'required|numeric',
      'latitude' => 'required|numeric',
      'longitude' => 'required|numeric',
      'elevation' => 'required|numeric',
      'postcode_sector' => 'required|string',
      'local_government_area' => 'required|string',
      'nuts_region' => 'required|string',
      'type' => 'required|string',
    ]);
    try {
      $data = $this->model::find($id);
      $data->update($validator);
      return redirect()->route($this->index_route);
    } catch (\Exception $ex) {
      Log::error($ex->getMessage());
      return redirect()->back();
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\Towns $towns
   * @return \Illuminate\Http\Response
   */
  public function destroy(Towns $town)
  {
    try {
      $town->delete();
      return redirect()->route($this->index_route);
    } catch (\Exception $ex) {
      Log::error($ex->getMessage());
      return redirect()->route($this->index_route);
    }
  }
}
