<?php

namespace App\Http\Controllers;

use App\AdPriceList;
use App\Applications;
use App\Currency;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class PriceController extends Controller
{
    private $index_route = 'prices.index';
    private $create_route = 'prices.create';
    private $index_view = 'dash.Price.index';
    private $create_view = 'dash.Price.create';
    private $edit_view = 'dash.Price.edit';
    private $model = AdPriceList::class;

    public function __construct()
    {
      $this->middleware('auth');
    }
    
    public function index()
    {
        $data = $this->model::orderBy('id', 'DESC')->get();
        return view($this->index_view, compact('data'));
    }

    public function create()
    {
        $apps = Applications::all();
        $currencies = Currency::all();
        return view($this->create_view, compact('apps', 'currencies'));
    }
    
    public function store(Request $request)
    {
        $validator = $request->validate([
            'app_id' => 'required|string|not_in:0|exists:application,id',
            'name' => 'required|string',
            'value' => 'required|numeric',
            'currency' => 'required|string|exists:currency,code',
        ]);
        try 
        {
            $this->model::create($validator);
            return redirect()->route($this->index_route);
        } 
        catch (\Exception $ex) 
        {
            Log::error($ex->getMessage());
            return redirect()->route($this->create_route);
        }
    }

    public function edit(AdPriceList $price)
    {
        $apps = Applications::all();
        $currencies = Currency::all();
    
        return view($this->edit_view, compact('price', 'apps', 'currencies'));
    }

    public function update(Request $request, $id)
    {
        $validator = $request->validate([
            'app_id' => 'required|string|not_in:0|exists:application,id',
            'name' => 'required|string',
            'value' => 'required|numeric',
            'currency' => 'required|string|exists:currency,code',
        ]);
        try 
        {
            $data = $this->model::find($id);
            $data->update($validator);
            return redirect()->route($this->index_route);
        } 
        catch (\Exception $ex) 
        {
            Log::error($ex->getMessage());
            return redirect()->back();
        }
    }

    public function destroy(AdPriceList $price)
    {
        try {
            $price->delete();
            return redirect()->back();
        } catch (\Exception $ex) {
            Log::error($ex->getMessage());
            return redirect()->back();
        }
    }
}