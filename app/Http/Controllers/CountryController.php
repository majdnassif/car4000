<?php

namespace App\Http\Controllers;

use App\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CountryController extends Controller
{
  private $index_route = 'countries.index';
  private $create_route = 'countries.create';
  private $index_view = 'dash.Country.index';
  private $create_view = 'dash.Country.create';
  private $edit_view = 'dash.Country.edit';
  private $model = Country::class;

  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $data = $this->model::orderBy('id', 'DESC')->get();
    return view($this->index_view, compact('data'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view($this->create_view);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $validator = $request->validate([
      'name' => 'required|string|unique:' . $this->model,
    ]);
    try {
      $this->model::create($validator);
      return redirect()->route($this->index_route);
    } catch (\Exception $ex) {
      Log::error($ex->getMessage());
      return redirect()->route($this->create_route);
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\Country $country
   * @return \Illuminate\Http\Response
   */
  public function show(Country $country)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  \App\Country $country
   * @return \Illuminate\Http\Response
   */
  public function edit(Country $country)
  {
    return view($this->edit_view, compact('country'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @param  \App\Country $country
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $validator = $request->validate([
      'name' => 'required|string|unique:' . $this->model . ',name,' . $id . ',id',
    ]);
    try {
      $data = $this->model::find($id);
      $data->update($validator);
      return redirect()->route($this->index_route);
    } catch (\Exception $ex) {
      Log::error($ex->getMessage());
      return redirect()->back();
    }
  }

}
