<?php

namespace App\Http\Controllers;

use App\Applications;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;


class CategoriesController extends Controller
{
  private $index_route = 'categories.index';
  private $create_route = 'categories.create';
  private $index_view = 'dash.categories.index';
  private $create_view = 'dash.categories.create';
  private $edit_view = 'dash.categories.edit';
  private $model = Category::class;

  public function __construct()
  {
    $this->middleware('auth');
  }

  public function index()
  {
    $data = Applications::all(['id', 'name']);
    return view($this->index_view, compact('data'));
  }

  public function list(Request $request)
  {
    $categories = $this->model::with('subs')->with('application')
      ->where('status', '<>', '3')
      ->where('parent_id', 0)
      ->skip($request->start)
      ->take($request->length)
      ->orderBy('app_id', 'ASC')
      ->orderBy('ordering', 'ASC')
      ->when(isset($request->search_filter) && $request->search_filter !== '', function ($query) use ($request) {
        $query->where('name', 'like', '%' . $request->search_filter . '%');
      })
      ->when(isset($request->category_select) && $request->category_select != 0, function ($query) use ($request) {
        $query->where('app_id', $request->category_select);
      })
      ->when(isset($request->status_select) && $request->status_select !== '', function ($query) use ($request) {
        $query->where('status', $request->status_select);
      })
      ->get();
    $all = $this->model::where('status', '<>', '3')
      ->where('parent_id', 0)
      ->when(isset($request->search_filter) && $request->search_filter !== '', function ($query) use ($request) {
        $query->where('name', 'like', '%' . $request->search_filter . '%');
      })
      ->when(isset($request->category_select) && $request->category_select != 0, function ($query) use ($request) {
        $query->where('app_id', $request->category_select);
      })
      ->when(isset($request->status_select) && $request->status_select !== '', function ($query) use ($request) {
        $query->where('status', $request->status_select);
      })
      ->get('id');
    $data['draw'] = $request->draw;
    $data['recordsTotal'] = count($all);
    $data['recordsFiltered'] = count($all);
    $data['data'] = array();
    $i = 1;
    foreach ($categories as $category) {
      $a = $category->getOriginal();
      $a['order'] = $i;
      array_push($data['data'], array_values($a));
      array_push($data['data'][$i - 1], $category->app_name());
      array_push($data['data'][$i - 1], $category->subs);
      $i++;
    }
    return json_encode($data);
  }


  public function create()
  {
    $apps = Applications::all();
    return view($this->create_view, compact('apps'));
  }

  public function store(Request $request)
  {
    $validator = $request->validate([
      'name' => 'required|string',
      'app_id' => 'required|string|not_in:0|exists:application,id',
      'mobile_image' => 'nullable|image|mimes:jpeg,bmp,png,jpg',
      'web_image' => 'nullable|image|mimes:jpeg,bmp,png,jpg',
    ]);

    try {
      $cat = $request->all();
      $i = 1;
      if ($request->file('mobile_image') !== null) {
        $image = $request->file('mobile_image');
        $name = time() . '-' . $i . '.' . $image->getClientOriginalExtension();
        $destinationPath = public_path('/img');
        $image->move($destinationPath, $name);
        $cat['mobile_image'] = $name;
        $i++;
      }
      if ($request->file('web_image') !== null) {
        $image = $request->file('web_image');
        $name = time() . '-' . $i . '.' . $image->getClientOriginalExtension();
        $destinationPath = public_path('/img');
        $image->move($destinationPath, $name);
        $cat['web_image'] = $name;
      }
      if (!$request->parent_id) {
        $cat['parent_id'] = 0;
        $cOrder = Category::max('ordering');
      } else {
        $cOrder = Category::where('parent_id', $request->parent_id)->max('ordering');

      }
      if($cOrder === null) {
        $cOrder = 0;
      }
      $cat['status'] = 1;
      $cat['ordering'] = $cOrder + 1;
      $this->model::create($cat);
      return redirect()->back();

    } catch (\Exception $ex) {
      Log::error($ex->getMessage());
      return redirect()->back();
    }
  }

  public function edit(Category $category)
  {
    $apps = Applications::all();
    $categories = Category::all();

    return view($this->edit_view, compact('category', 'apps', 'categories'));
  }

  public function update(Request $request, $id)
  {
    $validator = $request->validate([
      'name' => 'required|string',
      'app_id' => 'required|string|not_in:0|exists:application,id',
    ]);
    try {
      $cat = $request->all();
      $i = 1;
      if ($request->file('mobile_image') !== null) {
        $request->validate([
          'mobile_image' => 'required|image|mimes:jpeg,bmp,png,jpg',
        ]);
        $image = $request->file('mobile_image');
        $name = time() . '-' . $i . '.' . $image->getClientOriginalExtension();
        $destinationPath = public_path('/img');
        $image->move($destinationPath, $name);
        $cat['mobile_image'] = $name;
        $i++;
      }
      if ($request->file('web_image') !== null) {
        $request->validate([
          'web_image' => 'required|image|mimes:jpeg,bmp,png,jpg',
        ]);
        $image = $request->file('web_image');
        $name = time() . '-' . $i . '.' . $image->getClientOriginalExtension();
        $destinationPath = public_path('/img');
        $image->move($destinationPath, $name);
        $cat['web_image'] = $name;
      }
      $data = $this->model::find($id);
      if (!$request->parent_id) {
        $cat['parent_id'] = 0;
      }
      $data->update($cat);
      return redirect()->route($this->index_route);
    } catch (\Exception $ex) {
      Log::error($ex->getMessage());
      return redirect()->back();
    }
  }

  public function destroy(Category $category)
  {
    try {
      $category->update([
        'status' => 3
      ]);
      return redirect()->back();
    } catch (\Exception $ex) {
      Log::error($ex->getMessage());
      return redirect()->back();
    }
  }

  public function changeStatus($id)
  {
    try {
      $status = [
        '1' => '2',
        '2' => '1'
      ];
      $data = $this->model::find($id);
      $data->update([
        'status' => $status[$data->status]
      ]);
      return redirect()->back();
    } catch (\Exception $ex) {
      Log::error($ex->getMessage());
      return redirect()->back();
    }
  }

  public function sub($id)
  {
    $mainCat = $this->model::find($id);
    $data = $this->model::with('subs')->where('parent_id', $id)->where('status', '<>', '3')->orderBy('ordering', 'ASC')->get();
    return view('dash.categories.sub_index', compact('data', 'id', 'mainCat'));
  }


  public function create_sub($id)
  {
    $data = $this->model::find($id);
    return view($this->create_view, compact('data'));
  }

  public function order()
  {
    $categories = Category::where('parent_id', 0)->orderBy('ordering')->get();
    return view('dash.categories.sort', compact('categories'));
  }

  public function categorySortAdd(Request $request)
  {
    if (!empty($request->all())) {
      $count = count($request['row_id']);
      $ordering = [];
      for ($i = 0; $i < $count; $i++) {
        if ($request['row_id'][$i] != 'No data available in table') {
          $ordering[] = Category::find($request['row_id'][$i])->ordering;
        }
      }
      sort($ordering);
      for ($i = 0; $i < $count; $i++) {
        if ($request['row_id'][$i] != 'No data available in table') {
          $category = Category::find($request['row_id'][$i]);
          $category->update([
            'ordering' => $ordering[$i],
          ]);
          $category->save();
        }
      }
    }
  }


  public function subOrder($id)
  {
    $categories = Category::where('parent_id', $id)->orderBy('ordering')->get();
    return view('dash.categories.sort', compact('categories'));
  }

  public function subCategorySortAdd(Request $request)
  {
    if (!empty($request->all())) {
      $count = count($request['row_id']);
      for ($i = 0; $i < $count; $i++) {
        if ($request['row_id'][$i] != 'No data available in table') {
          $category = Category::find($request['row_id'][$i]);
          $category->update([
            'ordering' => $i + 1,
          ]);
          $category->save();
        }
      }
    }
  }
}
