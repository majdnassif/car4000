<?php

namespace App\Http\Controllers;

use App\Applications;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ApplicationsController extends Controller
{
  private $index_route = 'applications.index';
  private $create_route = 'applications.create';
  private $index_view = 'dash.Applications.index';
  private $create_view = 'dash.Applications.create';
  private $edit_view = 'dash.Applications.edit';
  private $model = Applications::class;

  public function __construct()
  {
    $this->middleware('auth');
  }

  public function index()
  {
    $data = $this->model::orderBy('id', 'DESC')->get();
    return view($this->index_view, compact('data'));
  }

  public function create()
  {
    return view($this->create_view);
  }

  public function store(Request $request)
  {
    $validator = $request->validate([
      'name' => 'required|string|unique:application',
      'logo' => 'required|image|mimes:jpeg,bmp,png',
    ]);
    try {
      $image = $request->file('logo');
      $name = time() . '.' . $image->getClientOriginalExtension();
      $destinationPath = public_path('/img');
      $image->move($destinationPath, $name);
      $validator['logo'] = $name;
      $validator['status'] = 1;
      $this->model::create($validator);
      return redirect()->route($this->index_route);
    } catch (\Exception $ex) {
      Log::error($ex->getMessage());
      return redirect()->route($this->create_route);
    }
  }

  public function edit(Applications $application)
  {
    return view($this->edit_view, compact('application'));
  }

  public function update(Request $request, $id)
  {

    $validator = $request->validate([
      'name' => 'required|string|unique:application,name,' . $id . ',id',
    ]);
    try {
      if ($request->file('logo') !== null) {
        $validator = $request->validate([
          'name' => 'required|string|unique:application,name,' . $id . ',id',
          'logo' => 'required|image|mimes:jpeg,bmp,png',
        ]);
        $image = $request->file('logo');
        $name = time() . '.' . $image->getClientOriginalExtension();
        $destinationPath = public_path('/img');
        $image->move($destinationPath, $name);
        $validator['logo'] = $name;
      }
      $data = $this->model::find($id);
      $data->update($validator);
      return redirect()->route($this->index_route);
    } catch (\Exception $ex) {
      Log::error($ex->getMessage());
      return redirect()->back();
    }
  }

  public function destroy(Applications $application)
  {
    try {
      $application->delete();
      return redirect()->route($this->index_route);
    } catch (\Exception $ex) {
      Log::error($ex->getMessage());
      return redirect()->route($this->index_route);
    }
  }

  public function changeStatus($id)
  {
    try {
      $status = [
        '1' => '2',
        '2' => '1'
      ];
      $data = $this->model::find($id);
      $data->update([
        'status' => $status[$data->status]
      ]);
      return redirect()->route($this->index_route);
    } catch (\Exception $ex) {
      Log::error($ex->getMessage());
      return redirect()->route($this->index_route);
    }
  }
}
