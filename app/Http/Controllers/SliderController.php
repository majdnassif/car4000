<?php

namespace App\Http\Controllers;

use App\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class SliderController extends Controller
{
  private $index_route = 'slider.index';
  private $create_route = 'slider.create';
  private $index_view = 'dash.slider.index';
  private $create_view = 'dash.slider.create';
  private $edit_view = 'dash.slider.edit';
  private $model = Slider::class;


  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $data = $this->model::orderBy('ordering')->get();
    return view($this->index_view, compact('data'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    return view($this->create_view);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $validator = $request->validate([
      'photo' => 'required|image|mimes:jpeg,bmp,png,jpg',
    ]);

    try {
      $image = $request->file('photo');
      $name = time() . '.' . $image->getClientOriginalExtension();
      $destinationPath = public_path('/img');
      $image->move($destinationPath, $name);
      $validator['photo'] = $name;
      $cOrder = Slider::max('ordering');

      if ($cOrder === null) {
        $cOrder = 0;
      }
      $validator['ordering'] = $cOrder + 1;
      $this->model::create($validator);
      return redirect()->route($this->index_route);

    } catch (\Exception $ex) {
      Log::error($ex->getMessage());
      return redirect()->back();
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\Slider $slider
   * @return \Illuminate\Http\Response
   */
  public function show(Slider $slider)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  \App\Slider $slider
   * @return \Illuminate\Http\Response
   */
  public function edit(Slider $slider)
  {
    //
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @param  \App\Slider $slider
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, Slider $slider)
  {
    //
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  \App\Slider $slider
   * @return \Illuminate\Http\Response
   */
  public function destroy(Slider $slider)
  {
    try {
      $slider->delete();
      return redirect()->back();
    } catch (\Exception $ex) {
      Log::error($ex->getMessage());
      return redirect()->back();
    }
  }

  public function sliderSortAdd (Request $request)
  {
    if (!empty($request->all())) {
      $count = count($request['row_id']);
      $ordering = [];
      for ($i = 0; $i < $count; $i++) {
        if ($request['row_id'][$i] != 'No data available in table') {
          $ordering[] = Slider::find($request['row_id'][$i])->ordering;
        }
      }
      sort($ordering);
      for ($i = 0; $i < $count; $i++) {
        if ($request['row_id'][$i] != 'No data available in table') {
          $category = Slider::find($request['row_id'][$i]);
          $category->update([
            'ordering' => $ordering[$i],
          ]);
          $category->save();
        }
      }
    }
  }

}
