<?php

namespace App\Http\Controllers\API;


use App\Ads;
use App\SiteSettings;
use App\Media;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use LaravelFCM\Facades\FCM;
use Carbon\Carbon;
class MediaController extends Controller
{
    // 
  public function store(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'ad_id' => 'required|exists:ads,id',
      'is_last' => 'required',
      'type' => 'required',
      'image' => 'required|mimes:jpeg,bmp,png,mp4,3gpp,avi',
    ]);
    if ($validator->fails()) {
      $errors = $validator->errors();
      $data['message'] = $errors->first();
      $data['data'] = null;
      $data['code'] = -1;
      return response()->json($data, 200);
    }
    try {
      $image = $request->file('image');
      $name = time() . '.' . $image->getClientOriginalExtension();
      $destinationPath = public_path('/img');
      $image->move($destinationPath, $name);
      $request['link'] = $name;
      Media::create($request->all());

      if ($request->is_last == 1) {
        $ad = Ads::find($request->ad_id);
        // $ad_attr = AdAttributes::where('re_ad_id', $request->ad_id)->first();
        // $users = UserInterest::where('town_id', $ad->town_id)
        //   ->orWhere('option_id', $ad_attr->option_id)
        //   ->orWhere('type_id', $ad_attr->type_id)
        //   ->orWhere('rooms', $ad_attr->rooms)
        //   ->orWhere('price_duration_id', $ad_attr->price_duration_id)
        //   ->orWhere('balcony', $ad_attr->balcony)
        //   ->orWhere('bath', $ad_attr->bath)
        //   ->orWhere('garage', $ad_attr->garage)
        //   ->orWhere('terrace', $ad_attr->terrace)
        //   ->orWhere('furniture_id', $ad_attr->furniture_id)
        //   ->orWhere('size_unit_id', $ad_attr->size_unit_id)
        //   ->orWhere('period', $ad_attr->period)
        //   ->orWhere('monthly_price', $ad_attr->monthly_price)
        //   ->orWhere('gym', $ad_attr->gym)
        //   ->orWhere('security', $ad_attr->security)
        //   ->when(isset($ad_attr->size), function ($q) use($ad_attr){
        //     $q->where('size_from', '<', $ad_attr->size)->where('size_to', '>', $ad_attr->size);
        //   })
        //   ->when(isset($ad_attr->price), function ($q) use($ad_attr){
        //     $q->where('price_from', '<', $ad_attr->price)->where('price_to', '>', $ad_attr->price);
        //   })
        //   ->get('user_id');

        // foreach ($users as $user) {
        //   $u = User::find($user->user_id);
        //   if ($u && $u->firebase_token != '' && $u->firebase_token != null) {
        //     try {
        //       $tokens = [$u->firebase_token];

        //       $optionBuilder = new OptionsBuilder();
        //       $optionBuilder->setTimeToLive(60 * 20);
        //       $notificationBuilder = new PayloadNotificationBuilder('4000');
        //       $notificationBuilder->setBody('There is a new advertisement you may be interested in')
        //         ->setSound('default');
        //       $dataBuilder = new PayloadDataBuilder();
        //       $option = $optionBuilder->build();
        //       $notification = $notificationBuilder->build();
        //       $data = $dataBuilder->build();
        //       $downstreamResponse = FCM::sendTo($tokens, $option, $notification, $data);
        //       $downstreamResponse->numberSuccess();
        //       $downstreamResponse->numberFailure();
        //       $downstreamResponse->numberModification();
        //       $downstreamResponse->tokensToDelete();
        //       $downstreamResponse->tokensToModify();
        //       $downstreamResponse->tokensToRetry();
        //       $downstreamResponse->tokensWithError();
        //       UserNotification::create([
        //         'user_id' => $user->user_id,
        //         'ad_id' => $request->ad_id

        //       ]);

        //     } catch (\Exception $ex) {
        //       Log::error($ex->getMessage());
        //     }
        //   }
        // }
        if($ad->is_free == 0){
          $ad->update([
            'status' => 3
          ]);
        }else{
          $ad->update([
            'status' => 1
          ]);
        };
      }
      $response['message'] = "Success";
      $response['data'] = null;
      $response['code'] = 200;
      return response()->json($response, 200);

    } catch (\Exception $ex) {
      Log::error($ex->getMessage());
      $response['message'] = "An error occured please try again later";
      $response['data'] = null;
      $response['code'] = -99;
      return response()->json($response, 200);
    }
  }
  
  public function updateUserPicture(Request $request)
  {
    $validator = Validator::make($request->all(), [
      // 'user_id' => 'required|exists:users,id',
      'image' => 'mimes:jpeg,bmp,png,mp4,3gpp,avi',
      'profile_video' => 'mimes:jpeg,bmp,png,mp4,3gpp,avi',
    ]);
    if ($validator->fails()) {
      $errors = $validator->errors();
      $data['message'] = $errors->first();
      $data['data'] = null;
      $data['code'] = -1;
      return response()->json($data, 200);
    }
    try {
      if ($request->image != null && $request->image != '') {
        $image = $request->file('image');
        $nameImage = time() . '.' . $image->getClientOriginalExtension();
        $destinationPath = public_path('/img');
        $image->move($destinationPath, $nameImage);
      }
      if ($request->profile_video != null && $request->profile_video != '') {
        $profile_video = $request->file('profile_video');
        $nameVideo = time() . '.' . $profile_video->getClientOriginalExtension();
        $destinationPath = public_path('/img');
        $profile_video->move($destinationPath, $nameVideo);
      }
      $user = User::where('id', auth('api')->user()->id)->first();
      if ($request->image == null || $request->image == '') {
        $nameImage = $user->profile_pic;
      }
      if ($request->profile_video == null || $request->profile_video == '') {
        $nameVideo = $user->profile_video;
      }
      $user = User::where('id', auth('api')->user()->id)->first();
      $user->update([
        'profile_pic' => $nameImage,
        'profile_video' => $nameVideo
      ]);
      $response['message'] = "Success";
      $response['data'] = array(
        'imagePath' => $nameImage,
        'videoPath' => $nameVideo
      );
      $response['code'] = 200;
      return response()->json($response, 200);

    } catch (\Exception $ex) {
      Log::error($ex->getMessage());
      $response['message'] = "An error occured please try again later";
      $response['data'] = null;
      $response['code'] = -99;
      return response()->json($response, 200);
    }
  }
}
