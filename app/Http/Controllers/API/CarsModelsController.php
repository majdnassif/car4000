<?php

namespace App\Http\Controllers\API;

use App\CarsModels;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CarsModelsController extends Controller
{
    //
    public function list(Request $request)
    {
        $data = CarsModels::when(isset($request->parent_id) && $request->parent_id !== '', function ($query) use ($request) {
        $query->where('parent_id', $request->parent_id);
        })
        ->orderBy('model_order', 'ASC')
        ->get();
        if (count($data) > 0) {
        $response['message'] = "Success";
        $response['data'] = $data;
        $response['code'] = 200;
        return response()->json($response, 200);

        } else {
        $response['message'] = "No data";
        $response['data'] = null;
        $response['code'] = -1;
        return response()->json($response, 200);
        }
    }
}
