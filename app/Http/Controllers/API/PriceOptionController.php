<?php

namespace App\Http\Controllers\API;

use App\PriceOption;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PriceOptionController extends Controller
{
    //
    public function list() {
        $data = PriceOption::
        // when(isset($request->option_id) && $request->option_id !== '', function ($query) use ($request) {
        //   $query->whereHas('option.option_id', function ($q) use ($request) {
        //     $q->where('option_id', $request->option_id);
        //   });
        // })
        all();
        if (count($data) > 0) {
          $response['message'] = "Success";
          $response['data'] = $data;
          $response['code'] = 200;
          return response()->json($response, 200);
  
        } else {
          $response['message'] = "No data";
          $response['data'] = null;
          $response['code'] = -1;
          return response()->json($response, 200);
        }
      }
}
