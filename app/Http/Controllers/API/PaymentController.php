<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;

use App\Ads;
use App\AdPayment;
use App\AdPriceList;

use URL;

use Illuminate\Http\Request;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Api\PaymentExecution;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;

class PaymentController extends Controller
{

    private $_api_context;
    
    public function __construct()
    {
        /** PayPal api context **/
        $paypal_conf = \Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential(
            $paypal_conf['client_id'],
            $paypal_conf['secret'])
        );
        $this->_api_context->setConfig($paypal_conf['settings']);
    }

    public function payWithpaypal(Request $request)
    {
        $price = AdPriceList::find($request->price_id);
        $cost = $price->value;
        $currency = $price->currency;
        
        $days = $request->days;
        $ad_id = $request->ad_id;
        $ad = Ads::find($ad_id);

        if($ad)
        {
            if($days > 0 && $days < 31)
            {
                $payer = new Payer();
                        $payer->setPaymentMethod('paypal');
                $item_1 = new Item();
                $item_1->setName($ad->title) /** item name **/
                            ->setCurrency($currency)
                            ->setQuantity($days) /** days number **/
                            ->setPrice($cost); /** unit price **/
                $item_list = new ItemList();
                        $item_list->setItems(array($item_1));
                $amount = new Amount();
                        $amount->setCurrency($currency)
                            ->setTotal($cost*$days);
                $transaction = new Transaction();
                        $transaction->setAmount($amount)
                            ->setItemList($item_list)
                            ->setDescription('Payment for your Ad for ('.$days.') days');
                $redirect_urls = new RedirectUrls();
                        $redirect_urls->setReturnUrl(URL::route('executePayment').'?success=true') /** Specify return URL **/
                            ->setCancelUrl(URL::route('executePayment').'?success=false');
                $payment = new Payment();
                        $payment->setIntent('Sale')
                            ->setPayer($payer)
                            ->setRedirectUrls($redirect_urls)
                            ->setTransactions(array($transaction));
                        /** dd($payment->create($this->_api_context));exit; **/
                try {
                    $payment->create($this->_api_context);
                } catch (\PayPal\Exception\PPConnectionException $ex) {
                    if (\Config::get('app.debug')) {
                        $response['message'] = "Connection timeout";
                        $response['data'] = null;
                        $response['code'] = -1;
                        return response()->json($response, 200);
                    } else {
                        $response['message'] = "Some error occur, sorry for inconvenient";
                        $response['data'] = null;
                        $response['code'] = -2;
                        return response()->json($response, 200);
                    }
                }
                foreach ($payment->getLinks() as $link) {
                    if ($link->getRel() == 'approval_url') {
                        $redirect_url = $link->getHref();
                        break;
                    }
                }
                /** add payment ID to session **/
                // Session::put('paypal_payment_id', $payment->getId());
                AdPayment::create([
                    'ad_id' => $ad_id,
                    'paypal_payment_id' => $payment->getId(),
                    'total_amount' => $cost*$days,
                    'currency' => $currency,
                    'days' => $days,
                    'executed' => 0,
                ]);
                if (isset($redirect_url)) {
                    /** redirect to paypal **/
                    $response['message'] = "Redirect To the following URL";
                    $response['data'] = ['redirect_url'=>$redirect_url];
                    $response['code'] = 200;
                    return response()->json($response, 200);
                }
                $response['message'] = "Unknown error occurred";
                $response['data'] = null;
                $response['code'] = -1;
                return response()->json($response, 200);
            }
            else
            {
                $response['message'] = "Days must be between 0 and 30";
                $response['data'] = null;
                $response['code'] = -3;
                return response()->json($response, 200);
            }
        }
        else
        {
            $response['message'] = "There is no Ad selected for This payment operation";
            $response['data'] = null;
            $response['code'] = -4;
            return response()->json($response, 200);
        }
    }

    public function ExecutePayment()
    {
        if(isset($_GET['success']) && $_GET['success'] == 'true')
        {
            $paymentId = $_GET['paymentId'];
            $payment = Payment::get($paymentId, $this->_api_context);
            
            $execution = new PaymentExecution();
            $execution->setPayerId($_GET['PayerID']);
            
            $transaction = new Transaction();
            $amount = new Amount();
            $details = new Details();
            
            $adPayment = AdPayment::where('paypal_payment_id',$paymentId)->first();
            
            $details->setSubtotal($adPayment->total_amount);
            $amount->setCurrency($adPayment->currency);
            $amount->setTotal($adPayment->total_amount);
            $amount->setDetails($details);
            $transaction->setAmount($amount);
            
            $execution->addTransaction($transaction);
            try
            {
                $result = $payment->execute($execution, $this->_api_context);
                try 
                {
                    $payment = Payment::get($paymentId, $this->_api_context);
                }
                catch(Exception $ex)
                {
                    $response['message'] = "Get Payment : ".$ex;
                    $response['data'] = null;
                    $response['code'] = -1;
                    return response()->json($response, 200);
                }
            }
            catch(Exception $ex)
            {
                $response['message'] = "Executed Payment : ".$ex;
                $response['data'] = null;
                $response['code'] = -2;
                return response()->json($response, 200);
            }
            $ad = Ads::where('id',$adPayment->ad_id)->first();
            $ad->update(['status'=>1]);
            $adPayment->update(['executed'=>1]);            
            
            $response['message'] = "Payment: ". $payment->getId()." Executed Successfully";
            $response['data'] = null;
            $response['code'] = 200;
            return response()->json($response, 200);
        }
        else
        {
            $response['message'] = "User Cancelled the Approval";
            $response['data'] = null;
            $response['code'] = -3;
            return response()->json($response, 200);
        }
    }

}
