<?php

namespace App\Http\Controllers\API;

use App\UserTypes;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserTypesController extends Controller
{
    //
    public function list(Request $request) {
        $data = UserTypes::where('id',2)->orWhere('app_id',$request->app_id)->get();
        if (count($data) > 0) {
          $response['message'] = "Success";
          $response['data'] = $data;
          $response['code'] = 200;
          return response()->json($response, 200);
  
        } else {
          $response['message'] = "No data";
          $response['data'] = null;
          $response['code'] = -1;
          return response()->json($response, 200);
        }
      }
}
