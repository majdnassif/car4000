<?php

namespace App\Http\Controllers\API;

use App\Ads;
use App\SiteSettings;
use App\User;
use App\PropertyAdsAttributes;
use App\SavedAds;
use App\Media;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use LaravelFCM\Facades\FCM;
use Carbon\Carbon;

class AdsController extends Controller
{
    //
    // public function propertyStoreAd(Request $request)
    // {
    //   $validator = Validator::make($request->all(), [
    //     'town_id' => 'exists:uk_towns,id',
    //     'option_id' => 'exists:property_options,id',
    //     'category_id' => 'exists:category,id',
    //     'size_unit_id' => 'exists:property_size_unites,id',
    //     "photo_number" => 'required',
    //     "videos_number" => 'required',
    //   ]);
    //   if ($validator->fails()) {
    //     $errors = $validator->errors();
    //     $data['message'] = $errors->first();
    //     $data['data'] = null;
    //     $data['code'] = -1;
    //     return response()->json($data, 200);
    //   }
    //   $user = User::where('id', auth('api')->user()->id)->first();
    //   if ($user->type_id == 1) {
    //     $response['message'] = "You can't add Ads, you are admin";
    //     $response['data'] = null;
    //     $response['code'] = -2;
    //     return response()->json($response, 200);
    //   }
    //   if ($user->type_id == 2) {
    //     $images = SiteSettings::where('id',3)->first();
    //     $videos = SiteSettings::where('id',4)->first();
    //     if($request->photo_number > $images->value) {
    //       $response['message'] = "You can't add more than 6 photos";
    //       $response['data'] = null;
    //       $response['code'] = -99;
    //       return response()->json($response, 200);
    //     }
    //     if($request->videos_number > $videos->value) {
    //       $response['message'] = "You can't add more than 1 video";
    //       $response['data'] = null;
    //       $response['code'] = -3;
    //       return response()->json($response, 200);
    //     }
    //     $count = Ads::where('owner_id', auth('api')->user()->id)->where('status', 1)->get()->count();
    //     $d = SiteSettings::find(1);
    //     if ($count >= $d->value) {
    //       $data['message'] = 'Sorry, you have reached the maximum limit of free ads, you can switch to business account to get more free';
    //       $data['data'] = null;
    //       $data['code'] = -2;
    //       return response()->json($data, 200);
    //     }
    //     try {
    //       $storedData = Ads::create([
    //         'title' => $request->title,
    //         'description' => $request->description,
    //         'owner_id' => auth('api')->user()->id,
    //         'town_id' => $request->town_id,
    //         'app_id' => $request->app_id,
    //         'thumbnail' => $request->thumbnail,
    //         'longitude' => $request->longitude,
    //         'latitiude' => $request->latitude,
    //         'status' => 7,
    //         'validty_date' => (new Carbon(Carbon::now()))->addDays(30),
    //         'is_free' => 1,
    //         'detailed_location' => $request->detailed_location
    //       ]);
    //       PropertyAdsAttributes::create([
    //         'ad_id' => $storedData->id,
    //         'category_id' => $request->category_id,
    //         'option_id' => $request->option_id,
    //         'price' => $request->price,
    //         'price_option_id' => $request->price_option_id,
    //         'size' => $request->size,
    //         'size_unit_id' => $request->size_unit_id,
    //         'room_number_id' => $request->room_id,
    //         'garage_number_id' => $request->garage_id,
    //         'furniture_type_id' => $request->furniture_id,
    //         'bath_number_id' => $request->bath_id,
    //         'balcony_number_id' => $request->balcony_id,
    //         'period' => $request->period,
    //         'monthly_price' => $request->monthly_price,
    //         'terrace' => $request->terrace,
    //         'gym' => $request->gym,
    //         'security' => $request->Security
    //       ]);
    //       $response['message'] = "Success";
    //       $response['data'] = array("id" => $storedData->id);
    //       $response['code'] = 200;
    //       return response()->json($response, 200);

    //     } catch (\Exception $ex) {
    //       Log::error($ex->getMessage());
    //       $response['message'] = "An error occured please try again later";
    //       $response['data'] = null;
    //       $response['code'] = -99;
    //       return response()->json($response, 200);
    //     }
    //   }
    //   if ($user->type_id != 2 && $user->type_id != 1) {
    //     $images = SiteSettings::where('id',5)->first();
    //     $videos = SiteSettings::where('id',6)->first();
    //     if($request->photo_number > $images->value) {
    //       $response['message'] = "You can't add more than 10 photos";
    //       $response['data'] = null;
    //       $response['code'] = -99;
    //       return response()->json($response, 200);
    //     }
    //     if($request->videos_number > $videos->value) {
    //       $response['message'] = "You can't add more than 2 videos";
    //       $response['data'] = null;
    //       $response['code'] = -3;
    //       return response()->json($response, 200);
    //     }
    //     $count = Ads::where('owner_id', auth('api')->user()->id)->where('status', 1)->get()->count();
    //     $d = SiteSettings::find(2);
    //     $d1 = SiteSettings::find(1);
    //     if ($count >= $d->value + $d1->value) {
    //       $data['message'] = 'Sorry, you have reached the maximum limit of free ads, you have to pay for this to be published';
    //       $data['data'] = null;
    //       $data['code'] = -4;
    //       return response()->json($data, 200);
    //     }
    //     try {
    //         $storedData = Ads::create([
    //           'title' => $request->title,
    //           'description' => $request->description,
    //           'owner_id' => auth('api')->user()->id,
    //           'town_id' => $request->town_id,
    //           'app_id' => $request->app_id,
    //           'thumbnail' => $request->thumbnail,
    //           'longitude' => $request->longitude,
    //           'latitude' => $request->latitude,
    //           'status' => 7,
    //           'validty_date' => (new Carbon(Carbon::now()))->addDays(30),
    //           'is_free' => 1,
    //           'detailed_location' => $request->detailed_location
    //         ]);
    //         PropertyAdsAttributes::create([
    //           'ad_id' => $storedData->id,
    //           'category_id' => $request->category_id,
    //           'option_id' => $request->option_id,
    //           'price' => $request->price,
    //           'price_option_id' => $request->price_option_id,
    //           'size' => $request->size,
    //           'size_unit_id' => $request->size_unit_id,
    //           'room_number_id' => $request->room_id,
    //           'garage_number_id' => $request->garage_id,
    //           'furniture_type_id' => $request->furniture_id,
    //           'bath_number_id' => $request->bath_id,
    //           'balcony_number_id' => $request->balcony_id,
    //           'period' => $request->period,
    //           'monthly_price' => $request->monthly_price,
    //           'terrace' => $request->terrace,
    //           'gym' => $request->gym,
    //           'security' => $request->Security
    //         ]);
    //       $response['message'] = "Success";
    //       $response['data'] = array("id" => $storedData->id);
    //       $response['code'] = 200;
    //       return response()->json($response, 200);

    //     } catch (\Exception $ex) {
    //       Log::error($ex->getMessage());
    //       $response['message'] = "An error occured please try again later";
    //       $response['data'] = null;
    //       $response['code'] = -99;
    //       return response()->json($response, 200);
    //     }
    //   }
    // }

    public function propertyAdsList(Request $request)
    {
      $data = Ads::where('status', 1)
        ->when(isset($request->app_id) && $request->app_id !== '', function ($query) use ($request) {
          $query->where('app_id', $request->app_id);
        })
        ->when(isset($request->town_id) && $request->town_id !== '', function ($query) use ($request) {
          $query->where('town_id', $request->town_id);
        })
        ->when(isset($request->owner_id) && $request->owner_id !== '', function ($query) use ($request) {
          $query->where('owner_id', $request->owner_id);
        })
        ->when(isset($request->status) && $request->status !== '', function ($query) use ($request) {
          $query->where('status', $request->status);
        })
        ->when(isset($request->validty_date) && $request->validty_date !== '', function ($query) use ($request) {
          $query->where('validty_date', $request->validty_date);
        })
        ->when(isset($request->option) && $request->option !== '', function ($query) use ($request) {
          $query->whereHas('property_attributes.option', function ($q) use ($request) {
            $q->where('id', $request->option);
          });
        })
        ->when(isset($request->category_id) && $request->category_id !== '', function ($query) use ($request) {
          $query->whereHas('property_attributes.category', function ($q) use ($request) {
            $q->where('id', $request->category_id);
          });
        })
        ->when(isset($request->size_unit_id) && $request->size_unit_id !== '', function ($query) use ($request) {
          $query->whereHas('property_attributes.size_unit', function ($q) use ($request) {
            $q->where('id', $request->size_unit_id);
          });
        })
        ->when(isset($request->price_option_id) && $request->price_option_id !== '', function ($query) use ($request) {
          $query->whereHas('property_attributes.price_option', function ($q) use ($request) {
            $q->where('id', $request->price_option_id);
          });
        })
        ->when(isset($request->furniture_id) && $request->furniture_id !== '', function ($query) use ($request) {
          $query->whereHas('property_attributes.furniture', function ($q) use ($request) {
            $q->where('id', $request->furniture_id);
          });
        })
        ->when(isset($request->rooms_id) && $request->rooms_id !== '', function ($query) use ($request) {
          $query->whereHas('property_attributes.rooms', function ($q) use ($request) {
            $q->where('id', $request->rooms_id);
          });
        })
        ->when(isset($request->balcony_id) && $request->balcony_id !== '', function ($query) use ($request) {
          $query->whereHas('property_attributes.balcony', function ($q) use ($request) {
            $q->where('id', $request->balcony_id);
          });
        })
        ->when(isset($request->bath_id) && $request->bath_id !== '', function ($query) use ($request) {
          $query->whereHas('property_attributes.bath', function ($q) use ($request) {
            $q->where('id', $request->bath_id);
          });
        })
        ->when(isset($request->terrace) && $request->terrace !== '', function ($query) use ($request) {
          $query->whereHas('property_attributes', function ($q) use ($request) {
            $q->where('terrace', $request->terrace);
          });
        })
        ->when(isset($request->from_price) && $request->from_price !== '' && isset($request->to_price) && $request->to_price !== '', function ($query) use ($request) {
          $query->whereHas('property_attributes', function ($q) use ($request) {
            $q->where('price', '>=', $request->from_price)->where('price', '<=', $request->to_price);
          });
        })
        ->when(isset($request->garage_id) && $request->garage_id !== '', function ($query) use ($request) {
          $query->whereHas('property_attributes.garage', function ($q) use ($request) {
            $q->where('id', $request->garage_id);
          });
        })
        ->when(isset($request->from_size) && $request->from_size !== '' && isset($request->to_size) && $request->to_size !== '', function ($query) use ($request) {
          $query->whereHas('property_attributes', function ($q) use ($request) {
            $q->where('size', '>=', $request->from_size)->where('size', '<=', $request->to_size);
          });
        })
        ->when(isset($request->mediaType) && $request->mediaType !== '', function ($query) use ($request) {
          $query->whereHas('media', function ($q) use ($request) {
            $q->where('type', $request->mediaType);
          });
        })
        ->skip($request->offset)
        ->take($request->limit)
        ->with(['town', 'town.country'])
        ->with(['owner.town','owner.town.country','owner.type'])
        ->with(['property_attributes.option', 'property_attributes.category', 'property_attributes.size_unit', 'property_attributes.price_option', 'property_attributes.furniture', 'property_attributes.rooms', 'property_attributes.balcony', 'property_attributes.bath', 'property_attributes.garage', 'media'])
        ->withCount(["savedAds" => function ($q) {
          $q->where('user_id', '=', auth('api')->user()->id);
        }])
        ->orderBy('ads.created_at', 'DESC')
        ->get();

      if (count($data) > 0) {
        $response['message'] = "Success";
        $response['data'] = [
          "adsList" => $data,
          "count" => $data->count()
        ];
        $response['code'] = 200;
        return response()->json($response, 200);

      } else {
        $response['message'] = "No data";
        $response['data'] = null;
        $response['code'] = -1;
        return response()->json($response, 200);
      }
    }
    public function propertyAdDetails(Request $request)
    {
      $ad1 = Ads::where('id', $request->ad_id)
        ->first();
      $data = Ads::updateOrCreate(['id' => $request->ad_id], ['total_views' => $ad1->total_views + 1]);
      $ad = Ads::where('id', $request->ad_id)
        ->with(['town', 'town.country'])
        ->with(['owner.town','owner.town.country','owner.type'])
        ->with(['property_attributes.option', 'property_attributes.category', 'property_attributes.size_unit', 'property_attributes.price_option', 'property_attributes.furniture', 'property_attributes.rooms', 'property_attributes.balcony', 'property_attributes.bath', 'property_attributes.garage', 'media'])
        ->withCount(["savedAds" => function ($q) {
          $q->where('user_id', '=', auth('api')->user()->id);
        }])
        ->first();
      if ($ad) {
        $response['message'] = "Success";
        $response['data'] = $ad;
        $response['code'] = 200;
        return response()->json($response, 200);
      } else {
        $response['message'] = "No data";
        $response['data'] = null;
        $response['code'] = -1;
        return response()->json($response, 200);
      }
    }
//new store
    public function propertyStoreAd(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'town_id' => 'exists:uk_towns,id',
        'option_id' => 'exists:property_options,id',
        'category_id' => 'exists:category,id',
        'size_unit_id' => 'exists:property_size_unites,id',
        "photo_number" => 'required',
        "videos_number" => 'required',
      ]);
      if ($validator->fails()) {
        $errors = $validator->errors();
        $data['message'] = $errors->first();
        $data['data'] = null;
        $data['code'] = -1;
        return response()->json($data, 200);
      }
      $user = User::where('id', auth('api')->user()->id)->first();
      if ($user->hasRoles(2,2)) {
        if($request->photo_number > 6) {
          $response['message'] = "You can't add more than 6 photos";
          $response['data'] = null;
          $response['code'] = -99;
          return response()->json($response, 200);
        }
        if($request->videos_number > 1) {
          $response['message'] = "You can't add more than 1 video";
          $response['data'] = null;
          $response['code'] = -3;
          return response()->json($response, 200);
        }
        if($request->is_free == 0){
          try {
            $storedData = Ads::create([
              'title' => $request->title,
              'description' => $request->description,
              'owner_id' => auth('api')->user()->id,
              'town_id' => $request->town_id,
              'app_id' => $request->app_id,
              'thumbnail' => $request->thumbnail,
              'longitude' => $request->longitude,
              'latitiude' => $request->latitude,
              'status' => 7,
              'validty_date' => (new Carbon(Carbon::now()))->addDays(30),
              'is_free' => 0,
              'detailed_location' => $request->detailed_location
            ]);
            PropertyAdsAttributes::create([
              'ad_id' => $storedData->id,
              'category_id' => $request->category_id,
              'option_id' => $request->option_id,
              'price' => $request->price,
              'price_option_id' => $request->price_option_id,
              'size' => $request->size,
              'size_unit_id' => $request->size_unit_id,
              'room_number_id' => $request->room_id,
              'garage_number_id' => $request->garage_id,
              'furniture_type_id' => $request->furniture_id,
              'bath_number_id' => $request->bath_id,
              'balcony_number_id' => $request->balcony_id,
              'period' => $request->period,
              'monthly_price' => $request->monthly_price,
              'terrace' => $request->terrace,
              'gym' => $request->gym,
              'security' => $request->Security
            ]);
          $data['message'] = 'you have to pay to activate your ad';
          $data['data'] = array("id" => $storedData->id);
          $data['code'] = -5;
          return response()->json($data, 200);
        } catch (\Exception $ex) {
          Log::error($ex->getMessage());
          $response['message'] = "An error occured please try again later";
          $response['data'] = null;
          $response['code'] = -99;
          return response()->json($response, 200);
          }
        }else{
          $count = Ads::where('owner_id', auth('api')->user()->id)->where('status', 1)->get()->count();
          $d = SiteSettings::find(1);
          if ($count >= $d->value) {
            try {
              $storedData = Ads::create([
                'title' => $request->title,
                'description' => $request->description,
                'owner_id' => auth('api')->user()->id,
                'town_id' => $request->town_id,
                'app_id' => $request->app_id,
                'thumbnail' => $request->thumbnail,
                'longitude' => $request->longitude,
                'latitiude' => $request->latitude,
                'status' => 7,
                'validty_date' => (new Carbon(Carbon::now()))->addDays(30),
                'is_free' => 0,
                'detailed_location' => $request->detailed_location
              ]);
              PropertyAdsAttributes::create([
                'ad_id' => $storedData->id,
                'category_id' => $request->category_id,
                'option_id' => $request->option_id,
                'price' => $request->price,
                'price_option_id' => $request->price_option_id,
                'size' => $request->size,
                'size_unit_id' => $request->size_unit_id,
                'room_number_id' => $request->room_id,
                'garage_number_id' => $request->garage_id,
                'furniture_type_id' => $request->furniture_id,
                'bath_number_id' => $request->bath_id,
                'balcony_number_id' => $request->balcony_id,
                'period' => $request->period,
                'monthly_price' => $request->monthly_price,
                'terrace' => $request->terrace,
                'gym' => $request->gym,
                'security' => $request->Security
              ]);
            $data['message'] = 'Sorry, you have reached the maximum limit of free ads, you have to pay to activate your ad';
            $data['data'] = array("id" => $storedData->id);
            $data['code'] = -5;
            return response()->json($data, 200);
          } catch (\Exception $ex) {
            Log::error($ex->getMessage());
            $response['message'] = "An error occured please try again later";
            $response['data'] = null;
            $response['code'] = -99;
            return response()->json($response, 200);
            }
          }
          try {
            $storedData = Ads::create([
              'title' => $request->title,
              'description' => $request->description,
              'owner_id' => auth('api')->user()->id,
              'town_id' => $request->town_id,
              'app_id' => $request->app_id,
              'thumbnail' => $request->thumbnail,
              'longitude' => $request->longitude,
              'latitiude' => $request->latitude,
              'status' => 7,
              'validty_date' => (new Carbon(Carbon::now()))->addDays(30),
              'is_free' => 1,
              'detailed_location' => $request->detailed_location
            ]);
            PropertyAdsAttributes::create([
              'ad_id' => $storedData->id,
              'category_id' => $request->category_id,
              'option_id' => $request->option_id,
              'price' => $request->price,
              'price_option_id' => $request->price_option_id,
              'size' => $request->size,
              'size_unit_id' => $request->size_unit_id,
              'room_number_id' => $request->room_id,
              'garage_number_id' => $request->garage_id,
              'furniture_type_id' => $request->furniture_id,
              'bath_number_id' => $request->bath_id,
              'balcony_number_id' => $request->balcony_id,
              'period' => $request->period,
              'monthly_price' => $request->monthly_price,
              'terrace' => $request->terrace,
              'gym' => $request->gym,
              'security' => $request->Security
            ]);
            $response['message'] = "Success";
            $response['data'] = array("id" => $storedData->id);
            $response['code'] = 200;
            return response()->json($response, 200);
          } catch (\Exception $ex) {
            Log::error($ex->getMessage());
            $response['message'] = "An error occured please try again later";
            $response['data'] = null;
            $response['code'] = -99;
            return response()->json($response, 200);
          }
        }
      }
      if (!$user->hasRoles(2,2) && !$user->hasRoles(1,2)) {
        if($request->photo_number > 10) {
          $response['message'] = "You can't add more than 10 photos";
          $response['data'] = null;
          $response['code'] = -99;
          return response()->json($response, 200);
        }
        if($request->videos_number > 2) {
          $response['message'] = "You can't add more than 2 videos";
          $response['data'] = null;
          $response['code'] = -3;
          return response()->json($response, 200);
        }
        if($request->is_free == 0){
          try {
            $storedData = Ads::create([
              'title' => $request->title,
              'description' => $request->description,
              'owner_id' => auth('api')->user()->id,
              'town_id' => $request->town_id,
              'app_id' => $request->app_id,
              'thumbnail' => $request->thumbnail,
              'longitude' => $request->longitude,
              'latitiude' => $request->latitude,
              'status' => 7,
              'validty_date' => (new Carbon(Carbon::now()))->addDays(30),
              'is_free' => 0,
              'detailed_location' => $request->detailed_location
            ]);
            PropertyAdsAttributes::create([
              'ad_id' => $storedData->id,
              'category_id' => $request->category_id,
              'option_id' => $request->option_id,
              'price' => $request->price,
              'price_option_id' => $request->price_option_id,
              'size' => $request->size,
              'size_unit_id' => $request->size_unit_id,
              'room_number_id' => $request->room_id,
              'garage_number_id' => $request->garage_id,
              'furniture_type_id' => $request->furniture_id,
              'bath_number_id' => $request->bath_id,
              'balcony_number_id' => $request->balcony_id,
              'period' => $request->period,
              'monthly_price' => $request->monthly_price,
              'terrace' => $request->terrace,
              'gym' => $request->gym,
              'security' => $request->Security
            ]);
          $data['message'] = 'you have to pay to activate your ad';
          $data['data'] = array("id" => $storedData->id);
          $data['code'] = -5;
          return response()->json($data, 200);
        } catch (\Exception $ex) {
          Log::error($ex->getMessage());
          $response['message'] = "An error occured please try again later";
          $response['data'] = null;
          $response['code'] = -99;
          return response()->json($response, 200);
          }
        }else{
          $count = Ads::where('owner_id', auth('api')->user()->id)->where('status', 1)->get()->count();
          $d = SiteSettings::find(2);
          $d1 = SiteSettings::find(1);
          if ($count >= $d->value + $d1->value) {
            try {
              $storedData = Ads::create([
                'title' => $request->title,
                'description' => $request->description,
                'owner_id' => auth('api')->user()->id,
                'town_id' => $request->town_id,
                'app_id' => $request->app_id,
                'thumbnail' => $request->thumbnail,
                'longitude' => $request->longitude,
                'latitiude' => $request->latitude,
                'status' => 7,
                'validty_date' => (new Carbon(Carbon::now()))->addDays(30),
                'is_free' => 0,
                'detailed_location' => $request->detailed_location
              ]);
              PropertyAdsAttributes::create([
                'ad_id' => $storedData->id,
                'category_id' => $request->category_id,
                'option_id' => $request->option_id,
                'price' => $request->price,
                'price_option_id' => $request->price_option_id,
                'size' => $request->size,
                'size_unit_id' => $request->size_unit_id,
                'room_number_id' => $request->room_id,
                'garage_number_id' => $request->garage_id,
                'furniture_type_id' => $request->furniture_id,
                'bath_number_id' => $request->bath_id,
                'balcony_number_id' => $request->balcony_id,
                'period' => $request->period,
                'monthly_price' => $request->monthly_price,
                'terrace' => $request->terrace,
                'gym' => $request->gym,
                'security' => $request->Security
              ]);
            $data['message'] = 'Sorry, you have reached the maximum limit of free ads, you have to pay for this to be published';
            $data['data'] = array("id" => $storedData->id);
            $data['code'] = -5;
            return response()->json($data, 200);
          } catch (\Exception $ex) {
            Log::error($ex->getMessage());
            $response['message'] = "An error occured please try again later";
            $response['data'] = null;
            $response['code'] = -99;
            return response()->json($response, 200);
            }
          }
          try {
            $storedData = Ads::create([
              'title' => $request->title,
              'description' => $request->description,
              'owner_id' => auth('api')->user()->id,
              'town_id' => $request->town_id,
              'app_id' => $request->app_id,
              'thumbnail' => $request->thumbnail,
              'longitude' => $request->longitude,
              'latitiude' => $request->latitude,
              'status' => 7,
              'validty_date' => (new Carbon(Carbon::now()))->addDays(30),
              'is_free' => 1,
              'detailed_location' => $request->detailed_location
            ]);
            PropertyAdsAttributes::create([
              'ad_id' => $storedData->id,
              'category_id' => $request->category_id,
              'option_id' => $request->option_id,
              'price' => $request->price,
              'price_option_id' => $request->price_option_id,
              'size' => $request->size,
              'size_unit_id' => $request->size_unit_id,
              'room_number_id' => $request->room_id,
              'garage_number_id' => $request->garage_id,
              'furniture_type_id' => $request->furniture_id,
              'bath_number_id' => $request->bath_id,
              'balcony_number_id' => $request->balcony_id,
              'period' => $request->period,
              'monthly_price' => $request->monthly_price,
              'terrace' => $request->terrace,
              'gym' => $request->gym,
              'security' => $request->Security
            ]);
            $response['message'] = "Success";
            $response['data'] = array("id" => $storedData->id);
            $response['code'] = 200;
            return response()->json($response, 200);
          } catch (\Exception $ex) {
            Log::error($ex->getMessage());
            $response['message'] = "An error occured please try again later";
            $response['data'] = null;
            $response['code'] = -99;
            return response()->json($response, 200);
          }
        }
      }
      if ($user->hasRoles(1,2)) {
        $response['message'] = "You can't add Ads, you are admin";
        $response['data'] = null;
        $response['code'] = -2;
        return response()->json($response, 200);
      }
    }
    public function getSavedAds(Request $request)
    {
      $data = SavedAds::
          when(isset($request->app_id) && $request->app_id !== '', function ($query) use ($request) {
            $query->whereHas('ads', function ($q) use ($request) {
              $q->where('app_id', $request->app_id);
            });
          })
        ->with('ads')

        ->with(['ads.town', 'ads.town.country'])
        ->with(['ads.owner.town','ads.owner.town.country'])
        ->with(['ads.property_attributes.option', 'ads.property_attributes.category', 'ads.property_attributes.size_unit', 'ads.property_attributes.price_option', 'ads.property_attributes.furniture', 'ads.property_attributes.rooms', 'ads.property_attributes.balcony', 'ads.property_attributes.bath', 'ads.property_attributes.garage', 'ads.media'])
        ->with('ads.application')
        ->where('user_id', auth('api')->user()->id)
        ->orderBy('created_at', 'DESC')
        ->skip($request->offset)
        ->take($request->limit)
        ->get();

      if (count($data) > 0) {
        $response['message'] = "Success";
        $response['data'] = [
          "savedAds" => $data,
          "count" => $data->count()
        ];
        $response['code'] = 200;
        return response()->json($response, 200);

      } else {
        $response['message'] = "No data";
        $response['data'] = null;
        $response['code'] = -1;
        return response()->json($response, 200);
      }
    }
    public function pauseAd(Request $request)
    {
      try {
        $ad1 = Ads::where('id', $request->ad_id)->where('owner_id', auth('api')->user()->id)->first();
        // dd($ad1);
        if ($ad1 == null) {
          $response['message'] = "Ad isn't Yours!";
          $response['data'] = null;
          $response['code'] = -2;
          return response()->json($response, 200);
        }
        if ($request->status == 2 && $ad1->status == 2) {
          $response['message'] = "Ad is already Paused/Deactivated";
          $response['data'] = null;
          $response['code'] = -3;
          return response()->json($response, 200);
        }
        if ($request->status == 1 && $ad1->status == 1) {
          $response['message'] = "Ad is already Activated";
          $response['data'] = null;
          $response['code'] = -3;
          return response()->json($response, 200);
        }
        if ($request->status == 2) {
          Ads::updateOrCreate(['id' => $request->ad_id], ['status' => $request->status, 'paused_at' => Carbon::now()]);
        }
        if ($request->status == 1) {
          $result = $ad1->created_at->diffInDays(Carbon::now(), false);
          Ads::updateOrCreate(['id' => $request->ad_id], ['status' => $request->status, 'validty_date' => (new Carbon($ad1->end_date))->addDays($result)]);
        }
        $response['message'] = "Success";
        $response['data'] = null;
        $response['code'] = 1;
        return response()->json($response, 200);
      } catch (\Exception $ex) {
        Log::error($ex->getMessage());
        $response['message'] = "An error occured please try again later";
        $response['data'] = null;
        $response['code'] = -99;
        return response()->json($response, 200);
      }
    }
    public function editAd(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'ad_id' => 'required|exists:ads,id',
        'title' => 'required|string',
        'description' => 'required|string',
      ]);
      if ($validator->fails()) {
        $errors = $validator->errors();
        $data['message'] = $errors->first();
        $data['data'] = null;
        $data['code'] = -1;
        return response()->json($data, 200);
      }
      try {
        $ad = Ads::find($request->ad_id);
        if ($ad->owner_id == auth('api')->user()->id) {
          $ad->update([
            'title' => $request->title,
            'description' => $request->description
          ]);
          $response['message'] = "Success";
          $response['data'] = null;
          $response['code'] = 200;
          return response()->json($response, 200);
        } else {
          $response['message'] = "You are not the owner of the ad";
          $response['data'] = null;
          $response['code'] = -2;
          return response()->json($response, 200);
        }

      } catch (\Exception $ex) {
        Log::error($ex->getMessage());
        $response['message'] = "An error occured please try again later";
        $response['data'] = null;
        $response['code'] = -99;
        return response()->json($response, 200);
      }
    }
    public function destroyPropertyAd(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'ad_id' => 'required|exists:ads,id',
      ]);
      if ($validator->fails()) {
        $errors = $validator->errors();
        $data['message'] = $errors->first();
        $data['data'] = null;
        $data['code'] = -1;
        return response()->json($data, 200);
      }
      $data = Ads::where('id', $request->ad_id)->where('owner_id', auth('api')->user()->id)->first();
      if ($data == null) {
        $response['message'] = "You can't delete ad not belongs to you";
        $response['data'] = null;
        $response['code'] = -1;
        return response()->json($response, 200);
      }
      try {
        Media::where('ad_id', $request->ad_id)->delete();
        PropertyAdsAttributes::where('ad_id', $request->ad_id)->delete();
        SavedAds::where('ad_id', $request->ad_id)->delete();
        Ads::find($request->ad_id)->delete();

        $response['message'] = "Success";
        $response['data'] = null;
        $response['code'] = 200;
        return response()->json($response, 200);
      } catch (\Exception $ex) {
        Log::error($ex->getMessage());
        $response['message'] = "An error occured please try again later";
        $response['data'] = null;
        $response['code'] = -99;
        return response()->json($response, 200);
      }
    }
//-----------------------------------------Cars------------------------------------------------------
public function carsAdsList(Request $request)
{
  $data = Ads::where('status', 1)
    ->when(isset($request->app_id) && $request->app_id !== '', function ($query) use ($request) {
      $query->where('app_id', $request->app_id);
    })
    ->when(isset($request->town_id) && $request->town_id !== '', function ($query) use ($request) {
      $query->where('town_id', $request->town_id);
    })
    ->when(isset($request->owner_id) && $request->owner_id !== '', function ($query) use ($request) {
      $query->where('owner_id', $request->owner_id);
    })
    ->when(isset($request->status) && $request->status !== '', function ($query) use ($request) {
      $query->where('status', $request->status);
    })
    ->when(isset($request->validty_date) && $request->validty_date !== '', function ($query) use ($request) {
      $query->where('validty_date', $request->validty_date);
    })
    ->when(isset($request->option) && $request->option !== '', function ($query) use ($request) {
      $query->whereHas('cars_attributes.option', function ($q) use ($request) {
        $q->where('id', $request->option);
      });
    })
    ->when(isset($request->model_id) && $request->model_id !== '', function ($query) use ($request) {
      $query->whereHas('cars_attributes.category', function ($q) use ($request) {
        $q->where('id', $request->model_id);
      });
    })
    ->when(isset($request->transmission_id) && $request->transmission_id !== '', function ($query) use ($request) {
      $query->whereHas('cars_attributes.transmission', function ($q) use ($request) {
        $q->where('id', $request->transmission_id);
      });
    })
    ->when(isset($request->fuel_id) && $request->fuel_id !== '', function ($query) use ($request) {
      $query->whereHas('cars_attributes.fuel', function ($q) use ($request) {
        $q->where('id', $request->fuel_id);
      });
    })
    ->when(isset($request->condition_id) && $request->condition_id !== '', function ($query) use ($request) {
      $query->whereHas('cars_attributes.condition', function ($q) use ($request) {
        $q->where('id', $request->condition_id);
      });
    })
    ->when(isset($request->color_id) && $request->color_id !== '', function ($query) use ($request) {
      $query->whereHas('cars_attributes.color', function ($q) use ($request) {
        $q->where('id', $request->color_id);
      });
    })
    ->when(isset($request->body_style_id) && $request->body_style_id !== '', function ($query) use ($request) {
      $query->whereHas('cars_attributes.bodyStyle', function ($q) use ($request) {
        $q->where('id', $request->body_style_id);
      });
    })
    ->when(isset($request->n_cylinders) && $request->n_cylinders !== '', function ($query) use ($request) {
      $query->whereHas('cars_attributes', function ($q) use ($request) {
        $q->where('n_cylinders', $request->n_cylinders);
      });
    })
    ->when(isset($request->n_doors) && $request->n_doors !== '', function ($query) use ($request) {
      $query->whereHas('cars_attributes', function ($q) use ($request) {
        $q->where('n_doors', $request->n_doors);
      });
    })
    ->when(isset($request->seats) && $request->seats !== '', function ($query) use ($request) {
      $query->whereHas('cars_attributes', function ($q) use ($request) {
        $q->where('seats', $request->seats);
      });
    })
    ->when(isset($request->finance) && $request->finance !== '', function ($query) use ($request) {
      $query->whereHas('cars_attributes', function ($q) use ($request) {
        $q->where('finance', $request->finance);
      });
    })
    ->when(isset($request->warranty) && $request->warranty !== '', function ($query) use ($request) {
      $query->whereHas('cars_attributes', function ($q) use ($request) {
        $q->where('warranty', $request->warranty);
      });
    })
    ->when(isset($request->insurance) && $request->insurance !== '', function ($query) use ($request) {
      $query->whereHas('cars_attributes', function ($q) use ($request) {
        $q->where('insurance', $request->insurance);
      });
    })
    ->when(isset($request->from_milage) && $request->from_milage !== '' && isset($request->to_milage) && $request->to_milage !== '', function ($query) use ($request) {
      $query->whereHas('cars_attributes', function ($q) use ($request) {
        $q->where('milage', '>=', $request->from_milage)->where('milage', '<=', $request->to_milage);
      });
    })
    ->when(isset($request->from_price) && $request->from_price !== '' && isset($request->to_price) && $request->to_price !== '', function ($query) use ($request) {
      $query->whereHas('cars_attributes', function ($q) use ($request) {
        $q->where('price', '>=', $request->from_price)->where('price', '<=', $request->to_price);
      });
    })
    ->when(isset($request->from_year) && $request->from_year !== '' && isset($request->to_year) && $request->to_year !== '', function ($query) use ($request) {
      $query->whereHas('cars_attributes', function ($q) use ($request) {
        $q->where('year', '>=', $request->from_year)->where('year', '<=', $request->to_year);
      });
    })
    ->when(isset($request->mediaType) && $request->mediaType !== '', function ($query) use ($request) {
      $query->whereHas('media', function ($q) use ($request) {
        $q->where('type', $request->mediaType);
      });
    })
    ->skip($request->offset)
    ->take($request->limit)
    ->with(['town', 'town.country'])
    ->with(['owner.town','owner.town.country','owner.type'])
    ->with(['cars_attributes.option', 'cars_attributes.model', 'cars_attributes.transmission', 'cars_attributes.fuel', 'cars_attributes.condition', 'cars_attributes.color', 'cars_attributes.bodyStyle', 'media'])
    ->withCount(["savedAds" => function ($q) {
      $q->where('user_id', '=', auth('api')->user()->id);
    }])
    ->orderBy('ads.created_at', 'DESC')
    ->get();

  if (count($data) > 0) {
    $response['message'] = "Success";
    $response['data'] = [
      "adsList" => $data,
      "count" => $data->count()
    ];
    $response['code'] = 200;
    return response()->json($response, 200);

  } else {
    $response['message'] = "No data";
    $response['data'] = null;
    $response['code'] = -1;
    return response()->json($response, 200);
  }
}
public function carsAdDetails(Request $request)
{
  $ad1 = Ads::where('id', $request->ad_id)
    ->first();
  $data = Ads::updateOrCreate(['id' => $request->ad_id], ['total_views' => $ad1->total_views + 1]);
  $ad = Ads::where('id', $request->ad_id)
    ->with(['town', 'town.country'])
    ->with(['owner.town','owner.town.country','owner.type'])
    ->with(['cars_attributes.option', 'cars_attributes.model', 'cars_attributes.transmission', 'cars_attributes.fuel', 'cars_attributes.condition', 'cars_attributes.color', 'cars_attributes.bodyStyle', 'media'])
    ->withCount(["savedAds" => function ($q) {
      $q->where('user_id', '=', auth('api')->user()->id);
    }])
    ->first();
  if ($ad) {
    $response['message'] = "Success";
    $response['data'] = $ad;
    $response['code'] = 200;
    return response()->json($response, 200);
  } else {
    $response['message'] = "No data";
    $response['data'] = null;
    $response['code'] = -1;
    return response()->json($response, 200);
  }
}
public function getSavedAdsCars(Request $request)
{
  $data = SavedAds::
      when(isset($request->app_id) && $request->app_id !== '', function ($query) use ($request) {
        $query->whereHas('ads', function ($q) use ($request) {
          $q->where('app_id', $request->app_id);
        });
      })
    ->with('ads')

    ->with(['ads.town', 'ads.town.country'])
    ->with(['ads.owner.town','ads.owner.town.country'])
    ->with(['ads.cars_attributes.option', 'ads.cars_attributes.model', 'ads.cars_attributes.transmission', 'ads.cars_attributes.fuel', 'ads.cars_attributes.condition', 'ads.cars_attributes.color', 'ads.cars_attributes.bodyStyle', 'ads.media'])
    ->with('ads.application')
    ->where('user_id', auth('api')->user()->id)
    ->orderBy('created_at', 'DESC')
    ->skip($request->offset)
    ->take($request->limit)
    ->get();

  if (count($data) > 0) {
    $response['message'] = "Success";
    $response['data'] = [
      "savedAds" => $data,
      "count" => $data->count()
    ];
    $response['code'] = 200;
    return response()->json($response, 200);

  } else {
    $response['message'] = "No data";
    $response['data'] = null;
    $response['code'] = -1;
    return response()->json($response, 200);
  }
}

}
