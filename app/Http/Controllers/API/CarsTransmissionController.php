<?php

namespace App\Http\Controllers\API;

use App\CarsTransmission;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CarsTransmissionController extends Controller
{
    //
    public function list() {
        $data = CarsTransmission::all();
        if (count($data) > 0) {
          $response['message'] = "Success";
          $response['data'] = $data;
          $response['code'] = 200;
          return response()->json($response, 200);
  
        } else {
          $response['message'] = "No data";
          $response['data'] = null;
          $response['code'] = -1;
          return response()->json($response, 200);
        }
      }
}
