<?php

namespace App\Http\Controllers\API;

use App\Towns;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TownsController extends Controller
{
    //
    public function list(Request $request) {
        $validator = Validator::make($request->all(), [
          'country_id' => 'required|exists:country,id',
        ]);
        if ($validator->fails()) {
          $errors = $validator->errors();
          $data['message'] = $errors->first();
          $data['data'] = null;
          $data['code'] = -1;
          return response()->json($data, 200);
        }
        $data = Towns::where('country_id',$request->country_id)
          ->with('country')
          ->get();
        if (count($data) > 0) {
          $response['message'] = "Success";
          $response['data'] = $data;
          $response['code'] = 200;
          return response()->json($response, 200);
  
        } else {
          $response['message'] = "No data";
          $response['data'] = null;
          $response['code'] = -1;
          return response()->json($response, 200);
        }
      }
}
