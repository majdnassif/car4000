<?php

namespace App\Http\Controllers\API;

use App\OptionPriceOptions;
use App\PriceOption;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class OptionPriceOptionsController extends Controller
{
    //
    public function getPriceOptions(Request $request)
    {
      $data = PriceOption::when(isset($request->option_id) && $request->option_id !== '', function ($query) use ($request) {
          $query->whereHas('option', function ($q) use ($request) {
            $q->where('option_id', $request->option_id);
          });
        })
        ->get();
  
      if (count($data) > 0) {
        $response['message'] = "Success";
        $response['data'] = $data;
        $response['code'] = 200;
        return response()->json($response, 200);
  
      } else {
        $response['message'] = "No data";
        $response['data'] = null;
        $response['code'] = -1;
        return response()->json($response, 200);
      }
    }
}
