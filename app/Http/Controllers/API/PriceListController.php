<?php

namespace App\Http\Controllers\API;

use App\AdPriceList;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PriceListController extends Controller
{
    public function list(Request $request) {
        $validator = Validator::make($request->all(), [
            'app_id' => 'required|exists:application,id',
          ]);
      $data = AdPriceList::where('app_id',$request->app_id)->get();
      if (count($data) > 0) {
        $response['message'] = "Success";
        $response['data'] = $data;
        $response['code'] = 200;
        return response()->json($response, 200);

      } else {
        $response['message'] = "No data";
        $response['data'] = null;
        $response['code'] = -1;
        return response()->json($response, 200);
      }
    }
}
