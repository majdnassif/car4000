<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Mail\ResetPasswordMail;
use App\Mail\SignUpMail;
use App\Mail\UpdateEmailMail;
use App\User;
use App\UserInterest;
use App\Ads;
use App\UserNotification;
use App\VerificationCode;
use App\UserTypes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Twilio\Rest\Client;
use Carbon\Carbon;


class UsersController extends Controller
{
  public function login(Request $request)
  {
    if (is_numeric($request->email_or_phone)) {
      $credentials = [
        'phone_number' => $request->email_or_phone,
        'password' => $request->password,
        'app_id' => $request->app_id,
        'status' => 1
      ];
      $att = Auth::attempt($credentials);
      if ($att) {
        $token = Str::random(80);
        $token = hash('sha256', $token);
        $user = User::where('phone_number', $request->email_or_phone)->first();
        if ($user->is_verify == 0) {
          $response['message'] = "Please verify your account first";
          $response['data'] = null;
          $response['code'] = -3;
          return response()->json($response, 200);
        } else {
          $user->update([
            'api_token' => $token,
          ]);
          $user = User::with('town', 'type', 'application')->where('phone_number', $request->email_or_phone)->where('app_id',$request->app_id)->first()->makeVisible('api_token');
          $response['message'] = 'Success';
          $response['data'] = $user;
          $response['code'] = 200;
          return response()->json($response, 200);
        }

      } else {
        $response['message'] = "Please check your information and try again later";
        $response['data'] = null;
        $response['code'] = -1;
        return response()->json($response, 200);
      }
    } else {
      $credentials = [
        'email' => $request->email_or_phone,
        'password' => $request->password,
        'app_id' => $request->app_id,
        'status' => 1
      ];
      $att = Auth::attempt($credentials);
      if ($att) {
        $token = Str::random(80);
        $token = hash('sha256', $token);
        $user = User::where('email', $request->email_or_phone)->first();
        if ($user->is_verify == 0) {
          $response['message'] = "Please verify your account first";
          $response['data'] = null;
          $response['code'] = -3;
          return response()->json($response, 200);
        } else {
          $user->update([
            'api_token' => $token,
          ]);
          $user = User::with('town', 'type', 'application')->where('email', $request->email_or_phone)->where('app_id',$request->app_id)->first()->makeVisible('api_token');
          $response['message'] = 'Success';
          $response['data'] = $user;
          $response['code'] = 200;
          return response()->json($response, 200);
        }
      } else {
        $response['message'] = "Please check your information and try again later";
        $response['data'] = null;
        $response['code'] = -1;
        return response()->json($response, 200);
      }
    }

  }

  public function signUp(Request $request)
  {
    if (is_numeric($request->email_or_phone)) {
      $user = User::where('phone_number',$request->email_or_phone)->where('app_id',$request->app_id)->where('status',1)->first();
      if($user){
        $data['message'] = 'phone is already exists';
        $data['data'] = null;
        $data['code'] = -1;
        return response()->json($data, 200);
      }  
      try {
        $pass = $request['password'];
        $code = rand(pow(10, 5), pow(10, 6) - 1);
        $request['password'] = Hash::make($request->password);
        $request['is_verify'] = 2;
        $request['phone_number'] = $request->email_or_phone;
        $id = User::create($request->all())->id;
        VerificationCode::create([
          'user_id' => $id,
          'code_type' => 1,
          'code' => $code
        ]);

        $sid = "AC8a36c593b23c5c00d77fc07b57c3fe46";
        $token = "69876817aa091159385b161e0426e1f6";
        $twilio = new Client($sid, $token);
        $message = $twilio->messages
          ->create($request->email_or_phone, // to
            [
              "body" => "Thank you for register in our app use the code " . $code . " to verify your account.",
              "from" => "+14793416298"
            ]
          );

        $response['message'] = "Success";
        $response['data'] = null;
        $response['code'] = 1;
        return response()->json($response, 200);
      } catch (\Exception $ex) {
        Log::error($ex->getMessage());
        $response['message'] = "An error occured please try again later";
        $response['data'] = null;
        $response['code'] = -99;
        return response()->json($response, 200);
      }
    } else {
      $user = User::where('email',$request->email_or_phone)->where('app_id',$request->app_id)->where('status',1)->first();
      if($user){
        $data['message'] = 'email is already exists';
        $data['data'] = null;
        $data['code'] = -1;
        return response()->json($data, 200);
      }  
      try {
        $pass = $request['password'];
        $code = rand(pow(10, 5), pow(10, 6) - 1);
        $request['password'] = Hash::make($request->password);
        $request['is_verify'] = 2;
        $request['email'] = $request->email_or_phone;
        $id = User::create($request->all())->id;
        VerificationCode::create([
          'user_id' => $id,
          'code_type' => 1,
          'code' => $code
        ]);
        $details = [
          'title' => "Welcome to our app",
          'code' => $code
        ];
        Mail::to($request->email)->send(new SignUpMail ($details));
        $response['message'] = "Success";
        $response['data'] = null;
        $response['code'] = 1;
        return response()->json($response, 200);
      } catch (\Exception $ex) {
        Log::error($ex->getMessage());
        $response['message'] = "An error occured please try again later";
        $response['data'] = null;
        $response['code'] = -99;
        return response()->json($response, 200);
      }
    }

  }

  public function verifyUser(Request $request)
  {
    $code = VerificationCode::where('code', $request->code)->where('code_type', 1)->where('active', 1)->first();
    if (!$code) {
      $data['message'] = 'Code is invalid!';
      $data['data'] = null;
      $data['code'] = -1;
      return response()->json($data, 200);
    }
    try {
      if (is_numeric($request->email_or_phone)) {
        $user = User::where('phone_number', $request->email_or_phone)->where('is_verify', 2)->where('status', 2)->where('app_id', $request->app_id)->first();
        if ($user) {
          $token = Str::random(80);
          $token = hash('sha256', $token);
          $user->update([
            'is_verify' => 1,
            'api_token' => $token,
            'status' => 1
          ]);
          $code->update([
            'active' => 2
          ]);
          $user = User::where('phone_number', $request->email_or_phone)->first()->makeVisible('api_token');
          $response['message'] = 'Success';
          $response['data'] = $user;
          $response['code'] = 200;
          return response()->json($response, 200);
        } else {
          $data['message'] = 'Your information is not correct please try again';
          $data['data'] = null;
          $data['code'] = -3;
          return response()->json($data, 200);
        }
      } else {
        $user = User::where('email', $request->email_or_phone)->where('is_verify', 2)->where('status', 2)->where('app_id', $request->app_id)->first();
        if ($user) {
          $token = Str::random(80);
          $token = hash('sha256', $token);
          $user->update([
            'is_verify' => 1,
            'api_token' => $token,
            'status' => 1
          ]);
          $code->update([
            'active' => 2
          ]);
          $user = User::where('email', $request->email_or_phone)->first()->makeVisible('api_token');
          $response['message'] = 'Success';
          $response['data'] = $user;
          $response['code'] = 200;
          return response()->json($response, 200);
        } else {
          $data['message'] = 'Your information is not correct please try again';
          $data['data'] = null;
          $data['code'] = -3;
          return response()->json($data, 200);
        }
      }

    } catch (\Exception $ex) {
      Log::error($ex->getMessage());
      $response['message'] = "An error occured please try again later";
      $response['data'] = null;
      $response['code'] = -99;
      return response()->json($response, 200);
    }
  }

  public function forgetPassword(Request $request)
  {
    if (is_numeric($request->email_or_phone)) {
      $data = User::where('phone_number', $request->email_or_phone)->where('status',1)->where('app_id',$request->app_id)->first();
      if ($data == null) {
        $data['message'] = 'Email is not exist';
        $data['data'] = null;
        $data['code'] = -3;
        return response()->json($data, 200);
      }
      try {
        $reset_code = rand(pow(10, 5), pow(10, 6) - 1);
        $user = User::where('phone_number', $request->email_or_phone)->first();
        VerificationCode::create([
          'user_id' => $user->id,
          'code_type' => 2,
          'code' => $reset_code
        ]);
        $sid = "AC8a36c593b23c5c00d77fc07b57c3fe46";
        $token = "69876817aa091159385b161e0426e1f6";
        $twilio = new Client($sid, $token);
        $message = $twilio->messages
          ->create($request->email_or_phone, // to
            [
              "body" => "Kindly use the code " . $reset_code . " to reset your password.",
              "from" => "+14793416298"
            ]
          );
        $response['message'] = "Success";
        $response['data'] = null;
        $response['code'] = 200;
        return response()->json($response, 200);
      } catch (\Exception $ex) {
        Log::error($ex->getMessage());
        $response['message'] = "An error occured, please try again later";
        $response['data'] = null;
        $response['code'] = -99;
        return response()->json($response, 200);
      }

    } else {
      $data = User::where('email', $request->email_or_phone)->where('status',1)->where('app_id',$request->app_id)->first();
      if ($data == null) {
        $data['message'] = 'Email is not exist';
        $data['data'] = null;
        $data['code'] = -3;
        return response()->json($data, 200);
      }
      try {
        $reset_code = rand(pow(10, 5), pow(10, 6) - 1);
        $user = User::where('email', $request->email_or_phone)->first();
        VerificationCode::create([
          'user_id' => $user->id,
          'code_type' => 2,
          'code' => $reset_code
        ]);
        $details = [
          'title' => "Reset Password Request",
          'code' => $reset_code,
        ];
        Mail::to($request->email_or_phone)->send(new ResetPasswordMail($details));
        $response['message'] = "Success";
        $response['data'] = null;
        $response['code'] = 200;
        return response()->json($response, 200);
      } catch (\Exception $ex) {
        Log::error($ex->getMessage());
        $response['message'] = "An error occured, please try again later";
        $response['data'] = null;
        $response['code'] = -99;
        return response()->json($response, 200);
      }
    }

  }

  public function resetPassword(Request $request)
  {
    $code = VerificationCode::where('code', $request->code)->where('code_type', 2)->where('active', 1)->first();
    if (!$code) {
      $data['message'] = 'Code is invalid!';
      $data['data'] = null;
      $data['code'] = -1;
      return response()->json($data, 200);
    }
    $validator = Validator::make($request->all(), [
      'password' => 'required|string|confirmed',
    ]);

    if ($validator->fails()) {
      $errors = $validator->errors();
      $data['message'] = $errors->first();
      $data['data'] = null;
      $data['code'] = -2;
      return response()->json($data, 200);
    }
    try {
      $user = VerificationCode::where('code', $request->code)->first();
      $user1 = User::where('id', $user->user_id)->first();
      $password = Hash::make($request->password);
      $user1->update([
        'password' => $password
      ]);
      $code->update([
        'active' => 2
      ]);
      $response['message'] = "Success";
      $response['data'] = null;
      $response['code'] = 200;
      return response()->json($response, 200);
    } catch (\Exception $ex) {
      Log::error($ex->getMessage());
      $response['message'] = "An error occured please try again later";
      $response['data'] = null;
      $response['code'] = -99;
      return response()->json($response, 200);
    }
  }

  public function resendVerificationCode(Request $request)
  {
    //type is 1: signup, 2: forgetpassword
    //typeSend is 1: email , 2: sms
    if ($request->type == 1 && $request->typeSend == 1) {
      try {
        $user = User::where('email', $request->email_phone)->where('status',1)->where('app_id',$request->app_id)->first();
        if(!$user) {
          $response['message'] = "User not found";
          $response['data'] = null;
          $response['code'] = -2;
          return response()->json($response, 200);
        }
        $code = VerificationCode::where('user_id',$user->id)->where('code_type',1)->where('active',1)->where('resend_times','<',2)->first();
        if($code){
          $details = [
            'title' => "Welcome to our app",
            'code' => $code->code
          ];
          $resend_times = $code->resend_times;
          $code->update([
            'resend_times' => $resend_times+1,
            'updated_at' => Carbon::now()
          ]);
          Mail::to($request->email_phone)->send(new SignUpMail ($details));
          $response['message'] = "Success";
          $response['data'] = null;
          $response['code'] = 201;
          return response()->json($response, 200);
        }
        $code = VerificationCode::where('user_id',$user->id)->where('code_type',1)->where('active',1)->where('resend_times',2)->first();
        if($code){
          $details = [
            'title' => "Welcome to our app",
            'code' => $code->code
          ];
          $resend_times = $code->resend_times;
          $code->update([
            'resend_times' => $resend_times+1,
            'active' => 2,
            'updated_at' => Carbon::now()
          ]);
          Mail::to($request->email_phone)->send(new SignUpMail ($details));
          $response['message'] = "Success";
          $response['data'] = null;
          $response['code'] = 201;
          return response()->json($response, 200);
        }
        $code = VerificationCode::where('user_id',$user->id)->where('code_type',1)->where('active',2)->where('resend_times',3)
        ->orderBy('id','desc')->first();
        // dd((Carbon::parse($code->updated_at)->diffInSeconds(Carbon::now(), false)));
        if($code){
          if((Carbon::parse($code->updated_at)->diffInSeconds(Carbon::now(), false)) < 900){
            $response['message'] = "You have to wait 15 minutes and then try again";
            $response['data'] = null;
            $response['code'] = -3;
            return response()->json($response, 200);
          }else{
            // dd($request->email_phone);
            $new_code = rand(pow(10, 5), pow(10, 6) - 1);
            VerificationCode::create([
              'user_id' => $user->id,
              'code_type' => 1,
              'code' => $new_code
            ]);
            $details = [
              'title' => "Reset Password Request",
              'code' => $new_code,
            ];
            Mail::to($request->email_phone)->send(new SignUpMail ($details));
            $response['message'] = "Success";
            $response['data'] = null;
            $response['code'] = 201;
            return response()->json($response, 200);
          }
        }
      } catch (\Exception $ex) {
        Log::error($ex->getMessage());
        $response['message'] = "An error occured please try again later";
        $response['data'] = null;
        $response['code'] = -99;
        return response()->json($response, 200);
      }
    } elseif ($request->type == 1 && $request->typeSend == 2) {
      try {
        $user = User::where('phone_number', $request->email_phone)->where('status',1)->where('app_id',$request->app_id)->first();
        if(!$user) {
          $response['message'] = "User not found";
          $response['data'] = null;
          $response['code'] = -2;
          return response()->json($response, 200);
        }
        $code = VerificationCode::where('user_id',$user->id)->where('code_type',1)->where('active',1)->where('resend_times','<',2)->first();
        if($code){
          $resend_times = $code->resend_times;
          $code->update([
            'resend_times' => $resend_times+1,
            'updated_at' => Carbon::now()
          ]);
          $sid = "AC8a36c593b23c5c00d77fc07b57c3fe46";
          $token = "69876817aa091159385b161e0426e1f6";
          $twilio = new Client($sid, $token);
          $message = $twilio->messages
            ->create($request->email_phone, // to
              [
                "body" => "Kindly use the code " . $code . " to reset your password.",
                "from" => "+14793416298"
              ]
          );
          $response['message'] = "Success";
          $response['data'] = null;
          $response['code'] = 201;
          return response()->json($response, 200);
        }
        $code = VerificationCode::where('user_id',$user->id)->where('code_type',1)->where('active',1)->where('resend_times',2)->first();
        if($code){
          $resend_times = $code->resend_times;
          $code->update([
            'resend_times' => $resend_times+1,
            'active' => 2,
            'updated_at' => Carbon::now()
          ]);
          $sid = "AC8a36c593b23c5c00d77fc07b57c3fe46";
          $token = "69876817aa091159385b161e0426e1f6";
          $twilio = new Client($sid, $token);
          $message = $twilio->messages
            ->create($request->email_phone, // to
              [
                "body" => "Kindly use the code " . $code . " to reset your password.",
                "from" => "+14793416298"
              ]
          );
          $response['message'] = "Success";
          $response['data'] = null;
          $response['code'] = 201;
          return response()->json($response, 200);
        }
        $code = VerificationCode::where('user_id',$user->id)->where('code_type',1)->where('active',2)->where('resend_times',3)
        ->orderBy('id','desc')->first();
        // dd((Carbon::parse($code->updated_at)->diffInSeconds(Carbon::now(), false)));
        if($code){
          if((Carbon::parse($code->updated_at)->diffInSeconds(Carbon::now(), false)) < 900){
            $response['message'] = "You have to wait 15 minutes and then try again";
            $response['data'] = null;
            $response['code'] = -3;
            return response()->json($response, 200);
          }else{
            // dd($request->email_phone);
            $new_code = rand(pow(10, 5), pow(10, 6) - 1);
            VerificationCode::create([
              'user_id' => $user->id,
              'code_type' => 1,
              'code' => $new_code
            ]);
            $sid = "AC8a36c593b23c5c00d77fc07b57c3fe46";
            $token = "69876817aa091159385b161e0426e1f6";
            $twilio = new Client($sid, $token);
            $message = $twilio->messages
              ->create($request->email_phone, // to
                [
                  "body" => "Kindly use the code " . $new_code . " to reset your password.",
                  "from" => "+14793416298"
                ]
            );
            $response['message'] = "Success";
            $response['data'] = null;
            $response['code'] = 201;
            return response()->json($response, 200);
          }
        }
      } catch (\Exception $ex) {
        Log::error($ex->getMessage());
        $response['message'] = "An error occured please try again later";
        $response['data'] = null;
        $response['code'] = -99;
        return response()->json($response, 200);
      }
    } elseif ($request->type == 2 && $request->typeSend == 1) {
      try {
        $user = User::where('email', $request->email_phone)->where('status',1)->where('app_id',$request->app_id)->first();
        if(!$user) {
          $response['message'] = "User not found";
          $response['data'] = null;
          $response['code'] = -2;
          return response()->json($response, 200);
        }
        $code = VerificationCode::where('user_id',$user->id)->where('code_type',2)->where('active',1)->where('resend_times','<',2)->first();
        if($code){
          $details = [
            'title' => "Welcome to our app",
            'code' => $code->code
          ];
          $resend_times = $code->resend_times;
          $code->update([
            'resend_times' => $resend_times+1,
            'updated_at' => Carbon::now()
          ]);
          Mail::to($request->email_phone)->send(new ResetPasswordMail ($details));
          $response['message'] = "Success";
          $response['data'] = null;
          $response['code'] = 201;
          return response()->json($response, 200);
        }
        $code = VerificationCode::where('user_id',$user->id)->where('code_type',2)->where('active',1)->where('resend_times',2)->first();
        if($code){
          $details = [
            'title' => "Welcome to our app",
            'code' => $code->code
          ];
          $resend_times = $code->resend_times;
          $code->update([
            'resend_times' => $resend_times+1,
            'active' => 2,
            'updated_at' => Carbon::now()
          ]);
          Mail::to($request->email_phone)->send(new ResetPasswordMail ($details));
          $response['message'] = "Success";
          $response['data'] = null;
          $response['code'] = 201;
          return response()->json($response, 200);
        }
        $code = VerificationCode::where('user_id',$user->id)->where('code_type',2)->where('active',2)->where('resend_times',3)
        ->orderBy('id','desc')->first();
        // dd((Carbon::parse($code->updated_at)->diffInSeconds(Carbon::now(), false)));
        if($code){
          if((Carbon::parse($code->updated_at)->diffInSeconds(Carbon::now(), false)) < 900){
            $response['message'] = "You have to wait 15 minutes and then try again";
            $response['data'] = null;
            $response['code'] = -3;
            return response()->json($response, 200);
          }else{
            // dd($request->email_phone);
            $new_code = rand(pow(10, 5), pow(10, 6) - 1);
            VerificationCode::create([
              'user_id' => $user->id,
              'code_type' => 2,
              'code' => $new_code
            ]);
            $details = [
              'title' => "Reset Password Request",
              'code' => $new_code,
            ];
            Mail::to($request->email_phone)->send(new ResetPasswordMail ($details));
            $response['message'] = "Success";
            $response['data'] = null;
            $response['code'] = 201;
            return response()->json($response, 200);
          }
        }
      } catch (\Exception $ex) {
        Log::error($ex->getMessage());
        $response['message'] = "An error occured please try again later";
        $response['data'] = null;
        $response['code'] = -99;
        return response()->json($response, 200);
      }
    } elseif ($request->type == 2 && $request->typeSend == 2) {
      try {
        $user = User::where('phone_number', $request->email_phone)->where('status',1)->where('app_id',$request->app_id)->first();
        if(!$user) {
          $response['message'] = "User not found";
          $response['data'] = null;
          $response['code'] = -2;
          return response()->json($response, 200);
        }
        $code = VerificationCode::where('user_id',$user->id)->where('code_type',2)->where('active',1)->where('resend_times','<',2)->first();
        if($code){
          $resend_times = $code->resend_times;
          $code->update([
            'resend_times' => $resend_times+1,
            'updated_at' => Carbon::now()
          ]);
          $sid = "AC8a36c593b23c5c00d77fc07b57c3fe46";
          $token = "69876817aa091159385b161e0426e1f6";
          $twilio = new Client($sid, $token);
          $message = $twilio->messages
            ->create($request->email_phone, // to
              [
                "body" => "Kindly use the code " . $code . " to reset your password.",
                "from" => "+14793416298"
              ]
          );
          $response['message'] = "Success";
          $response['data'] = null;
          $response['code'] = 201;
          return response()->json($response, 200);
        }
        $code = VerificationCode::where('user_id',$user->id)->where('code_type',2)->where('active',1)->where('resend_times',2)->first();
        if($code){
          $resend_times = $code->resend_times;
          $code->update([
            'resend_times' => $resend_times+1,
            'active' => 2,
            'updated_at' => Carbon::now()
          ]);
          $sid = "AC8a36c593b23c5c00d77fc07b57c3fe46";
          $token = "69876817aa091159385b161e0426e1f6";
          $twilio = new Client($sid, $token);
          $message = $twilio->messages
            ->create($request->email_phone, // to
              [
                "body" => "Kindly use the code " . $code . " to reset your password.",
                "from" => "+14793416298"
              ]
          );
          $response['message'] = "Success";
          $response['data'] = null;
          $response['code'] = 201;
          return response()->json($response, 200);
        }
        $code = VerificationCode::where('user_id',$user->id)->where('code_type',2)->where('active',2)->where('resend_times',3)
        ->orderBy('id','desc')->first();
        // dd((Carbon::parse($code->updated_at)->diffInSeconds(Carbon::now(), false)));
        if($code){
          if((Carbon::parse($code->updated_at)->diffInSeconds(Carbon::now(), false)) < 900){
            $response['message'] = "You have to wait 15 minutes and then try again";
            $response['data'] = null;
            $response['code'] = -3;
            return response()->json($response, 200);
          }else{
            // dd($request->email_phone);
            $new_code = rand(pow(10, 5), pow(10, 6) - 1);
            VerificationCode::create([
              'user_id' => $user->id,
              'code_type' => 2,
              'code' => $new_code
            ]);
            $sid = "AC8a36c593b23c5c00d77fc07b57c3fe46";
            $token = "69876817aa091159385b161e0426e1f6";
            $twilio = new Client($sid, $token);
            $message = $twilio->messages
              ->create($request->email_phone, // to
                [
                  "body" => "Kindly use the code " . $new_code . " to reset your password.",
                  "from" => "+14793416298"
                ]
            );
            $response['message'] = "Success";
            $response['data'] = null;
            $response['code'] = 201;
            return response()->json($response, 200);
          }
        }
      } catch (\Exception $ex) {
        Log::error($ex->getMessage());
        $response['message'] = "An error occured please try again later";
        $response['data'] = null;
        $response['code'] = -99;
        return response()->json($response, 200);
      }
    }
  }

  public function checkAccount(Request $request)
  {
    try {
      $data = User::where('status', 1)->where('email', $request->email_phone)
        ->orWhere('phone_number', $request->email_phone)
        ->first(['id', 'email', 'phone_number']);
      if ($data != null) {
        $response['message'] = "Success";
        $response['data'] = true;
        $response['code'] = 200;
        return response()->json($response, 200);
      } else {
        $response['message'] = "Not Exist";
        $response['data'] = false;
        $response['code'] = 200;
        return response()->json($response, 200);
      }
    } catch (\Exception $ex) {
      Log::error($ex->getMessage());
      $response['message'] = "An error occured please try again later";
      $response['data'] = $ex->getMessage();
      $response['code'] = -99;
      return response()->json($response, 200);
    }
  }

  public function getUsersInfo(Request $request)
  {
    try {
      $Users = $request['users'];
      $array = array();
      foreach ($Users as $user) {
        $userId = $user['userId'];
        if ((isset($userId) && $userId != "")) {
          $data = User::where('id', $userId)->where('status', 1)->first();
          if ($data != null) {
            $q = ["message" => "Success", "data" => $data, 'code' => "1"];
            array_push($array, $q);
          } else {
            $q = ["message" => "User isn't exist", "data" => null, 'code' => "-1"];
            array_push($array, $q);
          }
        }
      }
      if (empty($array)) {
        $q = ['code' => "-500", "message" => "Missing fields", 'data' => null];
        array_push($array, $q);
      }
      return $array;
    } catch (\Exception $ex) {
      Log::error($ex->getMessage());
      $response['message'] = "An error occured please try again later";
      $response['data'] = $ex->getMessage();
      $response['code'] = -99;
      return response()->json($response, 200);
    }
  }

  public function getUserAds(Request $request)
  {
    try {
      $data = Ads::where('owner_id', auth('api')->user()->id)
        ->with(['town', 'town.country'])
        ->with('owner.town')
        ->with(['property_attributes.option', 'property_attributes.category', 'property_attributes.size_unit',
                'property_attributes.price_option', 'property_attributes.furniture',
                'property_attributes.rooms', 'property_attributes.balcony', 'property_attributes.bath',
                'property_attributes.garage', 'media'])
        ->with(["savedAds" => function ($q) {
          $q->where('user_id', '=', auth('api')->user()->id);
        }])
        ->where('status', $request->status)
        ->orderBy('created_at', 'DESC')
        ->skip($request->offset)
        ->take($request->limit)
        ->get();
      $response['message'] = "Success";
      $response['data'] = $data;
      $response['code'] = 200;
      return response()->json($response, 200);
    } catch (\Exception $ex) {
      Log::error($ex->getMessage());
      $response['message'] = "An error occured please try again later";
      $response['data'] = null;
      $response['code'] = -99;
      return response()->json($response, 200);
    }
  }

  public function editProfile(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'email' => 'nullable|unique:users,email,' . auth('api')->user()->id . ',id',
      'username' => 'nullable|unique:users,username,' . auth('api')->user()->id . ',id',
      'website' => 'nullable|unique:users,website,' . auth('api')->user()->id . ',id',
      'phone_number' => 'nullable|unique:users,phone_number,' . auth('api')->user()->id . ',id',
      'country_id' => 'nullable|exists:uk_towns,id',
    ]);
    if ($validator->fails()) {
      $errors = $validator->errors();
      $data['message'] = $errors->first();
      $data['data'] = null;
      $data['code'] = -1;
      return response()->json($data, 200);
    }
    try {
      $input = collect(request()->all())->filter()->all();

      $user = User::find(auth('api')->user()->id);
      $user->update($input);
      $response['message'] = "Success";
      $response['data'] = null;
      $response['code'] = 200;
      return response()->json($response, 200);
    } catch (\Exception $ex) {
      Log::error($ex->getMessage());
      $response['message'] = "An error occured please try again later";
      $response['data'] = null;
      $response['code'] = -99;
      return response()->json($response, 200);
    }
  }

  public function updateFirebaseToken(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'token' => 'required',
    ]);

    if ($validator->fails()) {
      $errors = $validator->errors();
      $data['message'] = $errors->first();
      $data['data'] = null;
      $data['code'] = -2;
      return response()->json($data, 200);
    }
    try {
      $user = User::find(auth('api')->user()->id);
      $user->update([
        'firebase_token' => $request->token
      ]);
      $response['message'] = "Success";
      $response['data'] = null;
      $response['code'] = 200;
      return response()->json($response, 200);
    } catch (\Exception $ex) {
      Log::error($ex->getMessage());
      $response['message'] = "An error occured please try again later";
      $response['data'] = null;
      $response['code'] = -99;
      return response()->json($response, 200);
    }
  }

  public function updatePassword(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'old_password' => 'required|string',
      'password' => 'required|string|confirmed',
    ]);
    if ($validator->fails()) {
      $errors = $validator->errors();
      $data['message'] = $errors->first();
      $data['data'] = null;
      $data['code'] = -2;
      return response()->json($data, 200);
    }
    try {
      $user = User::where('id', auth('api')->user()->id)->first();
      if (Hash::check($request->old_password, $user->password)) {
        $user->update([
          'password' => Hash::make($request->password),
        ]);
        $response['message'] = "Success";
        $response['data'] = null;
        $response['code'] = 200;
        return response()->json($response, 200);
      } else {
        $response['message'] = "Old password is not correct";
        $response['data'] = null;
        $response['code'] = -2;
        return response()->json($response, 200);
      }
    } catch (\Exception $ex) {
      Log::error($ex->getMessage());
      $response['message'] = "An error occured please try again later";
      $response['data'] = null;
      $response['code'] = -99;
      return response()->json($response, 200);
    }
  }

  public function notifications(Request $request)
  {
    try {
      $user = UserNotification::where('user_id', auth('api')->user()->id)->with(['user.roles', 'user.roles.type', 'ad.owner', 'ad.property_attributes.option', 'ad.property_attributes.category'])
        ->orderBy('id', 'DESC')->get();
      $response['message'] = "Success";
      $response['data'] = $user;
      $response['code'] = 200;
      return response()->json($response, 200);
    } catch (\Exception $ex) {
      Log::error($ex->getMessage());
      $response['message'] = "An error occured please try again later";
      $response['data'] = null;
      $response['code'] = -99;
      return response()->json($response, 200);
    }
  }

  public function updateEmailPhone(Request $request)
  {
    //type is 1: email, 2: phone
    if ($request->type == 1) {
      try {
        $user = User::where('email', $request->email_phone)->where('status',1)->first();
        if($user) {
          $response['message'] = "Email already Exists";
          $response['data'] = null;
          $response['code'] = -2;
          return response()->json($response, 200);
        }
        $user = User::where('id', auth('api')->user()->id)->where('status',1)->first();
        $code = rand(pow(10, 5), pow(10, 6) - 1);
        VerificationCode::create([
          'user_id' => $user->id,
          'code_type' => 3,
          'code' => $code
        ]);
        $details = [
          'title' => "Welcome to our app",
          'code' => $code
        ];
        Mail::to($request->email_phone)->send(new UpdateEmailMail ($details));
        $response['message'] = "Success";
        $response['data'] = null;
        $response['code'] = 1;
        return response()->json($response, 200);
      } catch (\Exception $ex) {
        Log::error($ex->getMessage());
        $response['message'] = "An error occured please try again later";
        $response['data'] = null;
        $response['code'] = -99;
        return response()->json($response, 200);
      }
    } elseif ($request->type == 2) {
      try {
        $user = User::where('phone_number', $request->email_phone)->where('status',1)->first();
        if($user) {
          $response['message'] = "Phone already exists";
          $response['data'] = null;
          $response['code'] = -2;
          return response()->json($response, 200);
        }
        $user = User::where('id', auth('api')->user()->id)->where('status',1)->first();
        $code = rand(pow(10, 5), pow(10, 6) - 1);
        VerificationCode::create([
          'user_id' => $user->id,
          'code_type' => 3,
          'code' => $code
        ]);
        $sid = "AC090f96c15a70c4278101b2fe793bb511";
        $token = "61e1f7de52d433dd10db709df5f12bdb";
        $twilio = new Client($sid, $token);
        $message = $twilio->messages
          ->create($request->email_phone, // to
            [
              "body" => "To update your phone number use the code " . $code . " to update.",
              "from" => "+12055768004"
            ]
          );
        $response['message'] = "Success";
        $response['data'] = null;
        $response['code'] = 1;
        return response()->json($response, 200);
      } catch (\Exception $ex) {
        Log::error($ex->getMessage());
        $response['message'] = "An error occured please try again later";
        $response['data'] = null;
        $response['code'] = -99;
        return response()->json($response, 200);
      }
    }
  }

  public function completeUpdateEmailPhone(Request $request)
  {
    try {
      $user = User::where('id', auth('api')->user()->id)->first();
      if (!$user){
        $response['message'] = "User isn't Exist";
        $response['data'] = null;
        $response['code'] = -1;
        return response()->json($response, 200);
      }
      $code = VerificationCode::where('user_id', auth('api')->user()->id)->where('code',$request->code)->where('active',1)->first();
      if (!$code){
        $response['message'] = "Code isn't Exist";
        $response['data'] = null;
        $response['code'] = -2;
        return response()->json($response, 200);
      }
      if($request->type == 1){
        $user->update([
          'email' => $request->emailPhone,
        ]);
        $code->update([
          'active' => 2
        ]);
      }
      if($request->type == 2){
        $user->update([
          'phone_number' => $request->emailPhone,
        ]);
        $code->update([
          'active' => 2
        ]);
      }
      $response['message'] = "Success";
      $response['data'] = $user;
      $response['code'] = 1;
      return response()->json($response, 200);
    } catch (\Exception $ex) {
      Log::error($ex->getMessage());
      $response['message'] = "An error occured please try again later";
      $response['data'] = null;
      $response['code'] = -99;
      return response()->json($response, 200);
    }
  }

  public function getUserProfile(Request $request)
  {
    try {
      $data = User::with('town', 'type', 'application')->find(auth('api')->user()->id);
      $response['message'] = "Success";
      $response['data'] = array(
        'user' => $data,
        'active_ads' => Ads::where('owner_id', auth('api')->user()->id)->where('status', '1')->count(),
        'de_active_ads' => Ads::where('owner_id', auth('api')->user()->id)->where('status', '2')->count(),
        'pending_ads' => Ads::where('owner_id', auth('api')->user()->id)->where('status', '3')->count(),
      );
      $response['code'] = 200;
      return response()->json($response, 200);
    } catch (\Exception $ex) {
      Log::error($ex->getMessage());
      $response['message'] = "An error occured please try again later";
      $response['data'] = null;
      $response['code'] = -99;
      return response()->json($response, 200);
    }
  }

  public function getUserAccount(Request $request)
  {
    try {
      $data = Ads::where('owner_id', $request->user_id)->where('status', 1)
        ->with(['town', 'town.country'])
        ->with(['owner.town','owner.town.country'])
        ->with(['property_attributes.option', 'property_attributes.category', 'property_attributes.size_unit', 'property_attributes.price_option',
                'property_attributes.furniture', 'property_attributes.rooms', 'property_attributes.balcony', 'property_attributes.bath',
                'property_attributes.garage', 'media'])
        ->with(["savedAds" => function ($q) {
          $q->where('user_id', '=', auth('api')->user()->id);
        }])
        ->orderBy('created_at', 'DESC')
        ->get();
      $data1 = User::with('town','town.country', 'roles', 'roles.type')->where('id', $request->user_id)->first();
      $response['message'] = "Success";
      $response['data'] = array(
        'user' => $data1,
        'ads' => $data
      );
      $response['code'] = 200;
      return response()->json($response, 200);
    } catch (\Exception $ex) {
      Log::error($ex->getMessage());
      $response['message'] = "An error occured please try again later";
      $response['data'] = null;
      $response['code'] = -99;
      return response()->json($response, 200);
    }
  }

  public function switchToBusiness(Request $request)
  {
    // try {
      $user = User::where('id',auth('api')->user()->id)->where('app_id',$request->app_id)->where('status',1)->first();
      if(!$user){
        $response['message'] = "User isn't exist";
        $response['data'] = null;
        $response['code'] = -1;
        return response()->json($response, 200);
      }
      $type = UserTypes::where('app_id',$request->app_id)->where('id',$request->type_id)->first();
      if(!$type){
        $response['message'] = "This type don't belong to this application";
        $response['data'] = null;
        $response['code'] = -2;
        return response()->json($response, 200);
      }
      $data = User::where('id',auth('api')->user()->id)->where('app_id',$request->app_id)->where('type_id','!=',2)->where('status',1)->first();
      if($data){
        $response['message'] = "User isn already business";
        $response['data'] = null;
        $response['code'] = -3;
        return response()->json($response, 200);
      }
      $user->update([
        'type_id' => $request->type_id
      ]);
      $response['message'] = "Success";
      $response['data'] = null;
      $response['code'] = 200;
      return response()->json($response, 200);
    // } catch (\Exception $ex) {
    //   Log::error($ex->getMessage());
    //   $response['message'] = "An error occured please try again later";
    //   $response['data'] = null;
    //   $response['code'] = -99;
    //   return response()->json($response, 200);
    // }
  }

  public function addNewInterest(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'town_id' => 'nullable|exists:uk_towns,id',
      'option_id' => 'nullable|exists:property_options,id',
      'category_id' => 'nullable|exists:category,id',
      'rooms' => 'nullable|exists:property_rooms_number,id',
      'price_option_id' => 'nullable|exists:property_price_option,id',
      'balcony' => 'nullable|exists:property_balcony_number,id',
      'bath' => 'nullable|exists:property_baths_number,id',
      'furniture_id' => 'nullable|exists:property_furniture_types,id',
    ]);
    if ($validator->fails()) {
      $errors = $validator->errors();
      $data['message'] = $errors->first();
      $data['data'] = null;
      $data['code'] = -2;
      return response()->json($data, 200);
    }
    try {
      $request['user_id'] = auth('api')->user()->id;
      $input = collect($request->all())->filter()->all();
      UserInterest::create($input);
      $response['message'] = "Success";
      $response['data'] = null;
      $response['code'] = 200;
      return response()->json($response, 200);
    } catch (\Exception $ex) {
      Log::error($ex->getMessage());
      $response['message'] = "An error occured please try again later";
      $response['data'] = null;
      $response['code'] = -99;
      return response()->json($response, 200);
    }
  }

  public function deleteInterest(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'interest_id' => 'required|exists:user_interest,id',
    ]);
    if ($validator->fails()) {
      $errors = $validator->errors();
      $data['message'] = $errors->first();
      $data['data'] = null;
      $data['code'] = -2;
      return response()->json($data, 200);
    }
    $is_mine = UserInterest::where('id',$request->interest_id)->where('user_id',auth('api')->user()->id)->first();
    if(!$is_mine){
      $data['message'] = 'This interest is not yours';
      $data['data'] = null;
      $data['code'] = -3;
      return response()->json($data, 200);
    }
    try {
      $interest = UserInterest::where('id', $request->interest_id)->first()->delete();
      $response['message'] = "Success";
      $response['data'] = null;
      $response['code'] = 200;
      return response()->json($response, 200);
    } catch (\Exception $ex) {
      Log::error($ex->getMessage());
      $response['message'] = "An error occured please try again later";
      $response['data'] = null;
      $response['code'] = -99;
      return response()->json($response, 200);
    }
  }

  public function getInterest(Request $request)
  {
    try {
      $user = UserInterest::where('user_id', auth('api')->user()->id)
      ->with('option')
      ->with('category')
      ->with('size_unit')
      ->with('price_option')
      ->with('furniture')
      ->with('rooms')
      ->with('balcony')
      ->with('bath')
      ->with('garage')
      ->get();
      $response['message'] = "";
      $response['data'] = $user;
      $response['code'] = 200;
      return response()->json($response, 200);
    } catch (\Exception $ex) {
      Log::error($ex->getMessage());
      $response['message'] = "An error occured please try again later";
      $response['data'] = null;
      $response['code'] = -99;
      return response()->json($response, 200);
    }
  }
}
