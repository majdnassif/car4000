<?php

namespace App\Http\Controllers\API;

use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class CategoriesController extends Controller
{
  public function list(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'app_id' => 'required|exists:application,id'
    ]);
    if ($validator->fails()) {
      $errors = $validator->errors();
      $data['message'] = $errors->first();
      $data['data'] = null;
      $data['code'] = -1;
      return response()->json($data, 200);
    }
    $data = Category::where('app_id', $request->app_id)
    ->when(isset($request->category_id) && $request->category_id !== '', function ($query) use ($request) {
      $query->where('parent_id', $request->category_id);
    })
    ->where('status', '<>', '3')
    ->orderBy('ordering', 'DESC')
    ->get();
    if (count($data) > 0) {
      $response['message'] = "Success";
      $response['data'] = $data;
      $response['code'] = 200;
      return response()->json($response, 200);

    } else {
      $response['message'] = "No data";
      $response['data'] = null;
      $response['code'] = -1;
      return response()->json($response, 200);
    }
  }

  public function sub(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'category_id' => 'required|exists:category,id',
    ]);
    if ($validator->fails()) {
      $errors = $validator->errors();
      $data['message'] = $errors->first();
      $data['data'] = null;
      $data['code'] = -1;
      return response()->json($data, 200);
    }
    $data = Category::where('parent_id', $request->category_id)->where('status', '<>', '3')->orderBy('ordering', 'DESC')->get();
    if (count($data) > 0) {
      $response['message'] = "Success";
      $response['data'] = $data;
      $response['code'] = 200;
      return response()->json($response, 200);

    } else {
      $response['message'] = "No data";
      $response['data'] = null;
      $response['code'] = -1;
      return response()->json($response, 200);
    }
  }
}
