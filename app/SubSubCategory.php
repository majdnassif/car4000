<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubSubCategory extends Model
{
  use HasFactory;

  protected $table = 'sub_sub_category';

  protected $fillable = ['sub_category_id', 'name', 'mobile_image', 'web_image', 'status'];

  public function category()
  {
    return $this->belongsTo(SubCategory::class, 'sub_category_id', 'id');
  }
}
