<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login', 'API\UsersController@login');
Route::group(['middleware' => 'auth:api'], function () {
    Route::post('/propertyStoreAd', 'API\AdsController@propertyStoreAd');
    Route::post('/storeMedia', 'API\MediaController@store');
    Route::post('/propertyAdsList', 'API\AdsController@propertyAdsList');
    Route::post('/propertyAdDetails', 'API\AdsController@propertyAdDetails');
    Route::post('/saveAd', 'API\savedAdsController@saveAd');
    Route::post('/getSavedAds', 'API\AdsController@getSavedAds');
    Route::post('/pauseAd', 'API\AdsController@pauseAd');
    Route::post('/editAd', 'API\AdsController@editAd');
    Route::post('/deletePropertyAd', 'API\AdsController@destroyPropertyAd');
    Route::post('/getUserAds', 'API\UsersController@getUserAds');
    Route::post('/media/updateUserPicture', 'API\MediaController@updateUserPicture');
    Route::post('/editProfile', 'API\UsersController@editProfile');
    Route::post('/updateFirebaseToken', 'API\UsersController@updateFirebaseToken');
    Route::post('/updatePassword', 'API\UsersController@updatePassword');
    Route::post('/notifications', 'API\UsersController@notifications');
    Route::post('/updateEmailPhone', 'API\UsersController@updateEmailPhone');
    Route::post('/completeUpdateEmailPhone', 'API\UsersController@completeUpdateEmailPhone');
    Route::post('/getUserProfile', 'API\UsersController@getUserProfile');
    Route::post('/getUserAccount', 'API\UsersController@getUserAccount');
    Route::post('/switchToBusiness', 'API\UsersController@switchToBusiness');
    Route::post('/addInterest', 'API\UsersController@addNewInterest');
    Route::post('/deleteInterest', 'API\UsersController@deleteInterest');
    Route::post('/getInterest', 'API\UsersController@getInterest');

    //Payment Routes 
    Route::post('/payment','API\PaymentController@payWithpaypal');

    //Cars Routes
    Route::post('/carsAdsList', 'API\AdsController@carsAdsList');
    Route::post('/carsAdDetails', 'API\AdsController@carsAdDetails');
    Route::post('/getSavedAdsCars', 'API\AdsController@getSavedAdsCars');
});
Route::post('/appsList', 'API\AppsController@list');
Route::post('/categoriesList', 'API\CategoriesController@list');
Route::post('/subCategoriesList', 'API\CategoriesController@sub');
Route::post('/userTypesList', 'API\UserTypesController@list');
Route::post('/countriesList', 'API\CountryController@list');
Route::post('/townsList', 'API\TownsController@list');
Route::post('/signUp', 'API\UsersController@signUp');
Route::post('/verifyUser', 'API\UsersController@verifyUser');
Route::post('/forgetPassword', 'API\UsersController@forgetPassword');
Route::post('/resetPassword', 'API\UsersController@resetPassword');
Route::post('/resendVerificationCode', 'API\UsersController@resendVerificationCode');
Route::post('/sizeUnitesList', 'API\SizeUnitesController@list');
Route::post('/roomsNumberList', 'API\RoomsNumberController@list');
Route::post('/garagesNumberList', 'API\GaragesNumberController@list');
Route::post('/furnitureTypesList', 'API\FurnitureTypesController@list');
Route::post('/bathsNumberList', 'API\BathsNumberController@list');
Route::post('/balconyNumberList', 'API\BalconyNumberController@list');
Route::post('/optionsList', 'API\OptionController@list');
Route::post('/priceOptionsList', 'API\OptionPriceOptionsController@getPriceOptions');
Route::post('/adPricesList', 'API\PriceListController@list');
Route::post('/checkAccount', 'API\UsersController@checkAccount');
Route::post('/getUsersInfo', 'API\UsersController@getUsersInfo');
Route::post('/bodyTypesList', 'API\CarsBodyStyleController@list');
Route::post('/colorsList', 'API\CarsColorsController@list');
Route::post('/carConditionsList', 'API\CarsConditionsController@list');
Route::post('/carFuelsList', 'API\CarsFuelController@list');
Route::post('/carModelsList', 'API\CarsModelsController@list');
Route::post('/carsOptionsList', 'API\CarsOptionsController@list');
Route::post('/carsTransmissionList', 'API\CarsTransmissionController@list');
