<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {return view('views/index');})->name('index');
Route::get('/forgotpassword', function () {return view('views/forgotpassword');})->name('forgotpassword');
Route::get('/resetpassword', function () {return view('views/resetpassword');})->name('resetpassword');



Route::get('/index1', function () {return view('views/index1');})->name('index1');
Route::get('/index2', function () {return view('views/index2');})->name('index2');
Route::get('/index3', function () {return view('views/index3');})->name('index3');
Route::get('/index4', function () {return view('views/index4');})->name('index4');
Route::get('/index5', function () {return view('views/index5');})->name('index5');


Route::get('/listing', function () {return view('views/listing');})->name('listing');
Route::get('/listing4', function () {return view('views/listing4');})->name('listing4');
Route::get('/listing3', function () {return view('views/listing3');})->name('listing3');
Route::get('/adsList', function () {return view('views/adsList');})->name('adsList');
Route::get('/listing2', function () {return view('views/listing2');})->name('listing2');

Route::get('/listing6', function () {return view('views/listing6');})->name('listing6');
Route::get('/listing7', function () {return view('views/listing7');})->name('listing7');
Route::get('/listing8', function () {return view('views/listing8');})->name('listing8');


//Route::get('/single-page-listing', function () {return view('views/single-page-listing');})->name('single-page-listing');
Route::get('/single-page-listing-1', function () {return view('views/single-page-listing-1');})->name('single-page-listing-1');
Route::get('/single-page-listing-2', function () {return view('views/single-page-listing-2');})->name('single-page-listing-2');
Route::get('/single-page-listing-3', function () {return view('views/single-page-listing-3');})->name('single-page-listing-3');
Route::get('/icons', function () {return view('views/icons');})->name('icons');

Route::get('/reviews', function () {return view('views/reviews');})->name('reviews');
Route::get('/adDetail', function () {return view('views/single-page-listing');})->name('adDetail');

Route::get('/compare', function () {return view('views/compare');})->name('compare');
Route::get('/compare-1', function () {return view('views/compare-1');})->name('compare-1');
Route::get('/compare-2', function () {return view('views/compare-2');})->name('compare-2');

Route::get('/profile', function () {return view('views/profile');})->name('profile');
Route::get('/archives', function () {return view('views/archives');})->name('archives');
Route::get('/active-ads', function () {return view('views/active-ads');})->name('active-ads');
Route::get('/favourite', function () {return view('views/favourite');})->name('favourite');
Route::get('/messages', function () {return view('views/messages');})->name('messages');
Route::get('/deactive', function () {return view('views/deactive');})->name('deactive');

Route::get('/blog', function () {return view('views/blog');})->name('blog');
Route::get('/blog-1', function () {return view('views/blog-1');})->name('blog-1');
Route::get('/blog-2', function () {return view('views/blog-2');})->name('blog-2');
Route::get('/blog-details', function () {return view('views/blog-details');})->name('blog-details');

Route::get('/about', function () {return view('views/about');})->name('about');
Route::get('/about-1', function () {return view('views/about-1');})->name('about-1');
Route::get('/cooming-soon', function () {return view('views/cooming-soon');})->name('cooming-soon');
Route::get('/elements', function () {return view('views/elements');})->name('elements');

Route::get('/error', function () {return view('views/error');})->name('error');
Route::get('/faqs', function () {return view('views/faqs');})->name('faqs');
Route::get('/login', function () {return view('views/login');})->name('login');
Route::get('/register', function () {return view('views/register');})->name('register');

Route::get('/post-ad-1', function () {return view('views/post-ad-1');})->name('post-ad-1');
Route::get('/pricing', function () {return view('views/pricing');})->name('pricing');
Route::get('/site-map', function () {return view('views/site-map');})->name('site-map');
Route::get('/contact', function () {return view('views/contact');})->name('contact');

Route::get('/services', function () {return view('views/services');})->name('services');
Route::get('/services-1', function () {return view('views/services-1');})->name('services-1');


Route::get('/updatePassword', function () {return view('views/updatePassword');})->name('updatePassword');

/*Route::post('/testUrl', 'App\Http\Controllers\testController@getAjax')->name('testUrl');*/


