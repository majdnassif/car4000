<div class="preloader"></div>
<!-- =-=-=-=-=-=-= Color Switcher =-=-=-=-=-=-= -->
<div class="color-switcher" id="choose_color">
    <a href="#." class="picker_close"><i class="fa fa-gear"></i></a>
    <h5>STYLE SWITCHER</h5>
    <div class="theme-colours">
        <p> Choose Colour style </p>
        <ul>
            <li>
                <a href="#." class="defualt" id="defualt"></a>
            </li>
            <li>
                <a href="#." class="green" id="green"></a>
            </li>
            <li>
                <a href="#." class="purple" id="purple"></a>
            </li>
        </ul>
    </div>
    <div class="clearfix"> </div>
</div>
<!-- =-=-=-=-=-=-= Main Header =-=-=-=-=-=-= -->
<div class="colored-header">
    <!-- Top Bar -->
    <div class="header-top dark">
        <div class="container">
            <div class="row">
                <!-- Header Top Left -->
                <div class="header-top-left col-md-6 col-sm-6 col-xs-12 hidden-xs">
                    <ul class="listnone">
                       {{-- <li><a href="{{route('about')}}"><i class="fa fa-heart-o" aria-hidden="true"></i> About</a></li>
                        <li><a href="{{route('faqs')}}"><i class="fa fa-folder-open-o" aria-hidden="true"></i> </a></li>--}}
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-globe" aria-hidden="true"></i> Language <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">English</a></li>
                                <li><a href="#">Arabic</a></li>
                            </ul>
                        </li>

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bell" aria-hidden="true"></i> Notification <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#"></a></li>
                                <li><a href="#"></a></li>
                            </ul>
                        </li>


                    </ul>
                </div>
                <!-- Header Top Right Social -->
                <div class="header-right col-md-6 col-sm-6 col-xs-12 ">
                    <div class="pull-right">
                        <ul class="listnone">
                            <?php
/*
                            $isLogin = 2;

                            if(isset($_COOKIE['isLogin']))
                                  if($_COOKIE['isLogin']==1)
                                      $isLogin = 1;
*/

                              ?>

                            <li>   <label><a id="LogOutBtn" style="margin-left: 6rem;"> <i class="fa fa-sign-out"></i>Log Out</a></label>

                            </li>

                            <li><a id="LogInBtn" href="{{route('login')}}"><i class="fa fa-sign-in"></i> Log in</a></li>

                            <li class="hidden-xs hidden-sm"><a href="{{route('register')}}"><i class="fa fa-unlock" aria-hidden="true"></i> Register</a></li>
{{--
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img class="img-circle resize" alt="" src="{{asset('theme/images/users/3.jpg')}}"> <span class="myname hidden-xs"> Umair </span> <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li id="User_Profile_umair"><a href="{{route('profile')}}">User Profile</a></li>
                                    <li><a href="{{route('archives')}}">Archives</a></li>
                                    <li><a href="{{route('active-ads')}}" >Active Ads</a></li>
                                    <li><a href="{{route('favourite')}}">Favourite Ads</a></li>
                                    <li><a href="{{route('messages')}}">Message Panel</a></li>
                                    <li><a href="{{route('deactive')}}">Account Deactivation</a></li>
                                </ul>
                            </li>
--}}
                            <li id="Sell_Your_Car_Btn">
                                <a href="{{route('post-ad-1')}}" class="btn btn-theme">Post Your Ad</a>
                            </li>

                                <li id="Login_Sell_Your_Car_Btn">
                                    <a href="{{route('login')}}" class="btn btn-theme">Log In To Post Your Ad</a>
                                </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Top Bar End -->
    <!-- Navigation Menu -->
    <div class="clearfix"></div>
    <!-- menu start -->
    <nav id="menu-1" class="mega-menu">
        <!-- menu list items container -->
        <section class="menu-list-items">
            <div class="container">
                <div class="row">


                    <div class="col-lg-12 col-md-12">
                        <!-- menu logo -->
                        <ul class="menu-logo">
                            <li>
                                <a href="{{route('index')}}"><img src="{{asset('theme/images/logo.png')}}" alt="logo"> </a>
                            </li>
                        </ul>
                        <!-- menu links -->
                        <ul class="menu-links">
                            <!-- active class -->
                            <li>
                                <a href="{{route('index')}}">Home {{--<i class="fa fa-angle-down fa-indicator"></i>--}}</a>
                                <!-- drop down multilevel  -->
                               {{-- <ul class="drop-down-multilevel">
                                    <li><a href="{{route('index1')}}">Home 1</a></li>
                                    <li><a href="{{route('index2')}}">Home 2</a></li>
                                    <li><a href="{{route('index3')}}">Home 3</a></li>
                                    <li><a href="{{route('index4')}}">Home 4</a></li>
                                    <li><a href="{{route('index5')}}">Home 5</a></li>
                                </ul>--}}
                            </li>
                     {{--       <li>
                                <a href="javascript:void(0)"> Cars <i class="fa fa-angle-down fa-indicator"></i></a>
                                <div class="drop-down grid-col-12">
                                    <!--grid row-->
                                    <div class="grid-row">
                                        <!--grid column 3-->
                                        <div class="grid-col-2">
                                            <h3>Condition</h3>
                                            <ul>
                                                <li><a href="{{route('listing')}}">New</a></li>
                                                <li><a href="{{route('listing4')}}">Used</a></li>
                                                <li><a href="{{route('listing3')}}">Reconditioned </a></li>
                                                <li><a href="#">Featured Cars </a></li>
                                            </ul>
                                        </div>
                                        <div class="grid-col-6">
                                            <h3>Brands</h3>
                                            <ul class="by-make list-inline">
                                                <li>
                                                    <a href="#">
                                                        <img src="{{asset('theme/images/brands/1.png')}}" class="img-responsive" alt="Brand Image">
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <img src="{{asset('theme/images/brands/2.png')}}" class="img-responsive" alt="Brand Image">
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <img src="{{asset('theme/images/brands/3.png')}}" class="img-responsive" alt="Brand Image">
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <img src="{{asset('theme/images/brands/4.png')}}" class="img-responsive" alt="Brand Image">
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <img src="{{asset('theme/images/brands/5.png')}}" class="img-responsive" alt="Brand Image">
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <img src="{{asset('theme/images/brands/6.png')}}" class="img-responsive" alt="Brand Image">
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <img src="{{asset('theme/images/brands/7.png')}}" class="img-responsive" alt="Brand Image">
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <img src="{{asset('theme/images/brands/8.png')}}" class="img-responsive" alt="Brand Image">
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <img src="{{asset('theme/images/brands/9.png')}}" class="img-responsive" alt="Brand Image">
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <img src="{{asset('theme/images/brands/11.png')}}" class="img-responsive" alt="Brand Image">
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="grid-col-4">
                                            <h3>Body Type</h3>
                                            <ul class="list-inline by-category ">
                                                <li>
                                                    <a href="#">
                                                        <img alt="Hybrid" src="{{asset('theme/images/bodytype/1.png')}}">
                                                        Convertible
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <img alt="Hybrid" src="{{asset('theme/images/bodytype/2.png')}}">
                                                        Coupe
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <img alt="Hybrid" src="{{asset('theme/images/bodytype/3.png')}}">
                                                        Sedan
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <img alt="Hybrid" src="{{asset('theme/images/bodytype/4.png')}}">
                                                        Van/Minivan
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <img alt="Hybrid" src="{{asset('theme/images/bodytype/5.png')}}">
                                                        Truck
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#">
                                                        <img alt="Hybrid" src="{{asset('theme/images/bodytype/6.png')}}">
                                                        Hybrid
                                                    </a>
                                                </li>

                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </li>--}}

                          {{--  <li>
                                <a href="{{route('adsList')}}" >Ads List --}}{{--<i class="fa fa-angle-down fa-indicator"></i>--}}{{--
                                </a>
                                <!-- drop down multilevel  -->
                            --}}{{--    <ul class="drop-down-multilevel">
                                    <li>
                                        <a href="javascript:void(0)">Grid Style<i class="fa fa-angle-right fa-indicator"></i> </a>
                                        <!-- drop down second level -->
                                        <ul class="drop-down-multilevel">
                                            <li><a href="{{route('listing')}}"> Grid Style (Defualt)</a></li>
                                            <li><a href="{{route('listing1')}}"> Grid Style 1</a></li>
                                            <li><a href="{{route('listing2')}}"> Grid Style 2</a></li>
                                            <li><a href="{{route('listing3')}}"> Grid Style 3</a></li>
                                            <li><a href="{{route('listing4')}}"> Grid Style 4</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">List Style<i class="fa fa-angle-right fa-indicator"></i> </a>
                                        <!-- drop down second level -->
                                        <ul class="drop-down-multilevel">
                                            <li><a href="#">List View 1</a></li>
                                            <li><a href="{{route('listing6')}}">List View 2</a></li>
                                            <li><a href="{{route('listing7')}}">List View 3</a></li>
                                            <li><a href="{{route('listing8')}}">List View 4</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">Single Ad<i class="fa fa-angle-right fa-indicator"></i></a>
                                        <!-- drop down second level -->
                                        <ul class="drop-down-multilevel">
                                            <li><a href="{{route('single-page-listing')}}">Single Ad Detail</a></li>
                                            <li><a href="{{route('single-page-listing-1')}}">Single Ad (Gallery)</a></li>
                                            <li><a href="{{route('single-page-listing-2')}}">Single Ad (Gallery 2)</a></li>
                                            <li><a href="{{route('single-page-listing-3')}}">Single Ad Variation</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="{{route('icons')}}">Template Icons </a></li>
                                </ul>--}}{{--
                            </li>--}}






{{--                                        <li>--}}
{{--                                            <a href="javascript:void(0)">Single Ad<i class="fa fa-angle-right fa-indicator"></i></a>--}}
{{--                                            <!-- drop down second level -->--}}
{{--                                            <ul class="drop-down-multilevel">--}}
{{--                                                <li><a href="{{route('single-page-listing')}}">Single Ad Detail</a></li>--}}
{{--                                                <li><a href="{{route('listing6')}}">grid</a></li>--}}

{{--                                            </ul>--}}
{{--                                        </li>--}}



                           {{-- <li>
                                <a href="javascript:void(0)">Reviews <i class="fa fa-angle-down fa-indicator"></i></a>
                                <!-- drop down multilevel  -->
                                <ul class="drop-down-multilevel">
                                    <li><a href="{{route('reviews')}}" >Expert Reviews</a></li>
                                    <li><a href="{{route('adDetail')}}">Review Detial</a></li>
                                </ul>
                            </li>--}}
                          {{--  <li>
                                <a href="javascript:void(0)">Compare <i class="fa fa-angle-down fa-indicator"></i></a>
                                <!-- drop down multilevel  -->
                                <ul class="drop-down-multilevel">
                                    <li><a href="{{route('compare')}}">Car Comparison</a></li>
                                    <li><a href="{{route('compare-1')}}">Comparison Style 2</a></li>
                                    <li><a href="{{route('compare-2')}}">Comparison Detial</a></li>
                                </ul>
                            </li>--}}
                   {{--         <li>
                                <a href="javascript:void(0)">Dashboard <i class="fa fa-angle-down fa-indicator"></i></a>
                                <!-- drop down multilevel  -->
                                <ul class="drop-down-multilevel">
                                    <li id="user_profile"><a  href="{{route('profile')}}">User Profile</a></li>
                                    <li><a href="{{route('archives')}}">Archives</a></li>
                                    <li><a href="{{route('active-ads')}}">Active Ads</a></li>
                                    <li><a href="{{route('favourite')}}">Favourite Ads</a></li>
                                    <li><a href="{{route('messages')}}">Message Panel</a></li>
                                    <li><a href="{{route('deactive')}}">Account Deactivation</a></li>
                                </ul>
                            </li>--}}

                            <li id="user_profile"><a  href="{{route('profile')}}">User Profile</a></li>
                            <li id="userGetSaved" >
                                <a href="{{route('favourite')}}">Get Saved Ads</a>

                            </li>

                            <li>
                                <a href="{{route('contact')}}">CONTACT</a>

                            </li>


                            <li>
                                <a href="">Used</a>

                            </li>
                            <li>
                                <a href="">New</a>

                            </li>
                            <li>
                                <a href="">Heavy</a>

                            </li>
                            <li>
                                <a href="">Motorcycle</a>

                            </li>
                            <li>
                                <a href="">Boat</a>

                            </li>
                            <li>
                                <a href="">Part</a>

                            </li>
                            <li>
                                <a href="">Services</a>

                            </li>
                            <li>
                                <a href="">Dealer</a>

                            </li>
                            <li>
                                <a href="">Showrooms</a>

                            </li>
                            <li>
                                <a href="">Plate No</a>

                            </li>



                            {{--  <li>
                                <a href="javascript:void(0)">Pages <i class="fa fa-angle-down fa-indicator"></i></a>
                                <!-- drop down full width -->
                                <div class="drop-down grid-col-12">
                                    <!--grid row-->
                                    <div class="grid-row">
                                        <!--grid column 2-->
                                        <div class="grid-col-2">
                                            <h4>Blog</h4>
                                            <ul>
                                                <li><a href="{{route('blog')}}" > Right Sidebar</a></li>
                                                <li><a href="{{route('blog-1')}}"> Masonry Style</a></li>
                                                <li><a href="{{route('blog-2')}}"> Without Sidebar</a></li>
                                                <li><a href="{{route('blog-details')}}"> Single Blog </a></li>
                                            </ul>
                                        </div>
                                        <!--grid column 2-->
                                        <div class="grid-col-2">
                                            <h4>Miscellaneous</h4>
                                            <ul>
                                                <li><a href="{{route('about')}}">About Us</a></li>
                                                <li><a href="{{route('about-1')}}">About Us 2</a></li>
                                                <li><a href="{{route('cooming-soon')}}">Comming Soon</a></li>
                                                <li><a href="{{route('elements')}}">Shortcodes</a></li>

                                            </ul>
                                        </div>
                                        <!--grid column 2-->
                                        <div class="grid-col-2">
                                            <h4>Others</h4>
                                            <ul>
                                                <li><a href="{{route('error')}}" >404 Page</a></li>
                                                <li><a href="{{route('faqs')}}">FAQS</a></li>
                                                <li><a href="{{route('login')}}">Login</a></li>
                                                <li><a href="{{route('register')}}">Register</a></li>

                                            </ul>
                                        </div>
                                        <!--grid column 2-->
                                        <div class="grid-col-2">
                                            <h4>Extra Page</h4>
                                            <ul>
                                                <li><a href="{{route('post-ad-1')}}" >Post Ad</a></li>
                                                <li><a href="{{route('pricing')}}">Pricing</a></li>
                                                <li><a href="{{route('site-map')}}">Site Map</a></li>
                                                <li><a href="{{route('contact')}}">Contact Us</a></li>
                                            </ul>
                                        </div>
                                        <!--grid column 2-->
                                        <div class="grid-col-2">
                                            <h4>Services Page</h4>
                                            <ul>

                                                <li><a href="{{route('services')}}">Services</a></li>
                                                <li><a href="{{route('services-1')}}">Services 2</a></li>
                                                <li><a href="{{route('profile')}}">Profile</a></li>
                                                <li><a href="{{route('messages')}}">Messages</a></li>
                                            </ul>
                                        </div>
                                        <!--grid column 2-->
                                        <div class="grid-col-2">
                                            <h4>Trending</h4>
                                            <ul>
                                                <li><a href="{{route('reviews')}}">Reviews</a></li>
--}}{{--
                                                <li><a href="{{route('review-detail')}}" >Review Detail</a></li>
--}}{{--
                                                <li><a href="{{route('compare')}}">Compare</a></li>
                                                <li><a href="{{route('compare-2')}}">Comapre Detail</a></li>
                                            </ul>
                                        </div>
                                        <!--grid column 2-->
                                    </div>
                                </div>
                            </li>--}}


                        </ul>
                        {{--<ul class="menu-search-bar">
                            <li>
                                <a>
                                    <div class="contact-in-header clearfix">
                                        <i class="flaticon-customer-service"></i>
                                        <span>
                                    Call Us Now
                                    <br>
                                    <strong>111 222 333 444</strong>
                                    </span>
                                    </div>
                                </a>
                            </li>
                        </ul>--}}
                    </div>
                </div>
            </div>
        </section>
    </nav>
    <!-- menu end -->
</div>
