<!DOCTYPE html>
<html>
<head>
  <title>4000 Team</title>
</head>
<body>
<h1>{{ $details['title'] }}</h1>
<p>Dear Customer,</p>
<p>Kindly Verify the Email to complete update operation</p>
<p>Kindly use the code <b>{{$details['code']}}</b> to update your email.</p>
<p><b>4000 Team.</b></p>
</body>
</html>
