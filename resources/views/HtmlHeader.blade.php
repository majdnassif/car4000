<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <![endif]-->
    <meta name="description" content="">
    <meta name="author" content="ScriptsBundle">
    <title>Carspot | Car Dealership - Vehicle Marketplace And Car Services</title>
    <!-- =-=-=-=-=-=-= Favicons Icon =-=-=-=-=-=-= -->
    <link rel="icon" href="{{asset('theme/images/favicon.ico')}}" type="image/x-icon" />
    <!-- =-=-=-=-=-=-= Mobile Specific =-=-=-=-=-=-= -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- =-=-=-=-=-=-= Bootstrap CSS Style =-=-=-=-=-=-= -->
    <link rel="stylesheet" href="{{asset('theme/css/bootstrap.css')}}">
    <!-- =-=-=-=-=-=-= Template CSS Style =-=-=-=-=-=-= -->
    <link rel="stylesheet" href="{{asset('theme/css/style.css')}}">
    <!-- =-=-=-=-=-=-= Font Awesome =-=-=-=-=-=-= -->
    <link rel="stylesheet" href="{{asset('theme/css/font-awesome.css')}}" type="text/css">
    <!-- =-=-=-=-=-=-= Flat Icon =-=-=-=-=-=-= -->
    <link href="{{asset('theme/css/flaticon.css')}}" rel="stylesheet">
    <!-- =-=-=-=-=-=-= Et Line Fonts =-=-=-=-=-=-= -->
    <link rel="stylesheet" href="{{asset('theme/css/et-line-fonts.css')}}" type="text/css">
    <!-- =-=-=-=-=-=-= Menu Drop Down =-=-=-=-=-=-= -->
    <link rel="stylesheet" href="{{asset('theme/css/carspot-menu.css')}}" type="text/css">
    <!-- =-=-=-=-=-=-= Animation =-=-=-=-=-=-= -->
    <link rel="stylesheet" href="{{asset('theme/css/animate.min.css')}}" type="text/css">
    <!-- =-=-=-=-=-=-= Select Options =-=-=-=-=-=-= -->
    <link href="{{asset('theme/css/select2.min.css')}}" rel="stylesheet" />
    <!-- =-=-=-=-=-=-= noUiSlider =-=-=-=-=-=-= -->
    <link href="{{asset('theme/css/nouislider.min.css')}}" rel="stylesheet">
    <!-- =-=-=-=-=-=-= Listing Slider =-=-=-=-=-=-= -->
    <link href="{{asset('theme/css/slider.css')}}" rel="stylesheet">
    <!-- =-=-=-=-=-=-= Owl carousel =-=-=-=-=-=-= -->
    <link rel="stylesheet" type="text/css" href="{{asset('theme/css/owl.carousel.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('theme/css/owl.theme.css')}}">
    <!-- =-=-=-=-=-=-= Check boxes =-=-=-=-=-=-= -->
    <link href="{{asset('theme/skins/minimal/minimal.css')}}" rel="stylesheet">
    <!-- =-=-=-=-=-=-= PrettyPhoto =-=-=-=-=-=-= -->
    <link rel="stylesheet" href="{{asset('theme/css/jquery.fancybox.min.css')}}" type="text/css" media="screen"/>
    <!-- =-=-=-=-=-=-= Responsive Media =-=-=-=-=-=-= -->
    <link href="{{asset('theme/css/responsive-media.css')}}" rel="stylesheet">
    <!-- =-=-=-=-=-=-= Template Color =-=-=-=-=-=-= -->
    <link rel="stylesheet" id="color" href="{{asset('theme/css/colors/defualt.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600%7CSource+Sans+Pro:400,400i,600" rel="stylesheet">
    <!-- JavaScripts -->

    <script src="{{asset('theme/js/modernizr.js')}}"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- For This Page Only -->
    <!-- Base MasterSlider style sheet -->
    <link rel="stylesheet" href="{{asset('theme/js/masterslider/style/masterslider.css')}}" />
    <link rel="stylesheet" href="{{asset('theme/js/masterslider/skins/default/style.css')}}" />
    <link rel="stylesheet" href="{{asset('theme/js/masterslider/style/style.css')}}" />

    <!-- =-=-=-=-=-=-= For This Page Only =-=-=-=-=-=-= -->
    <link rel="stylesheet" href="{{asset('theme/css/dropzone.css')}}">
    <link href="css/jquery.tagsinp{{asset('theme/t.min.css')}}" rel="stylesheet">


{{--
    <link href="image_sprite/ip2location-image-sprite-5.css" rel="stylesheet">
--}}
{{--
    <meta name="csrf-token" content="{{ csrf_token() }}">
--}}

    <style>

.changAcolor:hover {
    color:red !important;
}
        .list-inline-item {
            display: inline-block;
        }

        .listing_list_style {
            margin-top: 18px;
            position: relative;
            text-align: right;
        }

        .mb0 {
            margin-bottom: 0px !important;
        }



        .listing_list_style ul li {
            background-color: #ffffff;
            border: 1px solid rgb(235, 235, 235);
            border-radius: 8px;
            height: 44px;
            line-height: 44px;
            text-align: center;
            width: 49px;
            -webkit-transition: all .3s ease;
            -moz-transition: all .3s ease;
            -o-transition: all .3s ease;
            transition: all .3s ease;
        }



    </style>
</head>
