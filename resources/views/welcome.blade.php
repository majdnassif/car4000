<!DOCTYPE html>
<html dir="ltr" lang="en">
@include('HtmlHeader')

<body>
{{--<div class="wrapper">--}}



@include('header')
{{--@include('Modal')--}}
{{--@include('headerMob')--}}


@yield('content')

@include('footer')

<script>
    $(document).ready(function() {
        @include('globalscript')
        @yield('script')
        // $(document).on('click', '.navigation', function() {
        //     $('#How-It-Works').append('<div class="loading">Loading&#8230;</div>');
        // });

    });





    function showPassword() {
        var x = document.getElementById("password");
        if (x.type === "password") {
            x.type = "text";
        } else {
            x.type = "password";
        }
    };

    function showPhoneFlag(){

        var x = document.getElementById("email_or_phone");
        if(!isNaN(x.value['0'])){
            $('#showHideFlage').show();

        }
        else{
            $('#showHideFlage').hide();
        }

    };

    function showPhoneFlagLogin(){

        var x = document.getElementById("email_phone");
        if(!isNaN(x.value['0'])){

            $('#showHideFlageLogin').show();

        }
        else{
            $('#showHideFlageLogin').hide();
        }

    };


    function hideShowFiltersSale(){
        var x = document.getElementById("condition").value;



        data4 = new FormData();
        data4.append('parent_id',x);
        var countries;

        $.ajax({

            type: "POST",
            url: 'api/carModelsList',
            data: data4,
            // mimeType: 'application/json',
            processData: false,
            contentType: false,
            success: function (res) {
                console.log(res);
                if(res.code == 200){

                    countries = res;
                    $('.modell').empty();
                    $('.modell').append(
                        '<option value="" label="Model">Nothing Selected</option>'
                    );

                    res.data.forEach(function (item, index, arr){

                        $('.modell').append(
                            '<option   value="'+item.id+'" data-id="' + item.id + '">'+item.title+'</option>'
                        );





                    });

                }else if ( res.code < 0){

                }

            },
            error: function (res, response) {
            },
        });



        //////////////





        if(x==1 || x==2){ //NewUsed
           console.log('wowooooooooooooow ',x);

          /* $('.forsaleBoat').hide();
           $('.forsaleHeavy').hide();
           $('.filtersAdvance').hide();
           $('.forsaleMotorcycle').hide();

           $('.forsaleNewUsed').show();
           $('.make_class').show();*/

            $('#saleBoatlength').hide();
            $('#saleBoatType').hide();
           $('#filtersAdvanceSale').hide();
           $('#makeSaleDiv').show();
            $('#ModeSaleDiv').show();

            $('#AdvanceSale').show();



       }

        else if(x==3) { //Heavy
           /* console.log('wowooooooooooooow ',x);
            $('.forsaleNewUsed').hide();
           $('.filtersAdvance').hide();
           $('.forsaleBoat').hide();
           $('.forsaleMotorcycle').hide();


           $('.forsaleHeavy').show();
           $('.make_class').show();*/

            $('#saleBoatlength').hide();
            $('#saleBoatType').hide();
           $('#filtersAdvanceSale').hide();
           $('#AdvanceSale').hide();
            $('#makeSaleDiv').show();
            $('#ModeSaleDiv').show();


        }


        else if(x==4){ //Boat
           /*console.log('wowooooooooooooow ',x);
           $('.forsaleNewUsed').hide();
           $('.filtersAdvance').hide();
           $('.forsaleHeavy').hide();
           $('.forsaleMotorcycle').hide();
           $('.make_class').hide();



           $('.forsaleBoat').show();*/

            $('#makeSaleDiv').hide();
            $('#ModeSaleDiv').hide();
           $('#filtersAdvanceSale').hide();
           $('#AdvanceSale').hide();

           $('#saleBoatlength').show();
            $('#saleBoatType').show();



       }

        else if(x==5){ //Motorcycle
           /*$('.forsaleNewUsed').hide();
           $('.filtersAdvance').hide();
           $('.forsaleHeavy').hide();
           $('.forsaleBoat').hide();


           $('.forsaleMotorcycle').show();
           $('.make_class').show();*/



            $('#saleBoatlength').hide();
            $('#saleBoatType').hide();
            $('#filtersAdvanceSale').hide();
            $('#makeSaleDiv').show();
            $('#ModeSaleDiv').show();

            $('#AdvanceSale').show();

       }


        //////////// get model to selected condition












    }



    function hideShowFiltersRent(){
        var x = document.getElementById("conditionRent").value;
        if(x==1 || x==2){ //NewUsed

            $('#rentBoatlength').hide();
            $('#rentBoatType').hide();
            $('#filtersAdvanceRent').hide();
            $('#makeRentDiv').show();
            $('#ModeRentDiv').show();
            $('#AdvanceRent').show();

        }

        else if(x==3) { //Heavy
            $('#rentBoatlength').hide();
            $('#rentBoatType').hide();
            $('#filtersAdvanceRent').hide();
            $('#AdvanceRent').hide();
            $('#makeRentDiv').show();
            $('#ModeRentDiv').show();

        }


        else if(x==4){ //Boat
            $('#makeRentDiv').hide();
            $('#ModeRentDiv').hide();
            $('#filtersAdvanceRent').hide();
            $('#AdvanceRent').hide();

            $('#rentBoatlength').show();
            $('#rentBoatType').show();


        }

        else if(x==5){ //Motorcycle


            $('#rentBoatType').hide();
            $('#rentBoatlength').hide();
            $('#filtersAdvanceRent').hide();
            $('#makeSaleDiv').show();
            $('#ModeRentDiv').show();

            $('#AdvanceRent').show();

        }


        //////////// get model to selected condition







        data4 = new FormData();
        data4.append('parent_id',x);
        var countries;

        $.ajax({

            type: "POST",
            url: 'api/carModelsList',
            data: data4,
            // mimeType: 'application/json',
            processData: false,
            contentType: false,
            success: function (res) {
                console.log(res);
                if(res.code == 200){

                    countries = res;
                    $('.modell').empty();
                    $('.modell').append(
                        '<option value="" label="Model">Nothing Selected</option>'
                    );

                    res.data.forEach(function (item, index, arr){

                        $('.modell').append(
                            '<option   value="'+item.id+'" data-id="' + item.id + '">'+item.title+'</option>'
                        );





                    });

                }else if ( res.code < 0){

                }

            },
            error: function (res, response) {
            },
        });






    }


    function hideShowFiltersWanted(){
        var x = document.getElementById("conditionWanted").value;
        if(x==1 || x==2){ //NewUsed

            $('#WantedBoatlength').hide();
            $('#WantedBoatType').hide();
            $('#filtersAdvanceWanted').hide();
            $('#makeWantedDiv').show();
            $('#modelWantedDiv').show();
            $('#AdvanceWanted').show();


        }

        else if(x==3) { //Heavy

            $('#WantedBoatlength').hide();
            $('#WantedBoatType').hide();
            $('#filtersAdvanceWanted').hide();
            $('#AdvanceWanted').hide();
            $('#makeWantedDiv').show();
            $('#modelWantedDiv').show();


        }


        else if(x==4){ //Boat
            $('#makeWantedDiv').hide();
            $('#modelWantedDiv').hide();
            $('#filtersAdvanceWanted').hide();
            $('#AdvanceWanted').hide();

            $('#WantedBoatlength').show();
            $('#WantedBoatType').show();


        }

        else if(x==5){ //Motorcycle

            $('#WantedBoatlength').hide();
            $('#WantedBoatType').hide();
            $('#filtersAdvanceWanted').hide();
            $('#makeWantedDiv').show();
            $('#modelWantedDiv').show();

            $('#AdvanceWanted').show();

        }


        //////////// get model to selected condition







        data4 = new FormData();
        data4.append('parent_id',x);
        var countries;

        $.ajax({

            type: "POST",
            url: 'api/carModelsList',
            data: data4,
            // mimeType: 'application/json',
            processData: false,
            contentType: false,
            success: function (res) {
                console.log(res);
                if(res.code == 200){

                    countries = res;
                    $('.modell').empty();
                    $('.modell').append(
                        '<option value="" label="Model">Nothing Selected</option>'
                    );

                    res.data.forEach(function (item, index, arr){

                        $('.modell').append(
                            '<option   value="'+item.id+'" data-id="' + item.id + '">'+item.title+'</option>'
                        );





                    });

                }else if ( res.code < 0){

                }

            },
            error: function (res, response) {
            },
        });






    }


    function hideShowFiltersOffers(){


        var x = document.getElementById("conditionOffers").value;
        if(x==1 || x==2){ //NewUsed

            $('#offersBoatlength').hide();
            $('#offersBoatType').hide();
            $('#filtersAdvanceOffers').hide();
            $('#makeOffersDiv').show();
            $('#modelOffersDiv').show();
            $('#AdvanceOffers').show();


        }

        else if(x==3) { //Heavy

            $('#offersBoatlength').hide();
            $('#offersBoatType').hide();
            $('#filtersAdvanceOffers').hide();
            $('#AdvanceOffers').hide();
            $('#makeOffersDiv').show();
            $('#modelOffersDiv').show();


        }


        else if(x==4){ //Boat
            $('#makeOffersDiv').hide();
            $('#modelOffersDiv').hide();
            $('#filtersAdvanceOffers').hide();
            $('#AdvanceOffers').hide();

            $('#offersBoatlength').show();
            $('#offersBoatType').show();


        }

        else if(x==5){ //Motorcycle

            $('#offersBoatlength').hide();
            $('#offersBoatType').hide();
            $('#filtersAdvanceOffers').hide();
            $('#makeOffersDiv').show();
            $('#modelOffersDiv').show();

            $('#AdvanceOffers').show();

        }


        //////////// get model to selected condition







        data4 = new FormData();
        data4.append('parent_id',x);
        var countries;

        $.ajax({

            type: "POST",
            url: 'api/carModelsList',
            data: data4,
            // mimeType: 'application/json',
            processData: false,
            contentType: false,
            success: function (res) {
                console.log(res);
                if(res.code == 200){

                    countries = res;
                    $('.modell').empty();
                    $('.modell').append(
                        '<option value="" label="Model">Nothing Selected</option>'
                    );

                    res.data.forEach(function (item, index, arr){

                        $('.modell').append(
                            '<option   value="'+item.id+'" data-id="' + item.id + '">'+item.title+'</option>'
                        );





                    });

                }else if ( res.code < 0){

                }

            },
            error: function (res, response) {
            },
        });




    }


  /* function callGetModel(){
       var y = document.getElementById("condition").value;
        console.log('selectttttttttted',y);
   }*/



</script>





</body>
</html>
