@extends('welcome')

@section('content')

    <!-- =-=-=-=-=-=-= Breadcrumb =-=-=-=-=-=-= -->
    <div class="page-header-area-2 gray">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="small-breadcrumb">
                        <div class=" breadcrumb-link">
                            <ul>
                                <li><a href="index.html">Home Page</a></li>
                                <li><a class="active" href="#">Favourite</a></li>
                            </ul>
                        </div>
                        <div class="header-page">
                            <h1>User Dashboard</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- =-=-=-=-=-=-= Breadcrumb End =-=-=-=-=-=-= -->
    <!-- =-=-=-=-=-=-= Main Content Area =-=-=-=-=-=-= -->
    <div class="main-content-area clearfix">
        <!-- =-=-=-=-=-=-= Latest Ads =-=-=-=-=-=-= -->
        <section class="custom-padding no-top gray">
            <!-- Main Container -->
            <div class="container">
                <!-- Row -->
{{--                <div class="row">--}}
{{--                    <!-- Middle Content Area -->--}}
{{--                    <div class="col-md-12 col-xs-12 col-sm-12">--}}
{{--                        <section class="search-result-item">--}}
{{--                            <a class="image-link" href="#"><img class="image center-block" alt="" src="{{asset('theme/images/users/9.jpg')}}"> </a>--}}
{{--                            <div class="search-result-item-body">--}}
{{--                                <div class="row">--}}
{{--                                    <div class="col-md-5 col-sm-12 col-xs-12">--}}
{{--                                        <h4 class="search-result-item-heading"><a href="#">Umair</a></h4>--}}
{{--                                        <p class="info">--}}
{{--                                            <span><a href="profile.html"><i class="fa fa-user "></i>Profile </a></span>--}}
{{--                                            <span><a href="javascript:void(0)"><i class="fa fa-edit"></i>Edit Profile </a></span>--}}
{{--                                        </p>--}}
{{--                                        <p class="description">You last logged in at: 14-01-2017 6:40 AM [ USA time (GMT + 6:00hrs)</p>--}}
{{--                                        <span class="label label-warning">Paid Package</span>--}}
{{--                                        <span class="label label-success">Dealer</span>--}}
{{--                                    </div>--}}
{{--                                    <div class="col-md-7 col-sm-12 col-xs-12">--}}
{{--                                        <div class="row ad-history">--}}
{{--                                            <div class="col-md-4 col-sm-4 col-xs-12">--}}
{{--                                                <div class="user-stats">--}}
{{--                                                    <h2>374</h2>--}}
{{--                                                    <small>Ad Sold</small>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                            <div class="col-md-4 col-sm-4 col-xs-12">--}}
{{--                                                <div class="user-stats">--}}
{{--                                                    <h2>980</h2>--}}
{{--                                                    <small>Total Listings</small>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                            <div class="col-md-4 col-sm-4 col-xs-12">--}}
{{--                                                <div class="user-stats">--}}
{{--                                                    <h2>413</h2>--}}
{{--                                                    <small>Favourites Ads</small>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </section>--}}
{{--                        <div class="dashboard-menu-container">--}}
{{--                            <ul>--}}
{{--                                <li>--}}
{{--                                    <a href="profile.html">--}}
{{--                                        <div class="menu-name"> Profile </div>--}}
{{--                                    </a>--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--                                    <a href="archives.html">--}}
{{--                                        <div class="menu-name">Archives</div>--}}
{{--                                    </a>--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--                                    <a href="active-ads.html">--}}
{{--                                        <div class="menu-name">My Ads</div>--}}
{{--                                    </a>--}}
{{--                                </li>--}}
{{--                                <li  class="active">--}}
{{--                                    <a href="favourite.html">--}}
{{--                                        <div class="menu-name">Favourites Ads</div>--}}
{{--                                    </a>--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--                                    <a href="messages.html">--}}
{{--                                        <div class="menu-name">Messages</div>--}}
{{--                                    </a>--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--                                    <a href="deactive.html">--}}
{{--                                        <div class="menu-name">Close Account</div>--}}
{{--                                    </a>--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--                                    <a href="#">--}}
{{--                                        <div class="menu-name">Logout</div>--}}
{{--                                    </a>--}}
{{--                                </li>--}}
{{--                            </ul>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <!-- Middle Content Area  End -->--}}
{{--                </div>--}}
                <!-- Row End -->

                <div class="row margin-top-40">
                    <!-- Middle Content Area -->
                    <div class="col-md-12 col-lg-12 col-sx-12">
                        <div id="ccardiv">

                        </div>

                        <form id="carrAdsDetailForm">

                        </form>
                        <!-- Row -->
{{--                        <ul class="list-unstyled">--}}
{{--                            <!-- Listing Grid -->--}}
{{--                            <li>--}}
{{--                                <div class="well ad-listing clearfix">--}}
{{--                                    <div class="col-md-3 col-sm-5 col-xs-12 grid-style no-padding">--}}
{{--                                        <!-- Image Box -->--}}
{{--                                        <div class="img-box">--}}
{{--                                            <img src="{{asset('theme/images/posting/25.jpg')}}" class="img-responsive" alt="">--}}
{{--                                            <div class="total-images"><strong>8</strong> photos </div>--}}
{{--                                            <div class="quick-view"> <a href="#ad-preview" data-toggle="modal" class="view-button"><i class="fa fa-search"></i></a> </div>--}}
{{--                                        </div>--}}
{{--                                        <!-- Ad Status --><span class="ad-status"> Featured </span>--}}
{{--                                        <!-- User Preview -->--}}
{{--                                        <div class="user-preview">--}}
{{--                                            <a href="#"> <img src="{{asset('theme/images/users/2.jpg')}}" class="avatar avatar-small" alt=""> </a>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="col-md-9 col-sm-7 col-xs-12">--}}
{{--                                        <!-- Ad Content-->--}}
{{--                                        <div class="row">--}}
{{--                                            <div class="content-area">--}}
{{--                                                <div class="col-md-9 col-sm-12 col-xs-12">--}}
{{--                                                    <!-- Ad Title -->--}}
{{--                                                    <h3><a>2016 Audi A6 2.0T Quattro Premium Plus</a></h3>--}}
{{--                                                    <!-- Ad Meta Info -->--}}
{{--                                                    <ul class="ad-meta-info">--}}
{{--                                                        <li> <i class="fa fa-map-marker"></i><a href="#">London</a> </li>--}}
{{--                                                        <li>15 minutes ago </li>--}}
{{--                                                    </ul>--}}
{{--                                                    <!-- Ad Description-->--}}
{{--                                                    <div class="ad-details">--}}
{{--                                                        <p>Lorem ipsum dolor sit amet consectetur adiscing das elited ultricies facilisis lacinia pell das elited ultricies facilisis ... </p>--}}
{{--                                                        <ul class="list-unstyled">--}}
{{--                                                            <li><i class="flaticon-gas-station-1"></i>Diesel</li>--}}
{{--                                                            <li><i class="flaticon-dashboard"></i>35,000 km</li>--}}
{{--                                                            <li><i class="flaticon-engine-2"></i>1800 cc</li>--}}
{{--                                                            <li><i class="flaticon-key"></i>Manual</li>--}}
{{--                                                            <li><i class="flaticon-calendar-2"></i>Year 2002</li>--}}
{{--                                                        </ul>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="col-md-3 col-xs-12 col-sm-12">--}}
{{--                                                    <!-- Ad Stats -->--}}
{{--                                                    <div class="short-info">--}}
{{--                                                        <div class="ad-stats hidden-xs"><span>Condition  : </span>Used</div>--}}
{{--                                                        <div class="ad-stats hidden-xs"><span>Type : </span>Coupe</div>--}}
{{--                                                        <div class="ad-stats hidden-xs"><span>Make : </span>Audi</div>--}}
{{--                                                    </div>--}}
{{--                                                    <!-- Price -->--}}
{{--                                                    <div class="price"> <span>$18,640</span> </div>--}}
{{--                                                    <!-- Ad View Button -->--}}
{{--                                                    <button class="btn btn-block btn-default"><i class="fa fa-times" aria-hidden="true"></i> UnFavorite</button>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <!-- Ad Content End -->--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </li>--}}




                            <!-- Listing Grid -->
{{--                            <li>--}}
{{--                                <div class="well ad-listing clearfix">--}}
{{--                                    <div class="col-md-3 col-sm-5 col-xs-12 grid-style no-padding">--}}
{{--                                        <!-- Image Box -->--}}
{{--                                        <div class="img-box">--}}
{{--                                            <img src="{{asset('theme/images/posting/26.jpg')}}" class="img-responsive" alt="">--}}
{{--                                            <div class="total-images"><strong>4</strong> photos </div>--}}
{{--                                            <div class="quick-view"> <a href="#ad-preview" data-toggle="modal" class="view-button"><i class="fa fa-search"></i></a> </div>--}}
{{--                                        </div>--}}
{{--                                        <!-- Ad Status --><span class="ad-status"> Featured </span>--}}
{{--                                        <!-- User Preview -->--}}
{{--                                        <div class="user-preview">--}}
{{--                                            <a href="#"> <img src="{{asset('theme/images/users/6.jpg')}}" class="avatar avatar-small" alt=""> </a>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="col-md-9 col-sm-7 col-xs-12">--}}
{{--                                        <!-- Ad Content-->--}}
{{--                                        <div class="row">--}}
{{--                                            <div class="content-area">--}}
{{--                                                <div class="col-md-9 col-sm-12 col-xs-12">--}}
{{--                                                    <!-- Ad Title -->--}}
{{--                                                    <h3><a>2010 Ford Shelby GT500 Coupe</a></h3>--}}
{{--                                                    <!-- Ad Meta Info -->--}}
{{--                                                    <ul class="ad-meta-info">--}}
{{--                                                        <li> <i class="fa fa-map-marker"></i><a href="#">London</a> </li>--}}
{{--                                                        <li>15 minutes ago </li>--}}
{{--                                                    </ul>--}}
{{--                                                    <!-- Ad Description-->--}}
{{--                                                    <div class="ad-details">--}}
{{--                                                        <p>Lorem ipsum dolor sit amet consectetur adiscing das elited ultricies facilisis lacinia pell das elited ultricies facilisis ... </p>--}}
{{--                                                        <ul class="list-unstyled">--}}
{{--                                                            <li><i class="flaticon-gas-station-1"></i>Diesel</li>--}}
{{--                                                            <li><i class="flaticon-dashboard"></i>35,000 km</li>--}}
{{--                                                            <li><i class="flaticon-engine-2"></i>1800 cc</li>--}}
{{--                                                            <li><i class="flaticon-key"></i>Manual</li>--}}
{{--                                                            <li><i class="flaticon-calendar-2"></i>Year 2002</li>--}}
{{--                                                        </ul>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="col-md-3 col-xs-12 col-sm-12">--}}
{{--                                                    <!-- Ad Stats -->--}}
{{--                                                    <div class="short-info">--}}
{{--                                                        <div class="ad-stats hidden-xs"><span>Condition  : </span>Used</div>--}}
{{--                                                        <div class="ad-stats hidden-xs"><span>Warranty : </span>7 Days</div>--}}
{{--                                                        <div class="ad-stats hidden-xs"><span>Sub Category : </span>Mobiles</div>--}}
{{--                                                    </div>--}}
{{--                                                    <!-- Price -->--}}
{{--                                                    <div class="price"> <span>$900</span> </div>--}}
{{--                                                    <!-- Ad View Button -->--}}

{{--                                                    <button class="btn btn-block btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View Ad.</button>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <!-- Ad Content End -->--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </li>--}}
                            <!-- Listing Grid -->
{{--                            <li>--}}
{{--                                <div class="well ad-listing clearfix">--}}
{{--                                    <div class="col-md-3 col-sm-5 col-xs-12 grid-style no-padding">--}}
{{--                                        <!-- Image Box -->--}}
{{--                                        <div class="img-box">--}}
{{--                                            <img src="{{asset('theme/images/posting/7.jpg')}}" class="img-responsive" alt="">--}}
{{--                                            <div class="total-images"><strong>5</strong> photos </div>--}}
{{--                                            <div class="quick-view"> <a href="#ad-preview" data-toggle="modal" class="view-button"><i class="fa fa-search"></i></a> </div>--}}
{{--                                        </div>--}}
{{--                                        <!-- User Preview -->--}}
{{--                                        <div class="user-preview">--}}
{{--                                            <a href="#"> <img src="{{asset('theme/images/users/5.jpg')}}" class="avatar avatar-small" alt=""> </a>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="col-md-9 col-sm-7 col-xs-12">--}}
{{--                                        <!-- Ad Content-->--}}
{{--                                        <div class="row">--}}
{{--                                            <div class="content-area">--}}
{{--                                                <div class="col-md-9 col-sm-12 col-xs-12">--}}
{{--                                                    <!-- Ad Title -->--}}
{{--                                                    <h3><a> 2010 Lamborghini Gallardo Spyder</a></h3>--}}
{{--                                                    <!-- Ad Meta Info -->--}}
{{--                                                    <ul class="ad-meta-info">--}}
{{--                                                        <li> <i class="fa fa-map-marker"></i><a href="#">London</a> </li>--}}
{{--                                                        <li>15 minutes ago </li>--}}
{{--                                                    </ul>--}}
{{--                                                    <!-- Ad Description-->--}}
{{--                                                    <div class="ad-details">--}}
{{--                                                        <p>Lorem ipsum dolor sit amet consectetur adiscing das elited ultricies facilisis lacinia pell das elited ultricies facilisis ... </p>--}}
{{--                                                        <ul class="list-unstyled">--}}
{{--                                                            <li><i class="flaticon-gas-station-1"></i>Diesel</li>--}}
{{--                                                            <li><i class="flaticon-dashboard"></i>35,000 km</li>--}}
{{--                                                            <li><i class="flaticon-engine-2"></i>1800 cc</li>--}}
{{--                                                            <li><i class="flaticon-key"></i>Manual</li>--}}
{{--                                                            <li><i class="flaticon-calendar-2"></i>Year 2002</li>--}}
{{--                                                        </ul>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="col-md-3 col-xs-12 col-sm-12">--}}
{{--                                                    <!-- Ad Stats -->--}}
{{--                                                    <div class="short-info">--}}
{{--                                                        <div class="ad-stats hidden-xs"><span>Condition  : </span>Used</div>--}}
{{--                                                        <div class="ad-stats hidden-xs"><span>Warranty : </span>7 Days</div>--}}
{{--                                                        <div class="ad-stats hidden-xs"><span>Sub Category : </span>Mobiles</div>--}}
{{--                                                    </div>--}}
{{--                                                    <!-- Price -->--}}
{{--                                                    <div class="price"> <span>$120</span> </div>--}}
{{--                                                    <!-- Ad View Button -->--}}
{{--                                                    <button class="btn btn-block btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View Ad.</button>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <!-- Ad Content End -->--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </li>--}}
                            <!-- Listing Grid -->
{{--                            <li>--}}
{{--                                <div class="well ad-listing clearfix">--}}
{{--                                    <div class="col-md-3 col-sm-5 col-xs-12 grid-style no-padding">--}}
{{--                                        <!-- Image Box -->--}}
{{--                                        <div class="img-box">--}}
{{--                                            <img src="{{asset('theme/images/posting/2.jpg')}}" class="img-responsive" alt="">--}}
{{--                                            <div class="total-images"><strong>3</strong> photos </div>--}}
{{--                                            <div class="quick-view"> <a href="#ad-preview" data-toggle="modal" class="view-button"><i class="fa fa-search"></i></a> </div>--}}
{{--                                        </div>--}}
{{--                                        <!-- User Preview -->--}}
{{--                                        <div class="user-preview">--}}
{{--                                            <a href="#"> <img src="{{asset('theme/images/users/3.jpg')}}" class="avatar avatar-small" alt=""> </a>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="col-md-9 col-sm-7 col-xs-12">--}}
{{--                                        <!-- Ad Content-->--}}
{{--                                        <div class="row">--}}
{{--                                            <div class="content-area">--}}
{{--                                                <div class="col-md-9 col-sm-12 col-xs-12">--}}
{{--                                                    <!-- Ad Title -->--}}
{{--                                                    <h3><a> Porsche 911 Carrera 2017 </a></h3>--}}
{{--                                                    <!-- Ad Meta Info -->--}}
{{--                                                    <ul class="ad-meta-info">--}}
{{--                                                        <li> <i class="fa fa-map-marker"></i><a href="#">London</a> </li>--}}
{{--                                                        <li>15 minutes ago </li>--}}
{{--                                                    </ul>--}}
{{--                                                    <!-- Ad Description-->--}}
{{--                                                    <div class="ad-details">--}}
{{--                                                        <p>Lorem ipsum dolor sit amet consectetur adiscing das elited ultricies facilisis lacinia pell das elited ultricies facilisis ... </p>--}}
{{--                                                        <ul class="list-unstyled">--}}
{{--                                                            <li><i class="flaticon-gas-station-1"></i>Diesel</li>--}}
{{--                                                            <li><i class="flaticon-dashboard"></i>35,000 km</li>--}}
{{--                                                            <li><i class="flaticon-engine-2"></i>1800 cc</li>--}}
{{--                                                            <li><i class="flaticon-key"></i>Manual</li>--}}
{{--                                                            <li><i class="flaticon-calendar-2"></i>Year 2002</li>--}}
{{--                                                        </ul>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="col-md-3 col-xs-12 col-sm-12">--}}
{{--                                                    <!-- Ad Stats -->--}}
{{--                                                    <div class="short-info">--}}
{{--                                                        <div class="ad-stats hidden-xs"><span>Condition  : </span>Used</div>--}}
{{--                                                        <div class="ad-stats hidden-xs"><span>Warranty : </span>7 Days</div>--}}
{{--                                                        <div class="ad-stats hidden-xs"><span>Sub Category : </span>Mobiles</div>--}}
{{--                                                    </div>--}}
{{--                                                    <!-- Price -->--}}
{{--                                                    <div class="price"> <span>$1,129</span> </div>--}}
{{--                                                    <!-- Ad View Button -->--}}
{{--                                                    <button class="btn btn-block btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View Ad.</button>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <!-- Ad Content End -->--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </li>--}}
                            <!-- Listing Grid -->
{{--                            <li>--}}
{{--                                <div class="well ad-listing clearfix">--}}
{{--                                    <div class="col-md-3 col-sm-5 col-xs-12 grid-style no-padding">--}}
{{--                                        <!-- Image Box -->--}}
{{--                                        <div class="img-box">--}}
{{--                                            <img src="{{asset('theme/images/posting/15.jpg')}}" class="img-responsive" alt="">--}}
{{--                                            <div class="total-images"><strong>4</strong> photos </div>--}}
{{--                                            <div class="quick-view"> <a href="#ad-preview" data-toggle="modal" class="view-button"><i class="fa fa-search"></i></a> </div>--}}
{{--                                        </div>--}}
{{--                                        <!-- Ad Status --><span class="ad-status"> Featured </span>--}}
{{--                                        <!-- User Preview -->--}}
{{--                                        <div class="user-preview">--}}
{{--                                            <a href="#"> <img src="{{asset('theme/images/users/7.jpg')}}" class="avatar avatar-small" alt=""> </a>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="col-md-9 col-sm-7 col-xs-12">--}}
{{--                                        <!-- Ad Content-->--}}
{{--                                        <div class="row">--}}
{{--                                            <div class="content-area">--}}
{{--                                                <div class="col-md-9 col-sm-12 col-xs-12">--}}
{{--                                                    <!-- Ad Title -->--}}
{{--                                                    <h3><a>Bugatti Veyron Super Sport </a></h3>--}}
{{--                                                    <!-- Ad Meta Info -->--}}
{{--                                                    <ul class="ad-meta-info">--}}
{{--                                                        <li> <i class="fa fa-map-marker"></i><a href="#">London</a> </li>--}}
{{--                                                        <li>15 minutes ago </li>--}}
{{--                                                    </ul>--}}
{{--                                                    <!-- Ad Description-->--}}
{{--                                                    <div class="ad-details">--}}
{{--                                                        <p>Lorem ipsum dolor sit amet consectetur adiscing das elited ultricies facilisis lacinia pell das elited ultricies facilisis ... </p>--}}
{{--                                                        <ul class="list-unstyled">--}}
{{--                                                            <li><i class="flaticon-gas-station-1"></i>Diesel</li>--}}
{{--                                                            <li><i class="flaticon-dashboard"></i>35,000 km</li>--}}
{{--                                                            <li><i class="flaticon-engine-2"></i>1800 cc</li>--}}
{{--                                                            <li><i class="flaticon-key"></i>Manual</li>--}}
{{--                                                            <li><i class="flaticon-calendar-2"></i>Year 2002</li>--}}
{{--                                                        </ul>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="col-md-3 col-xs-12 col-sm-12">--}}
{{--                                                    <!-- Ad Stats -->--}}
{{--                                                    <div class="short-info">--}}
{{--                                                        <div class="ad-stats hidden-xs"><span>Condition  : </span>Used</div>--}}
{{--                                                        <div class="ad-stats hidden-xs"><span>Warranty : </span>7 Days</div>--}}
{{--                                                        <div class="ad-stats hidden-xs"><span>Sub Category : </span>Mobiles</div>--}}
{{--                                                    </div>--}}
{{--                                                    <!-- Price -->--}}
{{--                                                    <div class="price"> <span>$350</span> </div>--}}
{{--                                                    <!-- Ad View Button -->--}}
{{--                                                    <button class="btn btn-block btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View Ad.</button>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <!-- Ad Content End -->--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </li>--}}
                            <!-- Listing Grid -->
{{--                            <li>--}}
{{--                                <div class="well ad-listing clearfix">--}}
{{--                                    <div class="col-md-3 col-sm-5 col-xs-12 grid-style no-padding">--}}
{{--                                        <!-- Image Box -->--}}
{{--                                        <div class="img-box">--}}
{{--                                            <img src="{{asset('theme/images/posting/28.jpg')}}" class="img-responsive" alt="">--}}
{{--                                            <div class="total-images"><strong>4</strong> photos </div>--}}
{{--                                            <div class="quick-view"> <a href="#ad-preview" data-toggle="modal" class="view-button"><i class="fa fa-search"></i></a> </div>--}}
{{--                                        </div>--}}
{{--                                        <!-- Ad Status --><span class="ad-status"> Featured </span>--}}
{{--                                        <!-- User Preview -->--}}
{{--                                        <div class="user-preview">--}}
{{--                                            <a href="#"> <img src="{{asset('theme/images/users/6.jpg')}}" class="avatar avatar-small" alt=""> </a>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="col-md-9 col-sm-7 col-xs-12">--}}
{{--                                        <!-- Ad Content-->--}}
{{--                                        <div class="row">--}}
{{--                                            <div class="content-area">--}}
{{--                                                <div class="col-md-9 col-sm-12 col-xs-12">--}}
{{--                                                    <!-- Ad Title -->--}}
{{--                                                    <h3><a>Audi A4 2.0T Quattro Premium</a></h3>--}}
{{--                                                    <!-- Ad Meta Info -->--}}
{{--                                                    <ul class="ad-meta-info">--}}
{{--                                                        <li> <i class="fa fa-map-marker"></i><a href="#">London</a> </li>--}}
{{--                                                        <li>15 minutes ago </li>--}}
{{--                                                    </ul>--}}
{{--                                                    <!-- Ad Description-->--}}
{{--                                                    <div class="ad-details">--}}
{{--                                                        <p>Lorem ipsum dolor sit amet consectetur adiscing das elited ultricies facilisis lacinia pell das elited ultricies facilisis ... </p>--}}
{{--                                                        <ul class="list-unstyled">--}}
{{--                                                            <li><i class="flaticon-gas-station-1"></i>Diesel</li>--}}
{{--                                                            <li><i class="flaticon-dashboard"></i>35,000 km</li>--}}
{{--                                                            <li><i class="flaticon-engine-2"></i>1800 cc</li>--}}
{{--                                                            <li><i class="flaticon-key"></i>Manual</li>--}}
{{--                                                            <li><i class="flaticon-calendar-2"></i>Year 2002</li>--}}
{{--                                                        </ul>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="col-md-3 col-xs-12 col-sm-12">--}}
{{--                                                    <!-- Ad Stats -->--}}
{{--                                                    <div class="short-info">--}}
{{--                                                        <div class="ad-stats hidden-xs"><span>Condition  : </span>Used</div>--}}
{{--                                                        <div class="ad-stats hidden-xs"><span>Warranty : </span>7 Days</div>--}}
{{--                                                        <div class="ad-stats hidden-xs"><span>Sub Category : </span>Computer & Laptops</div>--}}
{{--                                                    </div>--}}
{{--                                                    <!-- Price -->--}}
{{--                                                    <div class="price"> <span>$250</span> </div>--}}
{{--                                                    <!-- Ad View Button -->--}}
{{--                                                    <button class="btn btn-block btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View Ad.</button>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <!-- Ad Content End -->--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </li>--}}
                            <!-- Listing Grid -->
{{--                            <li>--}}
{{--                                <div class="well ad-listing clearfix">--}}
{{--                                    <div class="col-md-3 col-sm-5 col-xs-12 grid-style no-padding">--}}
{{--                                        <!-- Image Box -->--}}
{{--                                        <div class="img-box">--}}
{{--                                            <img src="{{asset('theme/images/posting/13.jpg')}}" class="img-responsive" alt="">--}}
{{--                                            <div class="total-images"><strong>4</strong> photos </div>--}}
{{--                                            <div class="quick-view"> <a href="#ad-preview" data-toggle="modal" class="view-button"><i class="fa fa-search"></i></a> </div>--}}
{{--                                        </div>--}}
{{--                                        <!-- Ad Status --><span class="ad-status"> Featured </span>--}}
{{--                                        <!-- User Preview -->--}}
{{--                                        <div class="user-preview">--}}
{{--                                            <a href="#"> <img src="{{asset('theme/images/users/7.jpg')}}" class="avatar avatar-small" alt=""> </a>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="col-md-9 col-sm-7 col-xs-12">--}}
{{--                                        <!-- Ad Content-->--}}
{{--                                        <div class="row">--}}
{{--                                            <div class="content-area">--}}
{{--                                                <div class="col-md-9 col-sm-12 col-xs-12">--}}
{{--                                                    <!-- Ad Title -->--}}
{{--                                                    <h3><a>Audi Q3 2.0T Premium Plus</a></h3>--}}
{{--                                                    <!-- Ad Meta Info -->--}}
{{--                                                    <ul class="ad-meta-info">--}}
{{--                                                        <li> <i class="fa fa-map-marker"></i><a href="#">London</a> </li>--}}
{{--                                                        <li>15 minutes ago </li>--}}
{{--                                                    </ul>--}}
{{--                                                    <!-- Ad Description-->--}}
{{--                                                    <div class="ad-details">--}}
{{--                                                        <p>Lorem ipsum dolor sit amet consectetur adiscing das elited ultricies facilisis lacinia pell das elited ultricies facilisis ... </p>--}}
{{--                                                        <ul class="list-unstyled">--}}
{{--                                                            <li><i class="flaticon-gas-station-1"></i>Diesel</li>--}}
{{--                                                            <li><i class="flaticon-dashboard"></i>35,000 km</li>--}}
{{--                                                            <li><i class="flaticon-engine-2"></i>1800 cc</li>--}}
{{--                                                            <li><i class="flaticon-key"></i>Manual</li>--}}
{{--                                                            <li><i class="flaticon-calendar-2"></i>Year 2002</li>--}}
{{--                                                        </ul>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="col-md-3 col-xs-12 col-sm-12">--}}
{{--                                                    <!-- Ad Stats -->--}}
{{--                                                    <div class="short-info">--}}
{{--                                                        <div class="ad-stats hidden-xs"><span>Condition  : </span>Used</div>--}}
{{--                                                        <div class="ad-stats hidden-xs"><span>Warranty : </span>7 Days</div>--}}
{{--                                                        <div class="ad-stats hidden-xs"><span>Sub Category : </span>Laptops</div>--}}
{{--                                                    </div>--}}
{{--                                                    <!-- Price -->--}}
{{--                                                    <div class="price"> <span>$440</span> </div>--}}
{{--                                                    <!-- Ad View Button -->--}}
{{--                                                    <button class="btn btn-block btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View Ad.</button>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <!-- Ad Content End -->--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </li>--}}
                            <!-- Listing Grid -->
{{--                            <li>--}}
{{--                                <div class="well ad-listing clearfix">--}}
{{--                                    <div class="col-md-3 col-sm-5 col-xs-12 grid-style no-padding">--}}
{{--                                        <!-- Image Box -->--}}
{{--                                        <div class="img-box">--}}
{{--                                            <img src="{{asset('theme/images/posting/27.jpg')}}" class="img-responsive" alt="">--}}
{{--                                            <div class="total-images"><strong>4</strong> photos </div>--}}
{{--                                            <div class="quick-view"> <a href="#ad-preview" data-toggle="modal" class="view-button"><i class="fa fa-search"></i></a> </div>--}}
{{--                                        </div>--}}
{{--                                        <!-- Ad Status --><span class="ad-status"> Featured </span>--}}
{{--                                        <!-- User Preview -->--}}
{{--                                        <div class="user-preview">--}}
{{--                                            <a href="#"> <img src="{{asset('theme/images/users/4.jpg')}}" class="avatar avatar-small" alt=""> </a>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="col-md-9 col-sm-7 col-xs-12">--}}
{{--                                        <!-- Ad Content-->--}}
{{--                                        <div class="row">--}}
{{--                                            <div class="content-area">--}}
{{--                                                <div class="col-md-9 col-sm-12 col-xs-12">--}}
{{--                                                    <!-- Ad Title -->--}}
{{--                                                    <h3><a>2014 Ford Fusion Titanium</a></h3>--}}
{{--                                                    <!-- Ad Meta Info -->--}}
{{--                                                    <ul class="ad-meta-info">--}}
{{--                                                        <li> <i class="fa fa-map-marker"></i><a href="#">London</a> </li>--}}
{{--                                                        <li>15 minutes ago </li>--}}
{{--                                                    </ul>--}}
{{--                                                    <!-- Ad Description-->--}}
{{--                                                    <div class="ad-details">--}}
{{--                                                        <p>Lorem ipsum dolor sit amet consectetur adiscing das elited ultricies facilisis lacinia pell das elited ultricies facilisis ... </p>--}}
{{--                                                        <ul class="list-unstyled">--}}
{{--                                                            <li><i class="flaticon-gas-station-1"></i>Diesel</li>--}}
{{--                                                            <li><i class="flaticon-dashboard"></i>35,000 km</li>--}}
{{--                                                            <li><i class="flaticon-engine-2"></i>1800 cc</li>--}}
{{--                                                            <li><i class="flaticon-key"></i>Manual</li>--}}
{{--                                                            <li><i class="flaticon-calendar-2"></i>Year 2002</li>--}}
{{--                                                        </ul>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                                <div class="col-md-3 col-xs-12 col-sm-12">--}}
{{--                                                    <!-- Ad Stats -->--}}
{{--                                                    <div class="short-info">--}}
{{--                                                        <div class="ad-stats hidden-xs"><span>Condition  : </span>Used</div>--}}
{{--                                                        <div class="ad-stats hidden-xs"><span>Warranty : </span>7 Days</div>--}}
{{--                                                        <div class="ad-stats hidden-xs"><span>Sub Category : </span>Furniture</div>--}}
{{--                                                    </div>--}}
{{--                                                    <!-- Price -->--}}
{{--                                                    <div class="price"> <span>$110,50</span> </div>--}}
{{--                                                    <!-- Ad View Button -->--}}
{{--                                                    <button class="btn btn-block btn-success"><i class="fa fa-eye" aria-hidden="true"></i> View Ad.</button>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <!-- Ad Content End -->--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
                        <!-- Advertizing -->
                        <section class="advertising">
                            <a href="post-ad-1.html">
{{--                                <div class="banner">--}}
{{--                                    <div class="wrapper">--}}
{{--                                        <span class="title">Do you want your property to be listed here?</span>--}}
{{--                                        <span class="submit">Submit it now! <i class="fa fa-plus-square"></i></span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
                                <!-- /.banner-->
                            </a>
                        </section>
                        <!-- Advertizing End -->
                        <!-- Ads Archive End -->
                        <div class="clearfix"></div>
                        <!-- Pagination -->
                        <div class="col-md-12 col-xs-12 col-sm-12">
                            <ul class="pagination pagination-lg">
                                <li> <a href="#"> <i class="fa fa-chevron-left" aria-hidden="true"></i></a></li>
                                <li> <a href="#">1</a> </li>
                                <li class="active"> <a href="#">2</a> </li>
                                <li> <a href="#">3</a> </li>
                                <li> <a href="#">4</a> </li>
                                <li><a href="#"> <i class="fa fa-chevron-right" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                        <!-- Pagination End -->
                    </div>
                    <!-- Middle Content Area  End -->
                </div>
            </div>
            <!-- Main Container End -->
        </section>
        <!-- =-=-=-=-=-=-= Ads Archives End =-=-=-=-=-=-= -->
    </div>
@endsection




@section('script')



    var lenght = 1;
    var pageSize = 3;
    var selectedPage = 1;
    var localstorageVar = 'Product_page';


    function getData(pageIndex, filter) {

    var userToken =   sessionStorage.getItem("api_token")

    if(userToken) {



    filter.append('app_id','3');
    filter.append('offset','0');
    filter.append('limit','10');
    console.log(filter,userToken);

    $.ajax({

    type: "POST",
    url: Global_Url + 'getSavedAdsCars',
    data: filter,
    // mimeType: 'application/json',
    processData: false,
    contentType: false,
    headers: {"Authorization": 'Bearer ' + userToken},

    success: function (res) {
    console.log('nawraaaaaaas ',res);

    sessionStorage.setItem('adsData',res.data);



    $('#ccardiv').empty();


    var selectHtml = '';
    res.data.savedAds.forEach(function (item, index, arr){

    console.log('item',item);
    selectHtml+= '  <div   class="col-md-6 col-xs-12 col-sm-6">'+
        '  <div  class="category-grid-box">'+
            '<div class="category-grid-img">'+
                '<img id="img-responsive_id" style="max-height:204px" class="img-responsive"  alt="" src="http://4000.global/public/img/'+item.ads.thumbnail+ '">'+
{{--                '<div class="user-preview">'+--}}
{{--                    '<a data-id="'+item.ads.id +'" class="save_car_item_id"> <img src="https://img.icons8.com/plasticine/100/000000/add-to-favorites.png"/> </a>'+--}}
{{--                    '</div>'+--}}
                '<a data-id="'+item.ads.id +'" class="view-details carr_item_id">View Details</a>'+
                '<div class="additional-information">'+
                    '<p></p>'+
                    '<p>'+"Owner: "+ item.ads.owner.name +'</p>'+
                    '<p>'+"town: "+ item.ads.town.name +'</p>'+
                    '<p>'+"country: "+ item.ads.town.country.name +'</p>'+
                    '<p>'+"type: "+ item.ads.town.type +'</p>'+

                    '</div> </div><div class="short-description">'+
                '<div class="category-title"> <span>'+item.ads.title +'</span> </div>'+
                '<h3><a title="" href="single-page-listing.html">  </a>'+ item.ads.description+'</h3>';
                if(item.cars_attributes!=null){
                selectHtml+=  '<div class="price">'+ item.ads.cars_attributes.price +'<span class="negotiable"></span></div>';
                }

                selectHtml+=  '</div>';
            if(item.ads.cars_attributes !=null){
            selectHtml+=
            '<div class="ad-info">'+
                '<ul>'+
                    '<li><i class="flaticon-fuel-1"></i>'+item.ads.cars_attributes.fuel.title+'</li>'+
                    '<li><i class="flaticon-dashboard"></i>35,000 km</li>'+
                    '<li><i class="flaticon-engine-2"></i>'+ item.ads.cars_attributes.n_cylinders+'</li>'+
                    '</ul>'+
                '</div>';
            }

            selectHtml+= '</div>'+
        '</div>';
    });
    $('#ccardiv').append(selectHtml);


    console.log('hey');





    },


    error: function (res, response) {
    },


    });
    }


    }



    var formDataInit = new FormData();
    getData(0, formDataInit);








@endsection
