@extends('welcome')

@section('content')

    <!-- =-=-=-=-=-=-= Breadcrumb =-=-=-=-=-=-= -->
    <div class="page-header-area-2 gray">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="small-breadcrumb">
                        <div class=" breadcrumb-link">
                            <ul>
                                <li><a href="index.html">Home Page</a></li>
                                <li><a class="active" href="#">Blog</a></li>
                            </ul>
                        </div>
                        <div class="header-page">
                            <h1>Latest News & Trends</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- =-=-=-=-=-=-= Breadcrumb End =-=-=-=-=-=-= -->
    <!-- =-=-=-=-=-=-= Main Content Area =-=-=-=-=-=-= -->
    <div class="main-content-area clearfix">
        <!-- =-=-=-=-=-=-= Latest Ads =-=-=-=-=-=-= -->
        <section class="section-padding no-top gray ">
            <!-- Main Container -->
            <div class="container">
                <!-- Row -->
                <div class="row">
                    <!-- Middle Content Area -->
                    <!-- Left Sidebar -->
                    <div class="col-md-4 col-xs-12 col-sm-12">
                        <!-- Sidebar Widgets -->
                        <div class="blog-sidebar">
                            <!-- Categories -->
                            <div class="widget">
                                <div class="widget-heading">
                                    <h4 class="panel-title"><a>Categories</a></h4>
                                </div>
                                <div class="categories">
                                    <ul>
                                        <li> <a href=""> Computer and IT <span>(121)</span> </a> </li>
                                        <li> <a href=""> Animal <span>(54)</span> </a> </li>
                                        <li> <a href=""> Electronics<span>(313)</span> </a> </li>
                                        <li> <a href=""> Real Estate<span>(23)</span> </a> </li>
                                        <li> <a href=""> Mobile / Laptop <span>(142)</span> </a> </li>
                                        <li> <a href=""> Car / Bike <span>(120)</span> </a> </li>
                                    </ul>
                                </div>
                            </div>
                            <!-- Latest News -->
                            <div class="widget">
                                <div class="widget-heading">
                                    <h4 class="panel-title"><a>Latest News</a></h4>
                                </div>
                                <div class="recent-ads">
                                    <!-- Ads -->
                                    <div class="recent-ads-list">
                                        <div class="recent-ads-container">
                                            <div class="recent-ads-list-image">
                                                <a href="#" class="recent-ads-list-image-inner">
                                                    <img src="{{asset('theme/images/posting/thumb-1.jpg')}}" alt="">
                                                </a><!-- /.recent-ads-list-image-inner -->
                                            </div>
                                            <!-- /.recent-ads-list-image -->
                                            <div class="recent-ads-list-content">
                                                <h3 class="recent-ads-list-title">
                                                    <a href="#">2010 Audi A5 Auto Premium quattro MY10</a>
                                                </h3>
                                                <ul class="recent-ads-list-location">
                                                    <li><a href="#">New York</a></li>
                                                </ul>
                                                <!-- /.recent-ads-list-price -->
                                            </div>
                                            <!-- /.recent-ads-list-content -->
                                        </div>
                                        <!-- /.recent-ads-container -->
                                    </div>
                                    <!-- Ads -->
                                    <div class="recent-ads-list">
                                        <div class="recent-ads-container">
                                            <div class="recent-ads-list-image">
                                                <a href="#" class="recent-ads-list-image-inner">
                                                    <img src="{{asset('theme/images/posting/thumb-2.jpg')}}" alt="">
                                                </a><!-- /.recent-ads-list-image-inner -->
                                            </div>
                                            <!-- /.recent-ads-list-image -->
                                            <div class="recent-ads-list-content">
                                                <h3 class="recent-ads-list-title">
                                                    <a href="#">Honda Civic 2017 Sports Edition With Turbo</a>
                                                </h3>
                                                <ul class="recent-ads-list-location">
                                                    <li><a href="#">Brooklyn</a></li>
                                                </ul>
                                                <!-- /.recent-ads-list-price -->
                                            </div>
                                            <!-- /.recent-ads-list-content -->
                                        </div>
                                        <!-- /.recent-ads-container -->
                                    </div>
                                    <!-- Ads -->
                                    <div class="recent-ads-list">
                                        <div class="recent-ads-container">
                                            <div class="recent-ads-list-image">
                                                <a href="#" class="recent-ads-list-image-inner">
                                                    <img src="{{asset('theme/images/posting/thumb-3.jpg')}}" alt="">
                                                </a><!-- /.recent-ads-list-image-inner -->
                                            </div>
                                            <!-- /.recent-ads-list-image -->
                                            <div class="recent-ads-list-content">
                                                <h3 class="recent-ads-list-title">
                                                    <a href="#">Ford Mustang EcoBoost Premium Convertible</a>
                                                </h3>
                                                <ul class="recent-ads-list-location">
                                                    <li><a href="#">Brooklyn</a></li>
                                                </ul>
                                            </div>
                                            <!-- /.recent-ads-list-content -->
                                        </div>
                                        <!-- /.recent-ads-container -->
                                    </div>
                                    <!-- Ads -->
                                    <div class="recent-ads-list">
                                        <div class="recent-ads-container">
                                            <div class="recent-ads-list-image">
                                                <a href="#" class="recent-ads-list-image-inner">
                                                    <img src="{{asset('theme/images/posting/thumb-4.jpg')}}" alt="">
                                                </a><!-- /.recent-ads-list-image-inner -->
                                            </div>
                                            <!-- /.recent-ads-list-image -->
                                            <div class="recent-ads-list-content">
                                                <h3 class="recent-ads-list-title">
                                                    <a href="#">2017 Bugatti Chiron: Again with the Overkill </a>
                                                </h3>
                                                <ul class="recent-ads-list-location">
                                                    <li><a href="#">Brooklyn</a></li>
                                                </ul>
                                            </div>
                                            <!-- /.recent-ads-list-content -->
                                        </div>
                                        <!-- /.recent-ads-container -->
                                    </div>
                                    <!-- Ads -->
                                    <div class="recent-ads-list">
                                        <div class="recent-ads-container">
                                            <div class="recent-ads-list-image">
                                                <a href="#" class="recent-ads-list-image-inner">
                                                    <img src="{{asset('theme/images/posting/thumb-5.jpg')}}" alt="">
                                                </a><!-- /.recent-ads-list-image-inner -->
                                            </div>
                                            <!-- /.recent-ads-list-image -->
                                            <div class="recent-ads-list-content">
                                                <h3 class="recent-ads-list-title">
                                                    <a href="#">Porsche 911 Carrera 2017  Super Charger</a>
                                                </h3>
                                                <ul class="recent-ads-list-location">
                                                    <li><a href="#">Brooklyn</a></li>
                                                </ul>
                                            </div>
                                            <!-- /.recent-ads-list-content -->
                                        </div>
                                        <!-- /.recent-ads-container -->
                                    </div>
                                </div>
                            </div>
                            <!-- Archives -->
                            <div class="widget">
                                <div class="widget-heading">
                                    <h4 class="panel-title"><a>Archives</a></h4>
                                </div>
                                <ul>
                                    <li><a href="#">August 2017</a></li>
                                    <li><a href="#">July 2017</a></li>
                                    <li><a href="#">June 2017 </a></li>
                                    <li><a href="#">May 2017  </a></li>
                                    <li><a href="#">April 2014 </a></li>
                                </ul>
                            </div>
                            <!-- Gallery -->
                            <div class="widget">
                                <div class="widget-heading">
                                    <h4 class="panel-title"><a>Gallery</a></h4>
                                </div>
                                <div class="gallery">
                                    <div class="gallery-image">
                                        <a href="#"><img alt="" src="{{asset('theme/images/blog/small-5.png')}}">
                                        </a>
                                        <a href="#"><img alt="" src="{{asset('theme/images/blog/small-6.png')}}">
                                        </a>
                                        <a href="#"><img alt="" src="{{asset('theme/images/blog/small-7.png')}}">
                                        </a>
                                        <a href="#"><img alt="" src="{{asset('theme/images/blog/small-8.png')}}">
                                        </a>
                                        <a href="#"><img alt="" src="{{asset('theme/images/blog/small-9.png')}}">
                                        </a>
                                        <a href="#"><img alt="" src="{{asset('theme/images/blog/small-10.png')}}">
                                        </a>
                                        <a href="#"><img alt="" src="{{asset('theme/images/blog/small-1.png')}}">
                                        </a>
                                        <a href="#"><img alt="" src="{{asset('theme/images/blog/small-2.png')}}">
                                        </a>
                                        <a href="#"><img alt="" src="{{asset('theme/images/blog/small-3.png')}}">
                                        </a>
                                        <a href="#"><img alt="" src="{{asset('theme/images/blog/small-4.png')}}">
                                        </a>
                                        <a href="#"><img alt="" src="{{asset('theme/images/blog/small-11.png')}}">
                                        </a>
                                        <a href="#"><img alt="" src="{{asset('theme/images/blog/small-12.png')}}">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!-- Tags -->
                            <div class="widget">
                                <div class="widget-heading">
                                    <h4 class="panel-title"><a>Tags cloud</a></h4>
                                </div>
                                <div class="tagcloud">
                                    <a href="#.">Hair</a>
                                    <a href="#.">Waxing</a>
                                    <a href="#.">Body</a>
                                    <a href="#.">Oil</a>
                                    <a href="#.">Facials</a>
                                    <a href="#.">Cutting</a>
                                    <a href="#.">Blowouts</a>
                                    <a href="#.">Curling</a>
                                    <a href="#.">Makeup</a>
                                </div>
                            </div>
                        </div>
                        <!-- Sidebar Widgets End -->
                    </div>
                    <div class="col-md-8 col-xs-12 col-sm-12">
                        <div class="row">
                            <!-- Blog Archive -->
                            <div class="posts-masonry">
                                <!-- Blog Post-->
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="blog-post">
                                        <div class="post-img">
                                            <a href="#"> <img class="img-responsive" alt="" src="{{asset('theme/images/posting/15.jpg')}}"> </a>
                                            <div class="user-preview">
                                                <a href="#"> <img src="{{asset('theme/images/users/2.jpg')}}" class="avatar avatar-small" alt=""> </a>
                                            </div>
                                        </div>
                                        <div class="post-info"> <a href="">Aug 30, 2017</a> <a href="#">23 comments</a> </div>
                                        <h3 class="post-title"> <a href="#"> 2017 Bugatti Chiron: Again with the Overkill </a> </h3>
                                        <p class="post-excerpt"> Lorem ipsum dolor sit amet,   Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam neque tempora odit atque repellat est molestiae perferendis consectetur adipisicing elit. Quisquam neque tempora odit atque repellat est molestiae perferendis.  																						<a href="#"><strong>Read More</strong></a>
                                        </p>
                                    </div>
                                </div>
                                <!-- Blog Post-->
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="blog-post">
                                        <div class="post-img">
                                            <a href="#"> <img class="img-responsive" alt="" src="{{asset('theme/images/posting/27.jpg')}}"> </a>
                                            <div class="user-preview">
                                                <a href="#"> <img src="{{asset('theme/images/users/2.jpg')}}" class="avatar avatar-small" alt=""> </a>
                                            </div>
                                        </div>
                                        <div class="post-info"> <a href="">Aug 30, 2017</a> <a href="#">23 comments</a> </div>
                                        <h3 class="post-title"> <a href="#"> Ford Mustang EcoBoost Premium Convertible</a> </h3>
                                        <p class="post-excerpt"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam neque tempora odit atque repellat est molestiae perferendis.  																						<a href="#"><strong>Read More</strong></a>
                                        </p>
                                    </div>
                                </div>
                                <!-- Blog Post-->
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="blog-post">
                                        <div class="post-img">
                                            <div class="user-preview">
                                                <a href="#"> <img src="{{asset('theme/images/users/2.jpg')}}" class="avatar avatar-small" alt=""> </a>
                                            </div>
                                        </div>
                                        <div class="post-info"> <a href="">Aug 30, 2017</a> <a href="#">23 comments</a> </div>
                                        <h3 class="post-title"> <a href="#"> 2010 Audi A5 Auto Premium quattro MY10 </a> </h3>
                                        <p class="post-excerpt"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam neque tempora odit atque repellat est molestiae perferendis.  																						<a href="#"><strong>Read More</strong></a>
                                        </p>
                                    </div>
                                </div>
                                <!-- Blog Grid -->
                                <!-- Blog Post-->
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="blog-post">
                                        <div class="post-img">
                                            <a href="#"> <img class="img-responsive" alt="" src="{{asset('theme/images/posting/4.jpg')}}"> </a>
                                            <div class="user-preview">
                                                <a href="#"> <img src="{{asset('theme/images/users/2.jpg')}}" class="avatar avatar-small" alt=""> </a>
                                            </div>
                                        </div>
                                        <div class="post-info"> <a href="">Aug 30, 2017</a> <a href="#">23 comments</a> </div>
                                        <h3 class="post-title"> <a href="#"> Eight Things You Should Know About The Mercedes-Benz E-Class</a> </h3>
                                        <p class="post-excerpt"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam neque tempora odit atque repellat est molestiae perferendis.  																						<a href="#"><strong>Read More</strong></a>
                                        </p>
                                    </div>
                                </div>
                                <!-- Blog Grid -->
                                <!-- Blog Post-->
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="blog-post">
                                        <div class="post-img">
                                            <a href="#"> <img class="img-responsive" alt="" src="{{asset('theme/images/posting/6.jpg')}}"> </a>
                                            <div class="user-preview">
                                                <a href="#"> <img src="{{asset('theme/images/users/2.jpg')}}" class="avatar avatar-small" alt=""> </a>
                                            </div>
                                        </div>
                                        <div class="post-info"> <a href="">Aug 30, 2017</a> <a href="#">23 comments</a> </div>
                                        <h3 class="post-title"> <a href="#">McLaren 570 GT review: watch out Porsche and Audi</a> </h3>
                                        <p class="post-excerpt"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam neque tempora odit atque repellat est molestiae perferendis.  																						<a href="#"><strong>Read More</strong></a>
                                        </p>
                                    </div>
                                </div>
                                <!-- Blog Grid -->
                                <!-- Blog Post-->
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="blog-post">
                                        <div class="post-img">
                                            <a href="#"> <img class="img-responsive" alt="" src="{{asset('theme/images/posting/7.jpg')}}"> </a>
                                            <div class="user-preview">
                                                <a href="#"> <img src="{{asset('theme/images/users/2.jpg')}}" class="avatar avatar-small" alt=""> </a>
                                            </div>
                                        </div>
                                        <div class="post-info"> <a href="">Aug 30, 2017</a> <a href="#">23 comments</a> </div>
                                        <h3 class="post-title"> <a href="#"> 2016 Lamborghini Huracán LP 610-4 Spyder Review</a> </h3>
                                        <p class="post-excerpt"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam neque tempora odit atque repellat est molestiae perferendis.  																						<a href="#"><strong>Read More</strong></a>
                                        </p>
                                    </div>
                                </div>
                                <!-- Blog Grid -->
                                <!-- Blog Post-->
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="blog-post">
                                        <div class="post-img">
                                            <a href="#"> <img class="img-responsive" alt="" src="{{asset('theme/images/posting/8.jpg')}}"> </a>
                                            <div class="user-preview">
                                                <a href="#"> <img src="{{asset('theme/images/users/2.jpg')}}" class="avatar avatar-small" alt=""> </a>
                                            </div>
                                        </div>
                                        <div class="post-info"> <a href="">Aug 30, 2017</a> <a href="#">23 comments</a> </div>
                                        <h3 class="post-title"> <a href="#"> Honda Civic 2017 Sports Edition With Turbo Charger</a> </h3>
                                        <p class="post-excerpt"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam neque tempora odit atque repellat est molestiae perferendis.  																						<a href="#"><strong>Read More</strong></a>
                                        </p>
                                    </div>
                                </div>
                                <!-- Blog Grid -->
                                <!-- Blog Post-->
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="blog-post">
                                        <div class="post-img">
                                            <a href="#"> <img class="img-responsive" alt="" src="{{asset('theme/images/posting/9.jpg')}}"> </a>
                                            <div class="user-preview">
                                                <a href="#"> <img src="{{asset('theme/images/users/2.jpg')}}" class="avatar avatar-small" alt=""> </a>
                                            </div>
                                        </div>
                                        <div class="post-info"> <a href="">Aug 30, 2017</a> <a href="#">23 comments</a> </div>
                                        <h3 class="post-title"> <a href="#"> 2018 Audi A5 Cabriolet and Audi S5 Cabriolet Review</a> </h3>
                                        <p class="post-excerpt"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam neque tempora odit atque repellat est molestiae perferendis.  																						<a href="#"><strong>Read More</strong></a>
                                        </p>
                                    </div>
                                </div>
                                <!-- Blog Grid -->
                                <!-- Blog Post-->
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="blog-post">
                                        <div class="post-img">
                                            <a href="#"> <img class="img-responsive" alt="" src="{{asset('theme/images/posting/10.jpg')}}"> </a>
                                            <div class="user-preview">
                                                <a href="#"> <img src="{{asset('theme/images/users/2.jpg')}}" class="avatar avatar-small" alt=""> </a>
                                            </div>
                                        </div>
                                        <div class="post-info"> <a href="">Aug 30, 2017</a> <a href="#">23 comments</a> </div>
                                        <h3 class="post-title"> <a href="#"> Eight Things You Should Know About The Mercedes-Benz E-Class</a> </h3>
                                        <p class="post-excerpt"> Lorem ipsum dolor sit amet,   Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam neque tempora odit atque repellat est molestiae perferendis consectetur adipisicing elit. Quisquam neque tempora odit atque repellat est molestiae perferendis.  																						<a href="#"><strong>Read More</strong></a>
                                        </p>
                                    </div>
                                </div>
                                <!-- Blog Grid -->
                                <!-- Blog Post-->
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="blog-post">
                                        <div class="post-img">
                                            <a href="#"> <img class="img-responsive" alt="" src="{{asset('theme/images/posting/11.jpg')}}"> </a>
                                            <div class="user-preview">
                                                <a href="#"> <img src="{{asset('theme/images/users/2.jpg')}}" class="avatar avatar-small" alt=""> </a>
                                            </div>
                                        </div>
                                        <div class="post-info"> <a href="">Aug 30, 2017</a> <a href="#">23 comments</a> </div>
                                        <h3 class="post-title"> <a href="#"> Best new cars for 2018 in the world</a> </h3>
                                        <p class="post-excerpt"> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam neque tempora odit atque repellat est molestiae perferendis.  																						<a href="#"><strong>Read More</strong></a>
                                        </p>
                                    </div>
                                </div>
                                <!-- Blog Grid -->
                            </div>
                            <div class="col-md-12 col-xs-12 col-sm-12">
                                <ul class="pagination pagination-lg">
                                    <li> <a href="#"> <i class="fa fa-chevron-left" aria-hidden="true"></i></a></li>
                                    <li> <a href="#">1</a> </li>
                                    <li class="active"> <a href="#">2</a> </li>
                                    <li> <a href="#">3</a> </li>
                                    <li> <a href="#">4</a> </li>
                                    <li><a href="#"> <i class="fa fa-chevron-right" aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- Middle Content Area  End -->
                </div>
                <!-- Row End -->
            </div>
            <!-- Main Container End -->
        </section>
        <!-- =-=-=-=-=-=-= Ads Archives End =-=-=-=-=-=-= -->
    </div>
@endsection
