@extends('welcome')

@section('content')

    <!-- =-=-=-=-=-=-= Breadcrumb =-=-=-=-=-=-= -->
    <div class="page-header-area-2 gray">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="small-breadcrumb">
                        <div class=" breadcrumb-link">
                            <ul>
                                <li><a href="{{route('index')}}">Home Page</a></li>
                                <li><a class="active" href="#">Login</a></li>
                            </ul>
                        </div>
                        <div class="header-page">
                            <h1>Sign In to your account </h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- =-=-=-=-=-=-= Breadcrumb End =-=-=-=-=-=-= -->
    <!-- =-=-=-=-=-=-= Main Content Area =-=-=-=-=-=-= -->
    <div class="main-content-area clearfix">
        <!-- =-=-=-=-=-=-= Latest Ads =-=-=-=-=-=-= -->
        <section class="section-padding no-top gray">
            <!-- Main Container -->
            <div class="container">
                <!-- Row -->
                <div class="row">
                    <!-- Middle Content Area -->
                    <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">


                        <h2 style="color:red" id="LoginFormShowError"></h2>

                        <!--  Form -->
                        <div class="form-grid">
                            <form id="LoginForm">
{{--                                <a class="btn btn-lg btn-block btn-social btn-facebook">--}}
{{--                                    <span class="fa fa-facebook"></span> Sign in with Facebook--}}
{{--                                </a>--}}

{{--                                <a class="btn btn-lg btn-block btn-social btn-google">--}}
{{--                                    <span class="fa fa-google"></span> Sign in with Facebook--}}
{{--                                </a>--}}

{{--                                <h2 class="no-span"><b>(OR)</b></h2>--}}

                                <div class="form-group">
                                    <label>Email Or Phone</label>



                                    <div style="display: flex">


                                    <span id="showHideFlageLogin"   style="width: 10%;height: 53px;border: solid 0.5px;display: none" class="form-control">

                                        {{--<img  style="height: 40px;" src="{{asset('theme/images/E_flag.png')}}">--}} +971
                                    </span>

                                    <input placeholder="Your Email, Or Your Phone" class="form-control" type="text" id="email_phone" name="email_or_phone" onkeyup="showPhoneFlagLogin()" >
                                    </div>


                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input placeholder="Your Password" class="form-control" type="password" id="passwordd" name="password">
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="skin-minimal">
                                                <ul class="list">
                                                    <li>
{{--                                                        <input  type="checkbox" id="minimal-checkbox-1" name="checkbox-1">--}}
{{--                                                        <label for="minimal-checkbox-1">Remember Me</label>--}}
                                                        <a href="{{route('forgotpassword')}}" style="margin-left: 37rem;">Forgot password?</a>
                                                    </li>
                                                </ul>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-theme btn-lg btn-block">Login With Us</button>
                            </form>
                        </div>
                        <!-- Form -->
                    </div>
                    <!-- Middle Content Area  End -->
                </div>
                <!-- Row End -->
            </div>
            <!-- Main Container End -->
        </section>
        <!-- =-=-=-=-=-=-= Ads Archives End =-=-=-=-=-=-= -->
    </div>
@endsection
