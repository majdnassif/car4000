@extends('welcome')

@section('content')
<!-- =-=-=-=-=-=-= Listing Map =-=-=-=-=-=-= -->
<div id="map"></div>
<!-- Map Navigation -->
<ul id="google-map-btn">
    <li><a href="#" id="prevpoint" title="Previous point on map">Prev</a></li>
    <li><a href="#" id="nextpoint" title="Next point on mp">Next</a></li>
</ul>
<!-- =-=-=-=-=-=-= Listing Map End =-=-=-=-=-=-= -->
<!-- =-=-=-=-=-=-= Main Content Area =-=-=-=-=-=-= -->
<div class="main-content-area clearfix">
    <section class="custom-padding" id="about-section-3">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                    <div class="about-title">
                        <h2> Services All<span class="heading-color"> Over The World</span></h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent congue justo scelerisque mattis iaculis. Maecenas vestibulum faucibus enim scelerisque egestas. Praesent aliquet tincidunt id mi. </p>
                        <ul class="custom-links">
                            <li><a href="#">Extend the life of your car</a></li>
                            <li><a href="#">Keep your engine cool</a></li>
                            <li><a href="#">Extend the life of your car</a></li>
                            <li><a href="#">Keep your engine cool</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                    <div class="right-side">
                        <h5>Lorem ipsum dolor sit amet consectetur adipisicing elit Perfer repudiandae nostrum alias quibusdam! </h5>
                        <p>Lorem ipsum dolor sit amet consectetur adipisic.Min commodi enim nemo illum repellendus accusantium maiores itu delectus doloribus alias ea quisquam cum nullavolupta delectu. <br><br>
                            nobis eius, deleniti dicta molestiae atque. Exercitationem odit dolor cumque facilis natus recusandae id, dolorum modi ducimus minus.
                        </p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent congue justo scelerisque mattis iaculis. Maecenas vestibulum faucibus enim scelerisque egestas. Praesent aliquet id hendrerit id, hendrerit ac odio. In dui mauris, auctor vel vestibulum vitae, tincidunt id mi. </p>
                        <img src="{{asset('theme/images/mechanic.png')}}"  alt="">
                    </div>
                    <!-- /.right-side -->
                </div>
            </div>
            <div class="row about-stats">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                        <div class="icons">
                            <i class="flaticon-vehicle"></i>
                        </div>
                        <div class="number"><span class="timer" data-from="0" data-to="1238" data-speed="1500" data-refresh-interval="5">0</span>+</div>
                        <h4>Total <span>Cars</span></h4>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                        <div class="icons">
                            <i class="flaticon-security"></i>
                        </div>
                        <div class="number"><span class="timer" data-from="0" data-to="820" data-speed="1500" data-refresh-interval="5">0</span>+</div>
                        <h4>Verified <span>Dealers</span></h4>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                        <div class="icons">
                            <i class="flaticon-like-1"></i>
                        </div>
                        <div class="number"><span class="timer" data-from="0" data-to="1042" data-speed="1500" data-refresh-interval="5">0</span>+</div>
                        <h4>Active <span>Users</span></h4>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                        <div class="icons">
                            <i class="flaticon-cup"></i>
                        </div>
                        <div class="number"><span class="timer" data-from="0" data-to="34" data-speed="1500" data-refresh-interval="5">0</span>+</div>
                        <h4>Featured <span>Ads</span></h4>
                    </div>
                </div>
            </div>
            <!-- end main-boxes -->
        </div>
        <!-- end container -->
    </section>
    <section class="section-padding-120 our-services">
        <!--Image One-->
        <div class="background-1"></div>
        <!--Image Two-->
        <div class="background-2"></div>
        <img class="img-responsive wow slideInRight custom-img" data-wow-delay="0ms" data-wow-duration="2000ms" src="{{asset('theme/images/sell-1.png')}}"  alt="">
        <div class="container">
            <div class="row clearfix">
                <!--Left Column-->
                <div class="left-column col-lg-4 col-md-4 col-md-12">
                    <div class="inner-box">
                        <h2>Our Services</h2>
                        <div class="text">A shadowy flight into the dangerous world of a man who does not exist. Flying away on a wing and a prayer. Who could it be. Believe it or not its just me. Movin' on up to the east side. We finally got a piece of the pie.</div>
                    </div>
                </div>
                <!--Transparent Column-->
                <div class="service-column col-lg-8 col-md-12">
                    <div class="inner-box wow fadeInRight animated" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <div class="row clearfix">
                            <!--Icon Column-->
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="services-grid-3">
                                    <div class="content-area">
                                        <h1>01.</h1>
                                        <a href="">
                                            <h4>Engine Diagnostic</h4>
                                        </a>
                                        <p>We have the right caring, experience and dedicated professional for you.</p>
                                        <div class="service-icon">
                                            <i class="flaticon-car-wash-1"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Icon Column-->
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="services-grid-3">
                                    <div class="content-area">
                                        <h1>02.</h1>
                                        <a href="">
                                            <h4>Wheel Alignment</h4>
                                        </a>
                                        <p>We have the right caring, experience and dedicated professional for you.</p>
                                        <div class="service-icon">
                                            <i class="flaticon-car-wash-1"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Icon Column-->
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="services-grid-3">
                                    <div class="content-area">
                                        <h1>03.</h1>
                                        <a href="">
                                            <h4>Oil Changing</h4>
                                        </a>
                                        <p>We have the right caring, experience and dedicated professional for you.</p>
                                        <div class="service-icon">
                                            <i class="flaticon-car-wash-1"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Icon Column-->
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="services-grid-3">
                                    <div class="content-area">
                                        <h1>04.</h1>
                                        <a href="">
                                            <h4>Brake Reparing</h4>
                                        </a>
                                        <p>We have the right caring, experience and dedicated professional for you.</p>
                                        <div class="service-icon">
                                            <i class="flaticon-car-wash-1"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- =-=-=-=-=-=-= Trending Ads =-=-=-=-=-=-= -->
    <section class="custom-padding">
        <!-- Main Container -->
        <div class="container">
            <!-- Row -->
            <div class="row">
                <!-- Heading Area -->
                <div class="heading-panel">
                    <div class="col-xs-12 col-md-8 col-sm-12 left-side">
                        <!-- Main Title -->
                        <h1>Latest <span class="heading-color"> Featured</span> Ads</h1>
                    </div>
                    <div class="col-md-4">
                        <a class="btn btn-default pull-right">View All Ads
                        </a>
                    </div>
                </div>
                <!-- Middle Content Box -->
                <div class="row grid-style-4">
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <div class="posts-masonry">
                            <div class="col-md-4 col-xs-12 col-sm-6">
                                <!-- Ad Box -->
                                <div class="white category-grid-box-1 ">
                                    <div class="featured-ribbon">
                                        <span>Featured</span>
                                    </div>
                                    <!-- Image Box -->
                                    <div class="image"> <img alt="Carspot" src="{{asset('theme/images/posting/4.jpg')}}" class="img-responsive">
                                    </div>
                                    <!-- Short Description -->
                                    <div class="short-description-1 ">
                                        <!-- Category Title -->
                                        <div class="category-title"> <span><a href="#">Sports & Equipment</a></span> </div>
                                        <!-- Ad Title -->
                                        <h3>
                                            <a title="" href="single-page-listing.html">2016 BMW 3 Series 328i</a>
                                        </h3>
                                        <!-- Location -->
                                        <p class="location"><i class="fa fa-map-marker"></i> Model Town Link Road London</p>
                                        <!-- Price -->
                                        <span class="ad-price">$370</span>
                                    </div>
                                    <!-- Ad Meta Stats -->
                                    <div class="ad-info-1">
                                        <ul>
                                            <li><i class="flaticon-fuel-1"></i>Diesel</li>
                                            <li><i class="flaticon-dashboard"></i>35,000 km</li>
                                            <li><i class="flaticon-engine-2"></i> 1800 cc</li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- Ad Box End -->
                            </div>
                            <div class="col-md-4 col-xs-12 col-sm-6">
                                <!-- Ad Box -->
                                <div class="white category-grid-box-1 ">
                                    <div class="featured-ribbon">
                                        <span>Featured</span>
                                    </div>
                                    <!-- Image Box -->
                                    <div class="image"> <img alt="Carspot" src="{{asset('theme/images/posting/3.jpg')}}" class="img-responsive">
                                    </div>
                                    <!-- Short Description -->
                                    <div class="short-description-1 ">
                                        <!-- Category Title -->
                                        <div class="category-title"> <span><a href="#">Sports & Equipment</a></span> </div>
                                        <!-- Ad Title -->
                                        <h3>
                                            <a title="" href="single-page-listing.html">2014 Ford Shelby GT500 Coupe</a>
                                        </h3>
                                        <!-- Location -->
                                        <p class="location"><i class="fa fa-map-marker"></i> Model Town Link Road London</p>
                                        <!-- Price -->
                                        <span class="ad-price">$370</span>
                                    </div>
                                    <!-- Ad Meta Stats -->
                                    <div class="ad-info-1">
                                        <ul>
                                            <li><i class="flaticon-fuel-1"></i>Diesel</li>
                                            <li><i class="flaticon-dashboard"></i>35,000 km</li>
                                            <li><i class="flaticon-engine-2"></i> 1800 cc</li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- Ad Box End -->
                            </div>
                            <div class="col-md-4 col-xs-12 col-sm-6">
                                <!-- Ad Box -->
                                <div class="white category-grid-box-1 ">
                                    <div class="featured-ribbon">
                                        <span>Featured</span>
                                    </div>
                                    <!-- Image Box -->
                                    <div class="image"> <img alt="Carspot" src="{{asset('theme/images/posting/2.jpg')}}" class="img-responsive">
                                    </div>
                                    <!-- Short Description -->
                                    <div class="short-description-1 ">
                                        <!-- Category Title -->
                                        <div class="category-title"> <span><a href="#">Sports & Equipment</a></span> </div>
                                        <!-- Ad Title -->
                                        <h3>
                                            <a title="" href="single-page-listing.html">Porsche 911 Carrera 2017 </a>
                                        </h3>
                                        <!-- Location -->
                                        <p class="location"><i class="fa fa-map-marker"></i> Model Town Link Road London</p>
                                        <!-- Price -->
                                        <span class="ad-price">$370</span>
                                    </div>
                                    <!-- Ad Meta Stats -->
                                    <div class="ad-info-1">
                                        <ul>
                                            <li><i class="flaticon-fuel-1"></i>Diesel</li>
                                            <li><i class="flaticon-dashboard"></i>35,000 km</li>
                                            <li><i class="flaticon-engine-2"></i> 1800 cc</li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- Ad Box End -->
                            </div>
                            <div class="col-md-4 col-xs-12 col-sm-6">
                                <!-- Ad Box -->
                                <div class="white category-grid-box-1 ">
                                    <!-- Image Box -->
                                    <div class="image"> <img alt="Carspot" src="{{asset('theme/images/posting/7.jpg')}}"  class="img-responsive">
                                    </div>
                                    <!-- Short Description -->
                                    <div class="short-description-1 ">
                                        <!-- Category Title -->
                                        <div class="category-title"> <span><a href="#">Sports & Equipment</a></span> </div>
                                        <!-- Ad Title -->
                                        <h3>
                                            <a title="" href="single-page-listing.html">Lamborghini Gallardo 5.0 V10 2dr</a>
                                        </h3>
                                        <!-- Location -->
                                        <p class="location"><i class="fa fa-map-marker"></i> Model Town Link Road London</p>
                                        <!-- Price -->
                                        <span class="ad-price">$370</span>
                                    </div>
                                    <!-- Ad Meta Stats -->
                                    <div class="ad-info-1">
                                        <ul>
                                            <li><i class="flaticon-fuel-1"></i>Diesel</li>
                                            <li><i class="flaticon-dashboard"></i>35,000 km</li>
                                            <li><i class="flaticon-engine-2"></i> 1800 cc</li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- Ad Box End -->
                            </div>
                            <div class="col-md-4 col-xs-12 col-sm-6">
                                <!-- Ad Box -->
                                <div class="white category-grid-box-1 ">
                                    <!-- Image Box -->
                                    <div class="image"> <img alt="Carspot" src="{{asset('theme/images/posting/8.jpg')}}"  class="img-responsive">
                                    </div>
                                    <!-- Short Description -->
                                    <div class="short-description-1 ">
                                        <!-- Category Title -->
                                        <div class="category-title"> <span><a href="#">Sports & Equipment</a></span> </div>
                                        <!-- Ad Title -->
                                        <h3>
                                            <a title="" href="single-page-listing.html">Honda Civic 2.0 i-VTEC Type R</a>
                                        </h3>
                                        <!-- Location -->
                                        <p class="location"><i class="fa fa-map-marker"></i> Model Town Link Road London</p>
                                        <!-- Price -->
                                        <span class="ad-price">$370</span>
                                    </div>
                                    <!-- Ad Meta Stats -->
                                    <div class="ad-info-1">
                                        <ul>
                                            <li><i class="flaticon-fuel-1"></i>Diesel</li>
                                            <li><i class="flaticon-dashboard"></i>35,000 km</li>
                                            <li><i class="flaticon-engine-2"></i> 1800 cc</li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- Ad Box End -->
                            </div>
                            <div class="col-md-4 col-xs-12 col-sm-6">
                                <!-- Ad Box -->
                                <div class="white category-grid-box-1 ">
                                    <div class="featured-ribbon">
                                        <span>Featured</span>
                                    </div>
                                    <!-- Image Box -->
                                    <div class="image"> <img alt="Carspot" src="{{asset('theme/images/posting/9.jpg')}}"  class="img-responsive">
                                    </div>
                                    <!-- Short Description -->
                                    <div class="short-description-1 ">
                                        <!-- Category Title -->
                                        <div class="category-title"> <span><a href="#">Sports & Equipment</a></span> </div>
                                        <!-- Ad Title -->
                                        <h3>
                                            <a title="" href="single-page-listing.html">Audi A4 quattro Premium GT</a>
                                        </h3>
                                        <!-- Location -->
                                        <p class="location"><i class="fa fa-map-marker"></i> Model Town Link Road London</p>
                                        <!-- Price -->
                                        <span class="ad-price">$370</span>
                                    </div>
                                    <!-- Ad Meta Stats -->
                                    <div class="ad-info-1">
                                        <ul>
                                            <li><i class="flaticon-fuel-1"></i>Diesel</li>
                                            <li><i class="flaticon-dashboard"></i>35,000 km</li>
                                            <li><i class="flaticon-engine-2"></i> 1800 cc</li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- Ad Box End -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Middle Content Box End -->
            </div>
            <!-- Row End -->
        </div>
        <!-- Main Container End -->
    </section>
    <!-- =-=-=-=-=-=-= Trending Ads End =-=-=-=-=-=-= -->
    <!-- =-=-=-=-=-=-= Blog Section =-=-=-=-=-=-= -->
    <section class="news section-padding gray">
        <div class="container">
            <div class="row">
                <div class="heading-panel">
                    <div class="col-xs-12 col-md-12 col-sm-12 left-side">
                        <!-- Main Title -->
                        <h1>Expert  <span class="heading-color"> Reviews</span> Feedback</h1>
                        <!-- Short Description -->
                        <p class="heading-text">Eu delicata rationibus usu. Vix te putant utroque, ludus fabellas duo eu, his dico ut debet consectetuer.</p>
                    </div>
                </div>
                <div class="col-md-7 col-sm-12 col-xs-12">
                    <div class="mainimage">
                        <a>
                            <img alt="" class="img-responsive" src="{{asset('theme/images/blog/1.jpg')}}">
                            <div class="overlay">
                                <h2>Eight Things You Should Know About The Mercedes-Benz E-Class LWB</h2>
                            </div>
                        </a>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-md-5 col-sm-12 col-xs-12">
                    <div class="newslist">
                        <ul>
                            <li>
                                <div class="imghold"> <a><img src="{{asset('theme/images/blog/s1.jpg')}}" alt=""></a> </div>
                                <div class="texthold">
                                    <h4><a>2017 Honda City: Which Variant Suits You? </a></h4>
                                    <p>With the 2017 facelifted avatar, the Honda City has significantly upped its game...&nbsp;</p>
                                </div>
                                <div class="clear"></div>
                            </li>
                            <li>
                                <div class="imghold"> <a><img src="{{asset('theme/images/blog/s2.jpg')}}" alt=""></a> </div>
                                <div class="texthold">
                                    <h4><a>Honda City Facelift &ndash; Expected Prices </a></h4>
                                    <p>Honda will launch the City facelift in India on Feb 14, 2017 and it promises to...&nbsp;</p>
                                </div>
                                <div class="clear"></div>
                            </li>
                            <li>
                                <div class="imghold"> <a><img src="{{asset('theme/images/blog/s3.jpg')}}" alt=""></a> </div>
                                <div class="texthold">
                                    <h4><a>Audi A4 Diesel Launched In India At Rs 40.20 Lakh </a></h4>
                                    <p>Audi India has launched a powerful diesel variant of its A4 sedan at Rs 40.20 la...&nbsp;</p>
                                </div>
                                <div class="clear"></div>
                            </li>
                            <li>
                                <div class="imghold"> <a><img src="{{asset('theme/images/blog/s4.jpg')}}"  alt=""></a> </div>
                                <div class="texthold">
                                    <h4><a>Audi A4 Diesel Launched In India At Rs 40.20 Lakh </a></h4>
                                    <p>Audi India has launched a powerful diesel variant of its A4 sedan at Rs 40.20 la...&nbsp;</p>
                                </div>
                                <div class="clear"></div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </section>
    <!-- =-=-=-=-=-=-= Blog Section End =-=-=-=-=-=-= -->
    <!-- =-=-=-=-=-=-= App Download Section =-=-=-=-=-=-= -->
    <div class="app-download-section style-2">
        <!-- app-download-section-wrapper -->
        <div class="app-download-section-wrapper">
            <!-- app-download-section-container -->
            <div class="app-download-section-container">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="mobile-image-content"> <img src="{{asset('theme/images/hand.png')}}"  alt=""> </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="app-text-section">
                                <h5>Download our app</h5>
                                <h3>Get Our App For Your Mobile</h3>
                                <ul>
                                    <li>Find nearby cars in your network with Scholar</li>
                                    <li>Browse real hirers reviews to know why choose Scholar</li>
                                    <li>Rent a car so easy with a tap !</li>
                                    <li>Rent a car so easy with a tap !</li>
                                </ul>
                                <div class="app-download-btn">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6">
                                            <!-- Windows Store -->
                                            <a href="#" title="Windows Store" class="btn app-download-button">
                                       <span class="app-store-btn">
                                       <i class="fa fa-windows"></i>
                                       <span>
                                       <span>Download From</span>
                                       <span>Windows Store </span>
                                       </span>
                                       </span>
                                            </a>
                                            <!-- /Windows Store -->
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <!-- Windows Store -->
                                            <a href="#" title="Windows Store" class="btn app-download-button"> <span class="app-store-btn">
                                       <i class="fa fa-apple"></i>
                                       <span>
                                       <span>Download From</span> <span>Apple Store </span> </span>
                                       </span>
                                            </a>
                                            <!-- /Windows Store -->
                                        </div>
                                        <!-- Windows Store -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /row -->
                </div>
                <!-- /container -->
            </div>
            <!-- /app-download-section-container -->
        </div>
        <!-- /download-section-wrapper -->
    </div>
    <!-- =-=-=-=-=-=-= App Download Section End =-=-=-=-=-=-= -->
    <!-- =-=-=-=-=-=-= Blog Section =-=-=-=-=-=-= -->
    <section class="news section-padding">
        <div class="container">
            <div class="row">
                <div class="heading-panel">
                    <div class="col-xs-12 col-md-12 col-sm-12 left-side">
                        <!-- Main Title -->
                        <h1>Clients  <span class="heading-color"> Reviews</span> Feedback</h1>
                        <!-- Short Description -->
                        <p class="heading-text">Eu delicata rationibus usu. Vix te putant utroque, ludus fabellas duo eu, his dico ut debet consectetuer.</p>
                    </div>
                </div>
                <!-- Middle Content Box -->
                <div class="col-md-12 col-xs-12 col-sm-12">
                    <div class="row">
                        <div class="owl-testimonial-1">
                            <div class="single_testimonial">
                                <div class="textimonial-content">
                                    <h4>Just fabulous</h4>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elitsed eiusmod tempor enim minim veniam quis notru.</p>
                                </div>
                                <div class="testimonial-meta-box">
                                    <img src="{{asset('theme/images/users/1.jpg')}}" alt="">
                                    <div class="testimonial-meta">
                                        <h3 class="">Jhon Emily Copper </h3>
                                        <p> Developer</p>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="single_testimonial">
                                <div class="textimonial-content">
                                    <h4>Awesome ! Loving It</h4>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elitsed eiusmod tempor enim minim veniam quis notru.</p>
                                </div>
                                <div class="testimonial-meta-box">
                                    <img src="{{asset('theme/images/users/2.jpg')}}" alt="">
                                    <div class="testimonial-meta">
                                        <h3 class="">Hania Sheikh </h3>
                                        <p> CEO Pvt. Inc.</p>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="single_testimonial">
                                <div class="textimonial-content">
                                    <h4>Very quick and Fast</h4>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elitsed eiusmod tempor enim minim veniam quis notru.</p>
                                </div>
                                <div class="testimonial-meta-box">
                                    <img src="{{asset('theme/images/users/3.jpg')}}" alt="">
                                    <div class="testimonial-meta">
                                        <h3 class="">Jaccica Albana </h3>
                                        <p>  CTO Albana Inc.</p>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="single_testimonial">
                                <div class="textimonial-content">
                                    <h4>Done in 3 Months! Awesome</h4>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elitsed eiusmod tempor enim minim veniam quis notru.</p>
                                </div>
                                <div class="testimonial-meta-box">
                                    <img src="{{asset('theme/images/users/4.jpg')}}" alt="">
                                    <div class="testimonial-meta">
                                        <h3 class="">Humayun Sarfraz </h3>
                                        <p>  CTO Glixen Technologies.</p>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="single_testimonial">
                                <div class="textimonial-content">
                                    <h4>Find It Quit Professional</h4>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elitsed eiusmod tempor enim minim veniam quis notru.</p>
                                </div>
                                <div class="testimonial-meta-box">
                                    <img src="{{asset('theme/images/users/4.jpg')}}" alt="">
                                    <div class="testimonial-meta">
                                        <h3 class="">Massica O'Brain </h3>
                                        <p> Audit Officer </p>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="single_testimonial">
                                <div class="textimonial-content">
                                    <h4>Just fabulous</h4>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elitsed eiusmod tempor enim minim veniam quis notru.</p>
                                </div>
                                <div class="testimonial-meta-box">
                                    <img src="{{asset('theme/images/users/1.jpg')}}" alt="">
                                    <div class="testimonial-meta">
                                        <h3 class="">Jhon Emily Copper </h3>
                                        <p> Developer</p>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="single_testimonial">
                                <div class="textimonial-content">
                                    <h4>Awesome ! Loving It</h4>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elitsed eiusmod tempor enim minim veniam quis notru.</p>
                                </div>
                                <div class="testimonial-meta-box">
                                    <img src="{{asset('theme/images/users/2.jpg')}}" alt="">
                                    <div class="testimonial-meta">
                                        <h3 class="">Hania Sheikh </h3>
                                        <p> CEO Pvt. Inc.</p>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="single_testimonial">
                                <div class="textimonial-content">
                                    <h4>Very quick and Fast</h4>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elitsed eiusmod tempor enim minim veniam quis notru.</p>
                                </div>
                                <div class="testimonial-meta-box">
                                    <img src="{{asset('theme/images/users/3.jpg')}}" alt="">
                                    <div class="testimonial-meta">
                                        <h3 class="">Jaccica Albana </h3>
                                        <p>  CTO Albana Inc.</p>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="single_testimonial">
                                <div class="textimonial-content">
                                    <h4>Done in 3 Months! Awesome</h4>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elitsed eiusmod tempor enim minim veniam quis notru.</p>
                                </div>
                                <div class="testimonial-meta-box">
                                    <img src="{{asset('theme/images/users/4.jpg')}}" alt="">
                                    <div class="testimonial-meta">
                                        <h3 class="">Humayun Sarfraz </h3>
                                        <p>  CTO Glixen Tech.</p>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="single_testimonial">
                                <div class="textimonial-content">
                                    <h4>Find It Quit Professional</h4>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elitsed eiusmod tempor enim minim veniam quis notru.</p>
                                </div>
                                <div class="testimonial-meta-box">
                                    <img src="{{asset('theme/images/users/4.jpg')}}"  alt="">
                                    <div class="testimonial-meta">
                                        <h3 class="">Massica O'Brain </h3>
                                        <p> Audit Officer </p>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <!-- Middle Content Box End -->
            </div>
            <div class="clearfix"></div>
        </div>
    </section>
    <!-- =-=-=-=-=-=-= Blog Section End =-=-=-=-=-=-= -->
    <!-- =-=-=-=-=-=-= Our Clients =-=-=-=-=-=-= -->
    <section class="client-section gray">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-12 col-xs-12">
                    <div class="margin-top-30">
                        <h3>Why Choose Us</h3>
                        <h2>Our premium Clients</h2>
                    </div>
                </div>
                <div class="col-md-8 col-sm-12 col-xs-12">
                    <div class="brand-logo-area clients-bg">
                        <div class="clients-list">
                            <div class="client-logo">
                                <a href="#"><img src="{{asset('theme/images/brands/16.png')}}"  class="img-responsive" alt="Brand Image" /></a>
                            </div>
                            <div class="client-logo">
                                <a href="#"><img src="{{asset('theme/images/brands/12.png')}}" class="img-responsive" alt="Brand Image" /></a>
                            </div>
                            <div class="client-logo">
                                <a href="#"><img src="{{asset('theme/images/brands/11.png')}}" class="img-responsive" alt="Brand Image" /></a>
                            </div>
                            <div class="client-logo">
                                <a href="#"><img src="{{asset('theme/images/brands/4.png')}}" class="img-responsive" alt="Brand Image" /></a>
                            </div>
                            <div class="client-logo">
                                <a href="#"><img src="{{asset('theme/images/brands/5.png')}}" class="img-responsive" alt="Brand Image" /></a>
                            </div>
                            <div class="client-logo">
                                <a href="#"><img src="{{asset('theme/images/brands/6.png')}}" class="img-responsive" alt="Brand Image" /></a>
                            </div>
                            <div class="client-logo">
                                <a href="#"><img src="{{asset('theme/images/brands/7.png')}}" class="img-responsive" alt="Brand Image" /></a>
                            </div>
                            <div class="client-logo">
                                <a href="#"><img src="{{asset('theme/images/brands/8.png')}}" class="img-responsive" alt="Brand Image" /></a>
                            </div>
                            <div class="client-logo">
                                <a href="#"><img src="{{asset('theme/images/brands/9.png')}}" class="img-responsive" alt="Brand Image" /></a>
                            </div>
                            <div class="client-logo">
                                <a href="#"><img src="{{asset('theme/images/brands/17.png')}}" class="img-responsive" alt="Brand Image" /></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- =-=-=-=-=-=-= Our Clients End =-=-=-=-=-=-= -->
</div>
@endsection
