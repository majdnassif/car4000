@extends('welcome')

@section('content')

    <!-- =-=-=-=-=-=-= Breadcrumb =-=-=-=-=-=-= -->
    <div class="page-header-area-2 no-bottom gray">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 no-padding  col-md-12 col-sm-12 col-xs-12">
                    <div class="small-breadcrumb">
                        <div class="col-md-12 col-xs-12 col-sm-12">
                            <div class=" breadcrumb-link">
                                <ul>
                                    <li><a href="index.html">Cars & Bikes</a></li>
                                    <li><a class="active" href="#">Ad Details</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- =-=-=-=-=-=-= Breadcrumb End =-=-=-=-=-=-= -->
    <!-- =-=-=-=-=-=-= Main Content Area =-=-=-=-=-=-= -->
    <div class="main-content-area clearfix">
        <!-- =-=-=-=-=-=-= Latest Ads =-=-=-=-=-=-= -->
        <section class="section-padding no-top gray ">
            <!-- Main Container -->
            <div class="container">
                <!-- Row -->
                <div class="row">
                    <div class="pricing-area">
                        <div class="col-md-9 col-xs-12 col-sm-8">
                            <div class="heading-zone">
                                <h1>Land Rover Freelander 2 Se</h1>
                                <div class="short-history">
                                    <ul>
                                        <li><b>June 20, 2017</b></li>
                                        <li>Category: <b><a href="#">Land Rover </a></b></li>
                                        <li>Views: <b>666</b></li>
                                        <li><a href="#">Edit</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-4 detail_price col-xs-12">
                            <div class="singleprice-tag">$ 15,000<span>(Fixed)</span></div>
                        </div>
                    </div>
                    <!-- Middle Content Area -->
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <!-- Single Ad -->
                        <div class="singlepage-detail ">
                            <!-- Listing Slider  -->
                            <div class="gallery-style-post clearfix">
                                <div class="row">
                                    <!-- Slider Start -->
                                    <div class="col-sm-8 col-md-8 col-xs-12 big-section">
                                        <a href="{{asset('theme/images/single-page/1.jpg')}}" data-fancybox="group"><img alt="" src="{{asset('theme/images/single-page/1.jpg')}}" title="Audi A4 2.0 TDie 136 SE Technik 4dr"></a>
                                        <div class="total-{{asset('theme/images"><strong>5</strong> photos </div>
                                    </div>
                                    <div class="col-sm-4 col-md-4 col-xs-12 small-section">
                                        <div class="img-thumb first">
                                            <a href="{{asset('theme/images/single-page/2.jpg')}}" data-fancybox="group"><img alt="" src="{{asset('theme/images/single-page/thumb1.jpg')}}" title="Audi A4 2.0 TDie 136 SE Technik 4dr"></a>
                                        </div>
                                        <div class="img-thumb">
                                            <a href="{{asset('theme/images/single-page/3.jpg')}}" data-fancybox="group"><img alt="" src="{{asset('theme/images/single-page/thumb2.jpg')}}" title="Audi A4 2.0 TDie 136 SE Technik 4dr"></a>
                                        </div>
                                    </div>
                                    <!-- Slide Small End -->
                                    <div class="extra-{{asset('theme/images hidden">
                                        <a href="{{asset('theme/images/single-page/4.jpg')}}" data-fancybox="group"><img alt="" src="{{asset('theme/images/single-page/thumb3.jpg')}}" title="Audi A4 2.0 TDie 136 SE Technik 4dr"></a>
                                        <a href="{{asset('theme/images/single-page/5.jpg')}}" data-fancybox="group"><img alt="" src="{{asset('theme/images/single-page/thumb4.jpg')}}" title="Audi A4 2.0 TDie 136 SE Technik 4dr"></a>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8 col-xs-12 col-sm-12">
                                    <div class="content-box-grid">
                                        <div class="short-features">
                                            <!-- Heading Area -->
                                            <div class="heading-panel">
                                                <h3 class="main-title text-left">
                                                    Description
                                                </h3>
                                            </div>
                                            <p>
                                                Bank Leased 5 Year plan 2013 Honda Civic 1.8 Vti Oriel Prosmatec Automatic ( New Shape ) Attractive Silver Color 1 year installments paid Lahore Reg number Well Maintained Insurance + tracker etc included Options: Sunroof
                                            </p>
                                            <div class="col-sm-4 col-md-4 col-xs-12 no-padding">
                                                <span><strong>Condition</strong> :</span> Used
                                            </div>
                                            <div class="col-sm-4 col-md-4 col-xs-12 no-padding">
                                                <span><strong>Brand</strong> :</span> Nokia
                                            </div>
                                            <div class="col-sm-4 col-md-4 col-xs-12 no-padding">
                                                <span><strong>Model</strong> :</span> Lumia 625
                                            </div>
                                            <div class="col-sm-4 col-md-4 col-xs-12 no-padding">
                                                <span><strong>Product Type</strong>:</span> Mobile
                                            </div>
                                            <div class="col-sm-4 col-md-4 col-xs-12 no-padding">
                                                <span><strong>Date</strong> :</span> 2014-10-06
                                            </div>
                                            <div class="col-sm-4 col-md-4 col-xs-12 no-padding">
                                                <span><strong>Price</strong> :</span> Rs. 22,000
                                            </div>
                                        </div>
                                        <!-- Short Features  -->
                                        <div class="desc-points">
                                            <ul>
                                                <li>
                                                    Looking to sell the car urgently.
                                                </li>
                                                <li>
                                                    Engine is good condition.
                                                </li>
                                                <li>
                                                    Complete service history available.
                                                </li>
                                                <li>
                                                    Original return file is available.
                                                </li>
                                                <li>
                                                    After Market Alloy rims.
                                                </li>
                                                <li>
                                                    As good as a brand new car.
                                                </li>
                                                <li>
                                                    Lady Driven Car in Immaculate Condition.
                                                </li>
                                                <li>
                                                    No Work Required in Car.
                                                </li>
                                                <li>
                                                    Excellent Mileage , Local Average = 14 km , Long Average = 16 km .
                                                </li>
                                            </ul>
                                        </div>
                                        <!-- Related Image  -->
                                        <div class="ad-related-img">
                                            <img src="{{asset('theme/images/car-img1.png')}}" alt="" class="img-responsive center-block">
                                        </div>
                                        <div class="short-features">
                                            <!-- Heading Area -->
                                            <div class="heading-panel">
                                                <h3 class="main-title text-left">
                                                    Car Features
                                                </h3>
                                            </div>
                                            <!-- Car Key Features  -->
                                            <ul class="car-feature-list ">
                                                <li><i class="flaticon-antenna"></i> AM/FM Radio</li>
                                                <li><i class="flaticon-air-conditioner-1"></i> Air Conditioning</li>
                                                <li><i class="flaticon-cd"></i> Cassette Player</li>
                                                <li><i class="flaticon-light-bulb"></i> Power Locks</li>
                                                <li><i class="flaticon-rearview-mirror"></i> Power Mirrors</li>
                                                <li><i class="flaticon-car-steering-wheel"></i> Power Steering</li>
                                                <li><i class="flaticon-car-door"></i> Power Windows</li>
                                                <li><i class="flaticon-disc-brake"></i> Anti-lock Braking</li>
                                                <li><i class="flaticon-rim"></i> 19 Inch Alloy Wheels</li>
                                                <li><i class="flaticon-message"></i> Cruise Control</li>
                                                <li><i class="flaticon-airbag"></i> Front Airbag Package</li>
                                                <li><i class="flaticon-photo-camera-1"></i> Reversing Camera</li>
                                            </ul>
                                        </div>
                                        <!-- Short Features  -->
                                        <div class="clearfix"></div>
                                    </div>
                                    <!-- Share Ad  -->
                                    <div class="ad-share text-center">
                                        <div data-toggle="modal" data-target=".share-ad" class="content-box-grid col-md-4 col-sm-4 col-xs-12">
                                            <i class="fa fa-share-alt"></i> <span class="hidetext">Share</span>
                                        </div>
                                        <a class="content-box-grid col-md-4 col-sm-4 col-xs-12" href="#"><i class="fa fa-star active"></i> <span class="hidetext">Add to watchlist</span></a>
                                        <div data-target=".report-quote" data-toggle="modal" class="content-box-grid col-md-4 col-sm-4 col-xs-12">
                                            <i class="fa fa-warning"></i> <span class="hidetext">Report</span>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <!-- Price Alert -->
                                    <div class="alert-box-container  margin-top-30">
                                        <div class="well">
                                            <h3>Create Alert</h3>
                                            <p>Receive emails for the latest ads matching your search criteria</p>
                                            <form>
                                                <div class="row">
                                                    <div class="col-md-5 col-xs-12 col-sm-12">
                                                        <input placeholder="Enter Your Email " type="text" class="form-control">
                                                    </div>
                                                    <div class="col-md-4 col-xs-12 col-sm-12">
                                                        <select class="alerts">
                                                            <option value="1">Daily</option>
                                                            <option value="7">Weekly</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-3 col-xs-12 col-sm-12">
                                                        <input class="btn btn-theme btn-block" value="Submit" type="submit">
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <!-- Price Alert End -->
                                    <!-- =-=-=-=-=-=-= Latest Ads =-=-=-=-=-=-= -->
                                    <div class="grid-panel margin-top-30">
                                        <div class="heading-panel">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <h3 class="main-title text-left">
                                                    Related Ads
                                                </h3>
                                            </div>
                                        </div>
                                        <!-- Ads Archive -->
                                        <div class="col-md-12 col-xs-12 col-sm-12">
                                            <div class="posts-masonry">
                                                <div class="ads-list-archive">
                                                    <!-- Image Block -->
                                                    <div class="col-lg-5 col-md-5 col-sm-5 no-padding">
                                                        <!-- Img Block -->
                                                        <div class="ad-archive-img">
                                                            <a href="#">
                                                                <img class="img-responsive" src="{{asset('theme/images/posting/10.jpg')}}" alt="">
                                                            </a>
                                                        </div>
                                                        <!-- Img Block -->
                                                    </div>
                                                    <!-- Ads Listing -->
                                                    <div class="clearfix visible-xs-block"></div>
                                                    <!-- Content Block -->
                                                    <div class="col-lg-7 col-md-7 col-sm-7 no-padding">
                                                        <!-- Ad Desc -->
                                                        <div class="ad-archive-desc">
                                                            <!-- Price -->
                                                            <div class="ad-price">$387,000</div>
                                                            <!-- Title -->
                                                            <h3>2014 Honda Accord VTi-S Auto</h3>
                                                            <!-- Category -->
                                                            <div class="category-title"> <span><a href="#">Car &amp; Bikes</a></span> </div>
                                                            <!-- Short Description -->
                                                            <div class="clearfix visible-xs-block"></div>
                                                            <p class="hidden-sm">Lorem ipsum dolor sit amet, quem convenire interesset ut vix, maiestatis inciderint no, eos in elit dicat.....</p>
                                                            <!-- Ad Features -->
                                                            <ul class="add_info">
                                                                <li><a href="#"><img  src="{{asset('theme/images/blog/s1.jpg')}}" alt=""></a></li>
                                                                <li><a href="#"><img  src="{{asset('theme/images/blog/s1.jpg')}}" alt=""></a></li>
                                                                <li><a href="#"><img  src="{{asset('theme/images/blog/s1.jpg')}}" alt=""></a></li>
                                                                <li><a href="#"><img  src="{{asset('theme/images/blog/s1.jpg')}}" alt=""></a></li>
                                                            </ul>
                                                            <!-- Ad History -->
                                                            <div class="clearfix archive-history">
                                                                <div class="last-updated">Last Updated: 1 day ago</div>
                                                                <div class="ad-meta"> <a class="btn save-ad"><i class="fa fa-heart-o"></i> Save Ad.</a> <a class="btn btn-success"><i class="fa fa-phone"></i> View Details.</a> </div>
                                                            </div>
                                                        </div>
                                                        <!-- Ad Desc End -->
                                                    </div>
                                                    <!-- Content Block End -->
                                                </div>
                                                <div class="ads-list-archive">
                                                    <!-- Image Block -->
                                                    <div class="col-lg-5 col-md-5 col-sm-5 no-padding">
                                                        <!-- Img Block -->
                                                        <div class="ad-archive-img">
                                                            <a href="#">
                                                                <img class="img-responsive" src="{{asset('theme/images/posting/14.jpg')}}" alt="">
                                                            </a>
                                                        </div>
                                                        <!-- Img Block -->
                                                    </div>
                                                    <!-- Ads Listing -->
                                                    <div class="clearfix visible-xs-block"></div>
                                                    <!-- Content Block -->
                                                    <div class="col-lg-7 col-md-7 col-sm-7 no-padding">
                                                        <!-- Ad Desc -->
                                                        <div class="ad-archive-desc">
                                                            <!-- Price -->
                                                            <div class="ad-price">$190,000</div>
                                                            <!-- Title -->
                                                            <h3>Bugatti Veyron Super Sport </h3>
                                                            <!-- Category -->
                                                            <div class="category-title"> <span><a href="#">Car &amp; Bikes</a></span> </div>
                                                            <!-- Short Description -->
                                                            <div class="clearfix visible-xs-block"></div>
                                                            <p class="hidden-sm">Lorem ipsum dolor sit amet, quem convenire interesset ut vix, maiestatis inciderint no, eos in elit dicat.....</p>
                                                            <!-- Ad Features -->
                                                            <ul class="add_info">
                                                                <li><a href="#"><img  src="{{asset('theme/images/blog/s2.jpg')}}" alt=""></a></li>
                                                                <li><a href="#"><img  src="{{asset('theme/images/blog/s1.jpg')}}" alt=""></a></li>
                                                            </ul>
                                                            <!-- Ad History -->
                                                            <div class="clearfix archive-history">
                                                                <div class="last-updated">Last Updated: 1 day ago</div>
                                                                <div class="ad-meta"> <a class="btn save-ad"><i class="fa fa-heart-o"></i> Save Ad.</a> <a class="btn btn-success"><i class="fa fa-phone"></i> View Details.</a> </div>
                                                            </div>
                                                        </div>
                                                        <!-- Ad Desc End -->
                                                    </div>
                                                    <!-- Content Block End -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- =-=-=-=-=-=-= Latest Ads End =-=-=-=-=-=-= -->
                                </div>
                                <!-- Right Sidebar -->
                                <div class="col-md-4 col-xs-12 col-sm-12">
                                    <!-- Sidebar Widgets -->
                                    <div class="sidebar">
                                        <!-- Contact info -->
                                        <div class="contact white-bg">
                                            <!-- Email Button trigger modal -->
                                            <button class="btn-block btn-contact contactEmail" data-toggle="modal" data-target=".price-quote" >Contact Seller Via Email</button>
                                            <!-- Email Modal -->
                                            <button class="btn-block btn-contact contactPhone number" data-last="111111X" >0320<span>XXXXXXX</span></button>
                                        </div>
                                        <!-- User Info -->
                                        <div class="white-bg user-contact-info">
                                            <div class="user-info-card">
                                                <div class="user-photo col-md-4 col-sm-3  col-xs-4">
                                                    <img src="{{asset('theme/images/users/3.jpg')}}" alt="">
                                                </div>
                                                <div class="user-information col-md-8 col-sm-9 col-xs-8">
                                                    <span class="user-name"><a class="hover-color" href="profile.html">Sonu Monu</a></span>
                                                    <div class="item-date">
                                                        <span class="ad-pub">Published on: 10 Dec 2017</span><br>
                                                        <a href="#" class="link">More Ads</a>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="ad-listing-meta">
                                                <ul>
                                                    <li>Ad Id: <span class="color">4143</span></li>
                                                    <li>Categories: <span class="color">Used Cars</span></li>
                                                    <li>Visits: <span class="color">9</span></li>
                                                    <li>Location: <span class="color">New York, USA</span></li>
                                                </ul>
                                            </div>
                                            <div id="itemMap"></div>
                                        </div>
                                        <!-- Recent Ads -->
                                        <div class="widget">
                                            <div class="widget-heading">
                                                <h4 class="panel-title"><a>Recent Ads</a></h4>
                                            </div>
                                            <div class="widget-content recent-ads">
                                                <!-- Ads -->
                                                <div class="recent-ads-list">
                                                    <div class="recent-ads-container">
                                                        <div class="recent-ads-list-image">
                                                            <a href="#" class="recent-ads-list-image-inner">
                                                                <img src="{{asset('theme/images/posting/thumb-1.jpg')}}" alt="">
                                                            </a><!-- /.recent-ads-list-image-inner -->
                                                        </div>
                                                        <!-- /.recent-ads-list-image -->
                                                        <div class="recent-ads-list-content">
                                                            <h3 class="recent-ads-list-title">
                                                                <a href="#">2010 Audi A5 Auto Premium quattro MY10</a>
                                                            </h3>
                                                            <ul class="recent-ads-list-location">
                                                                <li><a href="#">New York</a>,</li>
                                                                <li><a href="#">Brooklyn</a></li>
                                                            </ul>
                                                            <div class="recent-ads-list-price">
                                                                $ 17,000
                                                            </div>
                                                            <!-- /.recent-ads-list-price -->
                                                        </div>
                                                        <!-- /.recent-ads-list-content -->
                                                    </div>
                                                    <!-- /.recent-ads-container -->
                                                </div>
                                                <!-- Ads -->
                                                <div class="recent-ads-list">
                                                    <div class="recent-ads-container">
                                                        <div class="recent-ads-list-image">
                                                            <a href="#" class="recent-ads-list-image-inner">
                                                                <img src="{{asset('theme/images/posting/thumb-2.jpg')}}" alt="">
                                                            </a><!-- /.recent-ads-list-image-inner -->
                                                        </div>
                                                        <!-- /.recent-ads-list-image -->
                                                        <div class="recent-ads-list-content">
                                                            <h3 class="recent-ads-list-title">
                                                                <a href="#">Honda Civic 2017 Sports Edition With Turbo</a>
                                                            </h3>
                                                            <ul class="recent-ads-list-location">
                                                                <li><a href="#">New York</a>,</li>
                                                                <li><a href="#">Brooklyn</a></li>
                                                            </ul>
                                                            <div class="recent-ads-list-price">
                                                                $ 66,000
                                                            </div>
                                                            <!-- /.recent-ads-list-price -->
                                                        </div>
                                                        <!-- /.recent-ads-list-content -->
                                                    </div>
                                                    <!-- /.recent-ads-container -->
                                                </div>
                                                <!-- Ads -->
                                                <div class="recent-ads-list">
                                                    <div class="recent-ads-container">
                                                        <div class="recent-ads-list-image">
                                                            <a href="#" class="recent-ads-list-image-inner">
                                                                <img src="{{asset('theme/images/posting/thumb-3.jpg')}}" alt="">
                                                            </a><!-- /.recent-ads-list-image-inner -->
                                                        </div>
                                                        <!-- /.recent-ads-list-image -->
                                                        <div class="recent-ads-list-content">
                                                            <h3 class="recent-ads-list-title">
                                                                <a href="#">Ford Mustang EcoBoost Premium Convertible</a>
                                                            </h3>
                                                            <ul class="recent-ads-list-location">
                                                                <li><a href="#">New York</a>,</li>
                                                                <li><a href="#">Brooklyn</a></li>
                                                            </ul>
                                                            <div class="recent-ads-list-price">
                                                                $ 37,000
                                                            </div>
                                                            <!-- /.recent-ads-list-price -->
                                                        </div>
                                                        <!-- /.recent-ads-list-content -->
                                                    </div>
                                                    <!-- /.recent-ads-container -->
                                                </div>
                                                <!-- Ads -->
                                                <div class="recent-ads-list">
                                                    <div class="recent-ads-container">
                                                        <div class="recent-ads-list-image">
                                                            <a href="#" class="recent-ads-list-image-inner">
                                                                <img src="{{asset('theme/images/posting/thumb-4.jpg')}}" alt="">
                                                            </a><!-- /.recent-ads-list-image-inner -->
                                                        </div>
                                                        <!-- /.recent-ads-list-image -->
                                                        <div class="recent-ads-list-content">
                                                            <h3 class="recent-ads-list-title">
                                                                <a href="#">2017 Bugatti Chiron: Again with the Overkill </a>
                                                            </h3>
                                                            <ul class="recent-ads-list-location">
                                                                <li><a href="#">New York</a>,</li>
                                                                <li><a href="#">Brooklyn</a></li>
                                                            </ul>
                                                            <div class="recent-ads-list-price">
                                                                $ 11,000
                                                            </div>
                                                            <!-- /.recent-ads-list-price -->
                                                        </div>
                                                        <!-- /.recent-ads-list-content -->
                                                    </div>
                                                    <!-- /.recent-ads-container -->
                                                </div>
                                                <!-- Ads -->
                                                <div class="recent-ads-list">
                                                    <div class="recent-ads-container">
                                                        <div class="recent-ads-list-image">
                                                            <a href="#" class="recent-ads-list-image-inner">
                                                                <img src="{{asset('theme/images/posting/thumb-5.jpg')}}" alt="">
                                                            </a><!-- /.recent-ads-list-image-inner -->
                                                        </div>
                                                        <!-- /.recent-ads-list-image -->
                                                        <div class="recent-ads-list-content">
                                                            <h3 class="recent-ads-list-title">
                                                                <a href="#">Porsche 911 Carrera 2017  Super Charger</a>
                                                            </h3>
                                                            <ul class="recent-ads-list-location">
                                                                <li><a href="#">New York</a>,</li>
                                                                <li><a href="#">Brooklyn</a></li>
                                                            </ul>
                                                            <div class="recent-ads-list-price">
                                                                $ 20,000
                                                            </div>
                                                            <!-- /.recent-ads-list-price -->
                                                        </div>
                                                        <!-- /.recent-ads-list-content -->
                                                    </div>
                                                    <!-- /.recent-ads-container -->
                                                </div>
                                            </div>
                                        </div>
                                        <!--  Financing calculator  -->
                                        <div class="widget">
                                            <div class="widget-heading">
                                                <h4 class="panel-title"><a>Financing Calculator</a></h4>
                                            </div>
                                            <div class="widget-content ">
                                                <div class="finance-calculator">
                                                    <form>
                                                        <ul>
                                                            <li>
                                                                <label>Vehicle price</label>
                                                                <select>
                                                                    <option>$30,000</option>
                                                                    <option>$35,000</option>
                                                                    <option>$45,000</option>
                                                                    <option>$55,000</option>
                                                                </select>
                                                            </li>
                                                            <li>
                                                                <label>Interest rate</label>
                                                                <select>
                                                                    <option>30%</option>
                                                                    <option>35%</option>
                                                                    <option>45%</option>
                                                                    <option>55%</option>
                                                                </select>
                                                            </li>
                                                            <li>
                                                                <label>Period (month</label>
                                                                <span class="price-slider-value"><span id="month-min"></span> Months</span>
                                                                <div id="month-slider"></div>
                                                            </li>
                                                            <li class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="auto-field">
                                                                    <label>Down Payment</label>
                                                                    <input type="text" class="form-control" placeholder="$326,500">
                                                                </div>
                                                            </li>
                                                            <li class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                                <div class="auto-field">
                                                                    <input class="btn btn-theme btn-sm margin-bottom-20" type="submit" value="Calculate">
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Saftey Tips  -->
                                        <div class="widget">
                                            <div class="widget-heading">
                                                <h4 class="panel-title"><a>Safety tips for deal</a></h4>
                                            </div>
                                            <div class="widget-content saftey">
                                                <ol>
                                                    <li>Use a safe location to meet seller</li>
                                                    <li>Avoid cash transactions</li>
                                                    <li>Beware of unrealistic offers</li>
                                                </ol>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Sidebar Widgets End -->
                                </div>
                            </div>
                        </div>
                        <!-- Single Ad End -->
                    </div>
                    <!-- Middle Content Area  End -->
                </div>
                <!-- Row End -->
            </div>
            <!-- Main Container End -->
        </section>
        <!-- =-=-=-=-=-=-= Ads Archives End =-=-=-=-=-=-= -->
    </div>
@endsection
