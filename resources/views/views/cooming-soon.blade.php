@extends('welcome')

@section('content')

    <section class="comming-soon-grid" style="height: 500px;">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 comming-soon">
                    <div class="theme-logo">
                        <a href="index.html"><img src="{{asset('theme/images/logo.png')}}" alt=""></a>
                    </div>
                    <div class="count-down">
                        <div id="clock"></div>
                    </div>
                    <div class="subscribe">
                        <p>Our website is under construction.
                            <br> We'll be here soon with our new awesome site, subscribe to be notified.
                        </p>
                        <form action="#" method="post">
                            <input type="email" name="email" id="mail" placeholder="Valid E-mail Address" required>
                            <button type="submit"><i class="fa fa-paper-plane-o" aria-hidden="true"></i>Notify Me</button>
                        </form>
                    </div>
                    <div class="social-area-share">
                        <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        <a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
                        <a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                        <a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
