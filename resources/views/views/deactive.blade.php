@extends('welcome')

@section('content')

    <!-- =-=-=-=-=-=-= Breadcrumb =-=-=-=-=-=-= -->
    <div class="page-header-area-2 gray">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="small-breadcrumb">
                        <div class=" breadcrumb-link">
                            <ul>
                                <li><a href="index.html">Home Page</a></li>
                                <li><a class="active" href="#">Acount Deactivation</a></li>
                            </ul>
                        </div>
                        <div class="header-page">
                            <h1>Delete Your Account </h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- =-=-=-=-=-=-= Breadcrumb End =-=-=-=-=-=-= -->
    <!-- =-=-=-=-=-=-= Main Content Area =-=-=-=-=-=-= -->
    <div class="main-content-area clearfix">
        <!-- =-=-=-=-=-=-= Latest Ads =-=-=-=-=-=-= -->
        <section class="custom-padding no-top gray">
            <!-- Main Container -->
            <div class="container">
                <!-- Row -->
                <div class="row">
                    <!-- Middle Content Area -->
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <section class="search-result-item">
                            <a class="image-link" href="#"><img class="image center-block" alt="" src="{{asset('theme/images/users/9.jpg')}}"> </a>
                            <div class="search-result-item-body">
                                <div class="row">
                                    <div class="col-md-5 col-sm-12 col-xs-12">
                                        <h4 class="search-result-item-heading"><a href="#">Umair</a></h4>
                                        <p class="info">
                                            <span><a href="profile.html"><i class="fa fa-user "></i>Profile </a></span>
                                            <span><a href="javascript:void(0)"><i class="fa fa-edit"></i>Edit Profile </a></span>
                                        </p>
                                        <p class="description">You last logged in at: 14-01-2017 6:40 AM [ USA time (GMT + 6:00hrs)</p>
                                        <span class="label label-warning">Paid Package</span>
                                        <span class="label label-success">Dealer</span>
                                    </div>
                                    <div class="col-md-7 col-sm-12 col-xs-12">
                                        <div class="row ad-history">
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <div class="user-stats">
                                                    <h2>374</h2>
                                                    <small>Ad Sold</small>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <div class="user-stats">
                                                    <h2>980</h2>
                                                    <small>Total Listings</small>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <div class="user-stats">
                                                    <h2>413</h2>
                                                    <small>Favourites Ads</small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <div class="dashboard-menu-container">
                            <ul>
                                <li>
                                    <a href="profile.html">
                                        <div class="menu-name"> Profile </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="archives.html">
                                        <div class="menu-name">Archives</div>
                                    </a>
                                </li>
                                <li>
                                    <a href="active-ads.html">
                                        <div class="menu-name">My Ads</div>
                                    </a>
                                </li>
                                <li>
                                    <a href="favourite.html">
                                        <div class="menu-name">Favourites Ads</div>
                                    </a>
                                </li>
                                <li>
                                    <a href="messages.html">
                                        <div class="menu-name">Messages</div>
                                    </a>
                                </li>
                                <li   class="active">
                                    <a href="deactive.html">
                                        <div class="menu-name">Close Account</div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <div class="menu-name">Logout</div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- Middle Content Area  End -->
                </div>
                <!-- Row End -->
                <!-- Row -->
                <div class="row margin-top-40">
                    <!-- Middle Content Area -->
                    <div class="col-md-12 col-xs-12 col-sm-12">
                        <div class="grid-card">

                            <div class="row">
                                <form>
                                    <div class="col-md-12 col-sm-12">
                                        <div class="alert alert-dismissible alert-info"> You can reactive your account anytime with your username and password</div>
                                    </div>
                                    <div class="col-md-12 col-sm-12">
                                        <div class="form-group">
                                            <label>Give Breif Description Here <span class="required">*</span></label>
                                            <textarea cols="6" rows="8" placeholder="Are you sure, you want to delete your account?" class="form-control"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12">
                                        <button class="btn btn-theme pull-right margin-bottom-20"><i class="fa fa-close"></i> Clsoe Now </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-- Middle Content Area  End -->
                </div>
                <!-- Row End -->
            </div>
            <!-- Main Container End -->
        </section>
        <!-- =-=-=-=-=-=-= Ads Archives End =-=-=-=-=-=-= -->
    </div>
@endsection
