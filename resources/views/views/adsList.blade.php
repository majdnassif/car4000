@extends('welcome')

@section('content')
    <!-- =-=-=-=-=-=-= Breadcrumb =-=-=-=-=-=-= -->
    <div class="page-header-area-2 gray">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="small-breadcrumb">
                        <div class=" breadcrumb-link">
                            <ul>
                                <li><a href="/">Home Page</a></li>
                                <li><a class="active" href="#">Ads List</a></li>
                            </ul>
                        </div>
                        <div class="header-page">
                          {{--  <h1>Grid Listing </h1>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>




    </div>




    <form id="carAdsDetailForm">

    </form>


    <form id="carAds_saveForm">

    </form>
    <!-- =-=-=-=-=-=-= Breadcrumb End =-=-=-=-=-=-= -->
    <!-- =-=-=-=-=-=-= Main Content Area =-=-=-=-=-=-= -->


{{--switch--}}
    <div class="col-lg-6" style="float: right;margin-right: 6.4%;margin-bottom: 2%;">
        <div class="listing_list_style mb20-xsd tal-991" style="z-index: 100">
            <ul class="mb0">
                <li id="gridbtn" class="list-inline-item"><a ><span class="fa fa-th-list"></span></a></li>
                <li id="listbtn" class="list-inline-item" style="background-color: rgb(227, 227, 227)"><a ><span class="fa fa-th-large"></span></a></li>
            </ul>
        </div>

    </div>
{{--switch--}}



    <div class="main-content-area clearfix">
        <!-- =-=-=-=-=-=-= Latest Ads =-=-=-=-=-=-= -->
        <section class="section-padding no-top gray">

            <!-- Main Container -->
            <div class="container">
                <!-- Row -->





                <div class="row">



                    <!-- Middle Content Area -->
                    <div class="col-md-8 col-md-push-4 col-lg-8 col-sx-12">
                        <!-- Row -->
                        <div class="row">
                            <!-- Sorting Filters -->
                            <!-- Sorting Filters End-->
                            <div class="clearfix"></div>
                            <!-- Ads Archive -->
                            <div class="row grid-style-2">
                                <div id="cardiv" class="posts-masonry">


                                </div>


                            </div>
                            <!-- Ads Archive End -->

                            <div class="clearfix">


                                <div id="cardivGrid" style="display: none" class="col-md-12 col-xs-12 col-xs-12" >
                                </div>
                                <!-- Ads Archive End -->
                            </div>



                            </div>
                            <!-- Pagination -->
                            <div class="text-center margin-top-30">
                                <ul class="pagination ">
                                    <li><a href="#"><i class="fa fa-chevron-left"></i></a></li>
                                    <li><a href="#">1</a></li>
                                    <li class="active"><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li><a href="#"><i class="fa fa-chevron-right"></i></a></li>
                                </ul>
                            </div>
                            <!-- Pagination End -->
                        </div>
                        <!-- Row End -->


                    <!-- Middle Content Area  End -->
                    <!-- Left Sidebar -->
                    <div class="col-md-4 col-md-pull-8 col-sx-12">
                        <!-- Sidebar Widgets -->
                        <div class="sidebar">
                            <!-- Panel group -->
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <!-- Brands Panel -->

                                <div class="panel panel-default">
                                    <!-- Heading -->
                                    <div class="panel-heading" >
                                        <h4 class="panel-title">
                                            <a>
                                                Quick Search
                                            </a>
                                        </h4>
                                    </div>
                                    <!-- Content -->
                                    <div class="panel-collapse">
                                        <div class="panel-body recent-ads" id="recent-ads">

                                          {{--  <div class="recent-ads-list">
                                                <div class="recent-ads-container">
                                                    <div class="recent-ads-list-image">
                                                        <a href="#" class="recent-ads-list-image-inner">
                                                            <img src="{{asset('theme/images/posting/thumb-1.jpg')}}" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="recent-ads-list-content">
                                                        <h3 class="recent-ads-list-title">
                                                            <a href="#">Audi Q5 2.0T quattro Premium </a>
                                                        </h3>
                                                        <ul class="recent-ads-list-location">
                                                            <li><a href="#">New York</a>,</li>
                                                            <li><a href="#">Brooklyn</a></li>
                                                        </ul>
                                                        <div class="recent-ads-list-price">
                                                            $ 17,000
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>--}}

                                        </div>
                                    </div>
                                </div>
                                <!-- Latest Ads Panel End -->
                            </div>
                            <!-- panel-group end -->
                        </div>
                        <!-- Sidebar Widgets End -->
                    </div>
                    <!-- Left Sidebar End -->
                </div>
                <!-- Row End -->

            </div>

            <!-- Main Container End -->
        </section>
        <!-- =-=-=-=-=-=-= Ads Archives End =-=-=-=-=-=-= -->
    </div>
@endsection







@section('script')




{{--
    <script>
--}}




    var lenght = 1;
    var pageSize = 3;
    var selectedPage = 1;
    var localstorageVar = 'Product_page';


    function getData(pageIndex, filter) {

    var userToken =   sessionStorage.getItem("api_token")

    if(userToken) {
var townFilter = localStorage.getItem("filter_town");
var filter_colorr = localStorage.getItem("filter_colorr");
var filter_condition = localStorage.getItem("filter_condition");
var filter_fuel = localStorage.getItem("filter_fuel");
var filter_modell = localStorage.getItem("filter_modell");
var filtter_option = localStorage.getItem("filtter_option");
var filter_transmition = localStorage.getItem("filter_transmition");
var filter_body = localStorage.getItem("filter_body");
var filter_form_mileage = localStorage.getItem("filter_form_mileage");
var filter_to_mileage = localStorage.getItem("filter_to_mileage");
var filter_from_year = localStorage.getItem("filter_from_year");
var filter_to_year = localStorage.getItem("filter_to_year");
var filter_from_price = localStorage.getItem("filter_from_price");
var filter_to_price = localStorage.getItem("filter_to_price");


if(filter_from_year != null){
filter.append('from_year', filter_from_year);
}else{
filter.append('from_year','');
}


if(filter_from_price != null){
filter.append('from_price', filter_from_price);
}else{
filter.append('from_price','');
}


if(filter_form_mileage != null){
filter.append('from_milage', filter_form_mileage);
}else{
filter.append('from_milage','');
}


if(filter_body != null){
filter.append('body_style_id', filter_body);
}else{
filter.append('body_style_id','');
}
if(townFilter != null){
            filter.append('town_id', townFilter);
        }else{
            filter.append('town_id','');
        }
        if(filter_colorr != null){
             filter.append('color_id', filter_colorr);
        }else{
             filter.append('color_id','');
        }

        if(filter_condition != null){
             filter.append('condition_id', filter_condition);
        }else{
              filter.append('condition_id','');
        }

        if(filter_fuel != null){
            filter.append('fuel_id', filter_fuel);
        }else{
             filter.append('fuel_id','');
        }

        if(filter_modell != null){
              filter.append('model_id', filter_modell);
        }else{
                filter.append('model_id','');
        }

        if(filtter_option != null){
              filter.append('option', filtter_option);
        }else{
               filter.append('option','');
        }

        if(filter_transmition != null){
             filter.append('transmission_id', filter_transmition);
        }else{
             filter.append('transmission_id','');
        }


filter.append('to_milage','');
filter.append('to_price','');
filter.append('to_year','');

    filter.append('app_id','3');
    filter.append('owner_id','');
    filter.append('status','');
    filter.append('validty_date','');
    filter.append('size_unit_id','');
    filter.append('finance','');
    filter.append('warranty','');
    filter.append('insurance','');
    filter.append('offset','0');
    filter.append('limit','10');




    $.ajax({

    type: "POST",
    url: Global_Url + 'carsAdsList',
    data: filter,
    // mimeType: 'application/json',
    processData: false,
    contentType: false,
    headers: {"Authorization": 'Bearer ' + userToken},

    success: function (res) {
    console.log('nawraaaaaaas ',res);

sessionStorage.setItem('adsData',res.data);



    $('#cardiv').empty();

     $('#cardivGrid').empty();



var selectHtml = '';
var selectHtmlGrid = '';
var selectHtmlLatestAds = '';
    res.data.adsList.forEach(function (item, index, arr){



selectHtml+= ' <div class="col-md-6 col-xs-12 col-sm-6">'+
    '<div class="category-grid-box-1">'+

        '<div class="image">'+


            '<img style="height: 250px;" alt="Carspot" class="img-responsive" src="http://4000.global/public/img/'+item.thumbnail+ '">'+


            ' <div class="ribbon popular"></div>'+

            '<div style="position: static" class="price-tag">'+
                ' <div class="price"><span>'+item.cars_attributes.price+'</span></div>'+


                ' <div style="float: right;margin-top: -7%;"><a class="save_car_item_id_FromHome" data-id="'+item.id +'"  data-saved="'+item.saved_ads_count +'"  >';

                        if(item.saved_ads_count==0){
                        selectHtml+=  '<i id="changeStarSave_'+item.id+'" style="margin-left: -50%;" class="fa fa-star-o"></i>';
                           }

                        if(item.saved_ads_count==1){
                        selectHtml+= '<i id="changeStarSave_'+item.id+'" style="margin-left: -50%;" class="fa fa-star"></i>';
                        }

                        selectHtml+=    '</a> </div>'+
                '</div>'+
            ' </div>'+
        '<div class="short-description-1 clearfix">'+
            {{--'<div class="category-title"> <span><a>Car</a></span> </div>'+--}}
            ' <h3><a  data-id="'+item.id +'" class="detailFromIndex" title="">'+item.title+' </a></h3>'+

            '<ul class="list-unstyled">'+
                '<li><a ><i class="flaticon-car-2"></i>'+item.cars_attributes.model.title+'</a></li>'+
                '<li><a ><i class="flaticon-dashboard"></i>'+ item.cars_attributes.milage+ 'km</a></li>'+
                '<li><a ><i class="flaticon-calendar"></i>'+item.cars_attributes.year+'</a></li>'+

                '</ul>'+
            '<p class="location"><i class="fa fa-map-marker"></i>'+ item.town.name+ '</p>'+

            '<p class="location"><i class="fa fa-eye"></i>'+ item.total_views+ '</p>'+

            ' <p><i class="flaticon-calendar"></i> &nbsp;<span>'+item.created_at.substr(0,10) +'</span> </p>'+

            '</div>'+
        ' <div class="ad-info-1">';

            if(item.owner.profile_pic!=null){
            selectHtml+= '<img style="max-width: 106px;display: inline" alt="profile_Picture" class="img-responsive" src="http://4000.global/public/img/'+item.owner.profile_pic+ '">';
            }

            selectHtml+=


            '<ul class="pull-right">';

                if(item.owner.name!=null){

                selectHtml+= '<li style="width: 110px;margin-top: 13px;"><i class="fa fa-user"></i>'+item.owner.name+'</li>';

                }
                if(item.owner.town!=null){
                selectHtml+= '<li style="width: 129px;margin-top: 13px;"><i class="fa fa-map-marker"></i>'+item.owner.town+'</li>';

                }


                selectHtml+= '</ul>'+
            ' </div>'+
        '</div>'+
    '</div>';






 selectHtmlGrid+='<div class="col-md-12 col-xs-12 col-xs-12">'+
                                '<div class="posts-masonry">'+
                                   ' <div class="ads-list-archive">'+
                                        '<div class="col-lg-5 col-md-5 col-sm-5 no-padding">'+
                                           '<div class="ad-archive-img">'+
                                                '<a >'+
                                                    '<img class="img-responsive" src="http://4000.global/public/img/'+item.thumbnail+ '" alt="">'+
                                                '</a>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="clearfix visible-xs-block"></div>'+
                                        '<div class="col-lg-7 col-md-7 col-sm-7 no-padding">'+
                                            '<div class="ad-archive-desc">'+
                                                '<img alt="" class="pull-right" src="{{asset('theme/images/certified.png')}}">'+
                                                '<h3>'+item.title+'</h3>'+
                                                '<div class="category-title"> <span><a href="#">Car &amp; Bikes</a></span> </div>'+
                                                '<div class="clearfix visible-xs-block"></div>'+
                                                '<p class="hidden-sm">'+item.description+'</p>'+
                                                '<ul class="short-meta list-inline">'+
                                                  '<li><a href="#">'+item.cars_attributes.fuel.title+'</a></li>'+
                                                    '<li><a href="#">'+item.cars_attributes.milage+'</a></li>'+
                                                    '<li><a href="#">'+item.cars_attributes.transmission.title+'</a></li>'+
                                                    '<li><a href="#">'+item.cars_attributes.location+'</a></li>'+
                                                '</ul>'+
                                                '<div class="ad-price-simple">'+item.cars_attributes.price+'</div>'+
                                                '<div class="clearfix archive-history">'+
                                                    '<div class="last-updated">Last Updated: '+item.created_at.substr(0,10)+'</div>'+
                                                    '<div class="ad-meta"><a data-id="'+item.id +'"  data-saved="'+item.saved_ads_count +'"   class="btn save-ad save_car_item_id_FromHome">';


                                if(item.saved_ads_count==0){
                                selectHtmlGrid+=   '<i id="changeStarSaveH_'+item.id+'" class="fa fa-heart-o"></i>';
                                }

                                if(item.saved_ads_count==1){
                                selectHtmlGrid+=  '<i id="changeStarSaveH_'+item.id+'" class="fa fa-heart"></i>';
                                }

                                selectHtmlGrid+= 'Save Ad.</a>'+
                                                    '<a data-id="'+item.id +'"    class="btn btn-success detailFromIndex"><i class="fa fa-phone"></i> View Details.</a> </div>'+
                                                '</div>'+


                                            '</div>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                           '</div>';


selectHtmlLatestAds+='<div class="recent-ads-list">'+
    '<div class="recent-ads-container">'+
       ' <div class="recent-ads-list-image">'+
            '<a  class="recent-ads-list-image-inner">'+
               ' <img  data-id="'+item.id +'" class="detailFromIndex"  src="http://4000.global/public/img/'+item.thumbnail+ '" alt="">'+
            '</a>'+
       ' </div>'+
       ' <div class="recent-ads-list-content">'+
            '<h3 class="recent-ads-list-title">'+
                '<a data-id="'+item.id +'" class="detailFromIndex">'+item.cars_attributes.model.title+' </a>'+
           ' </h3>'+
           ' <ul class="recent-ads-list-location">'+
            '    <li><a >'+item.town.name+'</a></li>'+
           ' </ul>'+
            '<div class="recent-ads-list-price">'+item.cars_attributes.price+ '</div>'+
        '</div>'+
    '</div>'+
'</div>';



});
            $('#cardiv').append(selectHtml);
             $('#cardivGrid').append(selectHtmlGrid);
$('#recent-ads').append(selectHtmlLatestAds);


    console.log('hey');





    },


    error: function (res, response) {
    },


    });
    }


    }



    var formDataInit = new FormData();
    getData(0, formDataInit);


localStorage.removeItem("filter_town");
localStorage.removeItem("filter_colorr");
localStorage.removeItem("filter_condition");
localStorage.removeItem("filter_fuel");
localStorage.removeItem("filter_modell");
localStorage.removeItem("filtter_option");
localStorage.removeItem("filter_transmition");
localStorage.removeItem("filter_body");
localStorage.removeItem("filter_form_mileage");
localStorage.removeItem("filter_from_year");
localStorage.removeItem("filter_from_price");



@endsection














