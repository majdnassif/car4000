@extends('welcome')

@section('content')

    <!-- =-=-=-=-=-=-= Breadcrumb =-=-=-=-=-=-= -->
    <div class="page-header-area-2 gray">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="small-breadcrumb">
                        <div class="breadcrumb-link">
                            <ul>
                                <li><a href="index.html">Home Page</a></li>
                                <li><a class="active" href="#">Compare</a></li>
                            </ul>
                        </div>
                        <div class="header-page">
                            <h1>Car Comparison</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- =-=-=-=-=-=-= Breadcrumb End =-=-=-=-=-=-= -->
    <!-- =-=-=-=-=-=-= Main Content Area =-=-=-=-=-=-= -->
    <div class="main-content-area clearfix">
        <!-- =-=-=-=-=-=-= Car Comparison =-=-=-=-=-=-= -->
        <section class="section-padding no-top reviews gray ">
            <!-- Main Container -->
            <div class="container">
                <!-- Row -->
                <div class="row">
                    <!-- Middle Content Area -->
                    <div class="col-md-8 col-xs-12 col-sm-12">
                        <div class="row">
                            <!-- Car Comparison Archive -->
                            <ul class="compare-masonry text-center">
                                <!-- Comparison -->
                                <li class="col-md-12 col-sm-6 col-xs-12">
                                    <div class="comparison-box">
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="compare-grid">
                                                <a href="#"><img src="{{asset('theme/images/compare/2.png')}}" alt="" class="img-responsive"></a>
                                                <h2><a href="#">2016 Ford Escape cape  </a></h2>
                                                <div class="rating">
                                                    <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <span class="rating-count">(4)</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vsbox">vs</div>
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="compare-grid">
                                                <a href="#"><img src="{{asset('theme/images/compare/1.png')}}" alt="" class="img-responsive"></a>
                                                <h2><a href="#">2017 Chevrolet Camaro </a></h2>
                                                <div class="rating">
                                                    <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <span class="rating-count">(3)</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </li>
                                <!-- Comparison -->
                                <li class="col-md-12 col-sm-6 col-xs-12">
                                    <div class="comparison-box">
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="compare-grid">
                                                <a href="#" class=""><img src="{{asset('theme/images/compare/3.png')}}" alt="" class="img-responsive"></a>
                                                <h2><a href="#">2017 Chevrolet Corvette </a></h2>
                                                <div class="rating">
                                                    <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <span class="rating-count">(3)</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vsbox">vs</div>
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="compare-grid">
                                                <a href="" class=""><img src="{{asset('theme/images/compare/4.png')}}" alt="" class="img-responsive"></a>
                                                <h2><a href="#">2017 Honda Accord Sedan </a></h2>
                                                <div class="rating">
                                                    <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <span class="rating-count">(3)</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </li>
                                <!-- Comparison -->
                                <li class="col-md-12 col-sm-6 col-xs-12">
                                    <div class="comparison-box">
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="compare-grid">
                                                <a href="#" class=""><img src="{{asset('theme/images/compare/5.png')}}" alt="" class="img-responsive"></a>
                                                <h2><a href="#">Mercedes-Benz C-Class </a></h2>
                                                <div class="rating">
                                                    <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <span class="rating-count">(3)</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vsbox">vs</div>
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="compare-grid">
                                                <a href="" class=""><img src="{{asset('theme/images/compare/6.png')}}" alt="" class="img-responsive"></a>
                                                <h2><a href="#">2017 Honda CR-V </a></h2>
                                                <div class="rating">
                                                    <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <span class="rating-count">(3)</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </li>
                                <!-- Comparison -->
                                <li class="col-md-12 col-sm-6 col-xs-12">
                                    <div class="comparison-box">
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="compare-grid">
                                                <a href="#" class=""><img src="{{asset('theme/images/compare/7.png')}}" alt="" class="img-responsive"></a>
                                                <h2><a href="#">2016 Ford Mustang</a></h2>
                                                <div class="rating">
                                                    <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <span class="rating-count">(3)</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vsbox">vs</div>
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="compare-grid">
                                                <a href="" class=""><img src="{{asset('theme/images/compare/8.png')}}" alt="" class="img-responsive"></a>
                                                <h2><a href="#">2017 Toyota RAV4 </a></h2>
                                                <div class="rating">
                                                    <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <span class="rating-count">(3)</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </li>
                            </ul>
                            <div class="col-md-12 col-xs-12 col-sm-12">
                                <ul class="pagination pagination-lg">
                                    <li> <a href="#"> <i class="fa fa-chevron-left" aria-hidden="true"></i></a></li>
                                    <li> <a href="#">1</a> </li>
                                    <li class="active"> <a href="#">2</a> </li>
                                    <li> <a href="#">3</a> </li>
                                    <li> <a href="#">4</a> </li>
                                    <li><a href="#"> <i class="fa fa-chevron-right" aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- Right Sidebar -->
                    <div class="col-md-4 col-xs-12 col-sm-12">
                        <!-- Sidebar Widgets -->
                        <div class="blog-sidebar">
                            <!-- Latest News -->
                            <div class="widget">
                                <div class="widget-heading">
                                    <h4 class="panel-title"><a>Latest Reviews</a></h4>
                                </div>
                                <div class="recent-ads">
                                    <!-- Ads -->
                                    <div class="recent-ads-list">
                                        <div class="recent-ads-container">
                                            <div class="recent-ads-list-image">
                                                <a href="#" class="recent-ads-list-image-inner">
                                                    <img src="{{asset('theme/images/posting/thumb-1.jpg')}}" alt="">
                                                </a><!-- /.recent-ads-list-image-inner -->
                                            </div>
                                            <!-- /.recent-ads-list-image -->
                                            <div class="recent-ads-list-content">
                                                <h3 class="recent-ads-list-title">
                                                    <a href="#">2010 Audi A5 Auto Premium quattro MY10</a>
                                                </h3>
                                                <ul class="recent-ads-list-location">
                                                    <li><a href="#">New York</a></li>
                                                </ul>
                                                <!-- /.recent-ads-list-price -->
                                            </div>
                                            <!-- /.recent-ads-list-content -->
                                        </div>
                                        <!-- /.recent-ads-container -->
                                    </div>
                                    <!-- Ads -->
                                    <div class="recent-ads-list">
                                        <div class="recent-ads-container">
                                            <div class="recent-ads-list-image">
                                                <a href="#" class="recent-ads-list-image-inner">
                                                    <img src="{{asset('theme/images/posting/thumb-2.jpg')}}" alt="">
                                                </a><!-- /.recent-ads-list-image-inner -->
                                            </div>
                                            <!-- /.recent-ads-list-image -->
                                            <div class="recent-ads-list-content">
                                                <h3 class="recent-ads-list-title">
                                                    <a href="#">Honda Civic 2017 Sports Edition With Turbo</a>
                                                </h3>
                                                <ul class="recent-ads-list-location">
                                                    <li><a href="#">Brooklyn</a></li>
                                                </ul>
                                                <!-- /.recent-ads-list-price -->
                                            </div>
                                            <!-- /.recent-ads-list-content -->
                                        </div>
                                        <!-- /.recent-ads-container -->
                                    </div>
                                    <!-- Ads -->
                                    <div class="recent-ads-list">
                                        <div class="recent-ads-container">
                                            <div class="recent-ads-list-image">
                                                <a href="#" class="recent-ads-list-image-inner">
                                                    <img src="{{asset('theme/images/posting/thumb-3.jpg')}}" alt="">
                                                </a><!-- /.recent-ads-list-image-inner -->
                                            </div>
                                            <!-- /.recent-ads-list-image -->
                                            <div class="recent-ads-list-content">
                                                <h3 class="recent-ads-list-title">
                                                    <a href="#">Ford Mustang EcoBoost Premium Convertible</a>
                                                </h3>
                                                <ul class="recent-ads-list-location">
                                                    <li><a href="#">Brooklyn</a></li>
                                                </ul>
                                            </div>
                                            <!-- /.recent-ads-list-content -->
                                        </div>
                                        <!-- /.recent-ads-container -->
                                    </div>
                                    <!-- Ads -->
                                    <div class="recent-ads-list">
                                        <div class="recent-ads-container">
                                            <div class="recent-ads-list-image">
                                                <a href="#" class="recent-ads-list-image-inner">
                                                    <img src="{{asset('theme/images/posting/thumb-4.jpg')}}" alt="">
                                                </a><!-- /.recent-ads-list-image-inner -->
                                            </div>
                                            <!-- /.recent-ads-list-image -->
                                            <div class="recent-ads-list-content">
                                                <h3 class="recent-ads-list-title">
                                                    <a href="#">2017 Bugatti Chiron: Again with the Overkill </a>
                                                </h3>
                                                <ul class="recent-ads-list-location">
                                                    <li><a href="#">Brooklyn</a></li>
                                                </ul>
                                            </div>
                                            <!-- /.recent-ads-list-content -->
                                        </div>
                                        <!-- /.recent-ads-container -->
                                    </div>
                                    <!-- Ads -->
                                    <div class="recent-ads-list">
                                        <div class="recent-ads-container">
                                            <div class="recent-ads-list-image">
                                                <a href="#" class="recent-ads-list-image-inner">
                                                    <img src="{{asset('theme/images/posting/thumb-5.jpg')}}" alt="">
                                                </a><!-- /.recent-ads-list-image-inner -->
                                            </div>
                                            <!-- /.recent-ads-list-image -->
                                            <div class="recent-ads-list-content">
                                                <h3 class="recent-ads-list-title">
                                                    <a href="#">Porsche 911 Carrera 2017  Super Charger</a>
                                                </h3>
                                                <ul class="recent-ads-list-location">
                                                    <li><a href="#">Brooklyn</a></li>
                                                </ul>
                                            </div>
                                            <!-- /.recent-ads-list-content -->
                                        </div>
                                        <!-- /.recent-ads-container -->
                                    </div>
                                </div>
                            </div>
                            <!-- Reviews By Brands  -->
                            <div class="widget">
                                <div class="widget-heading">
                                    <h4 class="panel-title"><a>Reviews By Top 20 Brands</a></h4>
                                </div>
                                <table class="table">
                                    <tbody>
                                    <tr>
                                        <td>
                                            <a href="#">
                                                Mercedes-Benz
                                            </a>
                                        </td>
                                        <td>
                                            <a href="#">
                                                Maruti Suzuki
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="#">
                                                Audi
                                            </a>
                                        </td>
                                        <td>
                                            <a href="#">
                                                Hyundai
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="#">
                                                BMW
                                            </a>
                                        </td>
                                        <td>
                                            <a href="#">
                                                Tata
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="#">
                                                Volkswagen
                                            </a>
                                        </td>
                                        <td>
                                            <a href="#">
                                                Mahindra
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="#">
                                                Renault
                                            </a>
                                        </td>
                                        <td>
                                            <a href="#">
                                                Honda
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="#">
                                                Toyota
                                            </a>
                                        </td>
                                        <td>
                                            <a href="#">
                                                Skoda
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="#">
                                                Nissan
                                            </a>
                                        </td>
                                        <td>
                                            <a href="#">
                                                Ford
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="#">
                                                Porsche
                                            </a>
                                        </td>
                                        <td>
                                            <a href="#">
                                                Fiat
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="#">
                                                Chevrolet
                                            </a>
                                        </td>
                                        <td>
                                            <a href="#">
                                                Volvo
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="#">
                                                Jaguar
                                            </a>
                                        </td>
                                        <td>
                                            <a href="#">
                                                Mini
                                            </a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- Sidebar Widgets End -->
                    </div>
                    <!-- Middle Content Area  End -->
                </div>
                <!-- Row End -->
            </div>
            <!-- Main Container End -->
        </section>
        <!-- =-=-=-=-=-=-= Comparison End =-=-=-=-=-=-= -->
    </div>
@endsection
