@extends('welcome')

@section('content')
<?php


if(isset($_COOKIE['carAdDetail']))
    $carAdDetail = json_decode($_COOKIE['carAdDetail'],true);
?>
    <!-- =-=-=-=-=-=-= Breadcrumb =-=-=-=-=-=-= -->
    <div class="page-header-area-2 gray">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="small-breadcrumb">
                        <div class=" breadcrumb-link">
                            <ul>
                                <li><a href="/">Home Page</a></li>
                                <li><a class="active" href="reviews">Detail</a></li>
                            </ul>
                        </div>
                        <div class="header-page">
                            <h1>{{  $carAdDetail['title']  }} </h1>
                       {{--     <div class="pro-rating ">
                                <a href="#"><i class="fa fa-star"></i></a>
                                <a href="#"><i class="fa fa-star"></i></a>
                                <a href="#"><i class="fa fa-star"></i></a>
                                <a href="#"><i class="fa fa-star"></i></a>
                                <a href="#"><i class="fa fa-star-o"></i></a>
                                <span class="star-score">
                           <strong>3.9</strong>
                           / <span>5</span>
                           </span>
                            </div>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- =-=-=-=-=-=-= Breadcrumb End =-=-=-=-=-=-= -->
    <!-- =-=-=-=-=-=-= Main Content Area =-=-=-=-=-=-= -->
    <div class="main-content-area clearfix">
        <!-- =-=-=-=-=-=-= Latest Ads =-=-=-=-=-=-= -->
        <section class="section-padding no-top gray review-details ">
            <!-- Main Container -->
            <div class="container">
                <!-- Row -->
                <div class="row">
                    <!-- Middle Content Area -->
                    <div class="col-md-8 col-xs-12 col-sm-12">
                        <div class="blog-detial">
                            <!-- Blog Archive -->
                            <div class="blog-post">
                                <div class="post-img">
                                    <a   data-fancybox="group" >
                                        <img class="img-responsive large-img" alt="" src="{{asset('http://4000.global/public/img/'.$carAdDetail['thumbnail'])}}" > </a>
                                </div>
                                <div class="review-excerpt">
                                    <h3>Overview Of Car</h3>
                                    <p>
{{--
                                        You only need to drive the A5 Cabriolet 100-meters to realise Audi has thrown everything it knows at its latest four-seat convertible. While the intense levels of body shake that blighted the previous A5 Cabriolet have been reduced exponentially – thanks in no small part to a body that is a whopping 40% stiffer – the line-up is complemented by a range of powerful, efficient and remarkably refined engines.
--}}

                                        {{$carAdDetail['description']}}
                                    </p>
                                 {{--   <p>
                                        And the cabin is a near-perfect execution of precision and quality, something we’ve almost come to take for granted from Audi these days Cabriolet.
                                    </p>--}}
                                    <table class="table">
                                        <tbody>
                                        {{--<tr>
                                            <th>On Sale:</th>
                                            <td>Now</td>
                                        </tr>--}}
                                        <tr>
                                            <th>Price:</th>

                                            <td>{{$carAdDetail['cars_attributes']['price']}}</td>
                                        </tr>

                                        <tr>
                                            <th>location:</th>

                                            <td>{{$carAdDetail['cars_attributes']['location']}}</td>
                                        </tr>

                                        {{--<tr>
                                            <th>Engine:</th>
                                            <td>2.0L DOHC 16-valve I-4/158 hp @ 6,500 rpm, 138 lb-ft @ 4,200 rpm</td>
                                        </tr>--}}
                                        <tr>
                                            <th>Transmission:</th>

                                            <td>{{$carAdDetail['cars_attributes']['transmission']['title']}}</td>
                                        </tr>
                                        <tr>
                                            <th>Layout:</th>
                                            <td> {{$carAdDetail['cars_attributes']['n_doors']  }}-door,  {{$carAdDetail['cars_attributes']['seats']  }}-seats,{{$carAdDetail['cars_attributes']['color']['title']  }}-color</td>
                                        </tr>
                                        <tr>
                                            <th>Mileage:</th>
                                            <td>{{$carAdDetail['cars_attributes']['milage']}}</td>
                                        </tr>
                                        <tr>
                                            <th>year:</th>
                                            <td>{{$carAdDetail['cars_attributes']['year']}}</td>
                                        </tr>
                                        <tr>
                                            <th>finance:</th>
                                            <td>{{$carAdDetail['cars_attributes']['finance']}}</td>
                                        </tr>
                                        <tr>
                                            <th>warranty:</th>
                                            <td>{{$carAdDetail['cars_attributes']['warranty']}}</td>
                                        </tr>
                                        <tr>
                                            <th>fuel:</th>
                                            <td>{{$carAdDetail['cars_attributes']['fuel']['title']}}</td>
                                        </tr>

                                        <tr>
                                            <th>condition:</th>
                                            <td>{{$carAdDetail['cars_attributes']['condition']['title']}}</td>
                                        </tr>

                                        <tr>
                                            <th>model:</th>
                                            <td>{{$carAdDetail['cars_attributes']['model']['title']}}</td>
                                        </tr>
                                        <tr>
                                            <th>option:</th>
                                            <td>{{$carAdDetail['cars_attributes']['option']['title']}}</td>
                                        </tr>

                                        <tr>
                                            <th>cylinders:</th>
                                            <td>{{$carAdDetail['cars_attributes']['n_cylinders']}}</td>
                                        </tr>


                                       {{-- <tr>
                                            <th>Top Speed:</th>
                                            <td>N/A</td>
                                        </tr>--}}
                                        </tbody>
                                    </table>
                                    <div class="row pro-cons">
                                        <div class="col-md-6 col-sm-12 col-xs-12 ">
                                            <div class="pro-section">
                                                <img src="{{asset('theme/images/like.png')}}"  alt="">
                                                <h3 class="cui-delta">What We Like</h3>
                                                <ul class="standard-list">
                                                    <li>Acceleration</li>
                                                    <li>Excellent ride quality</li>
                                                    <li>Rich interior materials</li>
                                                    <li>Audi refinement</li>
                                                    <li>Leather upholstery standard</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12 col-xs-12 ">
                                            <div class="cons-section">
                                                <img src="{{asset('theme/images/dislike.png')}}"  alt="">
                                                <h3>What We Don't</h3>
                                                <ul class="standard-list">
                                                    <li>Acceleration</li>
                                                    <li>Excellent ride quality</li>
                                                    <li>Rich interior materials</li>
                                                    <li>Audi refinement</li>
                                                    <li>Leather upholstery standard</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                               {{--     <h3>Exterior</h3>
                                    <p>
                                        You only need to drive the A5 Cabriolet 100-meters to realise Audi has thrown everything it knows at its latest four-seat convertible. While the intense levels of body shake that blighted the previous A5 Cabriolet have been reduced exponentially – thanks in no small part to a body that is a whopping 40% stiffer – the line-up is complemented by a range of powerful, efficient and remarkably refined engines.
                                    </p>
                                    <a href="{{asset('theme/images/review/1.jpg')}}" data-fancybox="group" ><img src="{{asset('theme/images/review/1.jpg')}}" alt="" class="img-responsive"></a>
                                    <p>
                                        You only need to drive the A5 Cabriolet 100-meters to realise Audi has thrown everything it knows at its latest four-seat convertible. While the intense levels of body shake that blighted the previous
                                    </p>
                                    <a href="{{asset('theme/images/review/2.jpg')}}" data-fancybox="group" ><img src="{{asset('theme/images/review/2.jpg')}}" alt="" class="img-responsive"></a>
                                    <h3>The Interiot</h3>
                                    <p>
                                        You only need to drive the A5 Cabriolet 100-meters to realise Audi has thrown everything it knows at its latest four-seat convertible. While the intense levels of body shake that blighted the previous A5 Cabriolet have been reduced exponentially – thanks in no small part to a body that is a whopping 40% stiffer – the line-up is complemented by a range of powerful, efficient and remarkably refined engines.
                                    </p>
                                    <a href="{{asset('theme/images/review/3.jpg')}}" data-fancybox="group" ><img src="{{asset('theme/images/review/3.jpg')}}" alt="" class="img-responsive"></a>
                                    <p>
                                        You only need to drive the A5 Cabriolet 100-meters to realise Audi has thrown everything it knows at its latest four-seat convertible. While the intense levels of body shake that blighted the previous
                                    </p>
                                    <a href="{{asset('theme/images/review/1.jpg')}}"  data-fancybox="group" ><img src="{{asset('theme/images/review/4.jpg')}}"  alt="" class="img-responsive"> </a>
                                    <ul class="gallery list-inline clearfix">
                                        <li><a href="{{asset('theme/images/review/1.jpg')}}" data-fancybox="group"><img src="{{asset('theme/images/posting/thumb-1.jpg')}}" alt="" /></a></li>
                                        <li><a href="{{asset('theme/images/review/2.jpg')}}" data-fancybox="group"><img src="{{asset('theme/images/posting/thumb-1.jpg')}}"  alt="" /></a></li>
                                        <li><a href="{{asset('theme/images/review/3.jpg')}}" data-fancybox="group"><img src="{{asset('theme/images/posting/thumb-2.jpg')}}"  alt="" /></a></li>
                                        <li><a href="{{asset('theme/images/review/4.jpg')}}" data-fancybox="group"><img src="{{asset('theme/images/posting/thumb-3.jpg')}}"  alt="" /></a></li>
                                        <li><a href="{{asset('theme/images/review/1.jpg')}}" data-fancybox="group"><img src="{{asset('theme/images/posting/thumb-4.jpg')}}"  alt="" /></a></li>
                                        <li><a href="{{asset('theme/images/review/2.jpg')}}"  data-fancybox="group"><img src="{{asset('theme/images/posting/thumb-5.jpg')}}"  alt="" /></a></li>
                                    </ul>--}}
                                    <h3>Video Review</h3>
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer quis erat sed lorem dictum ullamcorper. Sed vel elit sed nunc ornare auctor. Suspendisse id ullamcorper purus, sed cursus dui. Sed eget elit magna. Morbi pellentesque gravida vehicula. Nunc ullamcorper rutrum nunc, non consectetur ante egestas non. Donec elementum est at velit accumsan, nec accumsan neque porta. Nunc iaculis condimentum ipsum, eget molestie nulla.
                                    </p>
                                    <iframe  src="https://www.youtube.com/embed/jtztJikMRQc"  allowfullscreen></iframe>
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer quis erat sed lorem dictum ullamcorper. Sed vel elit sed nunc ornare auctor. Suspendisse id ullamcorper purus, sed cursus dui. Sed eget elit magna. Morbi pellentesque gravida vehicula. Nunc ullamcorper rutrum nunc, non consectetur ante egestas non. Donec elementum est at velit accumsan, nec accumsan neque porta. Nunc iaculis condimentum ipsum, eget molestie nulla.
                                    </p>
                                    <p>
                                        lacinia quis nec hendrerit purus Curabitur fermentum nec nisi eget tincidunt. Suspendisse consequat facilisis efficitur. Phasellus aliquet ipsum tellus, et dignissim orci vulputate in commodo erat ut quam ornare.
                                    </p>
                                   {{-- <h3>Related Gallery</h3>
                                    <ul class="gallery list-inline clearfix">
                                        <li><a href="{{asset('theme/images/review/1.jpg')}}" data-fancybox="group"><img src="{{asset('theme/images/posting/thumb-1.jpg')}}" alt="" /></a></li>
                                        <li><a href="{{asset('theme/images/review/2.jpg')}}" data-fancybox="group"><img src="{{asset('theme/images/posting/thumb-1.jpg')}}"  alt="" /></a></li>
                                        <li><a href="{{asset('theme/images/review/3.jpg')}}" data-fancybox="group"><img src="{{asset('theme/images/posting/thumb-2.jpg')}}"  alt="" /></a></li>
                                        <li><a href="{{asset('theme/images/review/4.jpg')}}" data-fancybox="group"><img src="{{asset('theme/images/posting/thumb-3.jpg')}}"  alt="" /></a></li>
                                        <li><a href="{{asset('theme/images/review/1.jpg')}}" data-fancybox="group"><img src="{{asset('theme/images/posting/thumb-4.jpg')}}"  alt="" /></a></li>
                                        <li><a href="{{asset('theme/images/review/2.jpg')}}"  data-fancybox="group"><img src="{{asset('theme/images/posting/thumb-5.jpg')}}"  alt="" /></a></li>
                                    </ul>--}}
                                    <p>
                                        lacinia quis nec hendrerit purus Curabitur fermentum nec nisi eget tincidunt. Suspendisse consequat facilisis efficitur. Phasellus aliquet ipsum tellus, et dignissim orci vulputate in commodo erat ut quam ornare.
                                    </p>
                                    <!-- Start "post-review" -->
                                    <div class="post-review">
                                        <h3>The CarSpot expert verdict:</h3>
                                        <div class="progress-bar-review">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-2">
                                                    <span class="progress-title">Power</span>
                                                </div>
                                                <div class="col-sm-12 col-md-9">
                                                    <div class="progress">
                                                        <div class="progress-bar">
                                                            <span data-percent="70"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-1">
                                                    <span class="progress-title">70%</span>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- / progress-bar-review -->
                                        <div class="progress-bar-review">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-2">
                                                    <span class="progress-title">Handling</span>
                                                </div>
                                                <div class="col-sm-12 col-md-9">
                                                    <div class="progress">
                                                        <div class="progress-bar">
                                                            <span data-percent="80"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-1">
                                                    <span class="progress-title">80%</span>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- / progress-bar-review -->
                                        <div class="progress-bar-review">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-2">
                                                    <span class="progress-title">Looks/Style</span>
                                                </div>
                                                <div class="col-sm-12 col-md-9">
                                                    <div class="progress">
                                                        <div class="progress-bar">
                                                            <span data-percent="95"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-1">
                                                    <span class="progress-title">65%</span>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- / progress-bar-review -->
                                        <div class="progress-bar-review">
                                            <div class="row">
                                                <div class="col-sm-12 col-md-2">
                                                    <span class="progress-title">Interior</span>
                                                </div>
                                                <div class="col-sm-12 col-md-9">
                                                    <div class="progress">
                                                        <div class="progress-bar">
                                                            <span data-percent="70"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-1">
                                                    <span class="progress-title">70%</span>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- / progress-bar-review -->
                                        <div class="summary-review">
                                            <div class="text-summary">
                                                <h5>Summary</h5>
                                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquat. Ut enim ad minim veniam,.</p>
                                            </div>
                                            <div class="final-rate">
                                                <h5 class="number">90%</h5>
                                                <h6 class="text">Excellent!</h6>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End "post-review" -->
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <!-- Blog Grid -->
                        </div>
                    </div>
                    <!-- Right Sidebar -->
{{--
                    <div class="col-md-4 col-xs-12 col-sm-12">
                        <!-- Sidebar Widgets -->
                        <div class="blog-sidebar">
                            <!-- Latest News -->
                            <div class="widget">
                                <div class="widget-heading">
                                    <h4 class="panel-title"><a>Latest Reviews</a></h4>
                                </div>
                                <div class="recent-ads">
                                    <!-- Ads -->
                                    <div class="recent-ads-list">
                                        <div class="recent-ads-container">
                                            <div class="recent-ads-list-image">
                                                <a href="#" class="recent-ads-list-image-inner">
                                                    <img src="{{asset('theme/images/posting/thumb-1.jpg')}}" alt="">
                                                </a><!-- /.recent-ads-list-image-inner -->
                                            </div>
                                            <!-- /.recent-ads-list-image -->
                                            <div class="recent-ads-list-content">
                                                <h3 class="recent-ads-list-title">
                                                    <a href="#">2010 Audi A5 Auto Premium quattro MY10</a>
                                                </h3>
                                                <ul class="recent-ads-list-location">
                                                    <li><a href="#">New York</a></li>
                                                </ul>
                                                <!-- /.recent-ads-list-price -->
                                            </div>
                                            <!-- /.recent-ads-list-content -->
                                        </div>
                                        <!-- /.recent-ads-container -->
                                    </div>
                                    <!-- Ads -->
                                    <div class="recent-ads-list">
                                        <div class="recent-ads-container">
                                            <div class="recent-ads-list-image">
                                                <a href="#" class="recent-ads-list-image-inner">
                                                    <img src="{{asset('theme/images/posting/thumb-2.jpg')}}" alt="">
                                                </a><!-- /.recent-ads-list-image-inner -->
                                            </div>
                                            <!-- /.recent-ads-list-image -->
                                            <div class="recent-ads-list-content">
                                                <h3 class="recent-ads-list-title">
                                                    <a href="#">Honda Civic 2017 Sports Edition With Turbo</a>
                                                </h3>
                                                <ul class="recent-ads-list-location">
                                                    <li><a href="#">Brooklyn</a></li>
                                                </ul>
                                                <!-- /.recent-ads-list-price -->
                                            </div>
                                            <!-- /.recent-ads-list-content -->
                                        </div>
                                        <!-- /.recent-ads-container -->
                                    </div>
                                    <!-- Ads -->
                                    <div class="recent-ads-list">
                                        <div class="recent-ads-container">
                                            <div class="recent-ads-list-image">
                                                <a href="#" class="recent-ads-list-image-inner">
                                                    <img src="{{asset('theme/images/posting/thumb-3.jpg')}}" alt="">
                                                </a><!-- /.recent-ads-list-image-inner -->
                                            </div>
                                            <!-- /.recent-ads-list-image -->
                                            <div class="recent-ads-list-content">
                                                <h3 class="recent-ads-list-title">
                                                    <a href="#">Ford Mustang EcoBoost Premium Convertible</a>
                                                </h3>
                                                <ul class="recent-ads-list-location">
                                                    <li><a href="#">Brooklyn</a></li>
                                                </ul>
                                            </div>
                                            <!-- /.recent-ads-list-content -->
                                        </div>
                                        <!-- /.recent-ads-container -->
                                    </div>
                                    <!-- Ads -->
                                    <div class="recent-ads-list">
                                        <div class="recent-ads-container">
                                            <div class="recent-ads-list-image">
                                                <a href="#" class="recent-ads-list-image-inner">
                                                    <img src="{{asset('theme/images/posting/thumb-4.jpg')}}" alt="">
                                                </a><!-- /.recent-ads-list-image-inner -->
                                            </div>
                                            <!-- /.recent-ads-list-image -->
                                            <div class="recent-ads-list-content">
                                                <h3 class="recent-ads-list-title">
                                                    <a href="#">2017 Bugatti Chiron: Again with the Overkill </a>
                                                </h3>
                                                <ul class="recent-ads-list-location">
                                                    <li><a href="#">Brooklyn</a></li>
                                                </ul>
                                            </div>
                                            <!-- /.recent-ads-list-content -->
                                        </div>
                                        <!-- /.recent-ads-container -->
                                    </div>
                                    <!-- Ads -->
                                    <div class="recent-ads-list">
                                        <div class="recent-ads-container">
                                            <div class="recent-ads-list-image">
                                                <a href="#" class="recent-ads-list-image-inner">
                                                    <img src="{{asset('theme/images/posting/thumb-5.jpg')}}"  alt="">
                                                </a><!-- /.recent-ads-list-image-inner -->
                                            </div>
                                            <!-- /.recent-ads-list-image -->
                                            <div class="recent-ads-list-content">
                                                <h3 class="recent-ads-list-title">
                                                    <a href="#">Porsche 911 Carrera 2017  Super Charger</a>
                                                </h3>
                                                <ul class="recent-ads-list-location">
                                                    <li><a href="#">Brooklyn</a></li>
                                                </ul>
                                            </div>
                                            <!-- /.recent-ads-list-content -->
                                        </div>
                                        <!-- /.recent-ads-container -->
                                    </div>
                                </div>
                            </div>
                            <!-- Reviews By Brands  -->
                            <div class="widget">
                                <div class="widget-heading">
                                    <h4 class="panel-title"><a>Reviews By Top 20 Brands</a></h4>
                                </div>
                                <table class="table">
                                    <tbody>
                                    <tr>
                                        <td>
                                            <a href="#">
                                                Mercedes-Benz
                                            </a>
                                        </td>
                                        <td>
                                            <a href="#">
                                                Maruti Suzuki
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="#">
                                                Audi
                                            </a>
                                        </td>
                                        <td>
                                            <a href="#">
                                                Hyundai
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="#">
                                                BMW
                                            </a>
                                        </td>
                                        <td>
                                            <a href="#">
                                                Tata
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="#">
                                                Volkswagen
                                            </a>
                                        </td>
                                        <td>
                                            <a href="#">
                                                Mahindra
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="#">
                                                Renault
                                            </a>
                                        </td>
                                        <td>
                                            <a href="#">
                                                Honda
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="#">
                                                Toyota
                                            </a>
                                        </td>
                                        <td>
                                            <a href="#">
                                                Skoda
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="#">
                                                Nissan
                                            </a>
                                        </td>
                                        <td>
                                            <a href="#">
                                                Ford
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="#">
                                                Porsche
                                            </a>
                                        </td>
                                        <td>
                                            <a href="#">
                                                Fiat
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="#">
                                                Chevrolet
                                            </a>
                                        </td>
                                        <td>
                                            <a href="#">
                                                Volvo
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="#">
                                                Jaguar
                                            </a>
                                        </td>
                                        <td>
                                            <a href="#">
                                                Mini
                                            </a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- Sidebar Widgets End -->
                    </div>
--}}
                    <!-- Middle Content Area  End -->
                </div>
                <!-- Row End -->
            </div>
            <!-- Main Container End -->
        </section>
        <!-- =-=-=-=-=-=-= Ads Archives End =-=-=-=-=-=-= -->
    </div>
@endsection
