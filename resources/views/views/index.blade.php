@extends('welcome')

@section('content')
    <div id="banner">
        <div class="container">
            <div class="search-container">
                <!-- Form -->
                <h2>What are you looking for ?</h2>
                <p>Search <strong>267,241</strong> new ads -<strong> 83 </strong> added today</p>
                <a id="Sell_Your_Car_BtnN" href="{{route('post-ad-1')}}" class="btn btn-theme">Post Your Ad</a>

                <a   id="Login_Sell_Your_Car_BtnN" href="{{route('login')}}" class="btn btn-theme">Log In To Post Your Ad</a>

            </div>

            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3 no-padding" style="margin-top: 2%;">

                <label> </label>
                <div class="form-group">
                    <select class=" form-control " id ="towns" name="towns">

                    </select>
                </div>
            </div>

        </div>


    </div>
    <!-- =-=-=-=-=-=-= Home Banner End =-=-=-=-=-=-= -->
    <!-- =-=-=-=-=-=-= Advanced Search =-=-=-=-=-=-= -->



    <div class="advance-search">

        <div class="section-search search-style-2">
            <div class="container">


                <div class="row">
                    <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs">
                            <li class="nav-item active">
                                <a class="nav-link" data-toggle="tab" href="#saletab">Sale </a>
                            </li>

                            <li class="nav-item ">
                                <a class="nav-link" data-toggle="tab" href="#renttab">Rent </a>
                            </li>

                            <li class="nav-item ">
                                <a class="nav-link" data-toggle="tab" href="#wantedtab">Wanted </a>
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link" data-toggle="tab" href="#Offerstab">Offers </a>
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link" data-toggle="tab" href="#parttab">Part </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#servicestab">Services </a>
                            </li>

{{--                            <li class="nav-item">--}}
{{--                                <a class="nav-link" data-toggle="tab" href="#tab2" >Search Car By Type</a>--}}
{{--                            </li>--}}
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content clearfix">
                            <div class="tab-pane fade in active" id="saletab">

                                <form id="searchForSaleAdForm">
                                    <div class="search-form pull-left" style="margin-bottom: -21px;">
                                        <div class="search-form-inner pull-left">


                                        {{--    <div class="col-md-3 no-padding">
                                                <div class="form-group">
                                                    <label>Keyword</label>
                                                    <input type="text" class="form-control" placeholder="Eg Honda Civic , Audi , Ford." />
                                                </div>
                                            </div>
--}}

                                 <div class="row">

                                     <div class="col-md-2 no-padding">
                                         <div class="form-group">
                                             <label> Condition</label>
                                             <select class=" form-control condition" id ="condition" name="condition"  onchange="hideShowFiltersSale()" >
                                                 <option label="Condition"></option>
                                             </select>
                                         </div>
                                     </div>



{{--
                                     <div class="col-md-2 no-padding forsaleNewUsed forsaleHeavy forsaleMotorcycle" style="display: none" >
--}}
                                     <div class="col-md-2 no-padding" id="makeSaleDiv" >

                                     <div class="form-group">
                                             <label>Make</label>
                                             <select class="form-control make" id ="make" name="make"  >
                                             </select>
                                         </div>
                                     </div>

{{--
                                     <div class="col-md-2 no-padding forsaleNewUsed  forsaleHeavy forsaleMotorcycle " style="display: none">
--}}
                                     <div class="col-md-2 no-padding" id="ModeSaleDiv">

                                     <div class="form-group">
                                             <label> Model</label>
                                             <select class=" form-control modell" id ='modell' name="modell" >

                                             </select>
                                         </div>
                                     </div>



                                     <div class="col-md-2 no-padding " id="saleBoatType"  style="display: none">
                                         <div class="form-group">
                                             <label> type</label>
                                             <select class=" form-control" id ='' name="" >

                                             </select>
                                         </div>
                                     </div>

                                     <div class="col-md-2 no-padding " id="saleBoatlength" style="display: none">
                                         <div class="form-group">
                                             <label> length</label>
                                             <input type="number" class="form-control" placeholder="ft " id="length" name="length" />

                                         </div>
                                     </div>


                                     <div class="col-md-2 no-padding">
                                         <div class="form-group">
                                             <label> Year</label>
                                             <input type="number" class="form-control" placeholder="Eg 2012,2020,.." id="from_year" name="from_year" />
                                         </div>
                                     </div>


                                     <div class="col-md-2 no-padding">
                                         <div class="form-group">
                                             <label> Price</label>
                                             <input type="number" class="form-control" placeholder="Eg 20000" id="from_price" name="from_price" />
                                         </div>
                                     </div>

                                   </div>
                                       <div class=" Show row" id="filtersAdvanceSale"  >
                                                <div class="col-md-2 no-padding">
                                                    <div class="form-group">
                                                        <label>Body</label>
                                                        <select class=" form-control body" id ="body" name="body" >
                                                            <option label="body"></option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div  class="col-md-2 no-padding " >
                                                    <div class="form-group">
                                                        <label> Color</label>
                                                        <select class=" form-control colorr" id ="colorr" name="colorr" >
                                                            <option label="Color"></option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-2 no-padding">
                                                    <div class="form-group">
                                                        <label> Mileage</label>
                                                        <input type="number" class="form-control" placeholder="Eg 1000 KM" id="from_mileage" name="from_mileage" />
                                                    </div>
                                                </div>

                                               {{-- <div class="col-md-2 no-padding">
                                                    <div class="form-group">
                                                        <label>To Mileage</label>
                                                        <input type="number" class="form-control" placeholder="Eg 1000,120000" id="to_mileage" name="to_mileage" />
                                                    </div>
                                                </div>--}}


                                                <div class="col-md-2 no-padding">
                                                    <div class="form-group">
                                                        <label>Transmition</label>
                                                        <select class=" form-control transmition" id ="transmition" name="transmition" >
                                                            <option label="Car Type"></option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-2 no-padding">
                                                <div class="form-group">
                                                    <label>Fuel</label>
                                                    <select class=" form-control fuel" id ="fuel" name="fuel" >
                                                        <option label="Fuel"></option>
                                                    </select>
                                                </div>
                                            </div>

                                            </div>

                                        </div>
                                        <div class="col-md-13 no-padding">
                                        <div class="form-group pull-right">
                                            <button type="submit" value="submit" class="btn btn-lg btn-theme" >Search Now</button>
                                        </div>
                                        </div>
                                    </div>
                                </form>
                                <button type="button" class="btn btn-outline-secondary   " id="AdvanceSale" style=" border-radius: 11px; margin-left: 39%;; margin-bottom: 1%;">Advance...</button>
                            </div>

                            <div class="tab-pane fade in " id="renttab">

                                <form id="searchToRentAdForm">
                                    <div class="search-form pull-left" style="margin-bottom: -21px;">
                                        <div class="search-form-inner pull-left">


                                            {{--    <div class="col-md-3 no-padding">
                                                    <div class="form-group">
                                                        <label>Keyword</label>
                                                        <input type="text" class="form-control" placeholder="Eg Honda Civic , Audi , Ford." />
                                                    </div>
                                                </div>
    --}}

                                            <div class="row">

                                                <div class="col-md-2 no-padding">
                                                    <div class="form-group">
                                                        <label> Condition</label>
                                                        <select class=" form-control condition" id ="conditionRent" name="conditionRent"  onchange="hideShowFiltersRent()" >
                                                            <option label="Condition"></option>
                                                        </select>
                                                    </div>
                                                </div>



                                                {{--
                                                                                     <div class="col-md-2 no-padding forsaleNewUsed forsaleHeavy forsaleMotorcycle" style="display: none" >
                                                --}}
                                                <div class="col-md-2 no-padding " id="makeRentDiv" >

                                                    <div class="form-group">
                                                        <label>Make</label>
                                                        <select class="form-control make" id ="makeRent" name="makeRent"  >
                                                            <option label="location"></option>
                                                        </select>
                                                    </div>
                                                </div>

                                                {{--
                                                                                     <div class="col-md-2 no-padding forsaleNewUsed  forsaleHeavy forsaleMotorcycle " style="display: none">
                                                --}}
                                                <div class="col-md-2 no-padding" id="ModeRentDiv">

                                                    <div class="form-group">
                                                        <label> Model</label>
                                                        <select class=" form-control modell" id ='modellRent' name="modellRent" >
                                                            <option label="Model"></option>
                                                        </select>
                                                    </div>
                                                </div>



                                                <div class="col-md-2 no-padding" id="rentBoatType" style="display: none">
                                                    <div class="form-group">
                                                        <label> type</label>
                                                        <select class=" form-control" id ='' name="" >

                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-2 no-padding " id="rentBoatlength" style="display: none">
                                                    <div class="form-group">
                                                        <label> length</label>
                                                        <input type="number" class="form-control" placeholder="ft " id="length" name="length" />

                                                    </div>
                                                </div>


                                                <div class="col-md-2 no-padding">
                                                    <div class="form-group">
                                                        <label> Year</label>
                                                        <input type="number" class="form-control" placeholder="Eg 2012,2020,.." id="from_yearRent" name="from_yearRent" />
                                                    </div>
                                                </div>



                                                <div class="col-md-2 no-padding">
                                                    <div class="form-group">
                                                        <label> Price</label>
                                                        <input type="number" class="form-control" placeholder="Eg 20000" id="from_priceRent" name="from_priceRent" />
                                                    </div>
                                                </div>
                                                {{--
                                                                                     <div class="col-md-2 no-padding">
                                                                                         <div class="form-group">
                                                                                             <label>To Price</label>
                                                                                             <input type="number" class="form-control" placeholder="Eg 2012,2020,.." id="to_price" name="to_price" />
                                                                                         </div>
                                                                                     </div>--}}

                                            </div>


                                            <div class=" Show row" id="filtersAdvanceRent" >


                                                <div class="col-md-2 no-padding">
                                                    <div class="form-group">
                                                        <label>Body</label>
                                                        <select class=" form-control body" id ="bodyRent" name="bodyRent" >
                                                            <option label="body"></option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div  class="col-md-2 no-padding " >
                                                    <div class="form-group">
                                                        <label> Color</label>
                                                        <select class=" form-control colorr" id ="colorrRent" name="colorrRent" >
                                                            <option label="Color"></option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-2 no-padding">
                                                    <div class="form-group">
                                                        <label> Mileage</label>
                                                        <input type="number" class="form-control" placeholder="Eg 1000 KM" id="from_mileageRent" name="from_mileageRent" />
                                                    </div>
                                                </div>

                                                {{-- <div class="col-md-2 no-padding">
                                                     <div class="form-group">
                                                         <label>To Mileage</label>
                                                         <input type="number" class="form-control" placeholder="Eg 1000,120000" id="to_mileage" name="to_mileage" />
                                                     </div>
                                                 </div>--}}


                                                <div class="col-md-2 no-padding">
                                                    <div class="form-group">
                                                        <label>Transmition</label>
                                                        <select class=" form-control transmition" id ="transmitionRent" name="transmitionRent" >
                                                            <option label="Car Type"></option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-2 no-padding">
                                                    <div class="form-group">
                                                        <label>Fuel</label>
                                                        <select class=" form-control fuel" id ="fuelRent" name="fuelRent" >
                                                            <option label="Fuel"></option>
                                                        </select>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                        <div class="col-md-13 no-padding">
                                            <div class="form-group pull-right">
                                                <button type="submit" value="submit" class="btn btn-lg btn-theme" >Search Now</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <button type="button" class="btn btn-outline-secondary  " id="AdvanceRent" style=" border-radius: 11px; margin-left: 39%;; margin-bottom: 1%;">Advance...</button>
                            </div>

                            <div class="tab-pane fade in " id="wantedtab">

                                <form id="searchWantedAdForm">
                                    <div class="search-form pull-left" style="margin-bottom: -21px;">
                                        <div class="search-form-inner pull-left">


                                            {{--    <div class="col-md-3 no-padding">
                                                    <div class="form-group">
                                                        <label>Keyword</label>
                                                        <input type="text" class="form-control" placeholder="Eg Honda Civic , Audi , Ford." />
                                                    </div>
                                                </div>
    --}}

                                            <div class="row">

                                                <div class="col-md-2 no-padding">
                                                    <div class="form-group">
                                                        <label> Condition</label>
                                                        <select class=" form-control condition" id ="conditionWanted" name="conditionWanted"  onchange="hideShowFiltersWanted()" >
                                                            <option label="Condition"></option>
                                                        </select>
                                                    </div>
                                                </div>



                                                {{--
                                                                                     <div class="col-md-2 no-padding forsaleNewUsed forsaleHeavy forsaleMotorcycle" style="display: none" >
                                                --}}
                                                <div class="col-md-2 no-padding" id="makeWantedDiv" >

                                                    <div class="form-group">
                                                        <label>Make</label>
                                                        <select class="form-control make" id ="makeWanted" name="makeWanted"  >
                                                            <option label="location"></option>
                                                        </select>
                                                    </div>
                                                </div>

                                                {{--
                                                                                     <div class="col-md-2 no-padding forsaleNewUsed  forsaleHeavy forsaleMotorcycle " style="display: none">
                                                --}}
                                                <div class="col-md-2 no-padding " id="modelWantedDiv">

                                                    <div class="form-group">
                                                        <label> Model</label>
                                                        <select class=" form-control modell" id ='modellWanted' name="modellWanted" >

                                                        </select>
                                                    </div>
                                                </div>



                                                <div class="col-md-2 no-padding " id="WantedBoatType" style="display: none">
                                                    <div class="form-group">
                                                        <label> type</label>
                                                        <select class=" form-control" id ='' name="" >

                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-2 no-padding " id="WantedBoatlength" style="display: none">
                                                    <div class="form-group">
                                                        <label> length</label>
                                                        <input type="number" class="form-control" placeholder="ft " id="length" name="length" />

                                                    </div>
                                                </div>


                                                <div class="col-md-2 no-padding">
                                                    <div class="form-group">
                                                        <label> Year</label>
                                                        <input type="number" class="form-control" placeholder="Eg 2012,2020,.." id="from_yearWanted" name="from_yearWanted" />
                                                    </div>
                                                </div>

                                                {{-- <div class="col-md-2 no-padding">
                                                     <div class="form-group">
                                                         <label>To Year</label>
                                                         <input type="number" class="form-control" placeholder="Eg 2012,2020,.." id="to_year" name="to_year" />
                                                     </div>
                                                 </div>
            --}}

                                                <div class="col-md-2 no-padding">
                                                    <div class="form-group">
                                                        <label> Price</label>
                                                        <input type="number" class="form-control" placeholder="Eg 20000" id="from_priceWanted" name="from_priceWanted" />
                                                    </div>
                                                </div>
                                                {{--
                                                                                     <div class="col-md-2 no-padding">
                                                                                         <div class="form-group">
                                                                                             <label>To Price</label>
                                                                                             <input type="number" class="form-control" placeholder="Eg 2012,2020,.." id="to_price" name="to_price" />
                                                                                         </div>
                                                                                     </div>--}}

                                            </div>


                                            <div class=" Show row" id="filtersAdvanceWanted"  >


                                                <div class="col-md-2 no-padding">
                                                    <div class="form-group">
                                                        <label>Body</label>
                                                        <select class=" form-control body" id ="bodyWanted" name="bodyWanted" >
                                                            <option label="body"></option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div  class="col-md-2 no-padding " >
                                                    <div class="form-group">
                                                        <label> Color</label>
                                                        <select class=" form-control colorr" id ="colorrWanted" name="colorrWanted" >
                                                            <option label="Color"></option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-2 no-padding">
                                                    <div class="form-group">
                                                        <label> Mileage</label>
                                                        <input type="number" class="form-control" placeholder="Eg 1000 KM" id="from_mileageWanted" name="from_mileageWanted" />
                                                    </div>
                                                </div>

                                                {{-- <div class="col-md-2 no-padding">
                                                     <div class="form-group">
                                                         <label>To Mileage</label>
                                                         <input type="number" class="form-control" placeholder="Eg 1000,120000" id="to_mileage" name="to_mileage" />
                                                     </div>
                                                 </div>--}}


                                                <div class="col-md-2 no-padding">
                                                    <div class="form-group">
                                                        <label>Transmition</label>
                                                        <select class=" form-control transmition" id ="transmitionWanted" name="transmitionWanted" >
                                                            <option label="Car Type"></option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-2 no-padding">
                                                    <div class="form-group">
                                                        <label>Fuel</label>
                                                        <select class=" form-control fuel" id ="fuelWanted" name="fuelWanted" >
                                                            <option label="Fuel"></option>
                                                        </select>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                        <div class="col-md-13 no-padding">
                                            <div class="form-group pull-right">
                                                <button type="submit" value="submit" class="btn btn-lg btn-theme" >Search Now</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <button type="button" class="btn btn-outline-secondary " id="AdvanceWanted" style=" border-radius: 11px; margin-left: 39%;; margin-bottom: 1%;">Advance...</button>
                            </div>

                            <div class="tab-pane fade in " id="Offerstab">

                                <form id="searchOffersForm">
                                    <div class="search-form pull-left" style="margin-bottom: -21px;">
                                        <div class="search-form-inner pull-left">

                                            <div class="row">

                                                <div class="col-md-2 no-padding">
                                                    <div class="form-group">
                                                        <label> Condition</label>
                                                        <select class=" form-control condition" id ="conditionOffers" name="conditionOffers"  onchange="hideShowFiltersOffers()" >
                                                            <option label="Condition"></option>
                                                        </select>
                                                    </div>
                                                </div>



                                                <div class="col-md-2 no-padding" id="makeOffersDiv" >

                                                    <div class="form-group">
                                                        <label>Make</label>
                                                        <select class="form-control make" id ="makeOffers" name="makeOffers"  >
                                                            <option label="location"></option>
                                                        </select>
                                                    </div>
                                                </div>


                                                <div class="col-md-2 no-padding " id="modelOffersDiv">

                                                    <div class="form-group">
                                                        <label> Model</label>
                                                        <select class=" form-control modell" id ='modellOffers' name="modellOffers" >

                                                        </select>
                                                    </div>
                                                </div>



                                                <div class="col-md-2 no-padding " id="offersBoatType" style="display: none">
                                                    <div class="form-group">
                                                        <label> type</label>
                                                        <select class=" form-control" id ='' name="" >

                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-2 no-padding " id="offersBoatlength" style="display: none">
                                                    <div class="form-group">
                                                        <label> length</label>
                                                        <input type="number" class="form-control" placeholder="ft " id="length" name="length" />

                                                    </div>
                                                </div>


                                                <div class="col-md-2 no-padding">
                                                    <div class="form-group">
                                                        <label> Year</label>
                                                        <input type="number" class="form-control" placeholder="Eg 2012,2020,.." id="from_yearOffers" name="from_yearOffers" />
                                                    </div>
                                                </div>

                                                <div class="col-md-2 no-padding">
                                                    <div class="form-group">
                                                        <label> Price</label>
                                                        <input type="number" class="form-control" placeholder="Eg 20000" id="from_priceOffers" name="from_priceOffers" />
                                                    </div>
                                                </div>

                                                            <div class="col-md-2 no-padding">
                                                                <div class="form-group">
                                                                            <label>Monthly Payment </label>
                                                                        <input type="number" class="form-control" placeholder="Eg 2012,2020,.." id="monthlyPaymentOffers" name="monthlyPaymentOffers" />
                                                                  </div>
                                                           </div>

                                            </div>


                                            <div class=" Show row" id="filtersAdvanceOffers"  >


                                                <div class="col-md-2 no-padding">
                                                    <div class="form-group">
                                                        <label>Body</label>
                                                        <select class=" form-control body" id ="bodyOffers" name="bodyOffers" >
                                                            <option label="body"></option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div  class="col-md-2 no-padding " >
                                                    <div class="form-group">
                                                        <label> Color</label>
                                                        <select class=" form-control colorr" id ="colorrOffers" name="colorrOffers" >
                                                            <option label="Color"></option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-2 no-padding">
                                                    <div class="form-group">
                                                        <label> Mileage</label>
                                                        <input type="number" class="form-control" placeholder="Eg 1000 KM" id="from_mileageOffers" name="from_mileageOffers" />
                                                    </div>
                                                </div>

                                                <div class="col-md-2 no-padding">
                                                    <div class="form-group">
                                                        <label>Transmition</label>
                                                        <select class=" form-control transmition" id ="transmitionOffers" name="transmitionOffers" >
                                                            <option label="Car Type"></option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-2 no-padding">
                                                    <div class="form-group">
                                                        <label>Fuel</label>
                                                        <select class=" form-control fuel" id ="fuelOffers" name="fuelOffers" >
                                                            <option label="Fuel"></option>
                                                        </select>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                        <div class="col-md-13 no-padding">
                                            <div class="form-group pull-right">
                                                <button type="submit" value="submit" class="btn btn-lg btn-theme" >Search Now</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <button type="button" class="btn btn-outline-secondary " id="AdvanceOffers" style=" border-radius: 11px; margin-left: 39%;; margin-bottom: 1%;">Advance...</button>
                            </div>


                            <div class="tab-pane fade in " id="parttab">

                                <form id="searchPartForm">
                                    <div class="search-form pull-left" style="margin-bottom: -21px;">
                                        <div class="search-form-inner pull-left">

                                            <div class="row">

                                                <div class="col-md-2 no-padding">
                                                    <div class="form-group">
                                                        <label> Option</label>
                                                        <select class=" form-control " id ="optionPart" name="optionPart">
                                                            <option value="" label="Option">Nothing Selected</option>
                                                            <option value="1" label="Option"> For Sale</option>
                                                            <option value="3" label="Option">Wanted</option>
                                                        </select>
                                                    </div>
                                                </div>


                                                <div class="col-md-2 no-padding">
                                                    <div class="form-group">
                                                        <label> Category</label>
                                                        <select class=" form-control " id ="categoryPart" name="categoryPart" >
                                                            <option value="" label="Condition"> Nothing Selected</option>
                                                        </select>
                                                    </div>
                                                </div>




                                                <div class="col-md-2 no-padding">
                                                    <div class="form-group">
                                                        <label> Price</label>
                                                        <input type="number" class="form-control" placeholder="Eg 20000" id="from_pricePart" name="from_pricePart" />
                                                    </div>
                                                </div>



                                            </div>


                                        </div>
                                        <div class="col-md-13 no-padding">
                                            <div class="form-group pull-right">
                                                <button type="submit" value="submit" class="btn btn-lg btn-theme" >Search Now</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
{{--
                                <button type="button" class="btn btn-outline-secondary " id="AdvanceOffers" style=" border-radius: 11px; margin-left: 39%;; margin-bottom: 1%;">Advance...</button>
--}}
                            </div>



                            <div class="tab-pane fade in " id="servicestab">

                                <form id="searchServiceForm">
                                    <div class="search-form pull-left" style="margin-bottom: -21px;">
                                        <div class="search-form-inner pull-left">

                                            <div class="row">

                                                <div class="col-md-2 no-padding">
                                                    <div class="form-group">
                                                        <label> Option</label>
                                                        <select class=" form-control " id ="optionService" name="optionService">
                                                            <option value="" label="Option">Nothing Selected</option>
                                                            <option value="4" label="Option">Offers</option>
                                                            <option value="3" label="Option">Wanted</option>
                                                        </select>
                                                    </div>
                                                </div>


                                                <div class="col-md-2 no-padding">
                                                    <div class="form-group">
                                                        <label> Category</label>
                                                        <select class=" form-control " id ="categoryService" name="categoryService" >
                                                            <option value="">Nothing Selected</option>

                                                            <option value="1">Car Rent</option>
                                                            <option value="2">Car Lift</option>
                                                            <option value="3">Transportation</option>
                                                            <option value="4">Delivery</option>
                                                            <option value="5">Recovery</option>
{{--
                                                            <option value="6">Repair - Sub category</option>
--}}
                                                            <optgroup label="Repair Sub category" id="subCategoryService">

                                                            </optgroup>


                                                            <option value="7">insurance</option>
                                                            <option value="8">Cleaning</option>
                                                        </select>




                                                    </div>
                                                </div>




                                                <div class="col-md-2 no-padding">
                                                    <div class="form-group">
                                                        <label> Price</label>
                                                        <input type="number" class="form-control" placeholder="Eg 20000" id="from_pricePart" name="from_pricePart" />
                                                    </div>
                                                </div>



                                            </div>


                                        </div>
                                        <div class="col-md-13 no-padding">
                                            <div class="form-group pull-right">
                                                <button type="submit" value="submit" class="btn btn-lg btn-theme" >Search Now</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                {{--
                                                                <button type="button" class="btn btn-outline-secondary " id="AdvanceOffers" style=" border-radius: 11px; margin-left: 39%;; margin-bottom: 1%;">Advance...</button>
                                --}}
                            </div>




                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- =-=-=-=-=-=-= Advanced Search End =-=-=-=-=-=-= -->
    <!-- =-=-=-=-=-=-= Main Content Area =-=-=-=-=-=-= -->
    <div class="main-content-area clearfix">
        <!-- =-=-=-=-=-=-= Featured Ads =-=-=-=-=-=-= -->
        <section class="custom-padding gray">
            <!-- Main Container -->
            <div class="container">
                <!-- Row -->
                <div class="row">


                    <!-- Heading Area -->
                    <div class="heading-panel">



                            <div class="col-xs-12 col-md-12 col-sm-12 left-side">
                            <!-- Main Title -->
                            <h1>Latest
{{--
                                <span class="heading-color"> Featured</span>
--}}
                                <span class="heading-color"> Ads</span>
                                </h1>
                        </div>


                    </div>


                    <!-- Middle Content Box -->
                    <div class="row grid-style-2 ">


                        <div class="col-md-12 col-xs-12 col-sm-12" id="adsArchive">

                        </div>


                    </div>

                   <a style="float: right;color: blue;" class="changAcolor"  href="{{route('adsList')}}">Show More...</a>



                    <!-- Middle Content Box End -->


                </div>
                <!-- Row End -->
            </div>

            <!-- Main Container End -->
        </section>

    </div>
@endsection






@section('script')

    {{--<script>--}}

    //==================================================================================================================
    //==================================================================================================================
    //==================================================================================================================

    var userToken =   sessionStorage.getItem("api_token")



    //==================================================================================================================
    //==================================================================================================================
    //==================================================================================================================

    dataSubCat = new FormData();
    dataSubCat.append('parent_id',6);

    var countries;
    $.ajax({

    type: "POST",
    url: Global_Url + 'carsAdsCategoriesList',
    data: dataSubCat,
    processData: false,
    contentType: false,
    success: function (res) {

    if(res.code == 200){
console.log('suuuuuub',res);
    $('#subCategoryService').empty();


    res.data.forEach(function (item, index, arr){

    $('#subCategoryService').append(
    '<option  value="'+item.id+'">'+item.name+'</option>'
    );





    });

    }else if ( res.code < 0){

    }

    },
    error: function (res, response) {
    },
    });



    //============================================================================

{{--    var vv = sessionStorage.getItem("api_token");--}}
{{--    console.log(vv);--}}
    data = new FormData();
    data.append('country_id', 2);

    var countries;
    $.ajax({

    type: "POST",
    //url: Global_Url + 'carsAdsList',
    url: Global_Url + 'townsList',
    data: data,
    // mimeType: 'application/json',
    processData: false,
    contentType: false,
    success: function (res) {
    console.log(res);
    if(res.code == 200){

    countries = res;
    $('#towns').empty();
    $('#towns').append(
    '<option value="" label="location">Location</option>'
    );

    res.data.forEach(function (item, index, arr){

    $('#towns').append(
    '<option   value="'+item.id+'" data-id="' + item.id + '">'+item.name+'</option>'
    );





    });

    }else if ( res.code < 0){

    }

    },
    error: function (res, response) {
    },
    });



    //==================================================================================================================
    //==================================================================================================================
    //==================================================================================================================

    data1 = new FormData();

    var countries;
    $.ajax({

    type: "POST",
    //url: Global_Url + 'colorsList',
    url: Global_Url + 'colorsList',
    data: data1,
    // mimeType: 'application/json',
    processData: false,
    contentType: false,
    success: function (res) {
    console.log(res);
    if(res.code == 200){

    countries = res;
    $('.colorr').empty();
    $('.colorr').append(
    '<option value="" label="Color">Nothing Selected</option>'
    );

    res.data.forEach(function (item, index, arr){

    $('.colorr').append(
    '<option   value="'+item.id+'" data-id="' + item.id + '">'+item.title+'</option>'
    );





    });

    }else if ( res.code < 0){

    }

    },
    error: function (res, response) {
    },
    });



    //==================================================================================================================
    //==================================================================================================================
    //==================================================================================================================

    data2 = new FormData();

    var countries;
    $.ajax({

    type: "POST",
    url: Global_Url + 'carConditionsList',
    data: data2,
    // mimeType: 'application/json',
    processData: false,
    contentType: false,
    success: function (res) {
    console.log(res);
    if(res.code == 200){

    countries = res;
    $('.condition').empty();
   {{-- $('#condition').append(
    '<option label="Condition"></option>'
    );--}}

    res.data.forEach(function (item, index, arr){

    $('.condition').append(
    '<option   value="'+item.id+'" data-id="' + item.id + '">'+item.title+'</option>'
    );





    });

    }else if ( res.code < 0){

    }

    },
    error: function (res, response) {
    },
    });



    //==================================================================================================================
    //==================================================================================================================
    //==================================================================================================================

    data3 = new FormData();

    var countries;
    $.ajax({

    type: "POST",
    url: Global_Url + 'carFuelsList',
    data: data3,
    // mimeType: 'application/json',
    processData: false,
    contentType: false,
    success: function (res) {
    console.log(res);
    if(res.code == 200){

    countries = res;
    $('.fuel').empty();
    $('.fuel').append(
    '<option value="" label="Fuel">Nothing Selected</option>'
    );

    res.data.forEach(function (item, index, arr){

    $('.fuel').append(
    '<option   value="'+item.id+'" data-id="' + item.id + '">'+item.title+'</option>'
    );





    });

    }else if ( res.code < 0){

    }

    },
    error: function (res, response) {
    },
    });


    //==================================================================================================================
    //==================================================================================================================
    //==================================================================================================================








    //==================================================================================================================
    //==================================================================================================================
    //==================================================================================================================


    data5 = new FormData();

    var countries;
    $.ajax({

    type: "POST",
    url: Global_Url + 'bodyTypesList',
    data: data5,
    // mimeType: 'application/json',
    processData: false,
    contentType: false,
    success: function (res) {
    console.log(res);
    if(res.code == 200){

    countries = res;
    $('.body').empty();
    $('.body').append(
    '<option  value="" label="Sell or Rent">Nothing Selected</option>'
    );

    res.data.forEach(function (item, index, arr){

    $('.body').append(
    '<option   value="'+item.id+'" data-id="' + item.id + '">'+item.title+'</option>'
    );





    });

    }else if ( res.code < 0){

    }

    },
    error: function (res, response) {
    },
    });



    //==================================================================================================================
    //==================================================================================================================
    //==================================================================================================================


    data6 = new FormData();

    var countries;
    $.ajax({

    type: "POST",
    url: Global_Url + 'carsTransmissionList',
    data: data6,
    // mimeType: 'application/json',
    processData: false,
    contentType: false,
    success: function (res) {
    console.log(res);
    if(res.code == 200){

    countries = res;
    $('.transmition').empty();
    $('.transmition').append(
    '<option value="" label="Type">Nothing Selected</option>'
    );

    res.data.forEach(function (item, index, arr){

    $('.transmition').append(
    '<option   value="'+item.id+'" data-id="' + item.id + '">'+item.title+'</option>'
    );





    });

    }else if ( res.code < 0){

    }

    },
    error: function (res, response) {
    },
    });


    //==================================================================================================================
    //==================================================================================================================
    //==================================================================================================================


    $('#searchServiceForm').submit(function(e){
    var formData = new FormData($(this)[0]);
    e.preventDefault();
    e.stopPropagation();
    {{-- localStorage.setItem('filter_condition', conditionOffers.value);

     localStorage.setItem('filter_modell', modellOffers.value);

     localStorage.setItem('filter_body', bodyOffers.value);
     localStorage.setItem('filter_colorr', colorrOffers.value);


     localStorage.setItem('filter_form_mileage', from_mileageOffers.value);
     localStorage.setItem('filter_from_year', from_yearOffers.value);--}}

    localStorage.setItem('filter_newOption', optionService.value);

    localStorage.setItem('filter_category', categoryService.value);
{{--
    localStorage.setItem('filter_from_price',);
--}}

    {{--localStorage.setItem('filter_transmition', transmitionOffers.value);
    localStorage.setItem('filter_fuel', fuelOffers.value);--}}
    localStorage.setItem('filter_town', towns.value);
    localStorage.setItem('filtter_option', 6);
    window.location.href = '/adsList';
    });


    $('#searchPartForm').submit(function(e){
    var formData = new FormData($(this)[0]);
    e.preventDefault();
    e.stopPropagation();
   {{-- localStorage.setItem('filter_condition', conditionOffers.value);

    localStorage.setItem('filter_modell', modellOffers.value);

    localStorage.setItem('filter_body', bodyOffers.value);
    localStorage.setItem('filter_colorr', colorrOffers.value);


    localStorage.setItem('filter_form_mileage', from_mileageOffers.value);
    localStorage.setItem('filter_from_year', from_yearOffers.value);--}}

    localStorage.setItem('filter_newOption', optionPart.value);

    localStorage.setItem('filter_category', categoryPart.value);
    localStorage.setItem('filter_from_price', from_pricePart.value);

    {{--localStorage.setItem('filter_transmition', transmitionOffers.value);
    localStorage.setItem('filter_fuel', fuelOffers.value);--}}
    localStorage.setItem('filter_town', towns.value);
    localStorage.setItem('filtter_option', 5);
    window.location.href = '/adsList';
    });



    $('#searchOffersForm').submit(function(e){
    var formData = new FormData($(this)[0]);
    e.preventDefault();
    e.stopPropagation();
    localStorage.setItem('filter_condition', conditionOffers.value);
    {{--
        localStorage.setItem('filter_make', );
    --}}
    localStorage.setItem('filter_modell', modellOffers.value);

    localStorage.setItem('filter_body', bodyOffers.value);
    localStorage.setItem('filter_colorr', colorrOffers.value);


    localStorage.setItem('filter_form_mileage', from_mileageOffers.value);
    {{--localStorage.setItem('filter_to_mileage', to_mileage.value);--}}
    localStorage.setItem('filter_from_year', from_yearOffers.value);
    {{-- localStorage.setItem('filter_to_year', to_year.value);--}}
    localStorage.setItem('filter_from_price', from_priceOffers.value);
    {{--localStorage.setItem('filter_to_price', to_price.value);--}}

    localStorage.setItem('filter_transmition', transmitionOffers.value);
    localStorage.setItem('filter_fuel', fuelOffers.value);

    localStorage.setItem('filter_town', towns.value);
    localStorage.setItem('filtter_option', 4);
    window.location.href = '/adsList';
    });



    $('#searchWantedAdForm').submit(function(e){
    var formData = new FormData($(this)[0]);
    e.preventDefault();
    e.stopPropagation();
    localStorage.setItem('filter_condition', conditionWanted.value);
    {{--
        localStorage.setItem('filter_make', );
    --}}
    localStorage.setItem('filter_modell', modellWanted.value);

    localStorage.setItem('filter_body', bodyWanted.value);
    localStorage.setItem('filter_colorr', colorrWanted.value);


    localStorage.setItem('filter_form_mileage', from_mileageWanted.value);
    {{--localStorage.setItem('filter_to_mileage', to_mileage.value);--}}
    localStorage.setItem('filter_from_year', from_yearWanted.value);
    {{-- localStorage.setItem('filter_to_year', to_year.value);--}}
    localStorage.setItem('filter_from_price', from_priceWanted.value);
    {{--localStorage.setItem('filter_to_price', to_price.value);--}}

    localStorage.setItem('filter_transmition', transmitionWanted.value);
    localStorage.setItem('filter_fuel', fuelWanted.value);
    localStorage.setItem('filter_town', towns.value);
    localStorage.setItem('filtter_option', 3);
    window.location.href = '/adsList';
    });



    $('#searchToRentAdForm').submit(function(e){
    var formData = new FormData($(this)[0]);
    e.preventDefault();
    e.stopPropagation();
    localStorage.setItem('filter_condition', conditionRent.value);
    {{--
        localStorage.setItem('filter_make', );
    --}}
    localStorage.setItem('filter_modell', modellRent.value);

    localStorage.setItem('filter_body', bodyRent.value);
    localStorage.setItem('filter_colorr', colorrRent.value);


    localStorage.setItem('filter_form_mileage', from_mileageRent.value);
    {{--localStorage.setItem('filter_to_mileage', to_mileage.value);--}}
    localStorage.setItem('filter_from_year', from_yearRent.value);
    {{-- localStorage.setItem('filter_to_year', to_year.value);--}}
    localStorage.setItem('filter_from_price', from_priceRent.value);
    {{--localStorage.setItem('filter_to_price', to_price.value);--}}

    localStorage.setItem('filter_transmition', transmitionRent.value);
    localStorage.setItem('filter_fuel', fuelRent.value);
    localStorage.setItem('filter_town', towns.value);
    localStorage.setItem('filtter_option', 2);
    window.location.href = '/adsList';
    });





    $('#searchForSaleAdForm').submit(function(e){
    var formData = new FormData($(this)[0]);
    e.preventDefault();
    e.stopPropagation();
    localStorage.setItem('filter_condition', condition.value);
{{--
    localStorage.setItem('filter_make', );
--}}
    localStorage.setItem('filter_modell', modell.value);

    localStorage.setItem('filter_body', body.value);
    localStorage.setItem('filter_colorr', colorr.value);


    localStorage.setItem('filter_form_mileage', from_mileage.value);
    {{--localStorage.setItem('filter_to_mileage', to_mileage.value);--}}
    localStorage.setItem('filter_from_year', from_year.value);
   {{-- localStorage.setItem('filter_to_year', to_year.value);--}}
    localStorage.setItem('filter_from_price', from_price.value);
    {{--localStorage.setItem('filter_to_price', to_price.value);--}}

    localStorage.setItem('filter_transmition', transmition.value);
    localStorage.setItem('filter_fuel', fuel.value);

    localStorage.setItem('filter_town', towns.value);
    localStorage.setItem('filtter_option', 1);
    window.location.href = '/adsList';
    });











    //==================================================================================================================
    //==================================================================================================================
    //==================================================================================================================














    var lenght = 1;
    var pageSize = 3;
    var selectedPage = 1;
    var localstorageVar = 'Product_page';

    function getData(pageIndex, filter) {

    var userToken =   sessionStorage.getItem("api_token")

    if(userToken) {
    filter.append('town_id','');
    filter.append('app_id','3');
    filter.append('owner_id','');
    filter.append('status','');
    filter.append('validty_date','');
    filter.append('option','');
    filter.append('category_id','');
    filter.append('size_unit_id','');
    filter.append('price_option_id','');
    filter.append('furniture_id','');
    filter.append('rooms_id','');
    filter.append('balcony_id','');
    filter.append('bath_id','');
    filter.append('terrace','');
    filter.append('from_price','');
    filter.append('to_price','');
    filter.append('garage_id','');
    filter.append('from_size','');
    filter.append('to_size','');
    filter.append('mediaType','');
    filter.append('offset','0');
    filter.append('limit','10');

    $.ajax({

    type: "POST",
    url: Global_Url + 'carsAdsList',
    data: filter,
    // mimeType: 'application/json',
    processData: false,
    contentType: false,
    headers: {"Authorization": 'Bearer ' + userToken},

    success: function (res) {



console.log('dddd',res);


    $('#adsArchive').empty();


    var selectHtml = '';
    res.data.adsList.forEach(function (item, index, arr){
if(index<=5){
    selectHtml+= ' <div class="col-md-4 col-xs-12 col-sm-6">'+
        '<div class="category-grid-box-1">'+
          {{-- ' <div class="featured-ribbon">'+
               ' <span>AD</span>'+
           ' </div>'+--}}



            '<div class="image">'+


             '<img style="height: 250px;" alt="Carspot" class="img-responsive" src="http://4000.global/public/img/'+item.thumbnail+ '">'+


               ' <div class="ribbon popular"></div>'+

                '<div style="position: static" class="price-tag">'+
                   ' <div class="price"><span>'+item.cars_attributes.price+'</span></div>'+



                ' <div style="float: right;margin-top: -7%;"><a data-id="'+item.id +'"  data-saved="'+item.saved_ads_count +'"   class="save_car_item_id_FromHome">';

                        if(item.saved_ads_count==0){
                        selectHtml+=  '<i id="changeStarSave" style="margin-left: -50%;" class="fa fa-star-o"></i>';
                           }

                        if(item.saved_ads_count==1){
                        selectHtml+= '<i id="changeStarSave"  style="margin-left: -50%;" class="fa fa-star"></i>';
                        }

                        selectHtml+=    '</a> </div>'+
                '</div>'+


           ' </div>'+
            '<div class="short-description-1 clearfix">'+
                {{--'<div class="category-title"> <span><a>Car</a></span> </div>'+--}}
               ' <h3><a  data-id="'+item.id +'" class="detailFromIndex" title="">'+item.title+' </a></h3>'+

                '<ul class="list-unstyled">'+
                    '<li><a ><i class="flaticon-car-2"></i>'+item.cars_attributes.model.title+'</a></li>'+
                    '<li><a ><i class="flaticon-dashboard"></i>'+ item.cars_attributes.milage+ 'km</a></li>'+
                    '<li><a ><i class="flaticon-calendar"></i>'+item.cars_attributes.year+'</a></li>'+

                '</ul>'+
                '<p class="location"><i class="fa fa-map-marker"></i>'+ item.town.name+ '</p>'+

                '<p class="location"><i class="fa fa-eye"></i>'+ item.total_views+ '</p>'+

                ' <p><i class="flaticon-calendar"></i> &nbsp;<span>'+item.created_at.substr(0,10) +'</span> </p>'+

                '</div>'+
           ' <div class="ad-info-1">';

                if(item.owner.profile_pic!=null){
                selectHtml+= '<img style="max-width: 106px;display: inline" alt="profile_Picture" class="img-responsive" src="http://4000.global/public/img/'+item.owner.profile_pic+ '">';
                }

                {{--

               ' <p><i class="flaticon-calendar"></i> &nbsp;<span>'+item.created_at.substr(0,10) +'</span> </p>'+
--}}


                selectHtml+=


                '<ul class="pull-right">';

                    if(item.owner.name!=null){

                    selectHtml+= '<li style="width: 110px;margin-top: 13px;"><i class="fa fa-user"></i>'+item.owner.name+'</li>';

                    }
                    if(item.owner.town!=null){
                    selectHtml+= '<li style="width: 129px;margin-top: 13px;"><i class="fa fa-map-marker"></i>'+item.owner.town+'</li>';

                    }


                    selectHtml+= '</ul>'+
           ' </div>'+
        '</div>'+
    '</div>';

}
    });



    $('#adsArchive').append(selectHtml);




    console.log('endForStartTest',res.data);

    {{--$.ajaxSetup({
    headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    });
--}}
var myData=  res.data ;
  {{--  $.ajax({
    url: 'testUrl',
    type: 'POST',
    //contentType: 'application/json',
    data:{
    myData
    } ,

    success: function(response)
    {

    }
    });--}}




    console.log('endTeeest');




    },


    error: function (res, response) {
    },


    });
    }


    }



    var formDataInit = new FormData();
    getData(0, formDataInit);


    $('#filtersAdvanceSale').hide(0);

    $('#filtersAdvanceRent').hide(0);

    $('#filtersAdvanceWanted').hide(0);

    $('#filtersAdvanceOffers').hide(0);

{{--    $('.toggle').click(function() {--}}
{{--    $('#ff').show('slow');--}}
{{--    });--}}


    });

    $(document).ready(function(){
    $("#AdvanceSale").click(function(){
    console.log('hereee');
    $("#filtersAdvanceSale").toggle();
    });


    $("#AdvanceRent").click(function(){
    console.log('hereee');
    $("#filtersAdvanceRent").toggle();

    });

        $("#AdvanceWanted").click(function(){
        console.log('hereee');
        $("#filtersAdvanceWanted").toggle();
        });


    $("#AdvanceOffers").click(function(){
    console.log('hereee');
    $("#filtersAdvanceOffers").toggle();
    });


    data4 = new FormData();
    data4.append('parent_id',1); //
    var countries;

    $.ajax({

    type: "POST",
    url: 'api/carModelsList',
    data: data4,
    // mimeType: 'application/json',
    processData: false,
    contentType: false,
    success: function (res) {
    console.log(res);
    if(res.code == 200){

    countries = res;
    $('.modell').empty();
    $('.modell').append(
    '<option value="" label="Model">Nothing Selected</option>'
    );

    res.data.forEach(function (item, index, arr){

    $('.modell').append(
    '<option   value="'+item.id+'" data-id="' + item.id + '">'+item.title+'</option>'
    );





    });

    }else if ( res.code < 0){

    }

    },
    error: function (res, response) {
    },
    });






    dataMake = new FormData();
    dataMake.append('parent_id',''); //
    var countries;

    $.ajax({

    type: "POST",
    url: 'api/carModelsList',
    data: dataMake,
    // mimeType: 'application/json',
    processData: false,
    contentType: false,
    success: function (res) {
    console.log(res);
    if(res.code == 200){

    countries = res;
    $('.make').empty();
    $('.make').append(
    '<option label="Model">Nothing Selected</option>'
    );

    res.data.forEach(function (item, index, arr){

    $('.make').append(
    '<option   value="'+item.id+'" data-id="' + item.id + '">'+item.title+'</option>'
    );





    });

    }else if ( res.code < 0){

    }

    },
    error: function (res, response) {
    },
    });









@endsection
