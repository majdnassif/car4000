@extends('welcome')

@section('content')


    <section class="buysell-section">
        <div class="background-3"></div>
        <div class="background-4"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12 no-padding">
                    <div class="section-container-left">
                        <h1>Find Your Dream Car?</h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.At vero eos et accusamus et iusto.</p>
                        <a href="post-ad-1.html" class="btn btn-lg btn-theme">Search </a>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 no-padding">
                    <div class="section-container-right">
                        <h1>Want To Sale Your Car ?</h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.At vero eos et accusamus et iusto.</p>
                        <a href="services-1.html" class="btn btn-lg btn-primary">Hire Services</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- =-=-=-=-=-=-= Buy & Sell End =-=-=-=-=-=-= -->

    <!-- =-=-=-=-=-=-= Main Content Area =-=-=-=-=-=-= -->
    <div class="main-content-area clearfix">
        <!-- =-=-=-=-=-=-= Search Bar =-=-=-=-=-=-= -->
        <div class="search-bar">
            <div class="section-search search-style-2">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                            <div class="clearfix">
                                <form>
                                    <div class="search-form pull-left">
                                        <div class="search-form-inner pull-left">
                                            <div class="col-md-3 col-sm-6 col-xs-12 no-padding">
                                                <div class="form-group">
                                                    <label>Keyword</label>
                                                    <input type="text" class="form-control" placeholder="Eg Honda Civic , Audi , Ford." />
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-12 no-padding">
                                                <div class="form-group">
                                                    <label>Select Make</label>
                                                    <select class=" form-control make">
                                                        <option label="Any Make"></option>
                                                        <option>BMW</option>
                                                        <option>Honda </option>
                                                        <option>Hyundai </option>
                                                        <option>Nissan </option>
                                                        <option>Mercedes Benz </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-12 no-padding">
                                                <div class="form-group">
                                                    <label>Select Year</label>
                                                    <select class=" form-control search-year">
                                                        <option label="Any Year"></option>
                                                        <option>Year</option>
                                                        <option>2010</option>
                                                        <option>2011</option>
                                                        <option>2012</option>
                                                        <option>2013</option>
                                                        <option>2014</option>
                                                        <option>2015</option>
                                                        <option>2016</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6 col-xs-12 no-padding">
                                                <div class="form-group">
                                                    <label>Select Location</label>
                                                    <select class="search-loaction form-control">
                                                        <option label="location"></option>
                                                        <option value="0">America</option>
                                                        <option value="1">Australia</option>
                                                        <option value="2">Africa</option>
                                                        <option value="3">Pakistan</option>
                                                        <option value="4">Japan</option>
                                                        <option value="5">Srilanka</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group pull-right">
                                            <button type="submit" value="submit" class="btn btn-lg btn-theme" >Search Now</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--================================../SEARCH STYLE 2================================-->
        </div>
        <!-- =-=-=-=-=-=-= Search Bar End =-=-=-=-=-=-= -->

        <!-- =-=-=-=-=-=-= Featured Ads =-=-=-=-=-=-= -->
        <section class="custom-padding gray">
            <div class="container">
                <div class="row">
                    <!-- Heading Area -->
                    <div class="heading-panel">
                        <div class="col-xs-12 col-md-7 col-sm-6 left-side">
                            <!-- Main Title -->
                            <h1>Lateat  <span class="heading-color"> Featured</span> Ads</h1>
                        </div>
                        <div class="col-sm-6 col-xs-12 col-md-5">
                            <ul class="list-unstyled list-inline pull-right">
                                <li><a href="#" class="btn btn-default">Honda</a>
                                </li>
                                <li><a href="#" class="btn btn-default">Bmw</a>
                                </li>
                                <li><a href="#" class="btn btn-default">Audi</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- Heading Area End -->
                    <div class="col-sm-12 col-xs-12 col-md-12">

                        <!-- Latest Featured Ads  -->
                        <div class="row ">
                            <div class="grid-style-2 ">
                                <div class="col-md-4 col-xs-12 col-sm-6">
                                    <div class="category-grid-box-1">
                                        <div class="featured-ribbon">
                                            <span>Featured</span>
                                        </div>
                                        <div class="image">
                                            <img alt="Carspot" src="{{asset('theme/images/posting/10.jpg')}}" class="img-responsive">
                                            <div class="ribbon popular"></div>
                                            <div class="price-tag">
                                                <div class="price"><span>$205,000</span></div>
                                            </div>
                                        </div>
                                        <div class="short-description-1 clearfix">
                                            <div class="category-title"> <span><a href="#">Sports & Equipment</a></span> </div>
                                            <h3><a title="" href="single-page-listing.html">Honda Civic 2017 Sports Edition</a></h3>
                                            <p class="location"><i class="fa fa-map-marker"></i> Model Town Link Road London</p>
                                            <ul class="list-unstyled">
                                                <li><a href="javascript:void(0)"><i class="flaticon-gas-station-1"></i>Diesel</a></li>
                                                <li><a href="javascript:void(0)"><i class="flaticon-dashboard"></i>35,000 km</a></li>
                                                <li><a href="javascript:void(0)"><i class="flaticon-engine-2"></i>1800 cc</a></li>
                                                <li><a href="javascript:void(0)"><i class="flaticon-car-2"></i>SUV</a></li>
                                                <li><a href="javascript:void(0)"><i class="flaticon-cogwheel-outline"></i>White</a></li>
                                            </ul>
                                        </div>
                                        <div class="ad-info-1">
                                            <p><i class="flaticon-calendar"></i> &nbsp;<span>5 Days ago</span> </p>
                                            <ul class="pull-right">
                                                <li> <a href="#"><i class="flaticon-like-1"></i></a> </li>
                                                <li> <a href="#"><i class="flaticon-message"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- Listing Ad Grid -->
                                </div>
                                <!-- /col -->
                                <div class="col-md-4 col-xs-12 col-sm-6">
                                    <div class="category-grid-box-1">
                                        <div class="featured-ribbon">
                                            <span>Featured</span>
                                        </div>
                                        <div class="image">
                                            <img alt="Carspot" src="{{asset('theme/images/posting/11.jpg')}}" class="img-responsive">
                                            <div class="ribbon popular"></div>
                                            <div class="price-tag">
                                                <div class="price"><span>$190,000</span></div>
                                            </div>
                                        </div>
                                        <div class="short-description-1 clearfix">
                                            <div class="category-title"> <span><a href="#">Sports & Equipment</a></span> </div>
                                            <h3><a title="" href="single-page-listing.html">Mazda RX8 For Sale</a></h3>
                                            <p class="location"><i class="fa fa-map-marker"></i> Model Town Link Road London</p>
                                            <ul class="list-unstyled">
                                                <li><a href="javascript:void(0)"><i class="flaticon-gas-station-1"></i>Diesel</a></li>
                                                <li><a href="javascript:void(0)"><i class="flaticon-dashboard"></i>35,000 km</a></li>
                                                <li><a href="javascript:void(0)"><i class="flaticon-engine-2"></i>1800 cc</a></li>
                                                <li><a href="javascript:void(0)"><i class="flaticon-car-2"></i>SUV</a></li>
                                                <li><a href="javascript:void(0)"><i class="flaticon-cogwheel-outline"></i>White</a></li>
                                            </ul>
                                        </div>
                                        <div class="ad-info-1">
                                            <p><i class="flaticon-calendar"></i> &nbsp;<span>5 Days ago</span> </p>
                                            <ul class="pull-right">
                                                <li> <a href="#"><i class="flaticon-like-1"></i></a> </li>
                                                <li> <a href="#"><i class="flaticon-message"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- Listing Ad Grid -->
                                </div>
                                <!-- /col -->
                                <div class="col-md-4 col-xs-12 col-sm-6">
                                    <div class="category-grid-box-1">
                                        <div class="featured-ribbon">
                                            <span>Featured</span>
                                        </div>
                                        <div class="image">
                                            <img alt="Carspot" src="{{asset('theme/images/posting/12.jpg')}}" class="img-responsive">
                                            <div class="ribbon popular"></div>
                                            <div class="price-tag">
                                                <div class="price"><span>$110,000</span></div>
                                            </div>
                                        </div>
                                        <div class="short-description-1 clearfix">
                                            <div class="category-title"> <span><a href="#">Sports & Equipment</a></span> </div>
                                            <h3><a title="" href="single-page-listing.html">BMW I8 1.5 Auto 4X4 2dr </a></h3>
                                            <p class="location"><i class="fa fa-map-marker"></i> Model Town Link Road London</p>
                                            <ul class="list-unstyled">
                                                <li><a href="javascript:void(0)"><i class="flaticon-gas-station-1"></i>Diesel</a></li>
                                                <li><a href="javascript:void(0)"><i class="flaticon-dashboard"></i>35,000 km</a></li>
                                                <li><a href="javascript:void(0)"><i class="flaticon-engine-2"></i>1800 cc</a></li>
                                                <li><a href="javascript:void(0)"><i class="flaticon-car-2"></i>SUV</a></li>
                                                <li><a href="javascript:void(0)"><i class="flaticon-cogwheel-outline"></i>White</a></li>
                                            </ul>
                                        </div>
                                        <div class="ad-info-1">
                                            <p><i class="flaticon-calendar"></i> &nbsp;<span>5 Days ago</span> </p>
                                            <ul class="pull-right">
                                                <li> <a href="#"><i class="flaticon-like-1"></i></a> </li>
                                                <li> <a href="#"><i class="flaticon-message"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- Listing Ad Grid -->
                                </div>
                                <!-- /col -->
                                <div class="col-md-4 col-xs-12 col-sm-6">
                                    <div class="category-grid-box-1">
                                        <div class="featured-ribbon">
                                            <span>Featured</span>
                                        </div>
                                        <div class="image">
                                            <img alt="Carspot" src="{{asset('theme/images/posting/13.jpg')}}" class="img-responsive">
                                            <div class="ribbon popular"></div>
                                            <div class="price-tag">
                                                <div class="price"><span>$75,000</span></div>
                                            </div>
                                        </div>
                                        <div class="short-description-1 clearfix">
                                            <div class="category-title"> <span><a href="#">Sports & Equipment</a></span> </div>
                                            <h3><a title="" href="single-page-listing.html">2017 Audi A4 quattro Premium</a></h3>
                                            <p class="location"><i class="fa fa-map-marker"></i> Model Town Link Road London</p>
                                            <ul class="list-unstyled">
                                                <li><a href="javascript:void(0)"><i class="flaticon-gas-station-1"></i>Diesel</a></li>
                                                <li><a href="javascript:void(0)"><i class="flaticon-dashboard"></i>35,000 km</a></li>
                                                <li><a href="javascript:void(0)"><i class="flaticon-engine-2"></i>1800 cc</a></li>
                                                <li><a href="javascript:void(0)"><i class="flaticon-car-2"></i>SUV</a></li>
                                                <li><a href="javascript:void(0)"><i class="flaticon-cogwheel-outline"></i>White</a></li>
                                            </ul>
                                        </div>
                                        <div class="ad-info-1">
                                            <p><i class="flaticon-calendar"></i> &nbsp;<span>5 Days ago</span> </p>
                                            <ul class="pull-right">
                                                <li> <a href="#"><i class="flaticon-like-1"></i></a> </li>
                                                <li> <a href="#"><i class="flaticon-message"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- Listing Ad Grid -->
                                </div>
                                <!-- /col -->
                                <div class="col-md-4 col-xs-12 col-sm-6">
                                    <div class="category-grid-box-1">
                                        <div class="featured-ribbon">
                                            <span>Featured</span>
                                        </div>
                                        <div class="image">
                                            <img alt="Carspot" src="{{asset('theme/images/posting/14.jpg')}}" class="img-responsive">
                                            <div class="ribbon popular"></div>
                                            <div class="price-tag">
                                                <div class="price"><span>$77,000</span></div>
                                            </div>
                                        </div>
                                        <div class="short-description-1 clearfix">
                                            <div class="category-title"> <span><a href="#">Sports & Equipment</a></span> </div>
                                            <h3><a title="" href="single-page-listing.html">Bugatti Veyron Super Sport </a></h3>
                                            <p class="location"><i class="fa fa-map-marker"></i> Model Town Link Road London</p>
                                            <ul class="list-unstyled">
                                                <li><a href="javascript:void(0)"><i class="flaticon-gas-station-1"></i>Diesel</a></li>
                                                <li><a href="javascript:void(0)"><i class="flaticon-dashboard"></i>35,000 km</a></li>
                                                <li><a href="javascript:void(0)"><i class="flaticon-engine-2"></i>1800 cc</a></li>
                                                <li><a href="javascript:void(0)"><i class="flaticon-car-2"></i>SUV</a></li>
                                                <li><a href="javascript:void(0)"><i class="flaticon-cogwheel-outline"></i>White</a></li>
                                            </ul>
                                        </div>
                                        <div class="ad-info-1">
                                            <p><i class="flaticon-calendar"></i> &nbsp;<span>5 Days ago</span> </p>
                                            <ul class="pull-right">
                                                <li> <a href="#"><i class="flaticon-like-1"></i></a> </li>
                                                <li> <a href="#"><i class="flaticon-message"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- Listing Ad Grid -->
                                </div>
                                <!-- /col -->
                                <div class="col-md-4 col-xs-12 col-sm-6">
                                    <div class="category-grid-box-1">
                                        <div class="featured-ribbon">
                                            <span>Featured</span>
                                        </div>
                                        <div class="image">
                                            <img alt="Carspot" src="{{asset('theme/images/posting/15.jpg')}}" class="img-responsive">
                                            <div class="ribbon popular"></div>
                                            <div class="price-tag">
                                                <div class="price"><span>$70,000</span></div>
                                            </div>
                                        </div>
                                        <div class="short-description-1 clearfix">
                                            <div class="category-title"> <span><a href="#">Sports & Equipment</a></span> </div>
                                            <h3><a title="" href="single-page-listing.html">Bugatti Veyron Super Sport </a></h3>
                                            <p class="location"><i class="fa fa-map-marker"></i> Model Town Link Road London</p>
                                            <ul class="list-unstyled">
                                                <li><a href="javascript:void(0)"><i class="flaticon-gas-station-1"></i>Diesel</a></li>
                                                <li><a href="javascript:void(0)"><i class="flaticon-dashboard"></i>35,000 km</a></li>
                                                <li><a href="javascript:void(0)"><i class="flaticon-engine-2"></i>1800 cc</a></li>
                                                <li><a href="javascript:void(0)"><i class="flaticon-car-2"></i>SUV</a></li>
                                                <li><a href="javascript:void(0)"><i class="flaticon-cogwheel-outline"></i>White</a></li>
                                            </ul>
                                        </div>
                                        <div class="ad-info-1">
                                            <p><i class="flaticon-calendar"></i> &nbsp;<span>5 Days ago</span> </p>
                                            <ul class="pull-right">
                                                <li> <a href="#"><i class="flaticon-like-1"></i></a> </li>
                                                <li> <a href="#"><i class="flaticon-message"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- Listing Ad Grid -->
                                </div>
                                <!-- /col -->
                            </div>
                        </div>
                        <!-- Top Dealer Ads  -->

                        <div class="row margin-top-30">
                            <div class="heading-panel">
                                <div class="col-xs-12 col-md-7 col-sm-6 left-side">
                                    <!-- Main Title -->
                                    <h1>Top  <span class="heading-color"> Dealer</span> Ads</h1>
                                </div>
                                <div class="col-sm-6 col-xs-12 col-md-5">
                                    <ul class="list-unstyled list-inline pull-right">
                                        <li><a href="#" class="btn btn-default">Most Polular</a>
                                        </li>
                                        <li><a href="#" class="btn btn-default">Trending</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="grid-style-1">
                                <!-- Listing Ad Grid -->
                                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12  ">
                                    <div class="white category-grid-box-1 ">
                                        <!-- Image Box -->
                                        <div class="image"> <img alt="Carspot" src="{{asset('theme/images/posting/6.jpg')}}" class="img-responsive"></div>
                                        <div class="ad-info-1">
                                            <ul>
                                                <li><i class="flaticon-fuel-1"></i>Diesel</li>
                                                <li><i class="flaticon-dashboard"></i>35,000 km</li>
                                                <li><i class="flaticon-engine-2"></i> 1800 cc</li>
                                            </ul>
                                        </div>
                                        <!-- Short Description -->
                                        <div class="short-description-1 ">
                                            <h3>
                                                <a title="" href="single-page-listing.html">2016 McLaren 570S Coupe </a>
                                            </h3>
                                            <!-- Location -->
                                            <p class="location"><i class="fa fa-map-marker"></i> Model Town Link Road London</p>
                                            <p>Till the one day when the lady met this fellow and they knew it was much more than </p>
                                            <hr>
                                            <!-- Ad Meta Stats -->
                                            <span class="ad-price">$370</span>
                                            <a class="btn btn-sm btn-theme pull-right"><i class="fa fa-phone"></i> View Details.</a>
                                        </div>
                                    </div>
                                </div>
                                <!-- Listing Ad Grid -->
                                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12  ">
                                    <div class="white category-grid-box-1 ">
                                        <!-- Image Box -->
                                        <div class="image"> <img alt="Carspot" src="{{asset('theme/images/posting/2.jpg')}}" class="img-responsive"></div>
                                        <div class="ad-info-1">
                                            <ul>
                                                <li><i class="flaticon-fuel-1"></i>Diesel</li>
                                                <li><i class="flaticon-dashboard"></i>35,000 km</li>
                                                <li><i class="flaticon-engine-2"></i> 1800 cc</li>
                                            </ul>
                                        </div>
                                        <!-- Short Description -->
                                        <div class="short-description-1 ">
                                            <h3>
                                                <a title="" href="single-page-listing.html">Porsche 911 Carrera 2017 </a>
                                            </h3>
                                            <!-- Location -->
                                            <p class="location"><i class="fa fa-map-marker"></i> Model Town Link Road London</p>
                                            <p>Till the one day when the lady met this fellow and they knew it was much more than </p>
                                            <hr>
                                            <!-- Ad Meta Stats -->
                                            <span class="ad-price">$210</span>
                                            <a class="btn btn-sm btn-theme pull-right"><i class="fa fa-phone"></i> View Details.</a>
                                        </div>
                                    </div>
                                </div>
                                <!-- Listing Ad Grid -->
                                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12  ">
                                    <div class="white category-grid-box-1 ">
                                        <!-- Image Box -->
                                        <div class="image"> <img alt="Carspot" src="{{asset('theme/images/posting/28.jpg')}}" class="img-responsive"></div>
                                        <div class="ad-info-1">
                                            <ul>
                                                <li><i class="flaticon-fuel-1"></i>Diesel</li>
                                                <li><i class="flaticon-dashboard"></i>35,000 km</li>
                                                <li><i class="flaticon-engine-2"></i> 1800 cc</li>
                                            </ul>
                                        </div>
                                        <!-- Short Description -->
                                        <div class="short-description-1 ">
                                            <h3>
                                                <a title="" href="single-page-listing.html">Audi A4 2.0T Quattro Premium</a>
                                            </h3>
                                            <!-- Location -->
                                            <p class="location"><i class="fa fa-map-marker"></i> Model Town Link Road London</p>
                                            <p>Till the one day when the lady met this fellow and they knew it was much more than </p>
                                            <hr>
                                            <!-- Ad Meta Stats -->
                                            <span class="ad-price">$370</span>
                                            <a class="btn btn-sm btn-theme pull-right"><i class="fa fa-phone"></i> View Details.</a>
                                        </div>
                                    </div>
                                </div>
                                <!-- Listing Ad Grid -->
                            </div>
                        </div>
                        <!-- /row -->
                    </div>
                </div>
            </div>
        </section>
        <!-- =-=-=-=-=-=-= Testimonials =-=-=-=-=-=-= -->
        <section class="section-padding parallex bg-img-3">
            <div class="container">
                <div class="row">
                    <div class="owl-testimonial-2">
                        <div class="single_testimonial">
                            <div class="textimonial-content">
                                <h4>Just fabulous</h4>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elitsed eiusmod tempor enim minim veniam quis notru.</p>
                            </div>
                            <div class="testimonial-meta-box">
                                <img src="{{asset('theme/images/users/1.jpg')}}" alt="">
                                <div class="testimonial-meta">
                                    <h3 class="">Jhon Emily Copper </h3>
                                    <p> Developer</p>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                            </div>
                        </div>
                        <div class="single_testimonial">
                            <div class="textimonial-content">
                                <h4>Awesome ! Loving It</h4>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elitsed eiusmod tempor enim minim veniam quis notru.</p>
                            </div>
                            <div class="testimonial-meta-box">
                                <img src="{{asset('theme/images/users/2.jpg')}}" alt="">
                                <div class="testimonial-meta">
                                    <h3 class="">Hania Sheikh </h3>
                                    <p> CEO Pvt. Inc.</p>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                            </div>
                        </div>
                        <div class="single_testimonial">
                            <div class="textimonial-content">
                                <h4>Very quick and Fast</h4>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elitsed eiusmod tempor enim minim veniam quis notru.</p>
                            </div>
                            <div class="testimonial-meta-box">
                                <img src="{{asset('theme/images/users/3.jpg')}}" alt="">
                                <div class="testimonial-meta">
                                    <h3 class="">Jaccica Albana </h3>
                                    <p>  CTO Albana Inc.</p>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                            </div>
                        </div>
                        <div class="single_testimonial">
                            <div class="textimonial-content">
                                <h4>Done in 3 Months! Awesome</h4>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elitsed eiusmod tempor enim minim veniam quis notru.</p>
                            </div>
                            <div class="testimonial-meta-box">
                                <img src="{{asset('theme/images/users/4.jpg')}}" alt="">
                                <div class="testimonial-meta">
                                    <h3 class="">Humayun Sarfraz </h3>
                                    <p>  CTO Glixen Technologies.</p>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                            </div>
                        </div>
                        <div class="single_testimonial">
                            <div class="textimonial-content">
                                <h4>Find It Quit Professional</h4>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elitsed eiusmod tempor enim minim veniam quis notru.</p>
                            </div>
                            <div class="testimonial-meta-box">
                                <img src="{{asset('theme/images/users/4.jpg')}}" alt="">
                                <div class="testimonial-meta">
                                    <h3 class="">Massica O'Brain </h3>
                                    <p> Audit Officer </p>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                            </div>
                        </div>
                        <div class="single_testimonial">
                            <div class="textimonial-content">
                                <h4>Just fabulous</h4>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elitsed eiusmod tempor enim minim veniam quis notru.</p>
                            </div>
                            <div class="testimonial-meta-box">
                                <img src="{{asset('theme/images/users/1.jpg')}}" alt="">
                                <div class="testimonial-meta">
                                    <h3 class="">Jhon Emily Copper </h3>
                                    <p> Developer</p>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                            </div>
                        </div>
                        <div class="single_testimonial">
                            <div class="textimonial-content">
                                <h4>Awesome ! Loving It</h4>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elitsed eiusmod tempor enim minim veniam quis notru.</p>
                            </div>
                            <div class="testimonial-meta-box">
                                <img src="{{asset('theme/images/users/2.jpg')}}" alt="">
                                <div class="testimonial-meta">
                                    <h3 class="">Hania Sheikh </h3>
                                    <p> CEO Pvt. Inc.</p>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                            </div>
                        </div>
                        <div class="single_testimonial">
                            <div class="textimonial-content">
                                <h4>Very quick and Fast</h4>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elitsed eiusmod tempor enim minim veniam quis notru.</p>
                            </div>
                            <div class="testimonial-meta-box">
                                <img src="{{asset('theme/images/users/3.jpg')}}" alt="">
                                <div class="testimonial-meta">
                                    <h3 class="">Jaccica Albana </h3>
                                    <p>  CTO Albana Inc.</p>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                            </div>
                        </div>
                        <div class="single_testimonial">
                            <div class="textimonial-content">
                                <h4>Done in 3 Months! Awesome</h4>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elitsed eiusmod tempor enim minim veniam quis notru.</p>
                            </div>
                            <div class="testimonial-meta-box">
                                <img src="{{asset('theme/images/users/4.jpg')}}" alt="">
                                <div class="testimonial-meta">
                                    <h3 class="">Humayun Sarfraz </h3>
                                    <p>  CTO Glixen Tech.</p>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                            </div>
                        </div>
                        <div class="single_testimonial">
                            <div class="textimonial-content">
                                <h4>Find It Quit Professional</h4>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elitsed eiusmod tempor enim minim veniam quis notru.</p>
                            </div>
                            <div class="testimonial-meta-box">
                                <img src="{{asset('theme/images/users/4.jpg')}}" alt="">
                                <div class="testimonial-meta">
                                    <h3 class="">Massica O'Brain </h3>
                                    <p> Audit Officer </p>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- =-=-=-=-=-=-= Testimonials Section End =-=-=-=-=-=-= -->

        <!-- =-=-=-=-=-=-= Car Comparison =-=-=-=-=-=-= -->
        <section class="section-padding">
            <!-- Main Container -->
            <div class="container">
                <!-- Row -->
                <div class="row">
                    <div class="clearfix"></div>
                    <!-- Heading Area -->
                    <div class="heading-panel">
                        <div class="col-xs-12 col-md-12 col-sm-12 text-center">
                            <!-- Main Title -->
                            <h1>Popular  <span class="heading-color"> Car</span> Comparison</h1>
                            <!-- Short Description -->
                            <p class="heading-text">Eu delicata rationibus usu. Vix te putant utroque, ludus fabellas duo eu, his dico ut debet consectetuer.</p>
                        </div>
                    </div>
                    <!-- Middle Content Box -->
                    <div class="row">
                        <div class="col-md-12 col-xs-12 col-sm-12 text-center ">
                            <ul class="compare-masonry">
                                <li class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="comparison-box">
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="compare-grid">
                                                <a href="#" class="">
                                                    <img src="{{asset('theme/images/compare/2.png')}}" alt="" class="img-responsive" /></a>
                                                <h2><a href="#">2016 Ford Escape cape  </a></h2>
                                                <div class="rating">
                                                    <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vsbox">vs</div>
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="compare-grid">
                                                <a href="" class=""><img src="{{asset('theme/images/compare/1.png')}}" alt="" class="img-responsive" /></a>
                                                <h2><a href="#">2017 Chevrolet Camaro </a></h2>
                                                <div class="rating">
                                                    <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </li>
                                <li class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="comparison-box">
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="compare-grid">
                                                <a href="#" class="">
                                                    <img src="{{asset('theme/images/compare/3.png')}}" alt="" class="img-responsive" /></a>
                                                <h2><a href="#">2017 Chevrolet Corvette </a></h2>
                                                <div class="rating">
                                                    <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vsbox">vs</div>
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="compare-grid">
                                                <a href="" class=""><img src="{{asset('theme/images/compare/4.png')}}" alt="" class="img-responsive" /></a>
                                                <h2><a href="#">2017 Honda Accord Sedan </a></h2>
                                                <div class="rating">
                                                    <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </li>
                                <li class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="comparison-box">
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="compare-grid">
                                                <a href="#" class="">
                                                    <img src="{{asset('theme/images/compare/5.png')}}" alt="" class="img-responsive" /></a>
                                                <h2><a href="#">Mercedes-Benz C-Class </a></h2>
                                                <div class="rating">
                                                    <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vsbox">vs</div>
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="compare-grid">
                                                <a href="" class=""><img src="{{asset('theme/images/compare/6.png')}}" alt="" class="img-responsive" /></a>
                                                <h2><a href="#">2017 Honda CR-V </a></h2>
                                                <div class="rating">
                                                    <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </li>
                                <li class="col-md-6 col-sm-6 col-xs-12">
                                    <div class="comparison-box">
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="compare-grid">
                                                <a href="#" class="">
                                                    <img src="{{asset('theme/images/compare/7.png')}}" alt="" class="img-responsive" /></a>
                                                <h2><a href="#">2016 Ford Mustang</a></h2>
                                                <div class="rating">
                                                    <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <span class="rating-count">(3)</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="vsbox">vs</div>
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <div class="compare-grid">
                                                <a href="" class=""><img src="{{asset('theme/images/compare/8.png')}}" alt="" class="img-responsive" /></a>
                                                <h2><a href="#">2017 Toyota RAV4 </a></h2>
                                                <div class="rating">
                                                    <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <span class="rating-count">(3)</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="text-center">
                        <div class="load-more-btn">
                            <button class="btn btn-theme"> View All Comparisons <i class="fa fa-refresh"></i> </button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- =-=-=-=-=-=-= Car Comparison End  =-=-=-=-=-=-= -->

        <!-- =-=-=-=-=-=-= Expert Reviews =-=-=-=-=-=-= -->
        <section class="news section-padding gray">
            <div class="container">
                <div class="row">
                    <div class="heading-panel">
                        <div class="col-xs-12 col-md-12 col-sm-12 left-side">
                            <!-- Main Title -->
                            <h1>Expert  <span class="heading-color"> Reviews</span> Feedback</h1>
                            <!-- Short Description -->
                            <p class="heading-text">Eu delicata rationibus usu. Vix te putant utroque, ludus fabellas duo eu, his dico ut debet consectetuer.</p>
                        </div>
                    </div>
                    <div class="col-md-7 col-sm-12 col-xs-12">
                        <div class="mainimage">
                            <a>
                                <img alt="" class="img-responsive" src="{{asset('theme/images/blog/1.jpg')}}">
                                <div class="overlay">
                                    <h2>Eight Things You Should Know About The Mercedes-Benz E-Class LWB</h2>
                                </div>
                            </a>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="col-md-5 col-sm-12 col-xs-12">
                        <div class="newslist">
                            <ul>
                                <li>
                                    <div class="imghold"> <a><img src="{{asset('theme/images/blog/s1.jpg')}}" alt=""></a> </div>
                                    <div class="texthold">
                                        <h4><a>2017 Honda City: Which Variant Suits You? </a></h4>
                                        <p>With the 2017 facelifted avatar, the Honda City has significantly upped its game...&nbsp;</p>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                                <li>
                                    <div class="imghold"> <a><img src="{{asset('theme/images/blog/s2.jpg')}}" alt=""></a> </div>
                                    <div class="texthold">
                                        <h4><a>Honda City Facelift &ndash; Expected Prices </a></h4>
                                        <p>Honda will launch the City facelift in India on Feb 14, 2017 and it promises to...&nbsp;</p>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                                <li>
                                    <div class="imghold"> <a><img src="{{asset('theme/images/blog/s3.jpg')}}" alt=""></a> </div>
                                    <div class="texthold">
                                        <h4><a>Audi A4 Diesel Launched In India At Rs 40.20 Lakh </a></h4>
                                        <p>Audi India has launched a powerful diesel variant of its A4 sedan at Rs 40.20 la...&nbsp;</p>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                                <li>
                                    <div class="imghold"> <a><img src="{{asset('theme/images/blog/s4.jpg')}}" alt=""></a> </div>
                                    <div class="texthold">
                                        <h4><a>Audi A4 Diesel Launched In India At Rs 40.20 Lakh </a></h4>
                                        <p>Audi India has launched a powerful diesel variant of its A4 sedan at Rs 40.20 la...&nbsp;</p>
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- =-=-=-=-=-=-= Expert Reviews End =-=-=-=-=-=-= -->

        <!-- =-=-=-=-=-=-= Car Inspection =-=-=-=-=-=-= -->
        <section class="car-inspection section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12 nopadding hidden-sm">
                        <div class="call-to-action-img-section-right">
                            <img src="{{asset('theme/images/car-in-red.png')}}" class="wow slideInLeft img-responsive" data-wow-delay="0ms" data-wow-duration="3000ms" alt="">
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12 nopadding">
                        <div class="call-to-action-detail-section">
                            <div class="heading-2">
                                <h3> Want To Sale Your Car ?</h3>
                                <h2>Car Inspection</h2>
                            </div>
                            <p> Our CarSure experts inspect the car on over 200 checkpoints so you get complete satisfaction and peace of mind before buying. </p>
                            <div class="row">
                                <ul>
                                    <li class="col-sm-4"> <i class="fa fa-check"></i> Transmission</li>
                                    <li class="col-sm-4"> <i class="fa fa-check"></i> Steering</li>
                                    <li class="col-sm-4"> <i class="fa fa-check"></i> Engine</li>
                                    <li class="col-sm-4"> <i class="fa fa-check"></i> Tires</li>
                                    <li class="col-sm-4"> <i class="fa fa-check"></i> Lighting</li>
                                    <li class="col-sm-4"> <i class="fa fa-check"></i> Interior</li>
                                    <li class="col-sm-4">  <i class="fa fa-check"></i> Suspension</li>
                                    <li class="col-sm-4">  <i class="fa fa-check"></i> Exterior</li>
                                    <li class="col-sm-4">  <i class="fa fa-check"></i> Brakes</li>
                                    <li class="col-sm-4">  <i class="fa fa-check"></i> Air Conditioning</li>
                                    <li class="col-sm-4">  <i class="fa fa-check"></i> Engine Diagnostics</li>
                                    <li class="col-sm-4">  <i class="fa fa-check"></i> Wheel Alignment</li>
                                </ul>
                            </div>
                            <a href="" class="btn-theme btn-lg btn">Schedule Inspection <i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- =-=-=-=-=-=-= Car Inspection End =-=-=-=-=-=-= -->
        <!-- =-=-=-=-=-=-= Call to Action =-=-=-=-=-=-= -->
        <div class="parallex bg-img-3  section-padding">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-sm-12">
                        <div class="call-action">
                            <i class="flaticon-like-1"></i>
                            <h4>Post featured ad and get great exposure </h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.</p>
                        </div>
                        <!-- end subsection-text -->
                    </div>
                    <!-- end col-md-8 -->
                    <div class="col-md-4 col-sm-12">
                        <div class="parallex-button"> <a href="#" class="btn btn-theme">Post Free Ad <i class="fa fa-angle-double-right "></i></a> </div>
                        <!-- end parallex-button -->
                    </div>
                    <!-- end col-md-4 -->
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>
        <!-- =-=-=-=-=-=-= Call to Action =-=-=-=-=-=-= -->
    </div>

@endsection
